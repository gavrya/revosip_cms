<?php
// Максимальное количество отображаемых номеров страниц пагинатора
$pages_limit = 6;
// Последний номер страницы пагинатора
$to_page = max($current_page, min($total_pages, $pages_limit));
if ($to_page == $current_page && $total_pages > $to_page) {
    $to_page++;
}
// Первый номер страницы пагинатора
$from_page = max($to_page - $pages_limit + 1, 1);
// Предшествующий номер страницы  пагинатора
$before_page = $from_page > 1 ? $from_page - 1 : FALSE;
// Последующий номер страницы  пагинатора
$after_page = $total_pages > $to_page ? $to_page + 1 : FALSE;
?>

<!--START PAGINATION-->
<div class="pagination">
    <ul>
        <?php if ($first_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($first_page)) ?>" rel="first"><<</a></li>
        <?php else: ?>
            <li class="disabled"><span><<</span></li>
        <?php endif ?>

        <?php if ($previous_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($previous_page)) ?>" rel="prev"><</a></li>
        <?php else: ?>
            <li class="disabled"><span><</span></li>
        <?php endif ?>

        <?php if ($before_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($before_page)) ?>" rel="before">...</a></li>
        <?php endif ?>

        <?php foreach (range($from_page, $to_page) as $number): ?>

            <?php if ($number == $current_page): ?>
                <li class="active"><span><?php echo $number ?></span></li>
            <?php else: ?>
                <li><a href="<?php echo HTML::chars($page->url($number)) ?>"><?php echo $number ?></a></li>
            <?php endif ?>

        <?php endforeach ?>

        <?php if ($after_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($after_page)) ?>" rel="after">...</a></li>
        <?php endif ?>        

        <?php if ($next_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($next_page)) ?>" rel="next">></a></li>
        <?php else: ?>
            <li class="disabled"><span>></span></li>
        <?php endif ?>

        <?php if ($last_page !== FALSE): ?>
            <li><a href="<?php echo HTML::chars($page->url($last_page)) ?>" rel="last">>></a></li>
        <?php else: ?>
            <li class="disabled"><span>>></span></li>
        <?php endif ?>
    </ul>
</div>
<!--END PAGINATION-->
