<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Mlang module request class
 * From the module https://github.com/GeertDD/kohana-lang
 */
class Mlang_Request extends Kohana_Request {

    /** 	
     *
     * Extension of the request factory method. If none given, the URI will
     * be automatically detected. If the URI contains no language segment, the user
     * will be redirected to the same URI with the default language prepended.
     * If the URI does contain a language segment, I18n and locale will be set.
     * Also, a cookie with the current language will be set. Finally, the language
     * segment is chopped off the URI and normal request processing continues.
     *
     * @param   string   URI of the request
     * @param	Kohana_Cache cache object
     * @return  Request
     */
    public static function factory($uri = TRUE, HTTP_Cache $cache = NULL, $injected_routes = array()) {
        // Current request 
        if (!Request::$initial && !Kohana::$is_cli) { // Only external requests!!!
            // Get the Mlang instance
            $mlang = Mlang::instance();

            if ($uri === TRUE) {
                // We need the current URI
                $uri = Request::detect_uri();
            }

            // Normalize URI
            $uri = ltrim($uri, '/');

            // Look for a supported language in the first URI segment
            if (!preg_match('~^(?:' . implode('|', array_keys($mlang->lang_list)) . ')(?=/|$)~i', $uri, $matches)) {
                // We can't find a language, we're gonna need to look deeper
                if ($mlang->detect_default_lang() === $mlang->lang_default) {
                    // Set default config based language
                    $mlang->set_lang($mlang->lang_default);
                } else {
                    // Use the default server protocol
                    $protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
                    // Redirect to the same URI, but with language prepended
                    header($protocol . ' 302 Found');
                    header('Location: ' . URL::base(TRUE, TRUE) . $mlang->detect_default_lang() . '/' . $uri);
                    // Stop execution
                    exit;
                }
            } else {
                // Language found in the URI
                $lang = strtolower($matches[0]);
                // Set language founded in the URI
                $mlang->set_lang($lang);
                // Remove language from URI
                $uri = (string) substr($uri, strlen($lang));
                // Redirect for default language with stripped URI
                if ($lang === $mlang->lang_default) {
                    // Use the default server protocol
                    $protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
                    // Redirect to the same URI, but with language stripped
                    header($protocol . ' 302 Found');
                    header('Location: ' . URL::base(TRUE, TRUE) . ltrim($uri, '/'));
                    // Stop execution
                    exit;
                }
            }
        }
        // Continue normal request processing with the URI without language*/
        return parent::factory($uri, $cache, $injected_routes);
    }

}