<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Mlang core class
 * From the module https://github.com/GeertDD/kohana-lang
 */
class Mlang_Core {

    public $lang_list;
    public $lang_default;
    public $lang_cookie;

    /**
     * Singleton instance
     * @var Mlang_Core
     */
    private static $instance;

    /**
     * Get instance
     * @return Mlang_Core
     */
    public static function instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
     * Constructor
     */

    private function __construct() {
        $this->init_lang_config();
        $this->init_default_lang();
    }

    /**
     * Init the lang config
     */
    private function init_lang_config() {
        // Get the list of supported languages
        $this->lang_list = (array) Kohana::$config->load('mlang.languages');
        // Get the default language
        $this->lang_default = Kohana::$config->load('mlang.default');
        // Get language cookie name
        $this->lang_cookie = Kohana::$config->load('mlang.cookie');
    }

    /**
     * Set default lang settings
     */
    private function init_default_lang() {
        $this->set_lang($this->detect_default_lang());
    }

    /**
     * Looks for the best default language available and returns it.
     * A language cookie and HTTP Accept-Language headers are taken into account.
     *
     * @return  string  language key, e.g. "en", "fr", "nl", etc.
     */
    public function detect_default_lang() {
        // Look for language cookie first
        if ($lang = Cookie::get($this->lang_cookie)) {
            // Valid language found in cookie
            if ($this->is_valid_lang($lang)) {
                return $lang;
            }
            // Delete cookie with unset language
            Cookie::delete($this->lang_cookie);
        }
        // Parse HTTP Accept-Language headers
        foreach (Request::accept_lang() as $lang => $quality) {
            // Return the first language found (the language with the highest quality)
            if ($this->is_valid_lang($lang)) {
                return $lang;
            }
        }
        // Return the hard-coded default language as final fallback
        return $this->lang_default;
    }

    /**
     * Check if language is valid
     */
    public function is_valid_lang($lang) {
        return isset($this->lang_list[$lang]);
    }

    /**
     * Set lang settings and cookies
     */
    public function set_lang($language) {
        // Set the language in I18n
        //I18n::lang($language);
        I18n::lang($this->lang_list[$language]['i18n']);
        // Set locale
        setlocale(LC_ALL, $this->lang_list[$language]['locale']);
        // Update language cookie if needed
        if (Cookie::get($this->lang_cookie) !== $language) {
            Cookie::set($this->lang_cookie, $language);
        }
    }

}