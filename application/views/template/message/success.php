<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START SUCCESS MESSAGE-->
<div class="alert alert-success">
    <?php echo $message ?>
</div>
<!--END SUCCESS MESSAGE-->
