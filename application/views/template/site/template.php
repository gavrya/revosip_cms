<!DOCTYPE html>
<html lang="<?php echo I18n::lang(); ?>">
    <head>
        <title><?php echo $title; ?></title>
		<meta name="google-site-verification" content="z_VIaFymfyvbdc4aO6865xcXl6DnuDm5f7tjgmXTGaA" />
        <meta name='yandex-verification' content='48e540b6c56e2320' />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <?php foreach ($meta_data as $meta_attributes): ?>
            <?php echo '<meta', HTML::attributes($meta_attributes), ' />'; ?>

        <?php endforeach; ?>

        <?php foreach ($links as $link_attributes): ?>
            <?php echo '<link', HTML::attributes($link_attributes), ' />'; ?>

        <?php endforeach; ?>

        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <?php foreach ($styles as $style => $attributes): ?>
            <?php echo Html::style($style, $attributes); ?>

        <?php endforeach; ?>

        <?php foreach ($scripts as $script): ?>
            <?php echo HTML::script($script); ?>

        <?php endforeach; ?>

    </head>

    <body>
        <?php if (isset($navigation_bar)) echo $navigation_bar; ?>

        <div class="container">
            <div class="row-fluid">
                <div class="span8">
                    <?php if (isset($content)) echo $content; ?>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <?php if (isset($widget_launcher)): ?>
                            <div class="span12">
                                <div class="span12">
                                    <?php echo $widget_launcher; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (isset($widget_news)): ?>
                        <div class="row-fluid">
                            <div class="span12">
                                <?php echo $widget_news; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="footer">
                <hr />
				<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43205629-1', 'revosip.com');
  ga('send', 'pageview');

</script>
                Revosip 2013-2014
                <?php if (!isset($footer)) echo $footer; ?>
            </div>
        </div>

    </body>
</html>