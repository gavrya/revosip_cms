<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START NAVIGATION BAR-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo HTML::anchor('', 'Revosip', array('class' => 'brand')); ?>
            <!--.nav-collapse -->
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li><?php echo HTML::anchor('/about', __('View_Template_Site.navigation_bar.o_servise')); ?></li>
                    <li><?php echo HTML::anchor('/news', __('View_Template_Site.navigation_bar.novosti')); ?></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('View_Template_Site.navigation_bar.podderzhka'); ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><?php echo HTML::anchor('/faq', __('View_Template_Site.navigation_bar.voprosy-otvety')); ?></li>
                            <?php if (!$logged_in): ?>
                                <li><?php echo HTML::anchor('/recovery', __('View_Template_Site.navigation_bar.vosstanovlenie_parolya')); ?></li>
                            <?php endif; ?>
                            <li><?php echo HTML::anchor('/feedback', __('View_Template_Site.navigation_bar.obratnaya_svyaz')); ?></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav pull-right">
                    <li><a href="<?php echo URL::site(Request::current()->uri(), FALSE, 'ru'); ?>" title="Русский"><?php echo HTML::image(Assets::IMAGE_FLAG_RU, array('alt' => 'Русский')); ?></a></li>
                    <li><a href="<?php echo URL::site(Request::current()->uri(), FALSE, 'ua'); ?>" title="Українська"><?php echo HTML::image(Assets::IMAGE_FLAG_UA, array('alt' => 'Українська')); ?></a></li>
                    <li><a href="<?php echo URL::site(Request::current()->uri(), FALSE, 'en'); ?>" title="English"><?php echo HTML::image(Assets::IMAGE_FLAG_EN, array('alt' => 'English')); ?></a></li>

                    <?php if ($logged_in): ?>
                        <li><?php echo HTML::anchor('/profile', __('View_Template_Site.navigation_bar.profil')); ?></li>
                        <li><?php echo HTML::anchor('/logout', __('View_Template_Site.navigation_bar.vyjti')); ?> </li>
                    <?php else: ?>
                        <li><?php echo HTML::anchor('/login', __('View_Template_Site.navigation_bar.vxod')); ?></li>
                        <li><?php echo HTML::anchor('/registration', __('View_Template_Site.navigation_bar.registraciya')); ?> </li>
                    <?php endif; ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!--END NAVIGATION BAR-->
