<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONTENT BLOCK-->
<div class="well" style="background-color: white;">
    <?php if (isset($content_title)): ?>
        <h4><?php echo $content_title; ?></h4><hr />
    <?php endif; ?>

    <?php if (isset($content_message)) echo $content_message; ?>

    <?php if (isset($content)): ?>
        <?php echo $content; ?>
    <?php endif; ?>
</div>
<?php if (!empty($content_pagination)): ?>
    <?php echo $content_pagination; ?>
<?php endif; ?>
<!--END CONTENT BLOCK-->