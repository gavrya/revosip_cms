<!DOCTYPE html>
<html lang="<?php echo I18n::lang(); ?>">
    <head>
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <?php foreach ($meta_data as $meta_attributes): ?>
            <?php echo '<meta', HTML::attributes($meta_attributes), ' />'; ?>

        <?php endforeach; ?>

        <?php foreach ($links as $link_attributes): ?>
            <?php echo '<link', HTML::attributes($link_attributes), ' />'; ?>

        <?php endforeach; ?>

        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <?php foreach ($styles as $style => $attributes): ?>
            <?php echo Html::style($style, $attributes); ?>

        <?php endforeach; ?>

        <?php foreach ($scripts as $script): ?>
            <?php echo HTML::script($script); ?>

        <?php endforeach; ?>

    </head>

    <body>
        <?php if (isset($navigation_bar)) echo $navigation_bar; ?>

        <div class="container">
            <div class="row-fluid">
                <div class="span12">
                    <?php if (isset($navigation)) echo $navigation; ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <?php if (isset($menu)) echo $menu; ?>
                </div>
                <div class="span8">
                    <?php if (isset($content)) echo $content; ?>
                </div>
            </div>
            <div class="footer">
                <hr />
                Revosip 2013-2014
                <?php if (!isset($footer)) echo $footer; ?>
            </div>
        </div>
    </body>
</html>