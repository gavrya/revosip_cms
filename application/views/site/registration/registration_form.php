<?php defined('SYSPATH') or die('No direct script access.'); ?>

<script>
    $(document).ready(function() {
        $("#check_id").click(function() {
            var login = $.trim($("#<?php echo Field::USER_LOGIN . Field::ID; ?>").val());
            if(login.length >= 4) {
                $("#result_id").load("<?php echo URL::site('/ajax/check/reglogin'); ?>"+"/"+"-"+encodeURIComponent(login)+"-");
            } else {
                $("#result_id").html("");
            }
        });
        $(document).ajaxStart(function() {
            $("#result_id").html("<img src='<?php echo URL::site(Assets::IMAGE_AJAX_LOADER, FALSE, FALSE); ?>' />");
        });
        $(document).ajaxError(function() {
            $("#result_id").html("");
        });
    }); 
</script>

<!--START REGISTRATION FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::USER_LOGIN . Field::ID, __('Controller_Site_Registration.registration.login'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_LOGIN, HTML::chars(Arr::get($_POST, Field::USER_LOGIN, '')), array('id' => Field::USER_LOGIN . Field::ID)); ?></td>
        <td><button id="check_id" class="btn btn-mini" type="button" ><?php echo __('Controller_Site_Registration.registration.proverit'); ?></button></td>
        <td id="result_id"></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_PASSWORD . Field::ID, __('Controller_Site_Registration.registration.parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::USER_PASSWORD, '', array('id' => Field::USER_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_REPASSWORD . Field::ID, __('Controller_Site_Registration.registration.podtverzhdenie_parolya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::USER_REPASSWORD, '', array('id' => Field::USER_REPASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_NAME . Field::ID, __('Controller_Site_Registration.registration.polnoe_imya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_NAME, HTML::chars(Arr::get($_POST, Field::USER_NAME, '')), array('id' => Field::USER_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_EMAIL . Field::ID, __('Controller_Site_Registration.registration.email_adres'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_EMAIL, HTML::chars(Arr::get($_POST, Field::USER_EMAIL, '')), array('id' => Field::USER_EMAIL . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_LANGUAGE . Field::ID, __('Controller_Site_Registration.registration.yazyk'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::USER_LANGUAGE, $registration_languages, HTML::chars(Arr::get($_POST, Field::USER_LANGUAGE, Model_Users::ENUM_LANGUAGE_RU)), array('id' => Field::USER_LANGUAGE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_TIMEZONE . Field::ID, __('Controller_Site_Registration.registration.vremennaya_zona'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::USER_TIMEZONE, $registration_timezones, HTML::chars(Arr::get($_POST, Field::USER_TIMEZONE, Model_Users::ENUM_TIMEZONE_P_2)), array('id' => Field::USER_TIMEZONE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2"><pre><?php echo $captcha ?></pre></td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::USER_ACCEPT, NULL, !empty($_POST) ? isset($_POST[Field::USER_ACCEPT]) : FALSE, array('id' => Field::USER_ACCEPT . Field::ID)); ?>
                <?php echo __('Controller_Site_Registration.registration.ya_prinimayu').' '. HTML::anchor('/conditions', __('Controller_Site_Registration.registration.usloviya_polzovatelskogo_soglasheniya'), array('target' => '_blank', 'rel' => 'nofollow')) ?>
            </label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_Registration.registration.zaregistrirovatsya'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>
<!--END REGISTRATION FORM-->
