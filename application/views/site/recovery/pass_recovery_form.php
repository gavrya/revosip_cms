<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START RECOVERY FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::USER_EMAIL . Field::ID, __('Controller_Site_Recovery.recovery.email_adres'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_EMAIL, HTML::chars(Arr::get($_POST, Field::USER_EMAIL, '')), array('id' => Field::USER_EMAIL . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <pre><?php echo $captcha ?></pre>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_Recovery.recovery.vosstanovit_parol'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--START RECOVERY FORM-->
