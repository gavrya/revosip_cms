<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START FAQ-->
<?php $section_num = 1; ?>
<div class="accordion" id="accordion_faq">
    <?php foreach ($faq as $section => $questions): ?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_faq" href="#collapse<?php echo $section_num; ?>">
                    <?php echo $section_num . ') ' . $section; ?>
                </a>
            </div>
            <div id="collapse<?php echo $section_num; ?>" class="accordion-body collapse<?php if ($section_id == $section_num) echo ' in'; ?>">
                <div class="accordion-inner">
                    <!--accordion-inner-->
                    <?php $question_num = 1; ?>
                    <div class="well">
                        <div class="accordion" id="accordion_section<?php echo $section_num; ?>">
                            <?php foreach ($questions as $question => $answer): ?>
                                <div class="accordion-group" id="<?php echo $section_num . '-' . $question_num; ?>">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_section<?php echo $section_num; ?>" href="#collapse<?php echo $section_num . '-' . $question_num; ?>">
                                            <?php echo $section_num . '.' . $question_num . ') ' . $question; ?>
                                        </a>
                                    </div>
                                    <div id="collapse<?php echo $section_num . '-' . $question_num; ?>" class="accordion-body collapse<?php if ($section_id == $section_num && $question_id == $question_num) echo ' in'; ?>">
                                        <div class="accordion-inner">
                                            <?php echo $answer; ?>
                                            <?php echo HTML::anchor('/faq/' . $section_num . '-' . $question_num . '#' . $section_num . '-' . $question_num, '<i class="icon-share"></i>', array('title' => __('Controller_Site_Home.faq.ssylka_na_otvet'))); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $question_num++; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!--/accordion-inner-->
                </div>
            </div>
        </div>
        <?php $section_num++; ?>
    <?php endforeach; ?>
</div>
<!--END FAQ-->
