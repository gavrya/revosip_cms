<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START HOME-->
<div class="hero-unit">
    <h3><?php echo __('Controller_Site_Home.home.revosip_-_oblachnyj_sip_softfon'); ?></h3>
    <p><?php echo __('Controller_Site_Home.home.otkrytyj_beta_test'); ?></p>
    <?php if (!$logged_in): ?>
        <a href="/registration" class="btn btn-success"><?php echo __('Controller_Site_Home.home.registraciya'); ?></a>
    <?php endif; ?>
</div>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#hometab1" data-toggle="tab"><?php echo __('Controller_Site_Home.home.preimushhestva'); ?></a></li>
        <li><a href="#hometab2" data-toggle="tab"><?php echo __('Controller_Site_Home.home.sravnenie_s_ip_telefonom'); ?></a></li>
        <li><a href="#hometab3" data-toggle="tab"><?php echo __('Controller_Site_Home.home.xarakteristiki'); ?></a></li>
        <li><a href="#hometab4" data-toggle="tab"><?php echo __('Controller_Site_Home.home.skrinshoty'); ?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="hometab1">
            <table class="table table-condensed">
                <tr> 
                    <th></th>
                    <th><?php echo __('Controller_Site_Home.home.osobennosti'); ?></th>
                    <th></th>
                    <th><?php echo __('Controller_Site_Home.home.vozmozhnosti'); ?></th>
                </tr>
                <?php foreach ($content['benefits'] as $features => $benefits): ?>
                    <tr>
                        <td  width="50"><?php echo HTML::image('/assets/images/check.png'); ?></td>
                        <td><?php echo $features; ?></td>
                        <td  width="50"><?php echo HTML::image('/assets/images/arrow.png'); ?></td>
                        <td><?php echo $benefits; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="tab-pane" id="hometab2">
            <table class="table table-bordered table-hover table-condensed">
                <tr> 
                    <th><?php echo __('Controller_Site_Home.home.predmet_sravneniya'); ?></th>
                    <th><?php echo __('Controller_Site_Home.home.programmnyj_telefon_revosip'); ?></th>
                    <th><?php echo __('Controller_Site_Home.home.apparatnyj_ip_telefon'); ?></th>
                </tr>
                <?php foreach ($content['comparison'] as $comparison): ?>
                    <tr>
                        <td><?php echo $comparison[0]; ?></td>
                        <td class="text-success"><?php echo $comparison[1]; ?></td>
                        <td class="text-error"><?php echo $comparison[2]; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="tab-pane" id="hometab3">
            <?php foreach ($content['features'] as $key => $features): ?>
                <h5><?php echo $key; ?></h5>
                <ul>
                    <?php foreach ($features as $param): ?>
                        <li><?php echo $param; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane" id="hometab4">
            <?php for ($i = 1; $i < 7; $i++): ?>
                <a href="<?php echo URL::site('/assets/images/screenshots/revosip-' . I18n::lang() . '-' . $i . '.jpg', FALSE, FALSE); ?>" target="_blank"><?php echo HTML::image('/assets/images/screenshots/revosip-' . I18n::lang() . '-' . $i . '.jpg', array('alt' => 'revosip cloud java sip client', 'class' => 'img-polaroid', 'width' => '80%', 'height' => '50%')); ?></a>
            <?php endfor; ?>
        </div>
    </div>
</div>
<!--END HOME-->

