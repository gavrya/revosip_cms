<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START FEEDBACK-->
<!--noindex-->
<p><?php echo __('Controller_Site_Home.feedback.revosip_-_oblachnyj_sip_softfon'); ?></p>
<p><?php echo __('Controller_Site_Home.feedback.email_adres'); ?> <a rel="nofollow" href="mailto:<?php echo $feedback_email; ?>"><?php echo $feedback_email; ?></a></p>
<p><?php echo __('Controller_Site_Home.feedback.skype'); ?> <a rel="nofollow" href="skype:<?php echo $feedback_skype; ?>?chat"><?php echo $feedback_skype; ?></a></p>
<p><?php echo __('Controller_Site_Home.feedback.kiev,_ukraina'); ?></p>
<!--/noindex-->
<!--END FEEDBACK-->
