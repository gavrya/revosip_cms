<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START LOGIN FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::USER_LOGIN . Field::ID, __('Controller_Site_Login.login.login'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_LOGIN, HTML::chars(Arr::get($_POST, Field::USER_LOGIN, '')), array('id' => Field::USER_LOGIN . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_PASSWORD . Field::ID, __('Controller_Site_Login.login.parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::USER_PASSWORD, '', array('id' => Field::USER_PASSWORD . Field::ID)); ?></td>
        <td><?php echo HTML::anchor('/recovery', __('Controller_Site_Login.login.zabyli_parol')); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2"><pre><?php echo $captcha ?></pre></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_Login.login.vojti'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END LOGIN FORM-->