<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START NEWS EDIT FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::NEWS_MARK . Field::ID, __('Controller_Site_News.add.uroven_vazhnosti'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::NEWS_MARK, $news_marks, Arr::get($_POST, Field::NEWS_MARK, $news_contents[0][Model_News::NEWS_MARK]), array('id' => Field::NEWS_MARK . Field::ID)); ?></td>
    </tr>
    <?php foreach ($news_contents as $news_content): ?>
        <?php $news_lang = $news_content[Model_News::NEWS_LANG]; ?>
        <tr>
            <td colspan="2"><h5><?php echo $news_languages[$news_lang]; ?></h5><hr /></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_TITLE . $news_lang . Field::ID, __('Controller_Site_News.add.oglavlenie_novosti'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_TITLE . $news_lang, Arr::get($_POST, Field::NEWS_TITLE . $news_lang, $news_content[Model_News::NEWS_TITLE]), array('id' => Field::NEWS_TITLE . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_META_KEYWORDS . $news_lang . Field::ID, __('Controller_Site_News.add.klyuchevye_slova'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_META_KEYWORDS . $news_lang, Arr::get($_POST, Field::NEWS_META_KEYWORDS . $news_lang, $news_content[Model_News::NEWS_META_KEYWORDS]), array('id' => Field::NEWS_META_KEYWORDS . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_META_DESCRIPTIONS . $news_lang . Field::ID, __('Controller_Site_News.add.kratkoe_opisanie'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_META_DESCRIPTIONS . $news_lang, Arr::get($_POST, Field::NEWS_META_DESCRIPTIONS . $news_lang, $news_content[Model_News::NEWS_META_DESCRIPTIONS]), array('id' => Field::NEWS_META_DESCRIPTIONS . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_CONTENT . $news_lang . Field::ID, __('Controller_Site_News.add.soderzhimoe_novosti'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::textarea(Field::NEWS_CONTENT . $news_lang, Arr::get($_POST, Field::NEWS_CONTENT . $news_lang, $news_content[Model_News::NEWS_CONTENT]), array('class' => 'input-xxlarge', 'id' => Field::NEWS_CONTENT . $news_lang . Field::ID)); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::MAGIC_PASSWORD . Field::ID, __('Controller_Site_News.add.magicheskij_parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::MAGIC_PASSWORD, NULL, array('id' => Field::MAGIC_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_News.edit.soxranit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END NEWS EDIT FORM-->
