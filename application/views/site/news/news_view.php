<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php
$text_mark = array(
    Model_News::ENUM_MARK_WARNING => 'text-error',
    Model_News::ENUM_MARK_HIGH => 'text-success',
    Model_News::ENUM_MARK_NORMAL => 'text-info',
);
?>

<!--START NEWS VIEW-->
<div class="page-header">
    <h4 <?php echo HTML::attributes(array('class' => $text_mark[$news[Model_News::NEWS_MARK]])) ?>><?php echo $news[Model_News::NEWS_TITLE]; ?>   <small><?php echo $news[Model_News::NEWS_DATE_TZ_FORMAT]; ?></small></h4>
</div>
<p><?php echo $news[Model_News::NEWS_CONTENT]; ?></p>
<!--END NEWS VIEW-->
