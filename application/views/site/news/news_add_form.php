<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START NEWS ADD FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::NEWS_MARK . Field::ID, __('Controller_Site_News.add.uroven_vazhnosti'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::NEWS_MARK, $news_marks, Arr::get($_POST, Field::NEWS_MARK, Model_News::ENUM_MARK_NORMAL), array('id' => Field::NEWS_MARK . Field::ID)); ?></td>
    </tr>
    <?php foreach (Model_News::$LANG_ENUM_LIST as $news_lang): ?>
        <tr>
            <td colspan="2"><h5><?php echo $news_languages[$news_lang]; ?></h5><hr /></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_TITLE . $news_lang . Field::ID, __('Controller_Site_News.add.oglavlenie_novosti'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_TITLE . $news_lang, Arr::get($_POST, Field::NEWS_TITLE . $news_lang, ''), array('id' => Field::NEWS_TITLE . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_META_KEYWORDS . $news_lang . Field::ID, __('Controller_Site_News.add.klyuchevye_slova'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_META_KEYWORDS . $news_lang, Arr::get($_POST, Field::NEWS_META_KEYWORDS . $news_lang, ''), array('id' => Field::NEWS_META_KEYWORDS . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_META_DESCRIPTIONS . $news_lang . Field::ID, __('Controller_Site_News.add.kratkoe_opisanie'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::NEWS_META_DESCRIPTIONS . $news_lang, Arr::get($_POST, Field::NEWS_META_DESCRIPTIONS . $news_lang, ''), array('id' => Field::NEWS_META_DESCRIPTIONS . $news_lang . Field::ID)); ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label(Field::NEWS_CONTENT . $news_lang . Field::ID, __('Controller_Site_News.add.soderzhimoe_novosti'), array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::textarea(Field::NEWS_CONTENT . $news_lang, Arr::get($_POST, Field::NEWS_CONTENT . $news_lang, ''), array('class' => 'input-xxlarge', 'id' => Field::NEWS_CONTENT . $news_lang . Field::ID)); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::MAGIC_PASSWORD . Field::ID, __('Controller_Site_News.add.magicheskij_parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::MAGIC_PASSWORD, NULL, array('id' => Field::MAGIC_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_News.add.dobavit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END NEWS ADD FORM-->
