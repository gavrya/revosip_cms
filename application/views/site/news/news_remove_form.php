<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START NEWS REMOVE FORM-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::MAGIC_PASSWORD . Field::ID, __('Controller_Site_News.remove.magicheskij_parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::MAGIC_PASSWORD, NULL, array('id' => Field::MAGIC_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Site_News.remove.udalit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END NEWS REMOVE FORM-->
