<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php
$text_mark = array(
    Model_News::ENUM_MARK_WARNING => 'text-error',
    Model_News::ENUM_MARK_HIGH => 'text-success',
    Model_News::ENUM_MARK_NORMAL => 'text-info',
);
?>

<!--START NEWS LIST-->
<?php foreach ($news_list as $news): ?>
    <h4><?php echo HTML::anchor('/news/view/' . $news[Model_News::NEWS_ID], $news[Model_News::NEWS_TITLE], array('class' => $text_mark[$news[Model_News::NEWS_MARK]])); ?>   <small><?php echo $news[Model_News::NEWS_DATE_TZ_FORMAT]; ?></small></h4>
    <p>
        <?php echo Text::limit_chars($news[Model_News::NEWS_CONTENT], 255, '...'); ?>
    </p>
    <br/>
<?php endforeach; ?>
<!--END NEWS LIST-->
