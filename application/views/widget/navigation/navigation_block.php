<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php
$last_element = end($elements);
reset($elements);
?>
<!--START NAVIGATION BLOCK-->
<ul class="breadcrumb">
    <?php foreach ($elements as $element): ?>
        <?php if ($element === $last_element): ?>
            
            <li><strong><?php echo HTML::anchor($element['link'], $element['label']); ?></strong></li>
        <?php else: ?>
            <li><?php echo HTML::anchor($element['link'], $element['label']); ?><span class="divider">/</span></li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
<!--END NAVIGATION BLOCK-->
