<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!DOCTYPE html>
<html lang="<?php echo I18n::lang(); ?>">
    <head>
        <title>Revosip</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="<?php echo __('View_Widget_Launch.applet.keywords'); ?>" />
        <meta name="description" content="<?php echo __('View_Widget_Launch.applet.description'); ?>" />
        <style type="text/css">
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
                width: 100%;
            }
        </style>
    </head>
    <body scroll="no" style="overflow: hidden">
        <div style='width:100%;height:100%;background:gainsboro;'>
            <applet height="100%" width="100%" align="bottom" codebase="<?php echo URL::site('/java', TRUE, FALSE); ?>" archive="<?php echo file_get_contents('java/version.txt'); ?>" code="com.revosip.ui.AppletApplication.class" name="Revosip">
                <param name="codebase_lookup" value="false" />
                <param name="permissions" value="all-permissions" />
                <?php echo __('View_Widget_Launch.applet.description') . '<br />'; ?>

                <?php echo __('View_Widget_Launch.applet.dlya_podderzhki_zapuska_java_appletov_neobxodimo_ustanovit_virtualnuyu_mashinu_java') . ' ' . HTML::anchor('http://www.oracle.com/technetwork/java/javase/downloads/index.html', __('View_Widget_Launch.applet.skachat'), array('target' => '_blank')) ?>

            </applet>
        </div>
    </body>
</html>