<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>' ?>

<jnlp spec="6.0+" version="1.0" codebase="<?php echo URL::site('/java', TRUE, FALSE) ?>">

    <!-- information -->
    <information>
        <title>Revosip</title>
        <vendor>Revosip</vendor>
        <homepage href="<?php echo URL::base(TRUE) ?>"/>
        <description>Revosip - cloud SIP client</description>
        <!-- icons -->
        <icon kind="default" href="res/icon64.png" width="64" height="64"/>
        <icon kind="shortcut" href="res/icon16.png" width="16" height="16"/>
        <icon kind="shortcut" href="res/icon24.png" width="24" height="24"/>
        <icon kind="shortcut" href="res/icon32.png" width="32" height="32"/>
        <icon kind="shortcut" href="res/icon48.png" width="48" height="48"/>
        <icon kind="shortcut" href="res/icon64.png" width="64" height="64"/>
        <icon kind="shortcut" href="res/icon72.png" width="72" height="72"/>
        <icon kind="shortcut" href="res/icon96.png" width="96" height="96"/>
        <icon kind="shortcut" href="res/icon128.png" width="128" height="128"/>
        <!-- desktop shortcut -->
        <shortcut online="true" install="true">
            <desktop/>
            <menu submenu="Revosip - cloud SIP client"/>
        </shortcut>
    </information>

    <!-- security -->
    <security>
        <all-permissions/>
    </security>

    <!-- update -->
    <update check="always" policy="always"/>

    <!-- resources -->
    <resources>
        <!-- JVM priorities -->
        <java version="1.7+" initial-heap-size="128m" max-heap-size="512m"/>
        <java version="1.6.0_10+" initial-heap-size="128m" max-heap-size="512m"/>
        <!-- jars -->
        <jar href="Revosip.jar" main="true" version="1.0"/>
        <jar href="lib/forms-1.2.0.jar" version="1.0"/>
        <jar href="lib/jain-sip-sdp-1.2.158.jar" version="1.0"/>
        <jar href="lib/jlibrtp.jar" version="1.0"/>
        <jar href="lib/jspeex.jar" version="1.0"/>
        <jar href="lib/laf-plugin-7.2.1.jar" version="1.0"/>
        <jar href="lib/laf-widget-7.2.1.jar" version="1.0"/>
        <jar href="lib/log4j-1.2.16.jar" version="1.0"/>
        <jar href="lib/stun4j.jar" version="1.0"/>
        <jar href="lib/substance-7.2.1.jar" version="1.0"/>
        <jar href="lib/trident-7.2.1.jar" version="1.0"/>
        <!-- version download protocol -->
        <property name="jnlp.versionEnabled" value="true"/>
    </resources>

    <!-- main class -->
    <application-desc main-class="com.revosip.ui.DesktopApplication"/>

</jnlp>
