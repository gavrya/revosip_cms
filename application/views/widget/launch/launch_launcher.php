<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START LAUNCHER-->
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#launchtab1" data-toggle="tab">Desktop <?php echo __('View_Widget_Launch.launcher.versiya'); ?></a></li>
        <li><a href="#launchtab2" data-toggle="tab">Applet <?php echo __('View_Widget_Launch.launcher.versiya'); ?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="launchtab1">
            <div class="hero-unit">
                <p class="text-center">Revosip desktop</p>
                <?php echo HTML::anchor('/launch/jnlp', __('View_Widget_Launch.launcher.zapustit'), array('class' => 'btn btn-large btn-block btn-primary', 'title' => 'Revosip desktop v' . $app_version . ' build ' . $app_build)) ?>
                
            </div>
        </div>
        <div class="tab-pane" id="launchtab2">
            <div class="hero-unit">
                <p class="text-center">Revosip applet</p>
                <?php echo HTML::anchor('/launch/applet', __('View_Widget_Launch.launcher.zapustit'), array('target' => '_blank', 'class' => 'btn btn-large btn-block btn-primary', 'title' => 'Revosip applet v' . $app_version . ' build ' . $app_build)) ?>

            </div>
        </div>
    </div>
</div>
<!--END LAUNCHER-->
