<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php
$text_mark = array(
    Model_News::ENUM_MARK_WARNING => 'text-error',
    Model_News::ENUM_MARK_HIGH => 'text-success',
    Model_News::ENUM_MARK_NORMAL => 'text-info',
);
?>

<!--START NEWS WIDGET-->
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab10" data-toggle="tab"><?php echo __('View_Widget_News.list.poslednie_novosti'); ?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab10">
            <div class="well">
                <?php foreach ($news_list as $news): ?>
                    <h5><?php echo HTML::anchor('/news/view/' . $news[Model_News::NEWS_ID], $news[Model_News::NEWS_TITLE], array('class' => $text_mark[$news[Model_News::NEWS_MARK]])); ?>   <small><?php echo $news[Model_News::NEWS_DATE_TZ_FORMAT]; ?></small></h5>
                <?php endforeach; ?>
                <?php echo HTML::anchor('/news', __('View_Widget_News.list.vse_novosti'), array('class' => 'muted')); ?>
            </div>
        </div>
    </div>
</div>
<!--END NEWS WIDGET-->
