<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START MENU BLOCK-->
<div class="well" style="background-color: white;">
    <ul class="nav nav-list">
        <?php foreach ($elements as $element): ?>
            <?php echo $element; ?>
        <?php endforeach; ?>
    </ul>    
</div>
<!--START MENU BLOCK-->
