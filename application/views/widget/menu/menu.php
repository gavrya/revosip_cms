<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START MENU ELEMENT-->
<strong><?php echo $title; ?></strong>
<?php foreach ($elements as $element): ?>
    <?php if ($element['selected'] == TRUE): ?>
        <li class="active"><strong><?php echo HTML::anchor($element['link'], $element['label']); ?></strong></li>
    <?php else: ?>
        <li><?php echo HTML::anchor($element['link'], $element['label']); ?></li>
    <?php endif; ?>
<?php endforeach; ?>
<br />
<!--END MENU ELEMENT-->
