<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONTACTS LIST-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Contacts.list.imya_kontakta'); ?></th>
        <th><?php echo __('Controller_Profile_Contacts.list.nomer_telefona'); ?></th>
        <th><?php echo __('Controller_Profile_Contacts.list.data_dobavleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Contacts.list.dejstvie'); ?></th>
    </tr>
    <?php foreach ($contact_profiles as $contact_data): ?>
        <tr>
            <td><?php echo HTML::anchor('/contact/' . $account_id . '/' . $contact_data[Model_Profile_Contact::CONTACT_ID] . '/info', HTML::chars($contact_data[Model_Profile_Contact::CONTACT_NAME])); ?></td>
            <td><?php echo HTML::chars($contact_data[Model_Profile_Contact::CONTACT_PHONE]); ?></td>
            <td><?php echo $contact_data[Model_Profile_Contact::CONTACT_CREATED_TZ_FORMAT]; ?></td>
            <td><?php echo HTML::anchor('/contact/' . $account_id . '/' . $contact_data[Model_Profile_Contact::CONTACT_ID] . '/remove', __('Controller_Profile_Contacts.list.udalit')); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END CONTACTS LIST-->
