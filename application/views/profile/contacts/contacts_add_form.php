<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONTACTS ADD-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::CONTACT_NAME . Field::ID, __('Controller_Profile_Contacts.add.imya_kontakta'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONTACT_NAME, HTML::chars(Arr::get($_POST, Field::CONTACT_NAME, '')), array('id' => Field::CONTACT_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CONTACT_PHONE . Field::ID, __('Controller_Profile_Contacts.add.nomer_telefona'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONTACT_PHONE, HTML::chars(Arr::get($_POST, Field::CONTACT_PHONE, '')), array('id' => Field::CONTACT_PHONE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Contacts.add.dobavit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END CONTACTS ADD-->
