<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONNECTION REMOVE-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td colspan="2"><?php echo __('Controller_Profile_Connection.remove.podtverdite_udalenie_profilya_podklyucheniya'); ?> <strong><?php echo HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_NAME]); ?></strong></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2"><pre><?php echo $captcha ?></pre></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Connection.remove.udalit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END CONNECTION REMOVE-->
