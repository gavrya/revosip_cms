<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php
$key_map = array(TRUE => __('Controller_Profile_Connection.info.da'), FALSE => __('Controller_Profile_Connection.info.net'));
?>

<!--START CONNECTION INFO-->
<h5><?php echo __('Controller_Profile_Connection.info.profil_podklyucheniya'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.nazvanie'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_NAME]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.kratkoe_opisanie'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_DESCRIPTION]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.aktivnyj'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::CONNECTION_ACTIVE_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.vvodit_parol_lokalno'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::CONNECTION_PASS_REQUIRED_FLAG]]; ?></td>
    </tr>
</table>

<h5><?php echo __('Controller_Profile_Connection.info.nastrojki_sip_akkaunta'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.otobrazhaemoe_imya'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_CALLER_ID]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.dobavochnyj_nomer'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_EXTENSION]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.imya_polzovatelya'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_USER_NAME]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.parol'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::ACCOUNT_PASSWORD]) ? '********' : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.sekretnyj_parol'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::ACCOUNT_SECRET_PASSWORD]) ? '********' : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_sekretnyj_parol'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::ACCOUNT_SECURE_FLAG]]; ?></td>
    </tr>
</table>

<h5><?php echo __('Controller_Profile_Connection.info.setevye_nastrojki'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.adres_sip_servera'); ?></td>
        <td><?php echo HTML::chars($connection_profile[Model_Profile_Connection::NETWORK_SIP_SERVER]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.port_sip_servera'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::NETWORK_SIP_SERVER_PORT]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.adres_sip_proksi_servera'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER]) ? HTML::chars($connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER]) : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.port_sip_proksi_servera'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER_PORT]) ? $connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER_PORT] : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.lokalnyj_ip_adres'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NETWORK_LOCAL_ADDRESS]) ? HTML::chars($connection_profile[Model_Profile_Connection::NETWORK_LOCAL_ADDRESS]) : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.lokalnyj_port'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NETWORK_LOCAL_PORT]) ? $connection_profile[Model_Profile_Connection::NETWORK_LOCAL_PORT] : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.minimalnyj_rtp_port'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::NETWORK_MIN_RTP_PORT]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.maksimalnyj_rtp_port'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::NETWORK_MAX_RTP_PORT]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.tajmer_registracii'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::NETWORK_REGISTER_EXPIRES]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_sluchajnyj_lokalnyj_port'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NETWORK_ANY_LOCAL_PORT_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.opredelyat_lokalnyj_adres_avtomaticheski'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NETWORK_DEFAULT_INTERFACE_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_proksi_server'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NETWORK_PROXY_FLAG]]; ?></td>
    </tr>
</table>

<h5><?php echo __('Controller_Profile_Connection.info.nastrojki_parametrov_nat'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.tajmer_keepalive'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::NAT_KEEP_ALIVE]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.adres_stun_servera'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NAT_STUN_SERVER]) ? HTML::chars($connection_profile[Model_Profile_Connection::NAT_STUN_SERVER]) : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.port_stun_servera'); ?></td>
        <td><?php echo!empty($connection_profile[Model_Profile_Connection::NAT_STUN_SERVER_PORT]) ? $connection_profile[Model_Profile_Connection::NAT_STUN_SERVER_PORT] : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_stun_razvedku'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NAT_STUN_LOOKUP_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_stun_server_po_umolchaniyu'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NAT_DEFAULT_STUN_SERVER_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_tajmer_keepalive'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NAT_KEEP_ALIVE_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_privyazku_k_sip_serveru'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NAT_SERVER_BIND_FLAG]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.ispolzovat_simmetricheskij_rtp'); ?></td>
        <td><?php echo $key_map[$connection_profile[Model_Profile_Connection::NAT_SYMMETRIC_RTP_FLAG]]; ?></td>
    </tr>
</table>

<h5><?php echo __('Controller_Profile_Connection.info.nastrojki_prioritetov_kodekov'); ?></h5>
<table class="table table-bordered table-hover" >
    <?php for ($i = 0; $i < 12; $i++): ?>
        <tr>
            <td><?php echo __('Controller_Profile_Connection.info.kodek') . ' №' . ($i + 1); ?>:</td>
            <td><?php echo isset($connection_codec_config[$i][Model_Profile_Connection::CODEC_TYPE]) ? $codec_types[$connection_codec_config[$i][Model_Profile_Connection::CODEC_TYPE]] : __('Controller_Profile_Connection.info.ne_ukazan'); ?></td>
        </tr>
    <?php endfor; ?>
</table>

<h5><?php echo __('Controller_Profile_Connection.info.nastrojki_parametrov_dtmf'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.predpochitaemyj_dtmf_metod'); ?></td>
        <td><?php echo $dtmf_methods[$connection_profile[Model_Profile_Connection::DTMF_METHOD]]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Connection.info.dlitelnost_dtmf_signala'); ?></td>
        <td><?php echo $connection_profile[Model_Profile_Connection::DTMF_DURATION]; ?></td>
    </tr>
    <tr>
        <td>DTMF payload:</td>
        <td><?php echo $connection_profile[Model_Profile_Connection::DTMF_PAYLOAD]; ?></td>
    </tr>
</table>
<!--END CONNECTION INFO-->
