<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNTS PROLONGS-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Accounts.prolongs.nazvanie_akkaunta'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.prolongs.data_prodleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.prolongs.summa_prodleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.prolongs.kolichestvo_dnej_prodleniya'); ?></th>
    </tr>
    <?php foreach ($accounts_prolong_history as $account_prolong): ?>
        <tr>
            <td><?php echo HTML::anchor('account/' . $account_prolong[Model_Profile_Account::ACCOUNT_ID] . '/info', HTML::chars($account_prolong[Model_Profile_Account::ACCOUNT_NAME])); ?></td>
            <td><?php echo $account_prolong[Model_Profile_Account::PROLONG_DATE_TZ_FORMAT]; ?></td>
            <td>$<?php echo $account_prolong[Model_Profile_Account::PROLONG_CREDIT]; ?></td>
            <td><?php echo $account_prolong[Model_Profile_Account::PROLONG_DAYS]; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END ACCOUNTS PROLONGS-->
