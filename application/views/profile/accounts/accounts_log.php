<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNTS LOG-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Accounts.log.nazvanie_akkaunta'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.log.data_sobytiya'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.log.ip_adres'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.log.tip_sobytiya'); ?></th>
    </tr>
    <?php foreach ($user_accounts_logs as $account_log): ?>
        <tr>
            <td><?php echo HTML::anchor('/account/' . $account_log[Model_Profile_Account::ACCOUNT_ID] . '/info', HTML::chars($account_log[Model_Profile_Account::ACCOUNT_NAME])); ?></td>
            <td><?php echo $account_log[Model_Profile_Account::LOG_DATE_TZ_FORMAT]; ?></td>
            <td><?php echo $account_log[Model_Profile_Account::LOG_IP]; ?></td>
            <td><?php echo $account_log[Model_Profile_Account::LOG_TYPE] == Model_Profile_Account::ENUM_LOG_LOGIN ? __('Controller_Profile_Accounts.log.vxod') : __('Controller_Profile_Accounts.log.vyxod'); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--START ACCOUNTS LOG-->
