<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNTS LIST-->
<table class="table table-bordered table-hover" >
    <tr>
        <th><?php echo __('Controller_Profile_Accounts.list.nazvanie_akkaunta'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.list.data_sozdaniya'); ?></th>
        <th><?php echo __('Controller_Profile_Accounts.list.status'); ?></th>
    </tr>
    <?php foreach ($account_profiles as $account_profile): ?>
        <tr
        <?php
        /*
        if ($account_profile[Model_Profile_Account::ACCOUNT_EXPIRED]) {
            echo 'class="error"';
        } else 
        */    
        if (!$account_profile[Model_Profile_Account::ACCOUNT_ACTIVE]) {
            echo 'class="warning"';
        }
        ?>
            >
            <td><?php echo HTML::anchor('/account/' . $account_profile[Model_Profile_Account::ACCOUNT_ID] . '/info', HTML::chars($account_profile[Model_Profile_Account::ACCOUNT_NAME])); ?></td>
            <td><?php echo $account_profile[Model_Profile_Account::ACCOUNT_CREATED_TZ_FORMAT]; ?></td>
            <td><?php echo HTML::anchor('/account/' . $account_profile[Model_Profile_Account::ACCOUNT_ID] . ($account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] ? '/disactivate' : '/activate'), ($account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] ? __('Controller_Profile_Accounts.list.aktivnyj') : __('Controller_Profile_Accounts.list.neaktivnyj'))); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END ACCOUNTS LIST-->
