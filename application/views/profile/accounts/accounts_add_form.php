<?php defined('SYSPATH') or die('No direct script access.'); ?>

<script>
    $(document).ready(function() {
        $("#check_id").click(function() {
            var login = $.trim($("#<?php echo Field::ACCOUNT_LOGIN . Field::ID; ?>").val());
            if (login.length >= 4) {
                $("#result_id").load("<?php echo URL::site('/ajax/check/acclogin'); ?>" + "/" + "-" + encodeURIComponent(login) + "-" + "/" + "<?php echo $user_data[Model_Users::USER_ID]; ?>");
            } else {
                $("#result_id").html("");
            }
        });
        $(document).ajaxStart(function() {
            $("#result_id").html("<img src='<?php echo URL::site(Assets::IMAGE_AJAX_LOADER, FALSE, FALSE); ?>' />");
        });
        $(document).ajaxError(function() {
            $("#result_id").html("");
        });
    });
</script>

<!--START ACCOUNTS ADD FORM-->
<?php echo Form::open(); ?>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::ACCOUNT_NAME . Field::ID, __('Controller_Profile_Accounts.add.nazvanie_akkaunta'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACCOUNT_NAME, HTML::chars(Arr::get($_POST, Field::ACCOUNT_NAME, '')), array('id' => Field::ACCOUNT_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACCOUNT_USER . Field::ID, __('Controller_Profile_Accounts.add.imya_polzovatelya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACCOUNT_USER, HTML::chars(Arr::get($_POST, Field::ACCOUNT_USER, '')), array('id' => Field::ACCOUNT_USER . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACCOUNT_LOGIN . Field::ID, __('Controller_Profile_Accounts.add.login_polzovatelya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACCOUNT_LOGIN, HTML::chars(Arr::get($_POST, Field::ACCOUNT_LOGIN, '')), array('id' => Field::ACCOUNT_LOGIN . Field::ID)); ?></td>
        <td><button id="check_id" class="btn btn-mini" type="button" ><?php echo __('Controller_Profile_Accounts.add.proverit'); ?></button></td>
        <td id="result_id"></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACCOUNT_PASSWORD . Field::ID, __('Controller_Profile_Accounts.add.parol_polzovatelya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::ACCOUNT_PASSWORD, NULL, array('id' => Field::ACCOUNT_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACCOUNT_REPASSWORD . Field::ID, __('Controller_Profile_Accounts.add.podtverzhdenie_parolya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::ACCOUNT_REPASSWORD, NULL, array('id' => Field::ACCOUNT_REPASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <label class="checkbox">
                <?php echo Form::checkbox(Field::ACCOUNT_ACTIVATE, NULL, !empty($_POST) ? isset($_POST[Field::ACCOUNT_ACTIVATE]) : TRUE, array('id' => Field::ACCOUNT_ACTIVATE . Field::ID)); ?>
                <?php echo __('Controller_Profile_Accounts.add.sdelat_akkaunt_aktivnym'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Accounts.add.dobavit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END ACCOUNTS ADD FORM-->
