<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE LOG-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Profile.log.data_sobytiya'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.log.ip_adres'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.log.tip_sobytiya'); ?></th>
    </tr>
    <?php foreach ($user_log_history as $user_log): ?>
        <tr>
            <td><?php echo $user_log[Model_Users::LOG_DATE_TZ_FORMAT]; ?></td>
            <td><?php echo $user_log[Model_Users::LOG_IP]; ?></td>
            <td><?php echo $user_log[Model_Users::LOG_TYPE] == Model_Users::ENUM_LOG_LOGIN ? __('Controller_Profile_Profile.log.vxod') : __('Controller_Profile_Profile.log.vyxod'); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END PROFILE LOG-->
