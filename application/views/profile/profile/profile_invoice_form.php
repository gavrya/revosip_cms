<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE INVOICE FORM-->
<?php echo Form::open(); ?>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::REQUEST_CREDIT . Field::ID, __('Controller_Profile_Profile.invoice.summa_popolneniya'), array('class' => 'control-label pull-right')); ?></td>
        <td>
            <div class="input-prepend input-append">
                <span class="add-on">$</span>
                <?php echo Form::input(Field::REQUEST_CREDIT, HTML::chars(Arr::get($_POST, Field::REQUEST_CREDIT, '')), array('class' => 'span9', 'id' => Field::REQUEST_CREDIT . Field::ID)); ?>
                <span class="add-on">.00</span>
            </div>
        </td>
        <td>(<?php echo __('Controller_Profile_Profile.invoice.minimum') . ' $' . $credit_min_limit; ?>)</td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::WEBMONEY_UNIT . Field::ID, __('Controller_Profile_Profile.invoice.tip_valyuty_webmoney'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::WEBMONEY_UNIT, $invoice_units, HTML::chars(Arr::get($_POST, Field::WEBMONEY_UNIT, Model_Webmoney_Invoice::ENUM_UNIT_WMZ)), array('id' => Field::WEBMONEY_UNIT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Profile.invoice.prodolzhit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END PROFILE INVOICE FORM-->
