<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE INVOICES-->
<table class="table table-bordered table-hover" >
    <tr>
        <th><?php echo __('Controller_Profile_Profile.invoices.schet'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.invoices.tip_valyuty'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.invoices.data_sozdaniya'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.invoices.sostoyanie'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.invoices.summa_popolneniya'); ?></th>
        <th><?php echo __('Controller_Profile_Profile.invoices.data_proplaty'); ?></th>
    </tr>
    <?php foreach ($invoices as $invoice): ?>
        <tr class="<?php
        if ($invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_FAILURE) {
            echo 'error';
        } else if ($invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_NOTIFY || $invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_SUCCESS) {
            echo 'success';
        } else {
            echo 'warning';
        }
        ?>">
            <td><?php echo $invoice[Model_Webmoney_Invoice::INVOICE_ID]; ?></td>
            <td><?php echo $invoice[Model_Webmoney_Invoice::INVOICE_UNIT]; ?></td>
            <td><?php echo $invoice[Model_Webmoney_Invoice::INVOICE_DATE_TZ_FORMAT]; ?></td>
            <td>
                <?php
                if ($invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_FAILURE) {
                    echo __('Controller_Profile_Profile.invoices.otmenen');
                } else if ($invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_NOTIFY || $invoice[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_SUCCESS) {
                    echo __('Controller_Profile_Profile.invoices.proplachen');
                } else {
                    echo __('Controller_Profile_Profile.invoices.ozhidaet_proplaty');
                }
                ?>
            </td>
            <td><?php echo number_format($invoice[Model_Webmoney_Invoice::INVOICE_REQUESTED_AMOUNT], 2, '.', ''); ?></td>
            <td><?php echo!is_null($invoice[Model_Webmoney_Invoice::INVOICE_PAYMENT_DATE_TZ_FORMAT]) ? $invoice[Model_Webmoney_Invoice::INVOICE_PAYMENT_DATE_TZ_FORMAT] : ''; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END PROFILE INVOICES-->
