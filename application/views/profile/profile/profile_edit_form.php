<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE EDIT FORM-->
<?php echo Form::open(); ?>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::USER_NAME . Field::ID, __('Controller_Profile_Profile.edit.polnoe_imya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_NAME, HTML::chars(Arr::get($_POST, Field::USER_NAME, $user_data[Model_Users::USER_NAME])), array('id' => Field::USER_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_LOGIN . Field::ID, __('Controller_Profile_Profile.edit.login'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_LOGIN, HTML::chars(Arr::get($_POST, Field::USER_LOGIN, $user_data[Model_Users::USER_LOGIN])), array('id' => Field::USER_LOGIN . Field::ID, 'disabled' => 'disabled')); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_PASSWORD . Field::ID, __('Controller_Profile_Profile.edit.parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::USER_PASSWORD, NULL, array('id' => Field::USER_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_REPASSWORD . Field::ID, __('Controller_Profile_Profile.edit.podtverzhdenie_parolya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::USER_REPASSWORD, NULL, array('id' => Field::USER_REPASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_EMAIL . Field::ID, __('Controller_Profile_Profile.edit.email'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_EMAIL, HTML::chars($user_data[Model_Users::USER_EMAIL]), array('id' => Field::USER_EMAIL . Field::ID, 'disabled' => 'disabled')); ?></td>
        <td><?php echo HTML::anchor('profile/email', __('Controller_Profile_Profile.edit.smenit_email_adres')); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_LANGUAGE . Field::ID, __('Controller_Profile_Profile.edit.yazyk'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::USER_LANGUAGE, $registration_languages, HTML::chars(Arr::get($_POST, Field::USER_LANGUAGE, $user_data[Model_Users::USER_LANG])), array('id' => Field::USER_LANGUAGE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::USER_TIMEZONE . Field::ID, __('Controller_Profile_Profile.edit.vremennaya_zona'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::USER_TIMEZONE, $registration_timezones, HTML::chars(Arr::get($_POST, Field::USER_TIMEZONE, $user_data[Model_Users::USER_TIMEZONE])), array('id' => Field::USER_TIMEZONE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Profile.edit.soxranit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END PROFILE EDIT FORM-->
