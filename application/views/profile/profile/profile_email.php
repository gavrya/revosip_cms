<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE EMAIL FORM-->
<?php echo Form::open(); ?>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::USER_EMAIL . Field::ID, __('Controller_Profile_Profile.email.novyj_email_adres'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::USER_EMAIL, HTML::chars(Arr::get($_POST, Field::USER_EMAIL, '')), array('id' => Field::USER_EMAIL . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Profile.email.izmenit'), array('class' => 'btn')) ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END PROFILE EMAIL FORM-->
