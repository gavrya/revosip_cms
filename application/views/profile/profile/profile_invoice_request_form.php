<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START PROFILE INVOICE REQUEST FORM-->
<?php echo Form::open('https://merchant.webmoney.ru/lmi/payment.asp'); ?>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo __('Controller_Profile_Profile.invoice.zhelaemaya_summa_popolneniya'); ?></td>
        <td>$<?php echo number_format($request_credit, 2, '.', ''); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Profile.invoice.neobxodimaya_summa_dlya_proplaty'); ?></td>
        <td><?php echo number_format($invoice_amount, 2, '.', '') . ' ' . $invoice_unit; ?> </td>
    </tr>
    <tr>
        <td>
            <?php
            echo Form::hidden(Field::LMI_PAYEE_PURSE, $invoice_purse);
            echo Form::hidden(Field::LMI_PAYMENT_AMOUNT, number_format($invoice_amount, 2, '.', ''));
            echo Form::hidden(Field::LMI_PAYMENT_NO, $invoice_id);
            echo Form::hidden(Field::LMI_PAYMENT_DESC_BASE64, $invoice_desc);
            if ($sim_mode === TRUE) {
                echo Form::hidden(Field::LMI_SIM_MODE, '0');
            }
            if ($use_url === TRUE) {
                echo Form::hidden(Field::LMI_RESULT_URL, $result_url);
                echo Form::hidden(Field::LMI_SUCCESS_URL, $success_url);
                echo Form::hidden(Field::LMI_SUCCESS_METHOD, '2');
                echo Form::hidden(Field::LMI_FAIL_URL, $fail_url);
                echo Form::hidden(Field::LMI_FAIL_METHOD, '2');
            }
            echo Form::submit(Field::SUBMIT, __('Controller_Profile_Profile.invoice.proplatit'), array('class' => 'btn'));
            ?>
        </td>
        <td></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END PROFILE INVOICE REQUEST FORM-->
