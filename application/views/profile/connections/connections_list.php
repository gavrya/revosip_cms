<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONNECTIONS LIST-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Connections.list.profil_podklyucheniya'); ?></th>
        <th><?php echo __('Controller_Profile_Connections.list.data_dobavleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Connections.list.status'); ?></th>
    </tr>
    <?php foreach ($connection_profiles as $connection_profile): ?>
        <tr 
        <?php
        if (!$connection_profile[Model_Profile_Connection::CONNECTION_ACTIVE_FLAG]) {
            echo 'class="warning"';
        }
        ?>
            >
            <td><?php echo HTML::anchor('connection/' . $account_id . '/' . $connection_profile[Model_Profile_Connection::CONNECTION_ID] . '/info', HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_NAME]), array('title' => HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_DESCRIPTION]))); ?></td>
            <td><?php echo $connection_profile[Model_Profile_Connection::CONNECTION_CREATED_TZ_FORMAT]; ?></td>
            <td><?php echo HTML::anchor('connection/' . $account_id . '/' . $connection_profile[Model_Profile_Connection::CONNECTION_ID] . ($connection_profile[Model_Profile_Connection::CONNECTION_ACTIVE_FLAG] ? '/disactivate' : '/activate'), ($connection_profile[Model_Profile_Connection::CONNECTION_ACTIVE_FLAG] ? __('Controller_Profile_Connections.list.aktivnyj') : __('Controller_Profile_Connections.list.neaktivnyj'))); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END CONNECTIONS LIST-->
