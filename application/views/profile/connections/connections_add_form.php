<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONNECTIONS ADD-->
<?php echo Form::open(); ?>

<h5><?php echo __('Controller_Profile_Connection.edit.profil_podklyucheniya'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::CONNECTION_NAME . Field::ID, __('Controller_Profile_Connection.edit.nazvanie'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONNECTION_NAME, HTML::chars(Arr::get($_POST, Field::CONNECTION_NAME, '')), array('id' => Field::CONNECTION_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CONNECTION_DESCRIPTION . Field::ID, __('Controller_Profile_Connection.edit.kratkoe_opisanie'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONNECTION_DESCRIPTION, HTML::chars(Arr::get($_POST, Field::CONNECTION_DESCRIPTION, '')), array('id' => Field::CONNECTION_DESCRIPTION . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::CONNECTION_ACTIVE_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::CONNECTION_ACTIVE_FLAG]) : TRUE, array('id' => Field::CONNECTION_ACTIVE_FLAG . Field::ID)); ?>
                <?php echo __('Controller_Profile_Connection.edit.sdelat_aktivnym'); ?>
            </label>
        </td>   
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::CONNECTION_PASS_REQUIRED_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::CONNECTION_PASS_REQUIRED_FLAG]) : FALSE); ?>
                <?php echo __('Controller_Profile_Connection.edit.vvodit_parol_lokalno'); ?>
            </label>
        </td>
    </tr>
</table>

<hr /><h5><?php echo __('Controller_Profile_Connection.edit.nastrojki_sip_akkaunta'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::ACC_CALLER_ID . Field::ID, __('Controller_Profile_Connection.edit.otobrazhaemoe_imya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACC_CALLER_ID, HTML::chars(Arr::get($_POST, Field::ACC_CALLER_ID, '')), array('id' => Field::ACC_CALLER_ID . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACC_EXTENSION . Field::ID, __('Controller_Profile_Connection.edit.dobavochnyj_nomer'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACC_EXTENSION, HTML::chars(Arr::get($_POST, Field::ACC_EXTENSION, '')), array('id' => Field::ACC_EXTENSION . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACC_USER_NAME . Field::ID, __('Controller_Profile_Connection.edit.imya_polzovatelya'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::ACC_USER_NAME, HTML::chars(Arr::get($_POST, Field::ACC_USER_NAME, '')), array('id' => Field::ACC_USER_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACC_PASSWORD . Field::ID, __('Controller_Profile_Connection.edit.parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::ACC_PASSWORD, NULL, array('id' => Field::ACC_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::ACC_SECRET_PASSWORD . Field::ID, __('Controller_Profile_Connection.edit.sekretnyj_parol'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::password(Field::ACC_SECRET_PASSWORD, NULL, array('id' => Field::ACC_SECRET_PASSWORD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::ACC_SECURE_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::ACC_SECURE_FLAG]) : FALSE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_sekretnyj_parol'); ?>
            </label>
        </td>
    </tr>
</table>

<hr /><h5><?php echo __('Controller_Profile_Connection.edit.setevye_nastrojki'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::NETWORK_SIP_SERVER . Field::ID, __('Controller_Profile_Connection.edit.adres_sip_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_SIP_SERVER, HTML::chars(Arr::get($_POST, Field::NETWORK_SIP_SERVER, '')), array('id' => Field::NETWORK_SIP_SERVER . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_SIP_SERVER_PORT . Field::ID, __('Controller_Profile_Connection.edit.port_sip_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_SIP_SERVER_PORT, HTML::chars(Arr::get($_POST, Field::NETWORK_SIP_SERVER_PORT, 5060)), array('id' => Field::NETWORK_SIP_SERVER_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_PROXY_SERVER . Field::ID, __('Controller_Profile_Connection.edit.adres_sip_proksi_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_PROXY_SERVER, HTML::chars(Arr::get($_POST, Field::NETWORK_PROXY_SERVER, '')), array('id' => Field::NETWORK_PROXY_SERVER . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_PROXY_SERVER_PORT . Field::ID, __('Controller_Profile_Connection.edit.port_sip_proksi_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_PROXY_SERVER_PORT, HTML::chars(Arr::get($_POST, Field::NETWORK_PROXY_SERVER_PORT, 5060)), array('id' => Field::NETWORK_PROXY_SERVER_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_LOCAL_ADDRESS . Field::ID, __('Controller_Profile_Connection.edit.lokalnyj_ip_adres'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_LOCAL_ADDRESS, HTML::chars(Arr::get($_POST, Field::NETWORK_LOCAL_ADDRESS, '')), array('id' => Field::NETWORK_LOCAL_ADDRESS . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_LOCAL_PORT . Field::ID, __('Controller_Profile_Connection.edit.lokalnyj_port'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_LOCAL_PORT, HTML::chars(Arr::get($_POST, Field::NETWORK_LOCAL_PORT, 5060)), array('id' => Field::NETWORK_LOCAL_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_MIN_RTP_PORT . Field::ID, __('Controller_Profile_Connection.edit.minimalnyj_rtp_port'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_MIN_RTP_PORT, HTML::chars(Arr::get($_POST, Field::NETWORK_MIN_RTP_PORT, 2000)), array('id' => Field::NETWORK_MIN_RTP_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_MAX_RTP_PORT . Field::ID, __('Controller_Profile_Connection.edit.maksimalnyj_rtp_port'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_MAX_RTP_PORT, HTML::chars(Arr::get($_POST, Field::NETWORK_MAX_RTP_PORT, 50000)), array('id' => Field::NETWORK_MAX_RTP_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NETWORK_REGISTER_EXPIRES . Field::ID, __('Controller_Profile_Connection.edit.tajmer_registracii'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NETWORK_REGISTER_EXPIRES, HTML::chars(Arr::get($_POST, Field::NETWORK_REGISTER_EXPIRES, 1800)), array('id' => Field::NETWORK_REGISTER_EXPIRES . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NETWORK_ANY_LOCAL_PORT_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NETWORK_ANY_LOCAL_PORT_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_sluchajnyj_lokalnyj_port'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NETWORK_DEFAULT_INTERFACE_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NETWORK_DEFAULT_INTERFACE_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.opredelyat_lokalnyj_adres_avtomaticheski'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NETWORK_PROXY_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NETWORK_PROXY_FLAG]) : FALSE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_proksi_server'); ?>
            </label>
        </td>
    </tr>
</table>

<hr /><h5><?php echo __('Controller_Profile_Connection.edit.nastrojki_parametrov_nat'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::NAT_KEEP_ALIVE . Field::ID, __('Controller_Profile_Connection.edit.tajmer_keepalive'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NAT_KEEP_ALIVE, HTML::chars(Arr::get($_POST, Field::NAT_KEEP_ALIVE, 30)), array('id' => Field::NAT_KEEP_ALIVE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NAT_STUN_SERVER . Field::ID, __('Controller_Profile_Connection.edit.adres_stun_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NAT_STUN_SERVER, HTML::chars(Arr::get($_POST, Field::NAT_STUN_SERVER, '')), array('id' => Field::NAT_STUN_SERVER . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::NAT_STUN_SERVER_PORT . Field::ID, __('Controller_Profile_Connection.edit.port_stun_servera'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::NAT_STUN_SERVER_PORT, HTML::chars(Arr::get($_POST, Field::NAT_STUN_SERVER_PORT, 3478)), array('id' => Field::NAT_STUN_SERVER_PORT . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NAT_STUN_LOOKUP_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NAT_STUN_LOOKUP_FLAG]) : FALSE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_stun_razvedku'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NAT_DEFAULT_STUN_SERVER_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NAT_DEFAULT_STUN_SERVER_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_stun_server_po_umolchaniyu'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NAT_KEEP_ALIVE_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NAT_KEEP_ALIVE_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_tajmer_keepalive'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NAT_SERVER_BIND_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NAT_SERVER_BIND_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_privyazku_k_sip_serveru'); ?>
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="checkbox">
                <?php echo Form::checkbox(Field::NAT_SYMMETRIC_RTP_FLAG, NULL, !empty($_POST) ? isset($_POST[Field::NAT_SYMMETRIC_RTP_FLAG]) : TRUE); ?>
                <?php echo __('Controller_Profile_Connection.edit.ispolzovat_simmetricheskij_rtp'); ?>
            </label>
        </td>
    </tr>
</table>

<hr /><h5><?php echo __('Controller_Profile_Connection.edit.nastrojki_prioritetov_kodekov'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <?php for ($i = 0; $i < 12; $i++): ?>
        <tr>
            <td><?php echo Form::label(Field::$CODEC_FIELDS[$i] . Field::ID, __('Controller_Profile_Connection.edit.kodek') . ' №' . ($i + 1) . ':', array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::select(Field::$CODEC_FIELDS[$i], $codec_types, HTML::chars(Arr::get($_POST, Field::$CODEC_FIELDS[$i], $i == 0 ? Model_Profile_Connection::ENUM_CODEC_ALAW : '')), array('id' => Field::$CODEC_FIELDS[$i] . Field::ID)); ?></td>
        </tr>
    <?php endfor; ?>
</table>

<hr /><h5><?php echo __('Controller_Profile_Connection.edit.nastrojki_parametrov_dtmf'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::DTMF_METHOD . Field::ID, __('Controller_Profile_Connection.edit.predpochitaemyj_dtmf_metod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::select(Field::DTMF_METHOD, $dtmf_methods, HTML::chars(Arr::get($_POST, Field::DTMF_METHOD, Model_Profile_Connection::ENUM_DTMF_SIP_INFO)), array('id' => Field::DTMF_METHOD . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::DTMF_DURATION . Field::ID, __('Controller_Profile_Connection.edit.dlitelnost_dtmf_signala'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::DTMF_DURATION, HTML::chars(Arr::get($_POST, Field::DTMF_DURATION, 250)), array('id' => Field::DTMF_DURATION . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::DTMF_PAYLOAD . Field::ID, 'DTMF payload:', array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::DTMF_PAYLOAD, HTML::chars(Arr::get($_POST, Field::DTMF_PAYLOAD, 101)), array('id' => Field::DTMF_PAYLOAD . Field::ID)); ?></td>
    </tr>
</table>
<hr />
<div class="row-fluid">
    <div class="span4 offset4">
        <?php echo Form::hidden(Field::CSRF, Security::token()); ?>

        <?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Connections.add.dobavit'), array('class' => 'btn')); ?>
    </div>
</div>
<?php echo Form::close(); ?>

<!--END CONNECTIONS ADD-->
