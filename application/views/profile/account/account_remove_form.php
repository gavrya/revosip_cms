<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT REMOVE-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td colspan="2"><?php echo __('Controller_Profile_Account.remove.podtverdite_udalenie_akkaunta'); ?> <strong><?php echo HTML::chars($account_profile[Model_Profile_Account::ACCOUNT_NAME]); ?></strong></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2"><pre><?php echo $captcha ?></pre></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Account.remove.udalit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END ACCOUNT REMOVE-->
