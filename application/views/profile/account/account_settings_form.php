<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT SETTINGS FORM-->
<?php echo Form::open(); ?>

<h5><?php echo __('Controller_Profile_Account.settings.vybor_profilya_podklyucheniya_po_umolchaniyu'); ?></h5>
<table class="table table-hover">
    <tr>
        <td><?php echo Form::radio(Field::DEFAULT_CONNECTION_ID, 0, empty($settings_profile[Model_Profile_Settings::SETTINGS_DEFAULT_CONNECTION_ID])); ?></td>
        <td><?php echo __('Controller_Profile_Account.settings.otsutstvuet'); ?></td>
    </tr>

    <?php if (is_array($connection_profiles)) : ?>
        <?php foreach ($connection_profiles as $connection_profile): ?>
            <tr>
                <td><?php echo Form::radio(Field::DEFAULT_CONNECTION_ID, $connection_profile[Model_Profile_Connection::CONNECTION_ID], ($connection_profile[Model_Profile_Connection::CONNECTION_ID] == $settings_profile[Model_Profile_Settings::SETTINGS_DEFAULT_CONNECTION_ID])); ?></td>
                <td><?php echo HTML::anchor('/connection/' . $account_id . '/' . $connection_profile[Model_Profile_Connection::CONNECTION_ID] . '/info', HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_NAME]), array('title' => HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_DESCRIPTION]))); ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>

    <tr>
        <td><?php echo Form::checkbox(Field::DEFAULT_CONNECTION_FLAG, NULL, $settings_profile[Model_Profile_Settings::SETTINGS_DEFAULT_CONNECTION_FLAG] == TRUE); ?></td>
        <td><?php echo __('Controller_Profile_Account.settings.ispolzovat_tolko_vybrannyj_profil_podklyucheniya'); ?></td>
    </tr>
</table>
<?php echo Form::hidden(Field::CSRF, Security::token()); ?>

<?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Account.settings.soxranit'), array('class' => 'btn')); ?>

<?php echo Form::close(); ?>

<!--END ACCOUNT SETTINGS FORM-->
