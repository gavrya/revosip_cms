<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT PROLONGS-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Account.prolongs.data_prodleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Account.prolongs.summa_prodleniya'); ?></th>
        <th><?php echo __('Controller_Profile_Account.prolongs.kolichestvo_dnej_prodleniya'); ?></th>
    </tr>
    <?php foreach ($account_prolongs as $account_prolong): ?>
        <tr>
            <td><?php echo $account_prolong[Model_Profile_Account::PROLONG_DATE_TZ_FORMAT]; ?></td>
            <td>$<?php echo $account_prolong[Model_Profile_Account::PROLONG_CREDIT]; ?></td>
            <td><?php echo $account_prolong[Model_Profile_Account::PROLONG_DAYS]; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END ACCOUNT PROLONGS-->
