<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT INFO-->
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.nazvanie'); ?></td>
        <td><?php echo HTML::chars($account_profile[Model_Profile_Account::ACCOUNT_NAME]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.imya_polzovatelya'); ?></td>
        <td><?php echo HTML::chars($account_profile[Model_Profile_Account::ACCOUNT_USER]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.login_polzovatelya'); ?></td>
        <td><?php echo $account_profile[Model_Profile_Account::ACCOUNT_LOGIN]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.status'); ?></td>
        <td><?php echo ($account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == TRUE ? __('Controller_Profile_Account.info.aktivnyj') : __('Controller_Profile_Account.info.neaktivnyj')); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.data_sozdaniya'); ?></td>
        <td><?php echo $account_profile[Model_Profile_Account::ACCOUNT_CREATED_TZ_FORMAT]; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.kolichestvo_kontaktov'); ?></td>
        <td><?php echo $contact_profiles_count > 0 ? HTML::anchor('/contacts/' . $account_profile[Model_Profile_Account::ACCOUNT_ID] . '/list', $contact_profiles_count) : $contact_profiles_count; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.info.kolichestvo_profilej_podklyuchenij'); ?></td>
        <td><?php echo $connection_profiles_count > 0 ? HTML::anchor('/connections/' . $account_profile[Model_Profile_Account::ACCOUNT_ID] . '/list', $connection_profiles_count) : $connection_profiles_count; ?></td>
    </tr>
</table>
<!--END ACCOUNT INFO-->
