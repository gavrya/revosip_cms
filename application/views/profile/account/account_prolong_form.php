<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT PROLONG-->
<?php echo Form::open(); ?>

<h5><?php echo __('Controller_Profile_Account.prolong.obshhie_svedeniya'); ?></h5>
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Account.prolong.tekushhij_balans_profilya'); ?></td>
        <td>$<?php echo $user_data[Model_Users::USER_CREDIT] . ' (' . $credit_prolong_days_limit . ' ' . __('Controller_Profile_Account.prolong.dnya/dnej_prodleniya') . ')'; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.prolong.tekushhij_tarif'); ?></td>
        <td>$<?php echo $prolong_tariff . ' = ' . $prolong_days . ' ' . __('Controller_Profile_Account.prolong.dnya/dnej_prodleniya'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.prolong.srok_dejstviya_akkaunta'); ?></td>
        <td><?php echo $account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE ? __('Controller_Profile_Account.prolong.istek') : __('Controller_Profile_Account.prolong.do_daty') . ' ' . $account_profile[Model_Profile_Account::ACCOUNT_EXPIRES_TZ_FORMAT] . ' (' . __('Controller_Profile_Account.prolong.ostalos') . ' ' . $account_profile[Model_Profile_Account::ACCOUNT_EXPIRES_DAYS] . ' ' . __('Controller_Profile_Account.prolong.dnya/dnej') . ')'; ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.prolong.dostupno_k_prodleniyu'); ?></td>
        <td><?php echo $prolong_days_remain_limit . ' ' . __('Controller_Profile_Account.prolong.dnya/dnej_(bez_ucheta_balansa)'); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Account.prolong.min-maks_srok_prodleniya_akkaunta'); ?></td>
        <td><?php echo $prolong_days_min_limit . '-' . $prolong_days_max_limit . ' ' . __('Controller_Profile_Account.prolong.dnya/dnej_(bez_ucheta_balansa)'); ?></td>
    </tr>
</table>

<h5><?php echo __('Controller_Profile_Account.prolong.prodlenie'); ?></h5>
<table cellpadding="5" cellspacing="0">
    <?php if ($prolong_days_min == $prolong_days_max): ?>
        <tr>
            <td><?php echo Form::label(Field::PROLONG_DAYS . Field::ID, __('Controller_Profile_Account.prolong.zhelaemoe_kolichestvo_dnej_prodleniya') . ':', array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::PROLONG_DAYS, HTML::chars(Arr::get($_POST, Field::PROLONG_DAYS, $prolong_days_min)), array('id' => Field::PROLONG_DAYS . Field::ID, 'readonly' => 'readonly')); ?></td>
        </tr>
    <?php else: ?>
        <tr>
            <td><?php echo Form::label(Field::PROLONG_DAYS . Field::ID, __('Controller_Profile_Account.prolong.zhelaemoe_kolichestvo_dnej_prodleniya') . ' (' . __('Controller_Profile_Account.prolong.ot') . ' ' . $prolong_days_min . ' ' . __('Controller_Profile_Account.prolong.do') . ' ' . $prolong_days_max . '):', array('class' => 'control-label pull-right')); ?></td>
            <td><?php echo Form::input(Field::PROLONG_DAYS, HTML::chars(Arr::get($_POST, Field::PROLONG_DAYS, '')), array('id' => Field::PROLONG_DAYS . Field::ID)); ?></td>
        </tr>
    <?php endif; ?>

    <tr>
        <td><?php echo Form::label(Field::CAPTCHA . Field::ID, __('Controller.proverochnyj_kod'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CAPTCHA, '', array('id' => Field::CAPTCHA . Field::ID)); ?></td>
    </tr>
    <tr>
        <td colspan="2"><pre><?php echo $captcha ?></pre></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Account.prolong.prodlit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END ACCOUNT PROLONG-->
