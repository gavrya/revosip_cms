<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START ACCOUNT LOG-->
<table class="table table-bordered table-hover" >
    <tr> 
        <th><?php echo __('Controller_Profile_Account.log.data_sobytiya'); ?></th>
        <th><?php echo __('Controller_Profile_Account.log.ip_adres'); ?></th>
        <th><?php echo __('Controller_Profile_Account.log.tip_sobytiya'); ?></th>
    </tr>
    <?php foreach ($account_logs as $account_log): ?>
        <tr>
            <td><?php echo $account_log[Model_Profile_Account::LOG_DATE_TZ_FORMAT]; ?></td>
            <td><?php echo $account_log[Model_Profile_Account::LOG_IP]; ?></td>
            <td><?php echo $account_log[Model_Profile_Account::LOG_TYPE] == Model_Profile_Account::ENUM_LOG_LOGIN ? __('Controller_Profile_Account.log.vxod') : __('Controller_Profile_Account.log.vyxod'); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<!--END ACCOUNT LOG-->
