<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONTACT EDIT-->
<?php echo Form::open(); ?>

<table cellpadding="5" cellspacing="0">
    <tr>
        <td><?php echo Form::label(Field::CONTACT_NAME . Field::ID, __('Controller_Profile_Contact.edit.imya_kontakta'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONTACT_NAME, HTML::chars(Arr::get($_POST, Field::CONTACT_NAME, $contact_profile[Model_Profile_Contact::CONTACT_NAME])), array('id' => Field::CONTACT_NAME . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::label(Field::CONTACT_PHONE . Field::ID, __('Controller_Profile_Contact.edit.nomer_telefona'), array('class' => 'control-label pull-right')); ?></td>
        <td><?php echo Form::input(Field::CONTACT_PHONE, HTML::chars(Arr::get($_POST, Field::CONTACT_PHONE, $contact_profile[Model_Profile_Contact::CONTACT_PHONE])), array('id' => Field::CONTACT_PHONE . Field::ID)); ?></td>
    </tr>
    <tr>
        <td><?php echo Form::hidden(Field::CSRF, Security::token()); ?></td>
        <td><?php echo Form::submit(Field::SUBMIT, __('Controller_Profile_Contact.edit.soxranit'), array('class' => 'btn')); ?></td>
    </tr>
</table>
<?php echo Form::close(); ?>

<!--END CONTACT EDIT-->
