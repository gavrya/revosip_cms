<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!--START CONTACT INFO-->
<table class="table table-bordered table-hover" >
    <tr>
        <td><?php echo __('Controller_Profile_Contact.info.imya_kontakta'); ?></td>
        <td><?php echo HTML::chars($contact_profile[Model_Profile_Contact::CONTACT_NAME]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Contact.info.nomer_telefona'); ?></td>
        <td><?php echo HTML::chars($contact_profile[Model_Profile_Contact::CONTACT_PHONE]); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Controller_Profile_Contact.info.data_dobavleniya_kontakta'); ?></td>
        <td><?php echo $contact_profile[Model_Profile_Contact::CONTACT_CREATED_TZ_FORMAT]; ?></td>
    </tr>
</table>
<!--END CONTACT INFO-->
