<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'default' => array(
        /**
         * The following options must be set:
         *
         * string   key     secret passphrase
         * integer  mode    encryption mode, one of MCRYPT_MODE_*
         * integer  cipher  encryption cipher, one of the Mcrpyt cipher constants
         */
        'key' => 'KqYXtZ1X9v4g2tU2a8232toDV3zBs9T7',
        'cipher' => MCRYPT_RIJNDAEL_128,
        'mode' => MCRYPT_MODE_NOFB,
    ),
    'AES' => array(
        /**
         * The following options must be set:
         *
         * string   key     secret passphrase
         * integer  mode    encryption mode, one of MCRYPT_MODE_*
         * integer  cipher  encryption cipher, one of the Mcrpyt cipher constants
         */
        'key' => 'E9Oqk5TotF5s305X',
        'cipher' => MCRYPT_RIJNDAEL_128,
        'mode' => MCRYPT_MODE_ECB,
    ),
);
