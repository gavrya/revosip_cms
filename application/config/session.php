<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'native' => array(
        // name of the session
        'name' => 'native_session', //  default 'session'
        // number of seconds the session should live for
        'lifetime' => 2629744, // default 0 (The value 0 means "until the browser is closed.")
    ),
    'cookie' => array(
        // name of the cookie used to store the session data
        'name' => 'cookie_session', //  default 'session'
        // encrypt the session data using Encrypt?
        'encrypted' => TRUE, //  default FALSE
        // number of seconds the session should live for
        'lifetime' => 2629744, //  ONE MONTH 2629744
    ),
);