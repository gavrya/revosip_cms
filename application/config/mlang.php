<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Mlang module config file
 */
/*
 * List of available languages
 */
return array(
    'default' => 'ru',
    'cookie' => 'lang',
    'languages' => array(
        'en' => array(
            'i18n' => 'en',
            'locale' => array('en_US.UTF-8'),
            'label' => 'english',
        ),
        'ru' => array(
            'i18n' => 'ru',
            'locale' => array('ru_RU.UTF-8'),
            'label' => 'russian',
        ),
        'ua' => array(
            'i18n' => 'ua',
            'locale' => array('uk_UA.UTF-8'),
            'label' => 'ukrainian',
        ),
    ),
);
