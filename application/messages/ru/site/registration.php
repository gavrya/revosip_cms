<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::USER_LOGIN => array(
        'not_empty' => 'Введите логин',
        'min_length' => 'Длина логина должна быть от 4 символов',
        'max_length' => 'Длина логина не должна превышать 50 символов',
        'Model_Users::is_user_login_available' => 'Указанный логин пользователя недоступен для выбора',
        'regex' => 'Логин содержит недопустимые символы',
        'default' => '',
    ),
    Field::USER_PASSWORD => array(
        'not_empty' => 'Введите пароль',
        'min_length' => 'Длина пароля должна быть от 5 символов',
        'max_length' => 'Длина пароля не должна превышать 50 символов',
        'default' => '',
    ),
    Field::USER_REPASSWORD => array(
        'default' => 'Пароли не совпадают',
    ),
    Field::USER_NAME => array(// 'regex' => 'Имя содержит недопустимые символы',
        'not_empty' => 'Введите полное имя',
        'max_length' => 'Длина имени не должна превышать 50 символов',
        'default' => '',
    ),
    Field::USER_EMAIL => array(
        'not_empty' => 'Введите email адрес',
        'email' => 'Email адрес не соответствует общепринятому формату',
        'Model_Users::is_user_email_available' => 'Указанный email адрес недоступен для выбора',
        'default' => '',
    ),
    Field::USER_LANGUAGE => array(
        'not_empty' => 'Выберите предпочитаемый язык',
        'in_array' => 'Выбранный язык не поддерживается',
        'default' => '',
    ),
    Field::USER_TIMEZONE => array(
        'not_empty' => 'Выберите вашу временную зону',
        'in_array' => 'Выбрана неизвестная временная зона',
        'default' => '',
    ),
    Field::USER_ACCEPT => array(
        'not_empty' => 'Для выполнения регистрации необходимо принять условия пользовательского соглашения',
        'default' => '',
    ),
    'registration_timezones' => array(
        Model_Users::ENUM_TIMEZONE_M_12 => '(UTC - 12:00) Острова Бейкер, Хауленд',
        Model_Users::ENUM_TIMEZONE_M_11 => '(UTC - 11:00) Американское Самоа, Мидуэй, Ниуэ',
        Model_Users::ENUM_TIMEZONE_M_10 => '(UTC - 10:00) Гавайи',
        Model_Users::ENUM_TIMEZONE_M_9 => '(UTC - 09:00) Аляска',
        Model_Users::ENUM_TIMEZONE_M_8 => '(UTC - 08:00) Североамериканское и тихоокеанское время (США и Канада)',
        Model_Users::ENUM_TIMEZONE_M_7 => '(UTC - 07:00) Горное время (США и Канада), Мексика (Чиуауа, Ла-Пас, Мацатлан)',
        Model_Users::ENUM_TIMEZONE_M_6 => '(UTC - 06:00) Центральное время (США и Канада), Центральноамериканское время, Мексика (Гвадалахара, Мехико, Монтеррей)',
        Model_Users::ENUM_TIMEZONE_M_5 => '(UTC - 05:00) Североамериканское восточное время (США и Канада), Южноамериканское тихоокеанское время (Богота, Лима, Кито)',
        Model_Users::ENUM_TIMEZONE_M_4 => '(UTC - 04:00) Атлантическое время (Канада), Южноамериканское тихоокеанское время, Ла-Пас, Сантьяго)',
        Model_Users::ENUM_TIMEZONE_M_3_3 => '(UTC - 03:30) Ньюфаундленд',
        Model_Users::ENUM_TIMEZONE_M_3 => '(UTC - 03:00) Южноамериканское восточное время (Аргентина, Бразилия, Буэнос-Айрес, Джорджтаун), Гренландия',
        Model_Users::ENUM_TIMEZONE_M_2 => '(UTC - 02:00) Среднеатлантическое время',
        Model_Users::ENUM_TIMEZONE_M_1 => '(UTC - 01:00) Азорские острова, Кабо-Верде',
        Model_Users::ENUM_TIMEZONE_P_0 => '(UTC + 00:00) Западноевропейское время (Дублин, Эдинбург, Лиссабон, Лондон, Касабланка, Монровия)',
        Model_Users::ENUM_TIMEZONE_P_1 => '(UTC + 01:00) Центральноевропейское время (Амстердам, Берлин, Брюссель, Копенгаген, Мадрид, Париж, Рим, Стокгольм, Будапешт, Варшава, Прага)',
        Model_Users::ENUM_TIMEZONE_P_2 => '(UTC + 02:00) Восточноевропейское время (Киев, Афины, Бухарест, Вильнюс, Кишинёв, Рига, София, Таллин, Тирасполь, Хельсинки), Египет, Израиль, Ливан, Ливия, Турция',
        Model_Users::ENUM_TIMEZONE_P_3 => '(UTC + 03:00) Восточноафриканское время, Ирак, Кувейт, Саудовская Аравия',
        Model_Users::ENUM_TIMEZONE_P_3_3 => '(UTC + 03:30) Тегеранское время',
        Model_Users::ENUM_TIMEZONE_P_4 => '(UTC + 04:00) Московское время, Самара, ОАЭ, Оман, Азербайджан, Армения, Грузия',
        Model_Users::ENUM_TIMEZONE_P_4_3 => '(UTC + 04:30) Афганистан',
        Model_Users::ENUM_TIMEZONE_P_5 => '(UTC + 05:00) Екатеринбург, Западноазиатское время (Исламабад, Карачи, Узбекистан)',
        Model_Users::ENUM_TIMEZONE_P_5_3 => '(UTC + 05:30) Индия, Шри-Ланка',
        Model_Users::ENUM_TIMEZONE_P_6 => '(UTC + 06:00) Омское время, Новосибирск, Кемерово, Центральноазиатское время (Бангладеш, Казахстан, Киргизия)',
        Model_Users::ENUM_TIMEZONE_P_7 => '(UTC + 07:00) Красноярское время, Юго-Восточная Азия (Бангкок, Джакарта, Ханой)',
        Model_Users::ENUM_TIMEZONE_P_8 => '(UTC + 08:00) Иркутское время, Улан-Батор, Куала-Лумпур, Гонконг, Китай, Сингапур, Тайвань, западноавстралийское время (Перт)',
        Model_Users::ENUM_TIMEZONE_P_9 => '(UTC + 09:00) Якутское время, Корея, Япония',
        Model_Users::ENUM_TIMEZONE_P_9_3 => '(UTC + 09:30) Центральноавстралийское время',
        Model_Users::ENUM_TIMEZONE_P_10 => '(UTC + 10:00) Владивостокское время, Восточноавстралийское время',
        Model_Users::ENUM_TIMEZONE_P_11 => '(UTC + 11:00) Магаданское время, Центрально-тихоокеанское время (Соломоновы Острова, Новая Каледония)',
        Model_Users::ENUM_TIMEZONE_P_12 => '(UTC + 12:00) Маршалловы Острова, Фиджи, Новая Зеландия',
    ),
    'registration_languages' => array(
        Model_Users::ENUM_LANGUAGE_RU => 'Русский',
        Model_Users::ENUM_LANGUAGE_UA => 'Украинский',
        Model_Users::ENUM_LANGUAGE_EN => 'Английский',
    ),
);