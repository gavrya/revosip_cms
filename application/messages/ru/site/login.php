<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::USER_LOGIN => array(
        'not_empty' => 'Введите логин',
        'min_length' => 'Длина логина должна быть от 4 символов',
        'max_length' => 'Длина логина не должна превышать 50 символов',
        'regex' => 'Логин содержит недопустимые символы',
        'default' => '',
    ),
    Field::USER_PASSWORD => array(
        'not_empty' => 'Введите пароль',
        'min_length' => 'Длина пароля должна быть от 5 символов',
        'max_length' => 'Длина пароля не должна превышать 50 символов',
        'default' => '',
    ),
);