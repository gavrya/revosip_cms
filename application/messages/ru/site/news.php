<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // news mark
    Field::NEWS_MARK => array(
        'not_empty' => 'Укажите уровень важности новости',
        'in_array' => 'Указан неизвестный уровень важности новости',
        'default' => '',
    ),
    // news meta title
    Field::NEWS_TITLE . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введите оглавление новости (русская версия)',
        'max_length' => 'Длина meta оглавления новости не должна превышать 75 символов (русская версия)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введите оглавление новости (украинская версия)',
        'max_length' => 'Длина meta оглавления новости не должна превышать 75 символов (украинская версия)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введите оглавление новости (английская версия)',
        'max_length' => 'Длина meta оглавления новости не должна превышать 75 символов (английская версия)',
        'default' => '',
    ),
    // news meta keywords
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введите meta ключевые слова новости (русская версия)',
        'max_length' => 'Длина meta ключевых слов новости не должна превышать 150 символов (русская версия)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введите meta ключевые слова новости (украинская версия)',
        'max_length' => 'Длина meta ключевых слов новости не должна превышать 150 символов (украинская версия)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введите meta ключевые слова новости (английская версия)',
        'max_length' => 'Длина meta ключевых слов новости не должна превышать 150 символов (английская версия)',
        'default' => '',
    ),
    // news meta descriptions
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введите meta описания новости (русская версия)',
        'max_length' => 'Длина meta описания новости не должна превышать 150 символов (русская версия)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введите meta описания новости (украинская версия)',
        'max_length' => 'Длина meta описания новости не должна превышать 150 символов (украинская версия)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введите meta описания новости (английская версия)',
        'max_length' => 'Длина meta описания новости не должна превышать 150 символов (английская версия)',
        'default' => '',
    ),
    // news content
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введите содержимое новости (русская версия)',
        'max_length' => 'Длина содержимого новости превышает допустимое значение (русская версия)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введите содержимое новости (украинская версия)',
        'max_length' => 'Длина содержимого новости превышает допустимое значение (украинская версия)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введите содержимое новости (английская версия)',
        'max_length' => 'Длина содержимого новости превышает допустимое значение (английская версия)',
        'default' => '',
    ),
    'news_languages' => array(
        Model_News::ENUM_LANG_RU => 'Русская версия',
        Model_News::ENUM_LANG_UA => 'Украинская версия',
        Model_News::ENUM_LANG_EN => 'Английская версия',
    ),
    'news_marks' => array(
        Model_News::ENUM_MARK_NORMAL => 'Обычный',
        Model_News::ENUM_MARK_HIGH => 'Высокий',
        Model_News::ENUM_MARK_WARNING => 'Критический',
    ),
);
