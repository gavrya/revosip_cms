<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'Общие вопросы' => array(
        'Что такое Revosip?' => 'Revosip - это облачный java SIP клиент',
        'Как запустить java sip клиент Revosip?' => 'Для запуска java sip клиента Revosip необходимо скачать и установить виртуальную машину Java (JRE) по адресу <a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">http://www.oracle.com/technetwork/java/javase/downloads/index.html</a>. На современных ОС, таких как Linux Ubuntu и Mac OS X, виртульная машина Java уже должна быть установлена в системе',
    ),
);