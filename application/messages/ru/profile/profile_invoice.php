<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::REQUEST_CREDIT => array(
        'not_empty' => 'Введите желаемую сумму',
        'digit' => 'Значение суммы должно быть целым числом',
        'range' => 'Значение суммы вне допустимом диапазоне',
        'default' => '',
    ),
    Field::WEBMONEY_UNIT => array(
        'not_empty' => 'Выберите тип валюты Webmoney',
        'in_array' => 'Неизвестный тип валюты Webmoney',
        'default' => '',
    ),
    'invoice_units' => array(
        Model_Webmoney_Invoice::ENUM_UNIT_WMZ => 'WMZ (эквивалент доллара)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMR => 'WMR (эквивалент рубля)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMU => 'WMU (эквивалент гривны)',
    ),
);
