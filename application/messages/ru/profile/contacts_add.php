<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::CONTACT_NAME => array(
        'not_empty' => 'Введите имя контакта',
        'max_length' => 'Длина имени контакта не должна превышать 50 символов',
        'default' => '',
    ),
    Field::CONTACT_PHONE => array(
        'not_empty' => 'Введите номер телефона контакта',
        'max_length' => 'Длина номера телефона не должна превышать 50 символов',
        'regex' => 'Номер телефона должен содержать только цифры и может начинаться со знака +',
        'Model_Profile_Contact::is_contact_phone_available' => 'Контакт с таким номером уже существует в списке контактов',
        'default' => '',
    ),
);
