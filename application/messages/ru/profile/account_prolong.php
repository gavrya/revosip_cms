<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::PROLONG_DAYS => array(
        'not_empty' => 'Введите желаемое количество дней продления аккаунта',
        'digit' => 'Количество дней продления должно быть целым числом',
        'range' => 'Количество дней продления должно быть в допустимом диапазоне',
        'default' => '',
    ),
);
