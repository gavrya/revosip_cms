<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::ACCOUNT_NAME => array(
        'not_empty' => 'Введите желаемое название аккаунта',
        'max_length' => 'Длина названия аккаунта не должна превышать 50 символов',
        'Model_Profile_Account::is_account_name_available' => 'Указанное название аккаунта недоступно для выбора',
        'default' => '',
    ),
    Field::ACCOUNT_USER => array(
        'not_empty' => 'Введите имя пользователя',
        'max_length' => 'Длина имени пользователя не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACCOUNT_LOGIN => array(
        'not_empty' => 'Введите логин пользователя',
        'min_length' => 'Длина логина пользователя должна быть от 4 символов',
        'max_length' => 'Длина логина пользователя не должна превышать 50 символов',
        'regex' => 'Логин пользователя содержит недопустимые символы',
        'Model_Profile_Account::is_account_login_available' => 'Указанный логин пользователя недоступен для выбора',
        'default' => '',
    ),
    Field::ACCOUNT_PASSWORD => array(
        'not_empty' => 'Введите пароль пользователя',
        'min_length' => 'Длина пароля пользователя должна быть от 5 символов',
        'max_length' => 'Длина пароля пользователя не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACCOUNT_REPASSWORD => array(
        'default' => 'Пароли не совпадают',
    ),
);