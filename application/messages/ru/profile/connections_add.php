<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // connection validation rules
    Field::CONNECTION_NAME => array(
        'not_empty' => 'Введите название профиля подключения',
        'max_length' => 'Длина названия профиля подключения не должна превышать 50 символов',
        'Model_Profile_Connection::is_connection_name_available' => 'Указанное название профиля подключения недоступно для выбора',
        'default' => '',
    ),
    Field::CONNECTION_DESCRIPTION => array(
        'not_empty' => 'Введите краткое описание профиля подключения',
        'max_length' => 'Длина описания профиля подключения не должна превышать 100 символов',
        'default' => '',
    ),
    // account validation rules
    Field::ACC_CALLER_ID => array(
        'not_empty' => 'Введите отображаемое имя',
        'max_length' => 'Длина отображаемого имени не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACC_EXTENSION => array(
        'not_empty' => 'Введите добавочный номер',
        'max_length' => 'Длина добавочного номера не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACC_USER_NAME => array(
        'not_empty' => 'Введите имя пользователя',
        'max_length' => 'Длина имени пользователя не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACC_PASSWORD => array(
        'not_empty' => 'Введите пароль',
        'max_length' => 'Длина пароля не должна превышать 50 символов',
        'default' => '',
    ),
    Field::ACC_SECRET_PASSWORD => array(
        'not_empty' => 'Введите секретный пароль',
        'exact_length' => 'Длина секретного пароля должна быть 32 символа',
        'regex' => 'Формат секретного пароля должен соответствовать формату MD5 хэша',
        'default' => '',
    ),
    // network validation rules
    Field::NETWORK_SIP_SERVER => array(
        'not_empty' => 'Введите адрес SIP сервера',
        'max_length' => 'Длина адреса SIP сервера не должна превышать 50 символов',
        'default' => '',
    ),
    Field::NETWORK_SIP_SERVER_PORT => array(
        'not_empty' => 'Введите порт SIP сервера',
        'digit' => 'Порт SIP сервера должен быть целым числом от 1 до 65535',
        'range' => 'Порт SIP сервера должен быть целым числом от 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER => array(
        'not_empty' => 'Введите адрес SIP прокси сервера',
        'max_length' => 'Длина адреса SIP прокси сервера не должна превышать 50 символов',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER_PORT => array(
        'not_empty' => 'Введите порт SIP прокси сервера',
        'digit' => 'Порт SIP прокси сервера должен быть целым числом от 1 до 65535',
        'range' => 'Порт SIP прокси сервера должен быть целым числом от 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_ADDRESS => array(
        'not_empty' => 'Введите локальный адрес',
        'max_length' => 'Длина локального адреса не должна превышать 50 символов',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_PORT => array(
        'not_empty' => 'Введите локальный порт',
        'digit' => 'Локальный порт должен быть целым числом от 1 до 65535',
        'range' => 'Локальный порт должен быть целым числом от 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_MIN_RTP_PORT => array(
        'not_empty' => 'Введите минимальный RTP порт',
        'digit' => 'Минимальный RTP порт должен быть целым числом от 1 до 65500',
        'range' => 'Минимальный RTP порт должен быть целым числом от 1 до 65500',
        'default' => '',
    ),
    Field::NETWORK_MAX_RTP_PORT => array(
        'not_empty' => 'Введите максимальный RTP порт',
        'digit' => 'Максимальный RTP порт должен быть целым числом от 100 до 65500',
        'range' => 'Максимальный RTP порт должен быть целым числом от 100 до 65500',
        'default' => '',
    ),
    Field::NETWORK_REGISTER_EXPIRES => array(
        'not_empty' => 'Введите таймер регистрации',
        'digit' => 'Таймер регистрации должен быть целым числом от 30 до 7200',
        'range' => 'Таймер регистрации должен быть целым числом от 30 до 7200',
        'default' => '',
    ),
    // NAT validation rules
    Field::NAT_KEEP_ALIVE => array(
        'not_empty' => 'Введите длительность таймера Keep-Alive',
        'digit' => 'Таймер Keep-Alive должен быть целым числом от 30 до 300',
        'range' => 'Таймер Keep-Alive должен быть целым числом от 30 до 300',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER => array(
        'not_empty' => 'Введите адрес STUN сервера',
        'max_length' => 'Длина адреса STUN сервера не должна превышать 50 символов',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER_PORT => array(
        'not_empty' => 'Введите порт STUN сервера',
        'digit' => 'Порт STUN сервера должен быть целым числом от 1 до 65535',
        'range' => 'Порт STUN сервера должен быть целым числом от 1 до 65535',
        'default' => '',
    ),
    // DTMF validation rules
    Field::DTMF_METHOD => array(
        'not_empty' => 'Укажите предпочитаемый DTMF метод',
        'in_array' => 'Указан неизвестный DTMF метод',
        'default' => '',
    ),
    Field::DTMF_DURATION => array(
        'not_empty' => 'Введите длительность DTMF сигнала',
        'digit' => 'Длительность DTMF сигнала должна быть целым числом от 100 до 5000',
        'range' => 'Длительность DTMF сигнала должна быть целым числом от 100 до 5000',
        'default' => '',
    ),
    Field::DTMF_PAYLOAD => array(
        'not_empty' => 'Введите DTMF payload',
        'digit' => 'DTMF payload должен быть целым числом от 95 до 128',
        'range' => 'DTMF payload должен быть целым числом от 95 до 128',
        'default' => '',
    ),
    // codec validation rules
    Field::CODEC_1 => array(
        'not_empty' => 'Введите кодек №1',
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_2 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_3 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_4 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_5 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_6 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_7 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_8 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_9 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_10 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_11 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    Field::CODEC_12 => array(
        'in_array' => 'Указан неизвестный кодек',
        'default' => '',
    ),
    'dtmf_methods' => array(
        Model_Profile_Connection::ENUM_DTMF_SIP_INFO => 'SIP INFO',
        Model_Profile_Connection::ENUM_DTMF_RFC2833 => 'RFC2833',
    ),
    'codec_types' => array(
        '' => 'Не указан',
        Model_Profile_Connection::ENUM_CODEC_ALAW => 'ALAW',
        Model_Profile_Connection::ENUM_CODEC_ULAW => 'ULAW',
        Model_Profile_Connection::ENUM_CODEC_GSM => 'GSM',
        Model_Profile_Connection::ENUM_CODEC_iLBC => 'iLBC',
        Model_Profile_Connection::ENUM_CODEC_G729A => 'G729A',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_NB => 'SPEEX NB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_WB => 'SPEEX WB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_UWB => 'SPEEX UWB',
        Model_Profile_Connection::ENUM_CODEC_SILK_NB => 'SILK NB',
        Model_Profile_Connection::ENUM_CODEC_SILK_MB => 'SILK MB',
        Model_Profile_Connection::ENUM_CODEC_SILK_WB => 'SILK WB',
        Model_Profile_Connection::ENUM_CODEC_SILK_SWB => 'SILK SWB',
    ),
);
