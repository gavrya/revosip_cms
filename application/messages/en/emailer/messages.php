<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'registration_confirm' => array(
        'html_flag' => FALSE,
        'subject' => 'Registration confirmation - :site_title',
        'message' => "
Hello, :user_name.

Your email address was indicated during the registration.
The procedure of user registration confirmation is available for 24 hours.
In order to confirm your registration, please use the following link:

:confirm_link
",
    ),
    'recovery_confirm' => array(
        'html_flag' => FALSE,
        'subject' => 'Password recovery - :site_title',
        'message' => "
Hello, :user_name.

You requested password recovery.
The procedure of password recovery is available for 2 hours.
In order to confirm the password recovery procedure, please use the following link:

:confirm_link
",
    ),
    'email_update' => array(
        'html_flag' => FALSE,
        'subject' => 'Changing email address - :site_title',
        'message' => "
Hello, :user_name.

You want to change the email address of your profile.
The procedure of profile email address changing is available for 2 hours.
In order to continue the profile email address changing procedure, please use the following link:

:confirm_link
",
    ),
    'email_confirm' => array(
        'html_flag' => FALSE,
        'subject' => 'Confirmation of email address changing - :site_title',
        'message' => "
Hello, :user_name.

You want to change the email address of your profile.
The procedure of profile email address changing is available for 2 hours.
In order to confirm the profile email address changing, please use the following link:

:confirm_link
",
    ),
    'invoice_payment' => array(
        'html_flag' => FALSE,
        'subject' => 'Balance - :site_title',
        'message' => "
Hello, :user_name.

Your profile balance has been refilled successfully.
In order to view your balance details, please use the following link:

:balance_link

In order to view your payment history, please use the following link:

:invoices_link
",
    ),
    'message_footer' => "
This message was created automatically, please do not respond it.
If you received this message by mistake, please delete it.
Best regards, service administration \":site_title\".
",
);