<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'benefits' => array(
        'Supports SIP Protocol' => 'Supports the most popular IP telephony protocol',
        'Cross-platform' => 'The ability to run and work on all popular desktop operating systems including Windows, Linux, Mac',
        'Cloud architecture' => 'Allows you to store application settings, contacts and IP ATC connection profiles centrally',
        'Calls from your browser' => 'Softphone can run as a web application directly from a web browser',
        'Data safety' => 'Secure access and secure data transfer using the AES encryption algorithm',
        'Support popular codecs' => 'Supports more than 10 types of codecs',
        'Modern interface' => 'Intuitive interface with the ability to change the design (skins)',
        'Multilingual' => 'Operating in Russian, Ukrainian and English languages',
        'NAT optimization' => 'Optimized to work correctly with NAT',
        'Mobility' => 'Possibility to use the application on any PC with Internet access, in any part of the world',
    ),
    'comparison' => array(
        array('Cost', 'Low', 'High'),
        array('Chance of breakdown', 'No', 'Yes'),
        array('Placement', 'Does not require additional space', 'Requires additional space on the table'),
        array('Interface', 'Intuitive and easy to use interface', 'Often awkward interface'),
        array('Mobility', 'The ability to run from a cloud on any PC', 'No mobility'),
        array('"Hands Free"', 'Ability to work with ordinary headset', 'Requires a special headset'),
        array('Software update', 'Automatic update from a cloud', 'Requiring a specialist to update device software'),
        array('NAT optimization', 'Implemented', 'Is missing or not working'),
        array('Configuration and Maintenance', 'Settings loaded from the cloud after entering a user login and password', 'Requires a specialist to connect and configure the device at the workplace'),
        array('Additional power supply', 'No need', 'Needs an extra source of power supply'),
        array('Telephone wire', 'No', 'Yes'),
    ),
    'features' => array(
        'Standard features' => array(
            'Cross-platform',
            'Ability to work as a standalone application or as a web application from a web browser',
            'Unlimited number of independent lines',
            'Displays information about the call and its condition',
            'Cloud contact list',
            'Supports network statuses',
            'Mode of auto-answering to incoming calls',
            'The software headphones and microphone volume boost',
            'Headphones and microphone mute mode',
            '"Silent mode"',
            'Last number redial',
            'Auto-Dial',
            'Call hold',
            'Call cancellation',
            'Attended and unattended call transfer',
            'Calls history',
            'DTMF signals sending',
            'Changing the design of the interface (skins)',
            'Operating in Russian, Ukrainian and English languages',
        ),
        'Technical characteristics' => array(
            'VoIP protocol: SIP 2.0 (RFC3261)',
            'Transport protocols: SIP/UDP, RTP/RTCP',
            'Network protocol: IPv4',
            'NAT optimization: "rport" parameter, STUN lookup, symmetric RTP, sending SIP and RTP KeepAlive messages, binding to SIP server IP address',
            'Audio codecs: ALAW, ULAW, GSM, iLBC, G729A, SPEEX NB, SPEEX WB, SPEEX UWB, SILK NB, SILK MB, SILK WB, SILK SWB',
            'DTMF sending methods: SIP INFO, RFC2833',
            'SIP Outbound Proxy support',
            'Automatic detection and configuration of audio devices',
        ),
        'System requirements' => array(
            'Minimum 1 GHz processor',
            'Graphics card with DirectX support',
            'Minimum 1 Gb RAM',
            'Minimum 50 Mb free space on the hard drive',
            'Operation systems: Microsoft Windows, Mac OS X, Linux',
            'Full-duplex, 16-bit sound Card',
            'The Java Virtual Machine version 1.6 update 10 or above',
            'Web browser with Java Plug-in support: Google Chrome, Mozilla Firefox, Opera, Safari, Microsoft Internet Explorer',
            'Broadband Internet connection',
            'Headset',
        ),
    ),
);