<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'Basic questions' => array(
        'What is Revosip?' => 'Revosip - its a cloud java SIP client',
        'How to launch the Revosip java sip client?' => 'A Java Runtime Environment (JRE) or Java Development Kit (JDK) must be installed in order to run the Revosip sip client <a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">http://www.oracle.com/technetwork/java/javase/downloads/index.html</a>',
    ),
);