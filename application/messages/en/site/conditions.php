<?php

defined('SYSPATH') or die('No direct script access.');

return array(
"<h5>1. Basic Terms and Definitions</h5>

The Owner of the Revosip Project — a person responsible for the creation of the Project, its maintenance and providing the Service on its base.<br/>
The Project — an entity, including the website, the Program, the Users, the Owner, the Developers and the Administration.<br/>
The Developer — the persons who developed the program components of the Project.<br/>
The Program — the components of the software posted on the Website by the Administration.<br/>
The Website — a complex of the Web Resources posted in the Internet web on the domain revosip.com and its subdomains.<br/>
The Service - a complex of services and options provided by the Administration via the Project software.<br/>
The User — any person, registered as a user of the service, who entered into the present Agreement.<br/>
The Administration — persons entitled by the Owner of the Project to the special use of the Project, including management, and acting on behalf of the Owner of the Project.<br/>
The registration as a user of the service — creation of the User Account in the form of a number of nominal identification data of the User (Profile), by means of which the User accesses the website.<br/>

<h5>2. General Conditions</h5>

2.1 The Service is provided to the persons previously registered on the Website - the Users.<br/>
2.2 The present Agreement with all its appendices for short is hereinafter referred to as the Agreement.<br/>
2.3 The present Agreement can be amendend and supplemented by the Owner of the Project.<br/>
2.4 The Service is provided based on the principle of \"as is\", that is to say that the User donesn't get any guarantees that the Service and the options provided by the Service will function uninterruptedly, permanently, without errors, and will comply with the requirements and expectations of the User.<br/>
2.5 The User of the Project, the Developers and the Administration are not liable to the  User for any damage or harm inflicted to the User, including his computer, software or other property, due to use or inability to use of the Service.<br/>
2.6 The use of the options of the Service is realized for a free.<br/>

<h5>3. Rights and Duties of the Administration</h5>

3.1 The Administration takes all possible measures for providing the secure, qualitative and permanent service, the efficiency of the Service, functioning of the Website and release of new versions of the Programs.<br/>
3.2 The Administration reserves the right to restrict, in whole or in part, the functionality of the Project or the access to it  due to technical, technological, prophylactic or other reasons in their sole discretion with advance notice of the Users and without.<br/>
3.3 The Administration has the right, in the manner of collecting statistical data, to trace and save the information about the IP-addresses of the access of the User to the Website and to use the technical information files (cookies) put on the User's PC.<br/>

<h5>4. Rights and Duties of the User</h5>

4.1 The User agrees to abide by the present Agreement and to get familiarized with the current version of the Agreement in a timely manner.<br/>
4.2 In case of disagreement of the User with the present Agreement, the User must stop the participation in the Project, including uninstalling of the Program.<br/>
4.3 The User agrees to use strong passwords for the access to different components of the Project and to have a permanent access to his/her mailbox, indicated upon the registration on the Website.<br/>
4.3 The User agrees not to disclose and pass to third parties his/her identification data which are used for authorization of the User on the Website of the Project.<br/>
4.4 The User agrees not to examine the code, decompile or disassemble the Project, not to create derivative works based on the Project or its components.<br/>
4.5 The User has the right to act according to the logics of the functioning of the Website and of the Programs of the Project.<br/>
4.6 The User has the right to use feedback to get information about the functioning of different components of the Website and the Program.<br/>

<h5>5. The User is Prohibited to</h5>

5.1 Use the Project in a way which can interfere with its normal functioning.<br/>
5.2 Post in the Project the personal data of third parties without their consent, including home addresses, phone numbers, full names, passport data.<br/>
5.3 Post in the Project obscene or abusive words or phrases, including using them in the User name field.<br/>
5.4 Post in the Project malware or links, which may contain malware.<br/>
5.5 Post in the Project any other information, which in the opinion of the Administration is junk, violates the norms of morality and ethics or violates applicable Law.<br/>
5.6 Perform any other illegal or criminal acts.

<h5>6. Responsibility of the Parties</h5>

6.1 The User is solely responsible for the loss of access to his/her mailbox specified at registration,  leakage of personal information of third parties, posted on the website, and of his/her own password to access different parts of the Project.<br/>
6.2 The User shall be responsible to the Administration and third parties for failure to comply with the terms of the present Agreement.<br/>
6.3 The Administration is not liable for any conduct of Users or third parties, including, but not limited to, theft, destruction or unauthorized access to the Users' data.<br/>
6.4 The Administration and the Owner of the Project, as well as their representatives under no circumstances be held liable to the User of the Project or other third parties for any indirect, incidental, unintentional damage, including but not limited to, lost data, damage to the honor, dignity or business reputation, lost profits, arising from the use of the Project, its components and materials, access to which was obtained via the Project, even if the Administration does not point to the possibility of such damages.<br/>
6.5 The Administration is released from responsibility for the complete or partial failure to fulfill obligations under the present Agreement if such failure would be a consequence of force majeure,  that is extraordinary and unavoidable by the parties under the given conditions, including DDOS attacks, restraining acts of the authorities, natural disasters, fires, accidents, and also failures in the telecommunications and energy networks, malware activityrelated to the Project, unscrupulous actions of third parties towards the Project, aimed at tampering or disabling the software or hardware complex.",
);
