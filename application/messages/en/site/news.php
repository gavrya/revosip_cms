<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // news mark
    Field::NEWS_MARK => array(
        'not_empty' => 'Indicate news importance level',
        'in_array' => 'The indicated news importance level is unknown',
        'default' => '',
    ),
    // news meta title
    Field::NEWS_TITLE . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Type the title of the news (russian version)',
        'max_length' => 'News title must not have more than 75 symbols (russian version)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Type the title of the news (ukrainian version)',
        'max_length' => 'News title must not have more than 75 symbols (ukrainian version)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Type the title of the news (english version)',
        'max_length' => 'News title must not have more than 75 symbols (english version)',
        'default' => '',
    ),
    // news meta keywords
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Type the meta keywords of the news (russian version)',
        'max_length' => 'News meta keywords must not have more than 150 symbols (russian version)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Type the meta keywords of the news (ukrainian version)',
        'max_length' => 'News meta keywords must not have more than 150 symbols (ukrainian version)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Type the meta keywords of the news (english version)',
        'max_length' => 'News meta keywords must not have more than 150 symbols (english version)',
        'default' => '',
    ),
    // news meta descriptions
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Type the meta description of the news (russian version)',
        'max_length' => 'News meta description must not have more than 150 symbols (russian version)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Type the meta description of the news (ukrainian version)',
        'max_length' => 'News meta description must not have more than 150 symbols (ukrainian version)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Type the meta description of the news (english version)',
        'max_length' => 'News meta description must not have more than 150 symbols (english version)',
        'default' => '',
    ),
    // news content
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Type the news content (russian version)',
        'max_length' => 'News content length exceeds the limit (russian version)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Type the news content (ukrainian version)',
        'max_length' => 'News content length exceeds the limit (ukrainian version)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Type the news content (english version)',
        'max_length' => 'News content length exceeds the limit (english version)',
        'default' => '',
    ),
    'news_languages' => array(
        Model_News::ENUM_LANG_RU => 'Russian version',
        Model_News::ENUM_LANG_UA => 'Ukrainian version',
        Model_News::ENUM_LANG_EN => 'English version',
    ),
    'news_marks' => array(
        Model_News::ENUM_MARK_NORMAL => 'Normal',
        Model_News::ENUM_MARK_HIGH => 'High',
        Model_News::ENUM_MARK_WARNING => 'Critical',
    ),
);
