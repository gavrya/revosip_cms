<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::USER_LOGIN => array(
        'not_empty' => 'Type login',
        'min_length' => 'Login must have more than 4 symbols',
        'max_length' => 'Login must not have more than 50 symbols',
        'regex' => 'Login contains symbols which are not allowed',
        'default' => '',
    ),
    Field::USER_PASSWORD => array(
        'not_empty' => 'Type password',
        'min_length' => 'Password must have more than 5 symbols',
        'max_length' => 'Password must not have more than 50 symbols',
        'default' => '',
    ),
);