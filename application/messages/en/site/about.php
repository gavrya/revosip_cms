<?php

defined('SYSPATH') or die('No direct script access.');

return array(
'<strong>Revosip</strong> - cloud service of SIP softphone<br/>
    
<h5>A brief description</h5>

The <strong>Revosip</strong> SIP client - is a crossplatform VoIP application which can be used with a digital office IP PBX supporting SIP protocol and is designed for performing phone calls.<br/>
The <strong>Revosip</strong> SIP softphone can function both as an independent window application and as a web application running directly from a web browser.<br/>
The implementation of the softphone as a cloud service allows its users to store centrally the application settings, the contact list, connection profiles and to use the applictaion on any PC with the Internet access in any part of the world.<br/>

<h5>Principles of the operation of the service</h5>

After having registered on the website, a user can create and set a required number of accounts to which the <strong>Revosip</strong> softphone can be connected directly to start the operation.<br/>
After connecting to the account, the softphone downloads all necessary application settings, the contact list and connection profiles.<br/>
After it is connected to the IP PBX, the softphone is ready to perform phone calls using SIP protocol.<br/>',
);