<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::PROLONG_DAYS => array(
        'not_empty' => 'Type the number of days of account prolongation',
        'digit' => 'The number of days of prolongation must be integer',
        'range' => 'The number of days of prolongation must be within the permissible range',
        'default' => '',
    ),
);
