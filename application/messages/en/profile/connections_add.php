<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // connection validation rules
    Field::CONNECTION_NAME => array(
        'not_empty' => 'Type the connection profile name',
        'max_length' => 'Connection profile name must not have more than 50 symbols',
        'Model_Profile_Connection::is_connection_name_available' => 'The specified connection profile name is not allowed',
        'default' => '',
    ),
    Field::CONNECTION_DESCRIPTION => array(
        'not_empty' => 'Type a short description of connection profile',
        'max_length' => 'The description of connection profile must not have more than 100 symbols',
        'default' => '',
    ),
    // account validation rules
    Field::ACC_CALLER_ID => array(
        'not_empty' => 'Type display name',
        'max_length' => 'Display name must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACC_EXTENSION => array(
        'not_empty' => 'Type extension',
        'max_length' => 'Extension must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACC_USER_NAME => array(
        'not_empty' => 'Type username',
        'max_length' => 'Username must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACC_PASSWORD => array(
        'not_empty' => 'Type user password',
        'max_length' => 'User password must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACC_SECRET_PASSWORD => array(
        'not_empty' => 'Type secret password',
        'exact_length' => 'Secret password shoud have 32 symbols',
        'regex' => 'Secret password must be in MD5 hash format',
        'default' => '',
    ),
    // network validation rules
    Field::NETWORK_SIP_SERVER => array(
        'not_empty' => 'Type SIP server address',
        'max_length' => 'SIP server address must not have more than 50 symbols',
        'default' => '',
    ),
    Field::NETWORK_SIP_SERVER_PORT => array(
        'not_empty' => 'Type SIP server port',
        'digit' => 'SIP server port must be integer number ranging from 1 to 65535',
        'range' => 'SIP server port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER => array(
        'not_empty' => 'Type SIP proxy server address',
        'max_length' => 'SIP proxy server address must not have more than 50 symbols',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER_PORT => array(
        'not_empty' => 'Type SIP proxy server port',
        'digit' => 'SIP proxy server port must be integer number ranging from 1 to 65535',
        'range' => 'SIP proxy server port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_ADDRESS => array(
        'not_empty' => 'Type local address',
        'max_length' => 'Local address must not have more than 50 symbols',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_PORT => array(
        'not_empty' => 'Type local port',
        'digit' => 'Local port must be integer number ranging from 1 to 65535',
        'range' => 'Local port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    Field::NETWORK_MIN_RTP_PORT => array(
        'not_empty' => 'Type the minimum RTP port',
        'digit' => 'Minimum RTP port must be integer number ranging from 1 to 65535',
        'range' => 'Minimum RTP port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    Field::NETWORK_MAX_RTP_PORT => array(
        'not_empty' => 'Type the maximum RTP port',
        'digit' => 'Maximum RTP port must be integer number ranging from 1 to 65535',
        'range' => 'Maximum RTP port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    Field::NETWORK_REGISTER_EXPIRES => array(
        'not_empty' => 'Type registration expires timer',
        'digit' => 'Registration expires timer must be integer number ranging from 30 to 7200',
        'range' => 'Registration expires timer must be integer number ranging from 30 to 7200',
        'default' => '',
    ),
    // NAT validation rules
    Field::NAT_KEEP_ALIVE => array(
        'not_empty' => 'Type Keep-Alive timer duration',
        'digit' => 'Keep-Alive timer duration must be integer number ranging from 30 to 300',
        'range' => 'Keep-Alive timer duration must be integer number ranging from 30 to 300',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER => array(
        'not_empty' => 'Type STUN server address',
        'max_length' => 'STUN server address must not have more than 50 symbols',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER_PORT => array(
        'not_empty' => 'Type STUN server port',
        'digit' => 'STUN server port must be integer number ranging from 1 to 65535',
        'range' => 'STUN server port must be integer number ranging from 1 to 65535',
        'default' => '',
    ),
    // DTMF validation rules
    Field::DTMF_METHOD => array(
        'not_empty' => 'Select DTMF method',
        'in_array' => 'The selected DTMF method is unknown',
        'default' => '',
    ),
    Field::DTMF_DURATION => array(
        'not_empty' => 'Type DTMF signal duration',
        'digit' => 'DTMF signal duration must be integer number ranging from 100 to 5000',
        'range' => 'DTMF signal duration must be integer number ranging from 100 to 5000',
        'default' => '',
    ),
    Field::DTMF_PAYLOAD => array(
        'not_empty' => 'Type DTMF payload',
        'digit' => 'DTMF payload must be integer number ranging from 95 to 128',
        'range' => 'DTMF payload must be integer number ranging from 95 to 128',
        'default' => '',
    ),
    // codec validation rules
    Field::CODEC_1 => array(
        'not_empty' => 'Select codec №1',
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_2 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_3 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_4 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_5 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_6 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_7 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_8 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_9 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_10 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_11 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    Field::CODEC_12 => array(
        'in_array' => 'The selected codec is unknown',
        'default' => '',
    ),
    'dtmf_methods' => array(
        Model_Profile_Connection::ENUM_DTMF_SIP_INFO => 'SIP INFO',
        Model_Profile_Connection::ENUM_DTMF_RFC2833 => 'RFC2833',
    ),
    'codec_types' => array(
        '' => 'Not specified',
        Model_Profile_Connection::ENUM_CODEC_ALAW => 'ALAW',
        Model_Profile_Connection::ENUM_CODEC_ULAW => 'ULAW',
        Model_Profile_Connection::ENUM_CODEC_GSM => 'GSM',
        Model_Profile_Connection::ENUM_CODEC_iLBC => 'iLBC',
        Model_Profile_Connection::ENUM_CODEC_G729A => 'G729A',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_NB => 'SPEEX NB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_WB => 'SPEEX WB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_UWB => 'SPEEX UWB',
        Model_Profile_Connection::ENUM_CODEC_SILK_NB => 'SILK NB',
        Model_Profile_Connection::ENUM_CODEC_SILK_MB => 'SILK MB',
        Model_Profile_Connection::ENUM_CODEC_SILK_WB => 'SILK WB',
        Model_Profile_Connection::ENUM_CODEC_SILK_SWB => 'SILK SWB',
    ),
);
