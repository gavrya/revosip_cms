<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::CONTACT_NAME => array(
        'not_empty' => 'Type contact\'s name',
        'max_length' => 'Contact\'s name must not have more than 50 symbols',
        'default' => '',
    ),
    Field::CONTACT_PHONE => array(
        'not_empty' => 'Type contact\'s phone number',
        'max_length' => 'Contact\'s phone number must not have more than 50 symbols',
        'regex' => 'Phone number contain only numbers and may start with a +',
        'Model_Profile_Contact::is_contact_phone_available' => 'The contact with the specified phone number is already exists',
        'default' => '',
    ),
);
