<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::REQUEST_CREDIT => array(
        'not_empty' => 'Type the amount',
        'digit' => 'The amount must be an integer number',
        'range' => 'The amount must be within the permissible range',
        'default' => '',
    ),
    Field::WEBMONEY_UNIT => array(
        'not_empty' => 'Select the Webmoney currency type',
        'in_array' => 'Unknown Webmoney currency type',
        'default' => '',
    ),
    'invoice_units' => array(
        Model_Webmoney_Invoice::ENUM_UNIT_WMZ => 'WMZ (USD equivalent)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMR => 'WMR (RUB equivalent)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMU => 'WMU (UAH equivalent)',
    ),
);
