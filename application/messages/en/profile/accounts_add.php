<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::ACCOUNT_NAME => array(
        'not_empty' => 'Type the account name',
        'max_length' => 'The account name must not have more than 50 symbols',
        'Model_Profile_Account::is_account_name_available' => 'The specified account name is not allowed',
        'default' => '',
    ),
    Field::ACCOUNT_USER => array(
        'not_empty' => 'Type username',
        'max_length' => 'Username must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACCOUNT_LOGIN => array(
        'not_empty' => 'Type login',
        'min_length' => 'Login must have more than 4 symbols',
        'max_length' => 'Login must not have more than 50 symbols',
        'regex' => 'Login contains symbols which are not allowed',
        'Model_Profile_Account::is_account_login_available' => 'The specified login is not allowed',
        'default' => '',
    ),
    Field::ACCOUNT_PASSWORD => array(
        'not_empty' => 'Type password',
        'min_length' => 'Password must have more than 5 symbols',
        'max_length' => 'Password must not have more than 50 symbols',
        'default' => '',
    ),
    Field::ACCOUNT_REPASSWORD => array(
        'default' => 'Passwords do not match',
    ),
);