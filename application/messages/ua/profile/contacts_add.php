<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::CONTACT_NAME => array(
        'not_empty' => 'Введіть ім\'я контакту',
        'max_length' => 'Довжина імені контакту не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::CONTACT_PHONE => array(
        'not_empty' => 'Введіть номер телефону контакту',
        'max_length' => 'Довжина номера телефону не повинна перевищувати 50 символів',
        'regex' => 'Номер телефону повинен містити тільки цифри і може починатися зі знака +',
        'Model_Profile_Contact::is_contact_phone_available' => 'Контакт з таким номером вже існує в списку контактів',
        'default' => '',
    ),
);
