<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::PROLONG_DAYS => array(
        'not_empty' => 'Введіть бажану кількість днів продовження терміну дії акаунта',
        'digit' => 'Кількість днів продовження має бути цілим числом',
        'range' => 'Кількість днів продовження повинно бути в допустимому діапазоні',
        'default' => '',
    ),
);
