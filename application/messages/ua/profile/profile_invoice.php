<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::REQUEST_CREDIT => array(
        'not_empty' => 'Введіть бажану суму',
        'digit' => 'Значення суми має бути цілим числом',
        'range' => 'Значення суми поза допустимому діапазоні',
        'default' => '',
    ),
    Field::WEBMONEY_UNIT => array(
        'not_empty' => 'Виберіть тип валюти Webmoney',
        'in_array' => 'Невідомий тип валюти Webmoney',
        'default' => '',
    ),
    'invoice_units' => array(
        Model_Webmoney_Invoice::ENUM_UNIT_WMZ => 'WMZ (еквівалент долара)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMR => 'WMR (еквівалент рубля)',
        Model_Webmoney_Invoice::ENUM_UNIT_WMU => 'WMU (еквівалент гривні)',
    ),
);
