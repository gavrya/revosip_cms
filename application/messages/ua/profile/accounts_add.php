<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::ACCOUNT_NAME => array(
        'not_empty' => 'Введіть бажану назву акаунта',
        'max_length' => 'Довжина назви акаунта не повинна перевищувати 50 символів',
        'Model_Profile_Account::is_account_name_available' => 'Вказана назва акаунта недоступна для вибору',
        'default' => '',
    ),
    Field::ACCOUNT_USER => array(
        'not_empty' => 'Введіть ім\'я користувача',
        'max_length' => 'Довжина імені користувача не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACCOUNT_LOGIN => array(
        'not_empty' => 'Введіть логін користувача',
        'min_length' => 'Довжина логіну користувача повинна бути від 4 символів',
        'max_length' => 'Довжина логіну користувача не повинна перевищувати 50 символів',
        'regex' => 'Логін користувача містить недопустимі символи',
        'Model_Profile_Account::is_account_login_available' => 'Вказаний логін користувача недоступний для вибору',
        'default' => '',
    ),
    Field::ACCOUNT_PASSWORD => array(
        'not_empty' => 'Введіть пароль користувача',
        'min_length' => 'Довжина пароля користувача має бути від 5 символів',
        'max_length' => 'Довжина пароля користувача не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACCOUNT_REPASSWORD => array(
        'default' => 'Паролі не збігаються',
    ),
);