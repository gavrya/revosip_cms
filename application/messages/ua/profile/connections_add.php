<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // connection validation rules
    Field::CONNECTION_NAME => array(
        'not_empty' => 'Введіть назву профілю підключення',
        'max_length' => 'Довжина назви профілю підключення не повинна перевищувати 50 символів',
        'Model_Profile_Connection::is_connection_name_available' => 'Вказана назва профілю підключення недоступна для вибору',
        'default' => '',
    ),
    Field::CONNECTION_DESCRIPTION => array(
        'not_empty' => 'Введіть короткий опис профілю підключення',
        'max_length' => 'Довжина опису профілю підключення не повинна перевищувати 100 символів',
        'default' => '',
    ),
    // account validation rules
    Field::ACC_CALLER_ID => array(
        'not_empty' => 'Введіть ім\'я для відображення',
        'max_length' => 'Довжина імені для відображення не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACC_EXTENSION => array(
        'not_empty' => 'Введіть додатковий номер',
        'max_length' => 'Довжина додаткового номера не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACC_USER_NAME => array(
        'not_empty' => 'Введіть ім\'я користувача',
        'max_length' => 'Довжина імені користувача не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACC_PASSWORD => array(
        'not_empty' => 'Введіть пароль',
        'max_length' => 'Довжина пароля не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::ACC_SECRET_PASSWORD => array(
        'not_empty' => 'Введіть секретний пароль',
        'exact_length' => 'Довжина секретного пароля повинна бути 32 символа',
        'regex' => 'Формат секретного пароля повинен відповідати формату MD5 хешу',
        'default' => '',
    ),
    // network validation rules
    Field::NETWORK_SIP_SERVER => array(
        'not_empty' => 'Введіть адресу SIP сервера',
        'max_length' => 'Довжина адреси SIP сервера не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::NETWORK_SIP_SERVER_PORT => array(
        'not_empty' => 'Введіть порт SIP сервера',
        'digit' => 'Порт SIP сервера повинен бути цілим числом від 1 до 65535',
        'range' => 'Порт SIP сервера повинен бути цілим числом від 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER => array(
        'not_empty' => 'Введіть адресу SIP проксі сервера',
        'max_length' => 'Довжина адреси SIP проксі сервера не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::NETWORK_PROXY_SERVER_PORT => array(
        'not_empty' => 'Введіть порт SIP проксі сервера',
        'digit' => 'Порт SIP проксі сервера повинен бути цілим числом від 1 до 65535',
        'range' => 'Порт SIP проксі сервера повинен бути цілим числом від 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_ADDRESS => array(
        'not_empty' => 'Введіть локальну адресу',
        'max_length' => 'Довжина локальної адреси не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::NETWORK_LOCAL_PORT => array(
        'not_empty' => 'Введіть локальний порт',
        'digit' => 'Локальний порт повинен бути цілим числом від 1 до 65535',
        'range' => 'Локальний порт повинен бути цілим числом від 1 до 65535',
        'default' => '',
    ),
    Field::NETWORK_MIN_RTP_PORT => array(
        'not_empty' => 'Введіть мінімальний RTP порт',
        'digit' => 'Мінімальний RTP порт повинен бути цілим числом від 1 до 65500',
        'range' => 'Мінімальний RTP порт повинен бути цілим числом від 1 до 65500',
        'default' => '',
    ),
    Field::NETWORK_MAX_RTP_PORT => array(
        'not_empty' => 'Введіть максимальний RTP порт',
        'digit' => 'Максимальний RTP порт повинен бути цілим числом від 100 до 65500',
        'range' => 'Максимальний RTP порт повинен бути цілим числом від 100 до 65500',
        'default' => '',
    ),
    Field::NETWORK_REGISTER_EXPIRES => array(
        'not_empty' => 'Введіть таймер реєстрації',
        'digit' => 'Таймер реєстрації повинен бути цілим числом від 30 до 7200',
        'range' => 'Таймер реєстрації повинен бути цілим числом від 30 до 7200',
        'default' => '',
    ),
    // NAT validation rules
    Field::NAT_KEEP_ALIVE => array(
        'not_empty' => 'Введіть тривалість таймера Keep-Alive',
        'digit' => 'Таймер Keep-Alive повинен бути цілим числом від 30 до 300',
        'range' => 'Таймер Keep-Alive повинен бути цілим числом від 30 до 300',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER => array(
        'not_empty' => 'Введіть адресу STUN сервера',
        'max_length' => 'Довжина адреси STUN сервера не повинна перевищувати 50 символів',
        'default' => '',
    ),
    Field::NAT_STUN_SERVER_PORT => array(
        'not_empty' => 'Введіть порт STUN сервера',
        'digit' => 'Порт STUN сервера повинен бути цілим числом від 1 до 65535',
        'range' => 'Порт STUN сервера повинен бути цілим числом від 1 до 65535',
        'default' => '',
    ),
    // DTMF validation rules
    Field::DTMF_METHOD => array(
        'not_empty' => 'Вкажіть бажаний DTMF метод',
        'in_array' => 'Вказано невідомий DTMF метод',
        'default' => '',
    ),
    Field::DTMF_DURATION => array(
        'not_empty' => 'Введіть тривалість DTMF сигналу',
        'digit' => 'Тривалість DTMF сигналу повинна бути цілим числом від 100 до 5000',
        'range' => 'Тривалість DTMF сигналу повинна бути цілим числом від 100 до 5000',
        'default' => '',
    ),
    Field::DTMF_PAYLOAD => array(
        'not_empty' => 'Введіть DTMF payload',
        'digit' => 'DTMF payload повинен бути цілим числом від 95 до 128',
        'range' => 'DTMF payload повинен бути цілим числом від 95 до 128',
        'default' => '',
    ),
    // codec validation rules
    Field::CODEC_1 => array(
        'not_empty' => 'Введіть кодек № 1',
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_2 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_3 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_4 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_5 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_6 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_7 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_8 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_9 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_10 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_11 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    Field::CODEC_12 => array(
        'in_array' => 'Вказано невідомий кодек',
        'default' => '',
    ),
    'dtmf_methods' => array(
        Model_Profile_Connection::ENUM_DTMF_SIP_INFO => 'SIP INFO',
        Model_Profile_Connection::ENUM_DTMF_RFC2833 => 'RFC2833',
    ),
    'codec_types' => array(
        '' => 'Не вказано',
        Model_Profile_Connection::ENUM_CODEC_ALAW => 'ALAW',
        Model_Profile_Connection::ENUM_CODEC_ULAW => 'ULAW',
        Model_Profile_Connection::ENUM_CODEC_GSM => 'GSM',
        Model_Profile_Connection::ENUM_CODEC_iLBC => 'iLBC',
        Model_Profile_Connection::ENUM_CODEC_G729A => 'G729A',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_NB => 'SPEEX NB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_WB => 'SPEEX WB',
        Model_Profile_Connection::ENUM_CODEC_SPEEX_UWB => 'SPEEX UWB',
        Model_Profile_Connection::ENUM_CODEC_SILK_NB => 'SILK NB',
        Model_Profile_Connection::ENUM_CODEC_SILK_MB => 'SILK MB',
        Model_Profile_Connection::ENUM_CODEC_SILK_WB => 'SILK WB',
        Model_Profile_Connection::ENUM_CODEC_SILK_SWB => 'SILK SWB',
    ),
);
