<?php

defined('SYSPATH') or die('No direct script access.');

return array(
"<h5>1. Основні терміни та визначення</h5>

Власник Проекту Revosip - особа відповідальна за створення Проекту, його підтримку та надання Сервісу на його основі.<br/>
Проект - сутність, що включає в себе Сайт, Програму, користувачів, Власника, Розробників і Адміністрацію.<br/>
Розробник - особи, які розробили програмні компоненти Проекту.<br/>
Програма - компоненти програмного забезпечення розміщені Адміністрацією на Сайті.<br/>
Сайт - сукупність веб-ресурсів розташованих у мережі Інтернет у домені revosip.com і його піддоменах.<br/>
Сервіс - сукупність послуг і можливостей що надаються Адміністрацією за допомогою програмного забезпечення Проекту.<br/>
Користувач - будь-яка особа зареєстрована як користувач сервісу і уклала цю Угоду.<br/>
Адміністрація - особи, уповноважені Власником Проекту на спеціальне використання Проекту, у тому числі управління, і діючі від імені Власника Проекту.<br/>
Реєстрація в якості користувача сервісу - створення облікового запису користувача у вигляді набору умовних ідентифікаційних даних Користувача (Профілю), за допомогою яких Користувач здійснює доступ до сайту.<br/>

<h5>2. Загальні умови</h5>

2.1 Сервіс надається особам які були попередньо зареєстровані на Сайті - Користувачам.<br/>
2.2 Ця Угода з усіма додатками до нього для стислості надалі іменується Угодою.<br/>
2.3 Ця Угода може бути змінена або доповнена Власником Проекту.<br/>
2.4 Сервіс надається за принципом «як є», тобто Користувачеві не надається гарантій, що Сервіс, а також надані їм можливості будуть функціонувати безперебійно, безперервно, без помилок, будуть відповідати вимогам та очікуванням Користувача.<br/>
2.5 Власник Проекту, Розробники та Адміністрація не несуть відповідальності перед Користувачем за будь-який збиток або шкоду, заподіяну Користувачеві, в тому числі його комп'ютера, програмного забезпечення або іншому майну у зв'язку з використанням або неможливістю використання Сервісу.<br/>
2.6 Використання можливостей Сервісу здійснюються на безкоштовній основі.<br/>

<h5>3. Права та обов'язки Адміністрації</h5>

3.1 Адміністрація вживає всі можливі заходи для надання безпечних, якісних та безперебійних послуг, працездатності Сервісу, функціонування Сайту і виходу нових версій Програм.<br/>
3.2 Адміністрація залишає за собою право повністю або частково обмежувати функціональність Проекту або доступ до нього з технічних, технологічних, профілактичним чи інших причин на свій власний розсуд з попереднім повідомленням користувачів або без такого.<br/>
3.3 Адміністрація має право в порядку збору статистичних даних відстежувати та зберігати інформацію про IP-адреси доступу Користувача до Сайту і використовувати файли технічної інформації (cookies), що розміщуються на персональному комп'ютері Користувача.<br/>

<h5>4. Права та обов'язки Користувача</h5>

4.1 Користувач зобов'язується дотримуватися цієї Угоди і своєчасно ознайомлюватися з діючою редакцією Угоди.<br/>
4.2 У разі незгоди Користувача з чинною Угодою Користувач зобов'язаний припинити участь у Проекті, в тому числі деінсталювати Програму.<br/>
4.3 Користувач зобов'язується використовувати складні паролі для доступу до різних частин Проекту, а також мати постійний доступ до своєї поштової скриньки вказаною при реєстрації на Сайті.<br/>
4.3 Користувач зобов'язується не розкривати і не передавати третім особам свої ідентифікаційні дані, по яких можлива авторизація Користувача на Сайті Проекту.<br/>
4.4 Користувач зобов'язується не досліджувати код, не декомпілювати і не дизасемблювати Проект, не створювати похідні продукти на базі Проекту або його частин.<br/>
4.5 Користувач має право вчиняти дії, передбачені логікою роботи Сайту і Програм Проекту.<br/>
4.6 Користувач має право звертатись з виникаючих питань функціонування різних частин Сайту та Програми через зворотній зв'язок на Сайті.<br/>

<h5>5. Користувачеві забороняються наступні дії</h5>

5.1 Використовувати Проект будь-яким способом, який може перешкодити його нормальному функціонуванню.<br/>
5.2 Розміщувати в Проекті особисту інформацію третіх осіб без їх згоди, у тому числі домашні адреси, телефони, ПІБ, паспортні дані.<br/>
5.3 Розміщувати в Проекті нецензурні або лайливі слова або словосполучення, у тому числі використовувати їх в імені Користувача.<br/>
5.4 Розміщувати в Проекті шкідливі програми або посилання, які можуть містити або містять шкідливі програми.<br/>
5.5 Розміщувати в Проекті будь-яку іншу інформацію, яка на думку Адміністрації є небажаною, порушує норми моралі та етики, порушує чинне Законодавство.<br/>
5.6 Здійснювати будь-які інші протиправні або злочинні дії.

<h5>6. Відповідальність сторін</h5>

6.1 Користувач несе особисту відповідальність за втрату доступу до своєї поштової скриньки вказаної при реєстрації, витік розміщеної персональної інформації третіх осіб на Сайті і власних паролів доступу до частин Проекту.<br/>
6.2 Користувач несе відповідальність перед Адміністрацією і третіми особами за недотримання умов цієї Угоди.<br/>
6.3 Адміністрація не несе відповідальності за будь-яку поведінку користувачів або третіх осіб, включаючи, але не обмежуючись, крадіжку, знищення або неправомірний доступ до матеріалів користувачів.<br/>
6.4 Адміністрація і Власник Проекту, а також їх представники ні за яких обставин не несуть відповідальності перед Користувачами Проекту або іншими третіми особами за будь-який непрямий, випадковий, ненавмисний збиток, включаючи, але не обмежуючись, втрачені дані, шкоду честі, гідності або діловій репутації, упущену вигоду, викликані використанням Проекту, його складових частин і матеріалів, доступ до яких був отриманий за допомогою Проекту, навіть якщо Адміністрація не вказувала на можливість такого збитку<br/>
6.5 Адміністрація звільняється від відповідальності за повне або часткове невиконання зобов'язань за цією Угодою, якщо таке невиконання з'явиться наслідком дії непереборної сили (форс-мажор), тобто надзвичайних і невідворотних сторонами за даних умов обставин, в тому числі DDOS атак, заборонних дій влади, стихійних лих, пожеж, катастроф, а також внаслідок збоїв в телекомунікаційних і енергетичних мережах, дій шкідливих програм, а також недобросовісних дій третіх осіб відносно до Проекту, спрямованих на несанкціонований доступ або виведення з ладу програмного або апаратного комплексу.",
);
