<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'Загальні питання' => array(
        'Що таке Revosip?' => 'Revosip - це хмарний java SIP клієнт',
        'Як запустити java sip клієнт Revosip?' => 'Для того, щоб запустити java sip клієнт Revosip необхідно скачати та встановити віртуальну машину Java (JRE) використовуючи посилання <a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">http://www.oracle.com/technetwork/java/javase/downloads/index.html</a>. На сучасних ОС, таких як Linux Ubuntu та Mac OS X, віртуальна машина Java вже повинна бути встановлена в системі',
    ),
);