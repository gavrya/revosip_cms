<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    Field::USER_LOGIN => array(
        'not_empty' => 'Введіть логін',
        'min_length' => 'Довжина логіну повинна бути від 4 символів',
        'max_length' => 'Довжина логіну не повинна перевищувати 50 символів',
        'regex' => 'Логін містить недопустимі символи',
        'default' => '',
    ),
    Field::USER_PASSWORD => array(
        'not_empty' => 'Введіть пароль',
        'min_length' => 'Довжина пароля повинна бути від 5 символів',
        'max_length' => 'Довжина пароля не повинна перевищувати 50 символів',
        'default' => '',
    ),
);