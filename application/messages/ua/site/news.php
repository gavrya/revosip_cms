<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // news mark
    Field::NEWS_MARK => array(
        'not_empty' => 'Вкажіть рівень важливості новини',
        'in_array' => 'Вказано невідомий рівень важливості новини',
        'default' => '',
    ),
    // news meta title
    Field::NEWS_TITLE . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введіть зміст новини (російська версія)',
        'max_length' => 'Довжина meta змісту новини не повинна перевищувати 75 символів (російська версія)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введіть зміст новини (українська версія)',
        'max_length' => 'Довжина meta змісту новини не повинна перевищувати 75 символів (українська версія)',
        'default' => '',
    ),
    Field::NEWS_TITLE . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введіть зміст новини (англійська версія)',
        'max_length' => 'Довжина meta змісту новини не повинна перевищувати 75 символів (англійська версія)',
        'default' => '',
    ),
    // news meta keywords
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введіть meta ключові слова новини (російська версія)',
        'max_length' => 'Довжина meta ключових слів новини не повинна перевищувати 150 символів (російська версія)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введіть meta ключові слова новини (українська версія)',
        'max_length' => 'Довжина meta ключових слів новини не повинна перевищувати 150 символів (українська версія)',
        'default' => '',
    ),
    Field::NEWS_META_KEYWORDS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введіть meta ключові слова новини (англійська версія)',
        'max_length' => 'Довжина meta ключових слів новини не повинна перевищувати 150 символів (англійська версія)',
        'default' => '',
    ),
    // news meta descriptions
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введіть meta опису новини (російська версія)',
        'max_length' => 'Довжина meta опису новини не повинна перевищувати 150 символів (російська версія)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введіть meta опису новини (українська версія)',
        'max_length' => 'Довжина meta опису новини не повинна перевищувати 150 символів (українська версія)',
        'default' => '',
    ),
    Field::NEWS_META_DESCRIPTIONS . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введіть meta опису новини (англійська версія)',
        'max_length' => 'Довжина meta опису новини не повинна перевищувати 150 символів (англійська версія)',
        'default' => '',
    ),
    // news content
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_RU => array(
        'not_empty' => 'Введіть вміст новини (російська версія)',
        'max_length' => 'Довжина вмісту новини перевищує допустиме значення (російська версія)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_UA => array(
        'not_empty' => 'Введіть вміст новини (українська версія)',
        'max_length' => 'Довжина вмісту новини перевищує допустиме значення (українська версія)',
        'default' => '',
    ),
    Field::NEWS_CONTENT . Model_News::ENUM_LANG_EN => array(
        'not_empty' => 'Введіть вміст новини (англійська версія)',
        'max_length' => 'Довжина вмісту новини перевищує допустиме значення (англійська версія)',
        'default' => '',
    ),
    'news_languages' => array(
        Model_News::ENUM_LANG_RU => 'Російська версія',
        Model_News::ENUM_LANG_UA => 'Українська версія',
        Model_News::ENUM_LANG_EN => 'Англійська версія',
    ),
    'news_marks' => array(
        Model_News::ENUM_MARK_NORMAL => 'Звичайний',
        Model_News::ENUM_MARK_HIGH => 'Високий',
        Model_News::ENUM_MARK_WARNING => 'Критичний',
    ),
);
