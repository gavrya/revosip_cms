<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Regexpr
 *
 * @author escalade
 */
class Regexpr {
    
    // regex for user login check
    const USER_LOGIN = '/^([a-z0-9]+[\._-]?)+[a-z0-9]+$/i';
    // regex for account profile login check
    const ACCOUNT_LOGIN = '/^([a-z0-9]+[\._-]?)+[a-z0-9]+$/i';
    // regex for contact profile phone number check
    const CONTACT_PHONE_NUMBER = '/^((\+?[\*#0-9]+)|(sip:\+?[\*#0-9a-z]+@[0-9a-z.:-_=;]+)){1,50}$/i';
    // regex for md5 hash check
    const MD5_HASH = '/^[0-9a-f]{32}$/i';

}