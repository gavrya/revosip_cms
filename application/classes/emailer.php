<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Emailer
 *
 * @author Гаврищук
 */
class Emailer {

    // data
    private $site_title;
    private $email_from;
    private $message_templates;

    /**
     * Singleton instance
     */
    private static $instance;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Init                                     */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_emailer() {
        // init parameters
        $config = Kohana::$config->load('app')->get('site');
        $this->site_title = $config['title'];
        $this->email_from = $config['email_from'];
        // unset($config);
        // init message templates
        $this->message_templates = Kohana::message(I18n::lang() . '/emailer/messages');
    }

    /**
     * Singleton
     */
    public static function instance() {
        if (self::$instance === null) {
            self::$instance = new Emailer();
        }
        return self::$instance;
    }

    /**
     * Private constructor
     */
    private function __construct() {
        $this->init_emailer();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                      Registration confirm link                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function send_registration_confirm_link($user_id, $user_name, $confirm_hash, $user_email) {
        try {
            // message data
            $message_data = $this->message_templates['registration_confirm'];
            $html_flag = (boolean) $message_data['html_flag'];
            $subject = __($message_data['subject'], array(':site_title' => $this->site_title));
            $message = __($message_data['message'], array(
                ':user_name' => Html::chars($user_name),
                ':site_link' => URL::base(TRUE),
                ':confirm_link' => URL::site('/registration/confirm/' . $user_id . '/' . $confirm_hash, TRUE),
                ':site_title' => $this->site_title,
            ));
            // unset($message_data);
            $this->send_email($user_email, $subject, $message, $html_flag);
            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Recovery confirm link                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function send_recovery_confirm_link($user_id, $user_name, $user_email, $recovery_hash) {
        try {
            // message data
            $message_data = $this->message_templates['recovery_confirm'];
            $html_flag = (boolean) $message_data['html_flag'];
            $subject = __($message_data['subject'], array(':site_title' => $this->site_title));
            $message = __($message_data['message'], array(
                ':user_name' => Html::chars($user_name),
                ':site_link' => URL::base(TRUE),
                ':confirm_link' => URL::site('/recovery/confirm/' . $user_id . '/' . $recovery_hash, TRUE),
                ':site_title' => $this->site_title,
            ));
            // unset($message_data);
            $this->send_email($user_email, $subject, $message, $html_flag);
            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          email update confirm link                    */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function send_profile_email_update_confirm_link($user_name, $user_email, $confirm_hash) {
        try {
            // message data
            $message_data = $this->message_templates['email_update'];
            $html_flag = (boolean) $message_data['html_flag'];
            $subject = __($message_data['subject'], array(':site_title' => $this->site_title));
            $message = __($message_data['message'], array(
                ':user_name' => Html::chars($user_name),
                ':site_link' => URL::base(TRUE),
                ':confirm_link' => URL::site('/profile/email/update/' . $confirm_hash, TRUE),
                ':site_title' => $this->site_title,
            ));
            // unset($message_data);
            $this->send_email($user_email, $subject, $message, $html_flag);
            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            email confirm link                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function send_profile_email_confirm_link($user_name, $user_email, $confirm_hash) {
        try {
            // message data
            $message_data = $this->message_templates['email_confirm'];
            $html_flag = (boolean) $message_data['html_flag'];
            $subject = __($message_data['subject'], array(':site_title' => $this->site_title));
            $message = __($message_data['message'], array(
                ':user_name' => Html::chars($user_name),
                ':site_link' => URL::base(TRUE),
                ':confirm_link' => URL::site('/profile/email/confirm/' . $confirm_hash, TRUE),
                ':site_title' => $this->site_title,
            ));
            // unset($message_data);
            $this->send_email($user_email, $subject, $message, $html_flag);
            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         send user balance notification                */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function send_invoice_payment_notification($user_name, $user_email) {
        try {
            // message data
            $message_data = $this->message_templates['invoice_payment'];
            $html_flag = (boolean) $message_data['html_flag'];
            $subject = __($message_data['subject'], array(':site_title' => $this->site_title));
            $message = __($message_data['message'], array(
                ':user_name' => Html::chars($user_name),
                ':balance_link' => URL::site('/profile/balance', TRUE),
                ':invoices_link' => URL::site('/profile/invoices', TRUE),
                ':site_title' => $this->site_title,
            ));
            // unset($message_data);
            $this->send_email($user_email, $subject, $message, $html_flag);
            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              send email                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function send_email($user_email, $subject, $message, $html_flag) {
        $message .= __($this->message_templates['message_footer'], array(':site_title' => $this->site_title));
        Email::factory($subject, $message)
                ->to($user_email)
                ->from($this->email_from, $this->site_title)
                ->send();
    }

}