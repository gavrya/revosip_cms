<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_News
 */
class Model_News extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             news table                                */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const NEWS_TABLE = 'news';
    const NEWS_ID = 'news_id';
    const NEWS_DATE = 'news_date';
    const NEWS_MARK = 'news_mark';

    // additional
    const NEWS_DATE_TZ_FORMAT = 'news_date_tz_format';

    // mark enum
    const ENUM_MARK_NORMAL = 'NORMAL';
    const ENUM_MARK_HIGH = 'HIGH';
    const ENUM_MARK_WARNING = 'WARNING';

    // mark enum list
    public static $MARK_ENUM_LIST = array(
        self::ENUM_MARK_NORMAL,
        self::ENUM_MARK_HIGH,
        self::ENUM_MARK_WARNING,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            news_content table                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const NEWS_CONTENT_TABLE = 'news_content';
    const NEWS_LANG = 'news_lang';
    const NEWS_TITLE = 'news_title';
    const NEWS_META_KEYWORDS = 'news_meta_keywords';
    const NEWS_META_DESCRIPTIONS = 'news_meta_descriptions';
    const NEWS_CONTENT = 'news_content';

    // language enum
    const ENUM_LANG_RU = 'RU';
    const ENUM_LANG_UA = 'UA';
    const ENUM_LANG_EN = 'EN';

    // language enum list
    public static $LANG_ENUM_LIST = array(
        self::ENUM_LANG_RU,
        self::ENUM_LANG_UA,
        self::ENUM_LANG_EN,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                check methods                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_news_exists($news_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::NEWS_TABLE)
                ->where(self::NEWS_ID, '=', (int) $news_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            count contacts                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function count_news() {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::NEWS_TABLE)
                ->execute();
        return $result->get('count', 0);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                               get methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_news_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $news_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::NEWS_TABLE . '.' . self::NEWS_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y\')', self::NEWS_DATE_TZ_FORMAT);
        return array('*', $news_date_tz_format);
    }

    public static function get_news_list($news_lang, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_news_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::NEWS_TABLE)
                ->join(self::NEWS_CONTENT_TABLE)
                ->on(self::NEWS_TABLE . '.' . self::NEWS_ID, '=', self::NEWS_CONTENT_TABLE . '.' . self::NEWS_ID)
                ->where(self::NEWS_CONTENT_TABLE . '.' . self::NEWS_LANG, '=', $news_lang)
                ->order_by(self::NEWS_TABLE . '.' . self::NEWS_ID, 'DESC')
                ->execute();
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function get_news($news_id, $news_lang, $user_timezone = '') {
        $result = DB::select_array(self::get_news_select_array($user_timezone))
                ->from(self::NEWS_TABLE)
                ->join(self::NEWS_CONTENT_TABLE)
                ->on(self::NEWS_TABLE . '.' . self::NEWS_ID, '=', self::NEWS_CONTENT_TABLE . '.' . self::NEWS_ID)
                ->where(self::NEWS_TABLE . '.' . self::NEWS_ID, '=', (int) $news_id)
                ->and_where(self::NEWS_CONTENT_TABLE . '.' . self::NEWS_LANG, '=', $news_lang)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    public static function get_news_contents($news_id, $user_timezone = '') {
        $result = DB::select_array(self::get_news_select_array($user_timezone))
                ->from(self::NEWS_TABLE)
                ->join(self::NEWS_CONTENT_TABLE)
                ->on(self::NEWS_TABLE . '.' . self::NEWS_ID, '=', self::NEWS_CONTENT_TABLE . '.' . self::NEWS_ID)
                ->where(self::NEWS_TABLE . '.' . self::NEWS_ID, '=', (int) $news_id)
                ->execute();
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              add news                                 */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_news($news_mark, $news_contents) {
        if (in_array($news_mark, self::$MARK_ENUM_LIST)) {
            // begin transaction
            $db = Database::instance();
            $db->begin();
            try {
                $into = array(
                    self::NEWS_DATE,
                    self::NEWS_MARK,
                );
                $values = array(
                    DB::expr('UTC_TIMESTAMP()'),
                    $news_mark,
                );
                // insert
                $news_id = DB::insert(self::NEWS_TABLE, $into)
                        ->values($values)
                        ->execute();
                // add news contents
                self::add_news_contents($news_id[0], $news_contents);
                // commit transaction
                $db->commit();
                return TRUE;
            } catch (Database_Exception $ex) {
                // rollback transaction
                $db->rollback();
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    private static function add_news_contents($news_id, $news_contents) {
        $into = array(
            self::NEWS_ID,
            self::NEWS_LANG,
            self::NEWS_TITLE,
            self::NEWS_META_KEYWORDS,
            self::NEWS_META_DESCRIPTIONS,
            self::NEWS_CONTENT,
        );
        // insert
        $query = DB::insert(self::NEWS_CONTENT_TABLE, $into);
        // loop array
        foreach ($news_contents as $news_content) {
            if (in_array($news_content[self::NEWS_LANG], self::$LANG_ENUM_LIST)) {
                $values = array(
                    (int) $news_id,
                    $news_content[self::NEWS_LANG],
                    $news_content[self::NEWS_TITLE],
                    $news_content[self::NEWS_META_KEYWORDS],
                    $news_content[self::NEWS_META_DESCRIPTIONS],
                    $news_content[self::NEWS_CONTENT],
                );
                $query->values($values);
            }
        }
        $query->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              update news                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_news($news_id, $news_mark, $news_contents) {
        if (in_array($news_mark, self::$MARK_ENUM_LIST)) {
            try {
                // values
                $values[self::NEWS_MARK] = $news_mark;
                // update
                DB::update(self::NEWS_TABLE)
                        ->set($values)
                        ->where(self::NEWS_ID, '=', (int) $news_id)
                        ->and_where(self::NEWS_MARK, '!=', $news_mark)
                        ->execute();
                // udate contents
                self::update_news_contents($news_id, $news_contents);
                return TRUE;
            } catch (Database_Exception $ex) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    private static function update_news_contents($news_id, $news_contents) {
        // loop array
        foreach ($news_contents as $news_content) {
            if (in_array($news_content[self::NEWS_LANG], self::$LANG_ENUM_LIST)) {
                //$news_content[self::NEWS_LANG];
                $news_content[self::NEWS_TITLE];
                $news_content[self::NEWS_META_KEYWORDS];
                $news_content[self::NEWS_META_DESCRIPTIONS];
                $news_content[self::NEWS_CONTENT];
                // update
                DB::update(self::NEWS_CONTENT_TABLE)
                        ->set($news_content)
                        ->where(self::NEWS_ID, '=', (int) $news_id)
                        ->and_where(self::NEWS_LANG, '=', $news_content[self::NEWS_LANG])
                        ->execute();
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              remove news                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function remove_news($news_id) {
        try {
            DB::delete(self::NEWS_TABLE)
                    ->where(self::NEWS_ID, '=', (int) $news_id)
                    ->execute();
            // update hash
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

}