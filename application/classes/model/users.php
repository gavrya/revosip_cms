<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Users
 */
class Model_Users extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Users table                                */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const USERS_TABLE = 'users';
    const USER_ID = 'user_id';
    const USER_LOGIN = 'user_login';
    const USER_PASS = 'user_pass';
    const USER_NAME = 'user_name';
    const USER_EMAIL = 'user_email';
    const USER_NEW_EMAIL = 'user_new_email';
    const USER_LANG = 'user_lang';
    const USER_TIMEZONE = 'user_timezone';
    const USER_REG_DATE = 'user_reg_date';
    const USER_CREDIT = 'user_credit';
    const USER_CONFIRMED = 'user_confirmed';
    const USER_BLOCKED = 'user_blocked';
    // new
    const USER_REG_IP = 'user_reg_ip';
    const USER_REG_EMAIL = 'user_reg_email';
    const USER_ONLINE_DATE = 'user_online_date';
    const USER_AUTH_IP = 'user_auth_ip';
    const USER_AUTH_HASH = 'user_auth_hash';
    // additional
    const USER_REG_DATE_TZ_FORMAT = 'user_reg_date_tz_format';
    const USER_TIME_TZ_FORMAT = 'user_time_tz_format';
    const USER_ONLINE_STATUS = 'user_online_status';

    // language enum
    const ENUM_LANGUAGE_RU = 'RU';
    const ENUM_LANGUAGE_UA = 'UA';
    const ENUM_LANGUAGE_EN = 'EN';

    // timezone enum
    const ENUM_TIMEZONE_M_12 = '-12:00';
    const ENUM_TIMEZONE_M_11 = '-11:00';
    const ENUM_TIMEZONE_M_10 = '-10:00';
    const ENUM_TIMEZONE_M_9 = '-09:00';
    const ENUM_TIMEZONE_M_8 = '-08:00';
    const ENUM_TIMEZONE_M_7 = '-07:00';
    const ENUM_TIMEZONE_M_6 = '-06:00';
    const ENUM_TIMEZONE_M_5 = '-05:00';
    const ENUM_TIMEZONE_M_4 = '-04:00';
    const ENUM_TIMEZONE_M_3_3 = '-03:30';
    const ENUM_TIMEZONE_M_3 = '-03:00';
    const ENUM_TIMEZONE_M_2 = '-02:00';
    const ENUM_TIMEZONE_M_1 = '-01:00';
    const ENUM_TIMEZONE_P_0 = '+00:00';
    const ENUM_TIMEZONE_P_1 = '+01:00';
    const ENUM_TIMEZONE_P_2 = '+02:00';
    const ENUM_TIMEZONE_P_3 = '+03:00';
    const ENUM_TIMEZONE_P_3_3 = '+03:30';
    const ENUM_TIMEZONE_P_4 = '+04:00';
    const ENUM_TIMEZONE_P_4_3 = '+04:30';
    const ENUM_TIMEZONE_P_5 = '+05:00';
    const ENUM_TIMEZONE_P_5_3 = '+05:30';
    const ENUM_TIMEZONE_P_6 = '+06:00';
    const ENUM_TIMEZONE_P_7 = '+07:00';
    const ENUM_TIMEZONE_P_8 = '+08:00';
    const ENUM_TIMEZONE_P_9 = '+09:00';
    const ENUM_TIMEZONE_P_9_3 = '+09:30';
    const ENUM_TIMEZONE_P_10 = '+10:00';
    const ENUM_TIMEZONE_P_11 = '+11:00';
    const ENUM_TIMEZONE_P_12 = '+12:00';

    // country enum
    const ENUM_COUNTRY_OTHER_COUNTRY = '-12:00';

    // language enum list
    public static $LANGUAGE_ENUM_LIST = array(
        self::ENUM_LANGUAGE_RU,
        self::ENUM_LANGUAGE_UA,
        self::ENUM_LANGUAGE_EN,
    );
    // timezone enum list
    public static $TIMEZONE_ENUM_LIST = array(
        self::ENUM_TIMEZONE_M_12,
        self::ENUM_TIMEZONE_M_11,
        self::ENUM_TIMEZONE_M_10,
        self::ENUM_TIMEZONE_M_9,
        self::ENUM_TIMEZONE_M_8,
        self::ENUM_TIMEZONE_M_7,
        self::ENUM_TIMEZONE_M_6,
        self::ENUM_TIMEZONE_M_5,
        self::ENUM_TIMEZONE_M_4,
        self::ENUM_TIMEZONE_M_3_3,
        self::ENUM_TIMEZONE_M_3,
        self::ENUM_TIMEZONE_M_2,
        self::ENUM_TIMEZONE_M_1,
        self::ENUM_TIMEZONE_P_0,
        self::ENUM_TIMEZONE_P_1,
        self::ENUM_TIMEZONE_P_2,
        self::ENUM_TIMEZONE_P_3,
        self::ENUM_TIMEZONE_P_3_3,
        self::ENUM_TIMEZONE_P_4,
        self::ENUM_TIMEZONE_P_4_3,
        self::ENUM_TIMEZONE_P_5,
        self::ENUM_TIMEZONE_P_5_3,
        self::ENUM_TIMEZONE_P_6,
        self::ENUM_TIMEZONE_P_7,
        self::ENUM_TIMEZONE_P_8,
        self::ENUM_TIMEZONE_P_9,
        self::ENUM_TIMEZONE_P_9_3,
        self::ENUM_TIMEZONE_P_10,
        self::ENUM_TIMEZONE_P_11,
        self::ENUM_TIMEZONE_P_12,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         User record table                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const RECORD_TABLE = 'user_record';
    const RECORD_ID = 'record_id';
    const RECORD_TYPE = 'record_type';
    const RECORD_HASH = 'record_hash';
    const RECORD_EXPIRES = 'record_expires';

    // record enum
    const ENUM_RECORD_USER_CONFIRM = 'USER_CONFIRM';
    const ENUM_RECORD_PASS_RECOVERY = 'PASS_RECOVERY';
    const ENUM_RECORD_EMAIL_UPDATE = 'EMAIL_UPDATE';

    // record enum list
    public static $EVENT_ENUM_LIST = array(
        self::ENUM_RECORD_USER_CONFIRM,
        self::ENUM_RECORD_PASS_RECOVERY,
        self::ENUM_RECORD_EMAIL_UPDATE,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        User log table                                 */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const LOG_TABLE = 'user_log';
    const LOG_ID = 'log_id';
    const LOG_DATE = 'log_date';
    const LOG_IP = 'log_ip';
    const LOG_TYPE = 'log_type';
    // additional
    const LOG_DATE_TZ_FORMAT = 'log_date_tz_format';

    // log enum
    const ENUM_LOG_LOGIN = 'LOGIN';
    const ENUM_LOG_LOGOUT = 'LOGOUT';

    // log enum list
    public static $LOG_ENUM_LIST = array(
        self::ENUM_LOG_LOGIN,
        self::ENUM_LOG_LOGOUT,
    );

    const REGISTRATION_EXPIRES_DAYS = 1;
    const PASSWORD_RECOVERY_EXPIRES_HOURS = 2;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              check methods                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_user_login_available($login) {
        self::remove_expired_registrations();
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::USERS_TABLE)
                ->where(self::USER_LOGIN, '=', $login)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    public static function is_user_email_available($email) {
        self::remove_expired_registrations();
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::USERS_TABLE)
                ->where(self::USER_EMAIL, '=', $email)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            registration method                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function register_user($user_login, $user_pass, $user_name, $user_email, $user_lang, $user_timezone) {
        // begin transaction
        $db = Database::instance();
        $db->begin();
        try {
            $user_pass_hash = self::hash_user_password($user_pass);
            $confirm_hash = md5(microtime());
            $user_email = UTF8::strtolower($user_email);
            // add user
            $users_into = array(
                self::USER_LOGIN,
                self::USER_PASS,
                self::USER_NAME,
                self::USER_EMAIL,
                self::USER_NEW_EMAIL,
                self::USER_LANG,
                self::USER_TIMEZONE,
                self::USER_REG_DATE,
                self::USER_REG_IP,
                self::USER_REG_EMAIL,
                self::USER_ONLINE_DATE,
            );
            $users_values = array(
                $user_login,
                $user_pass_hash,
                $user_name,
                $user_email,
                DB::expr('NULL'),
                $user_lang,
                in_array($user_timezone, self::$TIMEZONE_ENUM_LIST) ? $user_timezone : self::ENUM_TIMEZONE_P_0,
                DB::expr('UTC_TIMESTAMP()'),
                Request::$client_ip,
                $user_email,
                DB::expr('UTC_TIMESTAMP()'),
            );
            $users_result = DB::insert(self::USERS_TABLE, $users_into)
                    ->values($users_values)
                    ->execute();
            // user id
            $user_id = $users_result[0];
            // set confirm expires
            $confirm_id = self::add_user_confirm_record($user_id, $confirm_hash);
            // commit transaction
            $db->commit();
            // register data
            return array(
                self::USER_ID => $user_id,
                self::USER_LOGIN => $user_login,
                self::USER_NAME => $user_name,
                self::USER_EMAIL => $user_email,
                self::RECORD_ID => $confirm_id,
                self::RECORD_HASH => $confirm_hash,
            );
        } catch (Database_Exception $ex) {
            // rollback transaction
            $db->rollback();
            return FALSE;
        }
    }

    private static function remove_expired_registrations() {
        DB::delete(self::USERS_TABLE)
                ->where(self::USER_CONFIRMED, '=', FALSE)
                ->and_where(self::USER_REG_DATE, '<', DB::expr('ADDDATE(UTC_TIMESTAMP(), - ' . self::REGISTRATION_EXPIRES_DAYS . ')'))
                ->execute();
    }

    public static function remove_user($user_id) {
        DB::delete(self::USERS_TABLE)
                ->where(self::USER_ID, '=', (int) $user_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                               get methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_user_select_array() {
        $user_registered_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::USER_REG_DATE . '", \'+00:00\', "' . self::USER_TIMEZONE . '"), \'%d.%m.%Y\')', self::USER_REG_DATE_TZ_FORMAT);
        $user_time_tz_format = array('DATE_FORMAT(CONVERT_TZ(UTC_TIMESTAMP(), \'+00:00\', "' . self::USER_TIMEZONE . '"), \'%d.%m.%Y %H:%i\')', self::USER_TIME_TZ_FORMAT);
        $user_online_status = array('"' . self::USER_ONLINE_DATE . '" > DATE_ADD(UTC_TIMESTAMP(), INTERVAL -5 MINUTE)', self::USER_ONLINE_STATUS);
        return array('*', $user_registered_tz_format, $user_time_tz_format, $user_online_status);
    }

    public static function get_user($user_id, $user_auth_hash = NULL) {
        $query = DB::select_array(self::get_user_select_array())
                ->from(self::USERS_TABLE)
                ->where(self::USER_ID, '=', (int) $user_id);
        if (isset($user_auth_hash)) {
            $query->and_where(self::USER_AUTH_HASH, '=', $user_auth_hash)
                    ->and_where(self::USER_AUTH_IP, '=', Request::$client_ip);
        }
        $result = $query->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    public static function get_user_by_email($email) {
        $result = DB::select_array(self::get_user_select_array())
                ->from(self::USERS_TABLE)
                ->where(self::USER_EMAIL, '=', $email)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    public static function get_user_by_login_pass($user_login, $user_pass) {
        $user_pass_hash = self::hash_user_password($user_pass);
        $result = DB::select_array(self::get_user_select_array())
                ->from(self::USERS_TABLE)
                ->where(self::USER_LOGIN, '=', $user_login)
                ->and_where(self::USER_PASS, '=', $user_pass_hash)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }
    
    public static function get_user_by_login($user_login) {
        $result = DB::select_array(self::get_user_select_array())
                ->from(self::USERS_TABLE)
                ->where(self::USER_LOGIN, '=', $user_login)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             update methods                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_user_data($user_id, $user_name, $user_password, $user_language, $user_timezone) {
        try {
            $values[self::USER_NAME] = $user_name;
            $values[self::USER_LANG] = $user_language;
            $values[self::USER_TIMEZONE] = in_array($user_timezone, self::$TIMEZONE_ENUM_LIST) ? $user_timezone : self::ENUM_TIMEZONE_P_0;
            if (!empty($user_password)) {
                $values[self::USER_PASS] = self::hash_user_password($user_password);
            }
            DB::update(self::USERS_TABLE)
                    ->set($values)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function update_user_online_date($user_id) {
        try {
            $values[self::USER_ONLINE_DATE] = DB::expr('UTC_TIMESTAMP()');
            DB::update(self::USERS_TABLE)
                    ->set($values)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              credit methods                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function reduce_user_credit($user_id, $credit) {
        $credit = number_format($credit, 2, '.', ''); // !!!
        DB::update(self::USERS_TABLE)
                ->set(array(self::USER_CREDIT => DB::expr('`' . self::USER_CREDIT . '` - ' . $credit)))
                ->where(self::USER_ID, '=', (int) $user_id)
                ->execute();
    }

    public static function add_user_credit($user_id, $credit) {
        $credit = number_format($credit, 2, '.', ''); // !!!
        DB::update(self::USERS_TABLE)
                ->set(array(self::USER_CREDIT => DB::expr('`' . self::USER_CREDIT . '` + ' . $credit)))
                ->where(self::USER_ID, '=', (int) $user_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            confirm methods                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function add_user_confirm_record($user_id, $confirm_hash) {
        $into = array(
            self::USER_ID,
            self::RECORD_HASH,
            self::RECORD_TYPE,
            self::RECORD_EXPIRES
        );
        $values = array(
            (int) $user_id,
            $confirm_hash,
            self::ENUM_RECORD_USER_CONFIRM,
            DB::expr('ADDDATE(UTC_TIMESTAMP(), ' . self::REGISTRATION_EXPIRES_DAYS . ')')
        );
        $result = DB::insert(self::RECORD_TABLE, $into)
                ->values($values)
                ->execute();
        // return event_id
        return $result[0];
    }

    public static function get_user_confirm_record($user_id) {
        return self::get_user_record($user_id, Model_Users::ENUM_RECORD_USER_CONFIRM);
    }

    public static function confirm_user_registration($user_id) {
        // begin transaction
        $db = Database::instance();
        $db->begin();
        try {
            $values = array(
                self::USER_CONFIRMED => TRUE,
                self::USER_REG_DATE => DB::expr('UTC_TIMESTAMP()')
            );
            DB::update(self::USERS_TABLE)
                    ->set($values)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->and_where(self::USER_CONFIRMED, '<>', TRUE)
                    ->execute();
            // delete confirm record
            self::remove_user_record($user_id, self::ENUM_RECORD_USER_CONFIRM);
            // commit transaction
            $db->commit();
            return TRUE;
        } catch (Database_Exception $ex) {
            // rollback transaction
            $db->rollback();
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           recovery methods                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_pass_recovery_record($user_id) {
        try {
            $recovery_hash = md5(microtime());
            // remove user pass recovery
            self::remove_user_record($user_id, self::ENUM_RECORD_PASS_RECOVERY);
            // add user pass recovery record
            $into = array(
                self::USER_ID,
                self::RECORD_HASH,
                self::RECORD_TYPE,
                self::RECORD_EXPIRES
            );
            $values = array(
                (int) $user_id,
                $recovery_hash,
                self::ENUM_RECORD_PASS_RECOVERY,
                DB::expr('ADDDATE(UTC_TIMESTAMP(), INTERVAL ' . self::PASSWORD_RECOVERY_EXPIRES_HOURS . ' HOUR)')
            );
            $result = DB::insert(self::RECORD_TABLE, $into)
                    ->values($values)
                    ->execute();
            $recovery_data = array(
                self::RECORD_ID => $result[0],
                self::RECORD_HASH => $recovery_hash
            );
            // return recovery data
            return $recovery_data;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function get_user_pass_recovery_record($user_id) {
        return self::get_user_record($user_id, Model_Users::ENUM_RECORD_PASS_RECOVERY);
    }

    public static function update_user_password($user_id, $user_pass) {
        try {
            $user_pass_hash = self::hash_user_password($user_pass);
            // remove user auth
            self::remove_user_auth($user_id);
            // remove user pass recovery
            self::remove_user_record($user_id, self::ENUM_RECORD_PASS_RECOVERY);
            // update user password
            DB::update(self::USERS_TABLE)
                    ->set(array(self::USER_PASS => $user_pass_hash))
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         email update methods                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_user_email_update_record($user_id) {
        return self::get_user_record($user_id, self::ENUM_RECORD_EMAIL_UPDATE);
    }

    public static function add_user_email_update_record($user_id) {
        $event_hash = md5(microtime());
        // remove old email update event
        self::remove_user_record($user_id, self::ENUM_RECORD_EMAIL_UPDATE);
        // add email update event
        $into = array(
            self::USER_ID,
            self::RECORD_HASH,
            self::RECORD_TYPE,
            self::RECORD_EXPIRES
        );
        $values = array(
            (int) $user_id,
            $event_hash,
            self::ENUM_RECORD_EMAIL_UPDATE,
            DB::expr('ADDDATE(UTC_TIMESTAMP(), INTERVAL ' . self::PASSWORD_RECOVERY_EXPIRES_HOURS . ' HOUR)')
        );
        DB::insert(self::RECORD_TABLE, $into)
                ->values($values)
                ->execute();
        return $event_hash;
    }

    public static function set_user_new_email($user_id, $user_email) {
        try {
            DB::update(self::USERS_TABLE)
                    ->set(array(self::USER_NEW_EMAIL => $user_email))
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function update_user_email($user_id, $user_email) {
        try {
            $values = array(
                self::USER_NEW_EMAIL => DB::expr('`' . self::USER_EMAIL . '`'),
                self::USER_EMAIL => UTF8::strtolower($user_email),
            );
            DB::update(self::USERS_TABLE)
                    ->set($values)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            // remove old email update event
            self::remove_user_record($user_id, self::ENUM_RECORD_EMAIL_UPDATE);
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          user event methods                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_user_record($user_id, $record_type) {
        self::remove_expired_user_records();
        if (in_array($record_type, self::$EVENT_ENUM_LIST)) {
            $result = DB::select()
                    ->from(self::RECORD_TABLE)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->and_where(self::RECORD_TYPE, '=', $record_type)
                    ->and_where(self::RECORD_EXPIRES, '>', DB::expr('UTC_TIMESTAMP()'))
                    ->execute();
            $as_array = $result->as_array();
            return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
        } else {
            return FALSE;
        }
    }

    private static function remove_user_record($user_id, $record_type) {
        if (in_array($record_type, self::$EVENT_ENUM_LIST)) {
            DB::delete(self::RECORD_TABLE)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->and_where(self::RECORD_TYPE, '=', $record_type)
                    ->execute();
        }
    }

    private static function remove_expired_user_records() {
        DB::delete(self::RECORD_TABLE)
                ->where(self::RECORD_EXPIRES, '<', DB::expr('UTC_TIMESTAMP()'))
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             auth methods                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function auth_user($user_id) {
        try {
            $values[self::USER_AUTH_HASH] = md5(microtime());
            $values[self::USER_AUTH_IP] = Request::$client_ip;
            DB::update(self::USERS_TABLE)
                    ->set($values)
                    ->where(self::USER_ID, '=', (int) $user_id)
                    ->execute();
            return $values[self::USER_AUTH_HASH];
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function remove_user_auth($user_id) {
        DB::update(self::USERS_TABLE)
                ->set(array(self::USER_AUTH_HASH => DB::expr('NULL')))
                ->where(self::USER_ID, '=', (int) $user_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              log methods                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_user_log($user_id, $log_type) {
        if (in_array($log_type, self::$LOG_ENUM_LIST)) {
            $into = array(
                self::USER_ID,
                self::LOG_DATE,
                self::LOG_IP,
                self::LOG_TYPE
            );
            $values = array(
                (int) $user_id,
                DB::expr('UTC_TIMESTAMP()'),
                Request::$client_ip,
                $log_type,
            );
            DB::insert(self::LOG_TABLE, $into)
                    ->values($values)
                    ->execute();
        }
    }

    private static function get_user_log_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $log_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::LOG_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y %H:%i\')', self::LOG_DATE_TZ_FORMAT);
        return array("*", $log_date_tz_format);
    }

    public static function get_user_log($user_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_user_log_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::LOG_TABLE)
                ->where(self::USER_ID, '=', (int) $user_id)
                ->order_by(self::LOG_ID, 'DESC')
                ->execute();
        // result array
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function count_user_log($user_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::LOG_TABLE)
                ->where(self::USER_ID, '=', (int) $user_id)
                ->execute();
        return $result->get('count');
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         user password hash                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function hash_user_password($password) {
        return md5(md5($password));
    }

}