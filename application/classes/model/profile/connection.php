<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Profile_Connection
 *
 * @author Гаврищук
 */
class Model_Profile_Connection extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       connection_profiles table                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const CONNECTION_PROFILES_TABLE = 'connection_profiles';
    const CONNECTION_ID = 'connection_id';
    const CONNECTION_NAME = 'connection_name';
    const CONNECTION_DESCRIPTION = 'connection_description';
    const CONNECTION_CREATED = 'connection_created';
    const CONNECTION_ACTIVE_FLAG = 'connection_active_flag';
    const CONNECTION_PASS_REQUIRED_FLAG = 'connection_pass_required_flag';
    // additional
    const CONNECTION_CREATED_TZ_FORMAT = 'connection_created_tz_format';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       account_config table                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const ACCOUNT_CONFIG_TABLE = 'account_config';
    //const ACCOUNT_ID = 'account_id';
    const ACCOUNT_CALLER_ID = 'account_caller_id';
    const ACCOUNT_USER_NAME = 'account_user_name';
    const ACCOUNT_EXTENSION = 'account_extension';
    const ACCOUNT_PASSWORD = 'account_pass';
    const ACCOUNT_SECRET_PASSWORD = 'account_secret_pass';
    const ACCOUNT_SECURE_FLAG = 'account_secure_flag';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       network_config table                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const NETWORK_CONFIG_TABLE = 'network_config';
    //const NETWORK_ID = 'network_id';
    const NETWORK_SIP_SERVER = 'network_sip_server';
    const NETWORK_SIP_SERVER_PORT = 'network_sip_server_port';
    const NETWORK_PROXY_SERVER = 'network_proxy_server';
    const NETWORK_PROXY_SERVER_PORT = 'network_proxy_server_port';
    const NETWORK_LOCAL_ADDRESS = 'network_local_address';
    const NETWORK_LOCAL_PORT = 'network_local_port';
    const NETWORK_MIN_RTP_PORT = 'network_min_rtp_port';
    const NETWORK_MAX_RTP_PORT = 'network_max_rtp_port';
    const NETWORK_REGISTER_EXPIRES = 'network_register_expires';
    const NETWORK_ANY_LOCAL_PORT_FLAG = 'network_any_local_port_flag';
    const NETWORK_DEFAULT_INTERFACE_FLAG = 'network_default_interface_flag';
    const NETWORK_PROXY_FLAG = 'network_proxy_flag';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          nat_config table                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const NAT_CONFIG_TABLE = 'nat_config';
    //const NAT_ID = 'nat_id';
    const NAT_KEEP_ALIVE = 'nat_keep_alive';
    const NAT_STUN_SERVER = 'nat_stun_server';
    const NAT_STUN_SERVER_PORT = 'nat_stun_server_port';
    const NAT_STUN_LOOKUP_FLAG = 'nat_stun_lookup_flag';
    const NAT_DEFAULT_STUN_SERVER_FLAG = 'nat_default_stun_server_flag';
    const NAT_KEEP_ALIVE_FLAG = 'nat_keep_alive_flag';
    const NAT_SERVER_BIND_FLAG = 'nat_server_bind_flag';
    const NAT_SYMMETRIC_RTP_FLAG = 'nat_symmetric_rtp_flag';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          dtmf_config table                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const DTMF_CONFIG_TABLE = 'dtmf_config';
    //const DTMF_ID = 'dtmf_id';
    const DTMF_METHOD = 'dtmf_method';
    const DTMF_DURATION = 'dtmf_duration';
    const DTMF_PAYLOAD = 'dtmf_payload';

    // DTMF enum
    const ENUM_DTMF_SIP_INFO = 'SIP_INFO';
    const ENUM_DTMF_RFC2833 = 'RFC2833';

    // DTMF enum list
    public static $DTMF_ENUM_LIST = array(
        self::ENUM_DTMF_SIP_INFO,
        self::ENUM_DTMF_RFC2833
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          codec_config table                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const CODEC_CONFIG_TABLE = 'codec_config';
    const CODEC_TYPE = 'codec_type';
    const CODEC_PRIORITY = 'codec_priority';

    // Codec enum
    const ENUM_CODEC_ALAW = 'ALAW';
    const ENUM_CODEC_ULAW = 'ULAW';
    const ENUM_CODEC_GSM = 'GSM';
    const ENUM_CODEC_iLBC = 'iLBC';
    const ENUM_CODEC_G729A = 'G729A';
    const ENUM_CODEC_SPEEX_NB = 'SPEEX_NB';
    const ENUM_CODEC_SPEEX_WB = 'SPEEX_WB';
    const ENUM_CODEC_SPEEX_UWB = 'SPEEX_UWB';
    const ENUM_CODEC_SILK_NB = 'SILK_NB';
    const ENUM_CODEC_SILK_MB = 'SILK_MB';
    const ENUM_CODEC_SILK_WB = 'SILK_WB';
    const ENUM_CODEC_SILK_SWB = 'SILK_SWB';

    // Codec enum list
    public static $CODEC_ENUM_LIST = array(
        self::ENUM_CODEC_ALAW,
        self::ENUM_CODEC_ULAW,
        self::ENUM_CODEC_GSM,
        self::ENUM_CODEC_iLBC,
        self::ENUM_CODEC_G729A,
        self::ENUM_CODEC_SPEEX_NB,
        self::ENUM_CODEC_SPEEX_WB,
        self::ENUM_CODEC_SPEEX_UWB,
        self::ENUM_CODEC_SILK_NB,
        self::ENUM_CODEC_SILK_MB,
        self::ENUM_CODEC_SILK_WB,
        self::ENUM_CODEC_SILK_SWB
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                check methods                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_connection_name_available($connection_name, $account_id, $connection_id = 0) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONNECTION_PROFILES_TABLE)
                ->where(self::CONNECTION_ID, '<>', (int) $connection_id)
                ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->and_where(self::CONNECTION_NAME, '=', $connection_name)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    public static function is_connection_exists($account_id, $connection_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONNECTION_PROFILES_TABLE)
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              count method                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function count_connection_profiles($account_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONNECTION_PROFILES_TABLE)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        return $result->get('count', 0);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                               get methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_connection_profile_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_2;
        $connection_created_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_CREATED . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y\')', self::CONNECTION_CREATED_TZ_FORMAT);
        return array('*', $connection_created_tz_format);
    }

    private static function get_prepared_connection_profiles(&$result) {
        if ($result->count() > 0) {
            $connection_profiles = $result->as_array();
            foreach ($connection_profiles as &$connection_profile) {
                // connection data
                $connection_profile[self::CONNECTION_DESCRIPTION] = self::decode_string($connection_profile[self::CONNECTION_DESCRIPTION]);
                // account config
                $connection_profile[self::ACCOUNT_CALLER_ID] = self::decode_string($connection_profile[self::ACCOUNT_CALLER_ID]);
                $connection_profile[self::ACCOUNT_USER_NAME] = self::decode_string($connection_profile[self::ACCOUNT_USER_NAME]);
                $connection_profile[self::ACCOUNT_EXTENSION] = self::decode_string($connection_profile[self::ACCOUNT_EXTENSION]);
                $connection_profile[self::ACCOUNT_PASSWORD] = self::decode_string($connection_profile[self::ACCOUNT_PASSWORD]);
                $connection_profile[self::ACCOUNT_SECRET_PASSWORD] = self::decode_string($connection_profile[self::ACCOUNT_SECRET_PASSWORD]);
                // network config
                $connection_profile[self::NETWORK_SIP_SERVER] = self::decode_string($connection_profile[self::NETWORK_SIP_SERVER]);
                $connection_profile[self::NETWORK_PROXY_SERVER] = self::decode_string($connection_profile[self::NETWORK_PROXY_SERVER]);
                $connection_profile[self::NETWORK_LOCAL_ADDRESS] = self::decode_string($connection_profile[self::NETWORK_LOCAL_ADDRESS]);
                // nat config
                $connection_profile[self::NAT_STUN_SERVER] = self::decode_string($connection_profile[self::NAT_STUN_SERVER]);
            }
            unset($connection_profile);
            return $connection_profiles;
        } else {
            return FALSE;
        }
    }

    public static function get_connection_profiles($account_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_connection_profile_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::CONNECTION_PROFILES_TABLE)
                ->join(self::ACCOUNT_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::ACCOUNT_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::NETWORK_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::NETWORK_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::NAT_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::NAT_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::DTMF_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::DTMF_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->where(self::CONNECTION_PROFILES_TABLE . '.' . Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->order_by(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, 'DESC')
                ->execute();
        return self::get_prepared_connection_profiles($result);
    }

    public static function get_connection_profile($account_id, $connection_id, $user_timezone = '') {
        $result = DB::select_array(self::get_connection_profile_select_array($user_timezone))
                ->from(self::CONNECTION_PROFILES_TABLE)
                ->join(self::ACCOUNT_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::ACCOUNT_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::NETWORK_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::NETWORK_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::NAT_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::NAT_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->join(self::DTMF_CONFIG_TABLE)
                ->on(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', self::DTMF_CONFIG_TABLE . '.' . self::CONNECTION_ID)
                ->where(self::CONNECTION_PROFILES_TABLE . '.' . Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->and_where(self::CONNECTION_PROFILES_TABLE . '.' . self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
        $connection_profiles = self::get_prepared_connection_profiles($result);
        return (is_array($connection_profiles) && key_exists(0, $connection_profiles)) ? $connection_profiles[0] : FALSE;
    }

    public static function get_connection_codec_config($connection_id) {
        $result = DB::select()
                ->from(self::CODEC_CONFIG_TABLE)
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->order_by(self::CODEC_PRIORITY)
                ->execute();
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Add methods                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_connection_profile($account_id, $connection_data, $account_config, $network_config, $nat_config, $dtmf_config, $codec_types) {
        // begin transaction
        $db = Database::instance();
        $db->begin();
        try {
            $connection_id = self::add_connection_data($account_id, $connection_data);
            self::add_account_config($connection_id, $account_config);
            self::add_network_config($connection_id, $network_config);
            self::add_nat_config($connection_id, $nat_config);
            self::add_dtmf_config($connection_id, $dtmf_config);
            self::add_codec_config($connection_id, $codec_types);
            Model_Profile_Hash::update_connection_profiles_hash($account_id);
            // commit transaction
            $db->commit();
            return TRUE;
        } catch (Database_Exception $ex) {
            // rollback transaction
            $db->rollback();
            return FALSE;
        }
    }

    private static function add_connection_data($account_id, $connection_data) {
        $into = array(
            Model_Profile_Account::ACCOUNT_ID,
            self::CONNECTION_NAME,
            self::CONNECTION_DESCRIPTION, // encoded
            self::CONNECTION_CREATED,
            self::CONNECTION_ACTIVE_FLAG,
            self::CONNECTION_PASS_REQUIRED_FLAG
        );
        $values = array(
            (int) $account_id,
            $connection_data[self::CONNECTION_NAME],
            self::encode_string($connection_data[self::CONNECTION_DESCRIPTION]),
            DB::expr('UTC_TIMESTAMP()'),
            (bool) $connection_data[self::CONNECTION_ACTIVE_FLAG],
            (bool) $connection_data[self::CONNECTION_PASS_REQUIRED_FLAG]
        );
        $result = DB::insert(self::CONNECTION_PROFILES_TABLE, $into)
                ->values($values)
                ->execute();
        return (int) $result[0];
    }

    private static function add_account_config($connection_id, $account_config) {
        $into = array(
            self::CONNECTION_ID,
            self::ACCOUNT_CALLER_ID, // encoded
            self::ACCOUNT_USER_NAME, // encoded
            self::ACCOUNT_EXTENSION, // encoded
            self::ACCOUNT_PASSWORD, // encoded
            self::ACCOUNT_SECRET_PASSWORD, // encoded
            self::ACCOUNT_SECURE_FLAG
        );
        $values = array(
            (int) $connection_id,
            self::encode_string($account_config[self::ACCOUNT_CALLER_ID]),
            self::encode_string($account_config[self::ACCOUNT_USER_NAME]),
            self::encode_string($account_config[self::ACCOUNT_EXTENSION]),
            !empty($account_config[self::ACCOUNT_PASSWORD]) ? self::encode_string($account_config[self::ACCOUNT_PASSWORD]) : DB::expr('NULL'),
            !empty($account_config[self::ACCOUNT_SECRET_PASSWORD]) ? self::encode_string($account_config[self::ACCOUNT_SECRET_PASSWORD]) : DB::expr('NULL'),
            (bool) $account_config[self::ACCOUNT_SECURE_FLAG]
        );
        DB::insert(self::ACCOUNT_CONFIG_TABLE, $into)
                ->values($values)
                ->execute();
    }

    private static function add_network_config($connection_id, $network_config) {
        $into = array(
            self::CONNECTION_ID,
            self::NETWORK_SIP_SERVER, // encoded
            self::NETWORK_SIP_SERVER_PORT,
            self::NETWORK_PROXY_SERVER, // encoded
            self::NETWORK_PROXY_SERVER_PORT,
            self::NETWORK_LOCAL_ADDRESS, // encoded
            self::NETWORK_LOCAL_PORT,
            self::NETWORK_MIN_RTP_PORT,
            self::NETWORK_MAX_RTP_PORT,
            self::NETWORK_REGISTER_EXPIRES,
            self::NETWORK_ANY_LOCAL_PORT_FLAG,
            self::NETWORK_DEFAULT_INTERFACE_FLAG,
            self::NETWORK_PROXY_FLAG
        );
        $values = array(
            (int) $connection_id,
            self::encode_string($network_config[self::NETWORK_SIP_SERVER]),
            (int) $network_config[self::NETWORK_SIP_SERVER_PORT],
            !empty($network_config[self::NETWORK_PROXY_SERVER]) ? self::encode_string($network_config[self::NETWORK_PROXY_SERVER]) : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_PROXY_SERVER_PORT]) ? (int) $network_config[self::NETWORK_PROXY_SERVER_PORT] : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_LOCAL_ADDRESS]) ? self::encode_string($network_config[self::NETWORK_LOCAL_ADDRESS]) : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_LOCAL_PORT]) ? (int) $network_config[self::NETWORK_LOCAL_PORT] : DB::expr('NULL'),
            (int) $network_config[self::NETWORK_MIN_RTP_PORT],
            (int) $network_config[self::NETWORK_MAX_RTP_PORT],
            (int) $network_config[self::NETWORK_REGISTER_EXPIRES],
            (bool) $network_config[self::NETWORK_ANY_LOCAL_PORT_FLAG],
            (bool) $network_config[self::NETWORK_DEFAULT_INTERFACE_FLAG],
            (bool) $network_config[self::NETWORK_PROXY_FLAG]
        );
        DB::insert(self::NETWORK_CONFIG_TABLE, $into)
                ->values($values)
                ->execute();
    }

    private static function add_nat_config($connection_id, $nat_config) {
        $into = array(
            self::CONNECTION_ID,
            self::NAT_KEEP_ALIVE,
            self::NAT_STUN_SERVER, // encoded
            self::NAT_STUN_SERVER_PORT,
            self::NAT_STUN_LOOKUP_FLAG,
            self::NAT_DEFAULT_STUN_SERVER_FLAG,
            self::NAT_KEEP_ALIVE_FLAG,
            self::NAT_SERVER_BIND_FLAG,
            self::NAT_SYMMETRIC_RTP_FLAG
        );
        $values = array(
            (int) $connection_id,
            (int) $nat_config[self::NAT_KEEP_ALIVE],
            !empty($nat_config[self::NAT_STUN_SERVER]) ? self::encode_string($nat_config[self::NAT_STUN_SERVER]) : DB::expr('NULL'),
            !empty($nat_config[self::NAT_STUN_SERVER_PORT]) ? (int) $nat_config[self::NAT_STUN_SERVER_PORT] : DB::expr('NULL'),
            (bool) $nat_config[self::NAT_STUN_LOOKUP_FLAG],
            (bool) $nat_config[self::NAT_DEFAULT_STUN_SERVER_FLAG],
            (bool) $nat_config[self::NAT_KEEP_ALIVE_FLAG],
            (bool) $nat_config[self::NAT_SERVER_BIND_FLAG],
            (bool) $nat_config[self::NAT_SYMMETRIC_RTP_FLAG]
        );
        DB::insert(self::NAT_CONFIG_TABLE, $into)
                ->values($values)
                ->execute();
    }

    private static function add_dtmf_config($connection_id, $dtmf_config) {
        $into = array(
            self::CONNECTION_ID,
            self::DTMF_METHOD,
            self::DTMF_DURATION,
            self::DTMF_PAYLOAD,
        );
        $values = array(
            (int) $connection_id,
            in_array($dtmf_config[self::DTMF_METHOD], self::$DTMF_ENUM_LIST) ? $dtmf_config[self::DTMF_METHOD] : self::ENUM_DTMF_SIP_INFO,
            (int) $dtmf_config[self::DTMF_DURATION],
            (int) $dtmf_config[self::DTMF_PAYLOAD]
        );
        DB::insert(self::DTMF_CONFIG_TABLE, $into)
                ->values($values)
                ->execute();
    }

    private static function add_codec_config($connection_id, $codec_types) {
        $into = array(
            self::CONNECTION_ID,
            self::CODEC_TYPE
        );
        $query = DB::insert(self::CODEC_CONFIG_TABLE, $into);
        foreach (array_unique($codec_types) as $codec_type) {
            if (in_array($codec_type, self::$CODEC_ENUM_LIST)) {
                $query->values(array((int) $connection_id, $codec_type));
            }
        }
        $query->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              update methods                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_connection_profile($account_id, $connection_id, $connection_data, $account_config, $network_config, $nat_config, $dtmf_config, $codec_types) {
        // begin transaction
        $db = Database::instance();
        $db->begin();
        try {
            self::update_connection_data($connection_id, $connection_data);
            self::update_account_config($connection_id, $account_config);
            self::update_network_config($connection_id, $network_config);
            self::update_nat_config($connection_id, $nat_config);
            self::update_dtmf_config($connection_id, $dtmf_config);
            self::update_codec_config($connection_id, $codec_types);
            Model_Profile_Hash::update_connection_profiles_hash($account_id);
            // commit transaction
            $db->commit();
            return TRUE;
        } catch (Database_Exception $ex) {
            // rollback transaction
            $db->rollback();
            return FALSE;
        }
    }

    private static function update_connection_data($connection_id, $connection_data) {
        $into = array(
            self::CONNECTION_NAME,
            self::CONNECTION_DESCRIPTION, // encoded
            self::CONNECTION_ACTIVE_FLAG,
            self::CONNECTION_PASS_REQUIRED_FLAG,
        );
        $values = array(
            $connection_data[self::CONNECTION_NAME],
            self::encode_string($connection_data[self::CONNECTION_DESCRIPTION]),
            (bool) $connection_data[self::CONNECTION_ACTIVE_FLAG],
            (bool) $connection_data[self::CONNECTION_PASS_REQUIRED_FLAG],
        );
        // update
        DB::update(self::CONNECTION_PROFILES_TABLE)
                ->set(array_combine($into, $values))
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
    }

    private static function update_account_config($connection_id, $account_config) {
        $into = array(
            self::ACCOUNT_CALLER_ID, // encoded
            self::ACCOUNT_USER_NAME, // encoded
            self::ACCOUNT_EXTENSION, // encoded
            //self::ACCOUNT_PASSWORD, // encoded
            //self::ACCOUNT_SECRET_PASSWORD, // encoded
            self::ACCOUNT_SECURE_FLAG
        );
        $values = array(
            self::encode_string($account_config[self::ACCOUNT_CALLER_ID]),
            self::encode_string($account_config[self::ACCOUNT_USER_NAME]),
            self::encode_string($account_config[self::ACCOUNT_EXTENSION]),
            //!empty($account_config[self::ACCOUNT_PASSWORD]) ? self::encode_string($account_config[self::ACCOUNT_PASSWORD]) : DB::expr('NULL'),
            //!empty($account_config[self::ACCOUNT_SECRET_PASSWORD]) ? self::encode_string($account_config[self::ACCOUNT_SECRET_PASSWORD]) : DB::expr('NULL'),
            (bool) $account_config[self::ACCOUNT_SECURE_FLAG]
        );
        $combined = array_combine($into, $values);
        if (!empty($account_config[self::ACCOUNT_PASSWORD])) {
            $combined[self::ACCOUNT_PASSWORD] = self::encode_string($account_config[self::ACCOUNT_PASSWORD]);
        }
        if (!empty($account_config[self::ACCOUNT_SECRET_PASSWORD])) {
            $combined[self::ACCOUNT_SECRET_PASSWORD] = self::encode_string($account_config[self::ACCOUNT_SECRET_PASSWORD]);
        }
        // update
        DB::update(self::ACCOUNT_CONFIG_TABLE)
                ->set($combined)
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
    }

    private static function update_network_config($connection_id, $network_config) {
        $into = array(
            self::NETWORK_SIP_SERVER, // encoded
            self::NETWORK_SIP_SERVER_PORT,
            self::NETWORK_PROXY_SERVER, // encoded
            self::NETWORK_PROXY_SERVER_PORT,
            self::NETWORK_LOCAL_ADDRESS, // encoded
            self::NETWORK_LOCAL_PORT,
            self::NETWORK_MIN_RTP_PORT,
            self::NETWORK_MAX_RTP_PORT,
            self::NETWORK_REGISTER_EXPIRES,
            self::NETWORK_ANY_LOCAL_PORT_FLAG,
            self::NETWORK_DEFAULT_INTERFACE_FLAG,
            self::NETWORK_PROXY_FLAG
        );
        $values = array(
            self::encode_string($network_config[self::NETWORK_SIP_SERVER]),
            (int) $network_config[self::NETWORK_SIP_SERVER_PORT],
            !empty($network_config[self::NETWORK_PROXY_SERVER]) ? self::encode_string($network_config[self::NETWORK_PROXY_SERVER]) : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_PROXY_SERVER_PORT]) ? (int) $network_config[self::NETWORK_PROXY_SERVER_PORT] : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_LOCAL_ADDRESS]) ? self::encode_string($network_config[self::NETWORK_LOCAL_ADDRESS]) : DB::expr('NULL'),
            !empty($network_config[self::NETWORK_LOCAL_PORT]) ? (int) $network_config[self::NETWORK_LOCAL_PORT] : DB::expr('NULL'),
            (int) $network_config[self::NETWORK_MIN_RTP_PORT],
            (int) $network_config[self::NETWORK_MAX_RTP_PORT],
            (int) $network_config[self::NETWORK_REGISTER_EXPIRES],
            (bool) $network_config[self::NETWORK_ANY_LOCAL_PORT_FLAG],
            (bool) $network_config[self::NETWORK_DEFAULT_INTERFACE_FLAG],
            (bool) $network_config[self::NETWORK_PROXY_FLAG]
        );
        // update
        DB::update(self::NETWORK_CONFIG_TABLE)
                ->set(array_combine($into, $values))
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
    }

    private static function update_nat_config($connection_id, $nat_config) {
        $into = array(
            self::NAT_KEEP_ALIVE,
            self::NAT_STUN_SERVER, // encoded
            self::NAT_STUN_SERVER_PORT,
            self::NAT_STUN_LOOKUP_FLAG,
            self::NAT_DEFAULT_STUN_SERVER_FLAG,
            self::NAT_KEEP_ALIVE_FLAG,
            self::NAT_SERVER_BIND_FLAG,
            self::NAT_SYMMETRIC_RTP_FLAG
        );
        $values = array(
            (int) $nat_config[self::NAT_KEEP_ALIVE],
            !empty($nat_config[self::NAT_STUN_SERVER]) ? self::encode_string($nat_config[self::NAT_STUN_SERVER]) : DB::expr('NULL'),
            !empty($nat_config[self::NAT_STUN_SERVER_PORT]) ? (int) $nat_config[self::NAT_STUN_SERVER_PORT] : DB::expr('NULL'),
            (bool) $nat_config[self::NAT_STUN_LOOKUP_FLAG],
            (bool) $nat_config[self::NAT_DEFAULT_STUN_SERVER_FLAG],
            (bool) $nat_config[self::NAT_KEEP_ALIVE_FLAG],
            (bool) $nat_config[self::NAT_SERVER_BIND_FLAG],
            (bool) $nat_config[self::NAT_SYMMETRIC_RTP_FLAG]
        );
        // update
        DB::update(self::NAT_CONFIG_TABLE)
                ->set(array_combine($into, $values))
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
    }

    private static function update_dtmf_config($connection_id, $dtmf_config) {
        $into = array(
            self::DTMF_METHOD,
            self::DTMF_DURATION,
            self::DTMF_PAYLOAD,
        );
        $values = array(
            in_array($dtmf_config[self::DTMF_METHOD], self::$DTMF_ENUM_LIST) ? $dtmf_config[self::DTMF_METHOD] : self::ENUM_DTMF_SIP_INFO,
            (int) $dtmf_config[self::DTMF_DURATION],
            (int) $dtmf_config[self::DTMF_PAYLOAD]
        );
        // update
        DB::update(self::DTMF_CONFIG_TABLE)
                ->set(array_combine($into, $values))
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
    }

    private static function update_codec_config($connection_id, $codec_types) {
        DB::delete(self::CODEC_CONFIG_TABLE)
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->execute();
        // add codec config
        self::add_codec_config($connection_id, $codec_types);
    }

    public static function update_connection_profile_status($account_id, $connection_id, $status_flag) {
        DB::update(self::CONNECTION_PROFILES_TABLE)
                ->set(array(self::CONNECTION_ACTIVE_FLAG => $status_flag))
                ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                ->and_where(self::CONNECTION_ACTIVE_FLAG, '<>', $status_flag)
                ->execute();
        // update hash
        Model_Profile_Hash::update_connection_profiles_hash($account_id);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Remove method                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function remove_connection_profile($account_id, $connection_id) {
        try {
            DB::delete(self::CONNECTION_PROFILES_TABLE)
                    ->where(self::CONNECTION_ID, '=', (int) $connection_id)
                    ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                    ->execute();
            // update hash
            Model_Profile_Hash::update_connection_profiles_hash($account_id);
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Encryption methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function encode_string($string) {
        return Encrypt::instance()->encode($string);
    }

    private static function decode_string($string) {
        return Encrypt::instance()->decode($string);
    }

}

?>
