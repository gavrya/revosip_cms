<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Profile_Contact
 *
 * @author Гаврищук
 */
class Model_Profile_Contact extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       contact_profiles table                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const CONTACT_PROFILES_TABLE = 'contact_profiles';
    const CONTACT_ID = 'contact_id';
    const CONTACT_NAME = 'contact_name';
    const CONTACT_PHONE = 'contact_phone';
    const CONTACT_CREATED = 'contact_created';
    // additional
    const CONTACT_CREATED_TZ_FORMAT = 'contact_created_tz_format';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                               check contact                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_contact_phone_available($contact_phone, $account_id, $contact_id = 0) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONTACT_PROFILES_TABLE)
                ->where(self::CONTACT_ID, '<>', (int) $contact_id)
                ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->and_where(self::CONTACT_PHONE, '=', $contact_phone)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    public static function is_contact_exists($account_id, $contact_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONTACT_PROFILES_TABLE)
                ->where(self::CONTACT_ID, '=', (int) $contact_id)
                ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            count contacts                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function count_contact_profiles($account_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::CONTACT_PROFILES_TABLE)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        return $result->get('count', 0);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             get contact                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_contact_profile_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_2;
        $contact_created_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::CONTACT_CREATED . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y\')', self::CONTACT_CREATED_TZ_FORMAT);
        return array("*", $contact_created_tz_format);
    }

    public static function get_contact_profiles($account_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_contact_profile_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::CONTACT_PROFILES_TABLE)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->order_by(self::CONTACT_ID, 'DESC')
                ->execute();
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function get_contact_profile($contact_id, $user_timezone = '') {
        $result = DB::select_array(self::get_contact_profile_select_array($user_timezone))
                ->from(self::CONTACT_PROFILES_TABLE)
                ->where(self::CONTACT_ID, '=', (int) $contact_id)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             add contact                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_contact_profile($account_id, $contact_name, $contact_phone) {
        try {
            $into = array(
                Model_Profile_Account::ACCOUNT_ID,
                self::CONTACT_NAME,
                self::CONTACT_PHONE,
                self::CONTACT_CREATED
            );
            $values = array(
                (int) $account_id,
                $contact_name,
                $contact_phone,
                DB::expr('UTC_TIMESTAMP()')
            );
            // insert
            $result = DB::insert(self::CONTACT_PROFILES_TABLE, $into)
                    ->values($values)
                    ->execute();
            // update hash
            return array(
                'contact_id' => $result[0],
                'contact_profiles_hash' => Model_Profile_Hash::update_contact_profiles_hash($account_id),
            );
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             update contact                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_contact_profile($account_id, $contact_id, $contact_name, $contact_phone) {
        try {
            // values
            $values[self::CONTACT_NAME] = $contact_name;
            $values[self::CONTACT_PHONE] = $contact_phone;
            // update
            $rows = DB::update(self::CONTACT_PROFILES_TABLE)
                    ->set($values)
                    ->where(self::CONTACT_ID, '=', (int) $contact_id)
                    ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                    ->execute();
            // update hash
            if ($rows > 0) {
                return Model_Profile_Hash::update_contact_profiles_hash($account_id);
            } else {
                return Model_Profile_Hash::get_contact_profiles_hash($account_id);
            }
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             remove contact                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function remove_contact_profile($account_id, $contact_id) {
        try {
            DB::delete(self::CONTACT_PROFILES_TABLE)
                    ->where(self::CONTACT_ID, '=', (int) $contact_id)
                    ->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                    ->execute();
            // update hash
            return Model_Profile_Hash::update_contact_profiles_hash($account_id);
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

}

?>
