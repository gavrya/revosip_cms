<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Profile_Settings
 *
 * @author Гаврищук
 */
class Model_Profile_Settings extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                      Settings_profiles table                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const SETTINGS_PROFILES_TABLE = 'settings_profiles';
    const SETTINGS_ID = 'settings_id';
    const SETTINGS_DEFAULT_CONNECTION_ID = 'default_connection_id';
    const SETTINGS_DEFAULT_CONNECTION_FLAG = 'default_connection_flag';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             init method                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function init_account_settings($account_id) {
        $into = array(
            Model_Profile_Account::ACCOUNT_ID,
            self::SETTINGS_DEFAULT_CONNECTION_ID,
            self::SETTINGS_DEFAULT_CONNECTION_FLAG
        );
        $values = array(
            (int) $account_id,
            DB::expr('NULL'),
            FALSE,
        );
        DB::insert(self::SETTINGS_PROFILES_TABLE, $into)->values($values)->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              get method                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_settings_profile($account_id) {
        $result = DB::select()
                ->from(self::SETTINGS_PROFILES_TABLE)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            update method                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_settings_profile($account_id, $default_connection_id, $default_connection_flag) {
        try {
            $values[self::SETTINGS_DEFAULT_CONNECTION_ID] = $default_connection_id > 0 ? (int) $default_connection_id : DB::expr('NULL');
            $values[self::SETTINGS_DEFAULT_CONNECTION_FLAG] = (bool) $default_connection_flag;
            // update
            $rows = DB::update(self::SETTINGS_PROFILES_TABLE)
                    ->set($values)
                    ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                    ->execute();
            // update hash
            if ($rows > 0) {
                return Model_Profile_Hash::update_settings_profiles_hash($account_id);
            } else {
                return Model_Profile_Hash::get_settings_profiles_hash($account_id);
            }
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

}

?>
