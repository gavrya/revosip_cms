<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Profile_Hash
 *
 * @author Гаврищук
 */
class Model_Profile_Hash extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         profiles_hash table                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const HASH_PROFILES_TABLE = 'profiles_hash';
    const HASH_ID = 'hash_id';
    const HASH_CONTACT_PROFILES = 'contact_profiles';
    const HASH_CONNECTION_PROFILES = 'connection_profiles';
    const HASH_SETTINGS_PROFILES = 'settings_profiles';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             init method                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function init_profiles_hash($account_id) {
        $hash = md5(microtime());
        $into = array(
            Model_Profile_Account::ACCOUNT_ID,
            self::HASH_CONTACT_PROFILES,
            self::HASH_CONNECTION_PROFILES,
            self::HASH_SETTINGS_PROFILES,
        );
        $values = array(
            (int) $account_id,
            $hash,
            $hash,
            $hash,
        );
        DB::insert(self::HASH_PROFILES_TABLE, $into)
                ->values($values)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            update methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_contact_profiles_hash($account_id) {
        return self::get_profiles_hash($account_id, self::HASH_CONTACT_PROFILES);
    }

    public static function get_connection_profiles_hash($account_id) {
        return self::get_profiles_hash($account_id, self::HASH_CONNECTION_PROFILES);
    }

    public static function get_settings_profiles_hash($account_id) {
        return self::get_profiles_hash($account_id, self::HASH_SETTINGS_PROFILES);
    }

    private static function get_profiles_hash($account_id, $profile_type) {
        $result = DB::select($profile_type)
                ->from(self::HASH_PROFILES_TABLE)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0][$profile_type] : md5(microtime());
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            update methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_contact_profiles_hash($account_id) {
        return self::update_profiles_hash($account_id, self::HASH_CONTACT_PROFILES);
    }

    public static function update_connection_profiles_hash($account_id) {
        return self::update_profiles_hash($account_id, self::HASH_CONNECTION_PROFILES);
    }

    public static function update_settings_profiles_hash($account_id) {
        return self::update_profiles_hash($account_id, self::HASH_SETTINGS_PROFILES);
    }

    private static function update_profiles_hash($account_id, $profile_type) {
        $hash = MD5(microtime());
        $values = array($profile_type => $hash);
        DB::update(self::HASH_PROFILES_TABLE)
                ->set($values)
                ->where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        return $hash;
    }

}

?>
