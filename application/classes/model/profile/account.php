<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Profile_Account
 *
 * @author Гаврищук
 */
class Model_Profile_Account extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       account_profiles table                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const ACCOUNT_PROFILES_TABLE = 'account_profiles';
    const ACCOUNT_ID = 'account_id';
    const ACCOUNT_NAME = 'account_name';
    const ACCOUNT_USER = 'account_user';
    const ACCOUNT_LOGIN = 'account_login';
    const ACCOUNT_PASS = 'account_pass';
    const ACCOUNT_CREATED = 'account_created';
    const ACCOUNT_ACCESS_HASH = 'account_access_hash';
    const ACCOUNT_EXPIRES = 'account_expires';
    const ACCOUNT_ACTIVE = 'account_active';
    // additional
    const ACCOUNT_CREATED_TZ_FORMAT = 'account_created_tz_format';
    const ACCOUNT_EXPIRED = 'account_expired';
    const ACCOUNT_EXPIRES_TZ_FORMAT = 'account_expires_tz_format';
    const ACCOUNT_EXPIRES_DAYS = 'account_expires_days';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         prolong_history table                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const PROLONG_HISTORY_TABLE = 'prolong_history';
    const PROLONG_ID = 'prolong_id';
    const PROLONG_DATE = 'prolong_date';
    const PROLONG_CREDIT = 'prolong_credit';
    const PROLONG_DAYS = 'prolong_days';
    // additional
    const PROLONG_DATE_TZ_FORMAT = 'prolong_date_tz_format';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         account_log table                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const LOG_TABLE = 'account_log';
    const LOG_ID = 'log_id';
    const LOG_DATE = 'log_date';
    const LOG_IP = 'log_ip';
    const LOG_TYPE = 'log_type';
    // additional
    const LOG_DATE_TZ_FORMAT = 'log_date_tz_format';

    // log enum
    const ENUM_LOG_LOGIN = 'LOGIN';
    const ENUM_LOG_LOGOUT = 'LOGOUT';

    // log enum list
    public static $LOG_ENUM_LIST = array(
        self::ENUM_LOG_LOGIN,
        self::ENUM_LOG_LOGOUT,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                check methods                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_account_login_available($account_login, $user_id, $account_id = 0) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->and_where(self::ACCOUNT_ID, '<>', (int) $account_id)
                ->and_where(self::ACCOUNT_LOGIN, '=', $account_login)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    public static function is_account_name_available($account_name, $user_id, $account_id = 0) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->and_where(self::ACCOUNT_ID, '<>', (int) $account_id)
                ->and_where(self::ACCOUNT_NAME, '=', $account_name)
                ->execute();
        return (bool) $result->get('count') == 0;
    }

    public static function is_account_exists($account_id, $user_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->and_where(Model_Users::USER_ID, '=', (int) $user_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              count method                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function count_account_profiles($user_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->execute();
        return $result->get('count', 0);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              get methods                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_account_profile_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $account_created_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::ACCOUNT_CREATED . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y\')', self::ACCOUNT_CREATED_TZ_FORMAT);
        $account_expired = array('"' . self::ACCOUNT_EXPIRES . '" < UTC_TIMESTAMP()', self::ACCOUNT_EXPIRED);
        $account_expires_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::ACCOUNT_EXPIRES . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y\')', self::ACCOUNT_EXPIRES_TZ_FORMAT);
        $account_expires_days = array('GREATEST(DATEDIFF("' . self::ACCOUNT_EXPIRES . '", UTC_TIMESTAMP()), 0)', self::ACCOUNT_EXPIRES_DAYS);
        return array("*", $account_created_tz_format, $account_expired, $account_expires_tz_format, $account_expires_days);
    }

    public static function get_account_profiles($user_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_account_profile_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->order_by(self::ACCOUNT_ID, 'DESC')
                ->execute();
        // result array
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function get_account_profile($account_id, $user_timezone = '') {
        $result = DB::select_array(self::get_account_profile_select_array($user_timezone))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    public static function get_account_profile_by_login_pass($user_id, $login, $password, $user_timezone = '') {
        $result = DB::select_array(self::get_account_profile_select_array($user_timezone))
                ->from(self::ACCOUNT_PROFILES_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->and_where(self::ACCOUNT_LOGIN, '=', $login)
                ->and_where(self::ACCOUNT_PASS, '=', self::hash_account_password($password))
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              add method                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_account_profile($user_id, $user_registered, $account_name, $account_user, $account_login, $account_pass, $account_active) {
        // begin transaction
        $db = Database::instance();
        $db->begin();
        try {
            $account_pass_hash = self::hash_account_password($account_pass);
            $access_hash = md5(microtime());
            // add account_profile record
            $account_into = array(
                Model_Users::USER_ID,
                self::ACCOUNT_NAME,
                self::ACCOUNT_USER,
                self::ACCOUNT_LOGIN,
                self::ACCOUNT_PASS,
                self::ACCOUNT_CREATED,
                self::ACCOUNT_ACCESS_HASH,
                self::ACCOUNT_EXPIRES,
                self::ACCOUNT_ACTIVE
            );
            $account_values = array(
                (int) $user_id,
                $account_name,
                $account_user,
                $account_login,
                $account_pass_hash,
                DB::expr('UTC_TIMESTAMP()'),
                $access_hash,
                DB::expr('ADDDATE(\'' . $user_registered . '\', INTERVAL 10 YEAR)'), // INTERVAL 2 WEEK
                (boolean) $account_active,
            );
            $account_result = DB::insert(self::ACCOUNT_PROFILES_TABLE, $account_into)
                    ->values($account_values)
                    ->execute();
            // account id
            $account_id = $account_result[0];
            // init account settings
            Model_Profile_Settings::init_account_settings($account_id);
            // init profiles hash
            Model_Profile_Hash::init_profiles_hash($account_id);
            // commit transaction
            $db->commit();
            return TRUE;
        } catch (Database_Exception $ex) {
            // rollback transaction
            $db->rollback();
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              update methods                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function update_account_profile($account_id, $account_name = '', $account_user = '', $account_login = '', $account_pass = '', $account_active = '') {
        try {
            if (!empty($account_name)) {
                $values[self::ACCOUNT_NAME] = $account_name;
            }
            if (!empty($account_user)) {
                $values[self::ACCOUNT_USER] = $account_user;
            }
            if (!empty($account_login)) {
                $values[self::ACCOUNT_LOGIN] = $account_login;
            }
            if (!empty($account_pass)) {
                $values[self::ACCOUNT_PASS] = self::hash_account_password($account_pass);
            }
            if (is_bool($account_active)) {
                $values[self::ACCOUNT_ACTIVE] = (bool) $account_active;
            }
            if (count($values) > 0) {
                // update account
                DB::update(self::ACCOUNT_PROFILES_TABLE)
                        ->set($values)
                        ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                        ->execute();
            }
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function update_account_profile_status($account_id, $status_flag) {
        DB::update(self::ACCOUNT_PROFILES_TABLE)
                ->set(array(self::ACCOUNT_ACTIVE => $status_flag))
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->and_where(self::ACCOUNT_ACTIVE, '<>', $status_flag)
                ->execute();
    }

    public static function update_account_profile_access_hash($account_id, $access_hash) {
        DB::update(self::ACCOUNT_PROFILES_TABLE)
                ->set(array(self::ACCOUNT_ACCESS_HASH => $access_hash))
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              remove method                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function remove_account_profile($account_id) {
        DB::delete(self::ACCOUNT_PROFILES_TABLE)
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              prolong method                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function prolong_account_profile($account_id, $user_id, $prolong_credit, $prolong_days) {
        if ($prolong_credit > 0 && $prolong_days > 0) {
            // begin transaction
            $db = Database::instance();
            $db->begin();
            try {
                $prolong_credit = number_format($prolong_credit, 2, '.', '');
                // update account expires date
                DB::update(self::ACCOUNT_PROFILES_TABLE)
                        ->set(array(self::ACCOUNT_EXPIRES => DB::expr('ADDDATE(UTC_TIMESTAMP(), GREATEST(DATEDIFF(`' . self::ACCOUNT_EXPIRES . '`, UTC_TIMESTAMP()), 0) + ' . $prolong_days . ')')))
                        ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                        ->and_where(Model_Users::USER_ID, '=', (int) $user_id)
                        ->execute();
                // reduce user crerdit
                Model_Users::reduce_user_credit($user_id, $prolong_credit);
                // add prolong history record
                self::add_prolong_history($account_id, $user_id, $prolong_credit, $prolong_days);
                // commit transaction
                $db->commit();
                return TRUE;
            } catch (Database_Exception $ex) {
                // rollback transaction
                $db->rollback();
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          prolong history                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_prolong_history_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $prolong_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::PROLONG_HISTORY_TABLE . '.' . self::PROLONG_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y %H:%i\')', self::PROLONG_DATE_TZ_FORMAT);
        return array("*", $prolong_date_tz_format);
    }

    public static function get_prolong_history($user_id, $user_timezone = '', $account_id = 0, $offset = NULL, $limit = NULL) {
        $query = DB::select_array(self::get_prolong_history_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::PROLONG_HISTORY_TABLE)
                ->join(Model_Profile_Account::ACCOUNT_PROFILES_TABLE, 'LEFT')
                ->on(self::PROLONG_HISTORY_TABLE . '.' . Model_Profile_Account::ACCOUNT_ID, '=', Model_Profile_Account::ACCOUNT_PROFILES_TABLE . '.' . Model_Profile_Account::ACCOUNT_ID)
                ->where(self::PROLONG_HISTORY_TABLE . '.' . Model_Users::USER_ID, '=', (int) $user_id);
        if ($account_id > 0) {
            $query->and_where(self::PROLONG_HISTORY_TABLE . '.' . Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id);
        }
        $result = $query->order_by(self::PROLONG_HISTORY_TABLE . '.' . self::PROLONG_ID, 'DESC')->execute();
        // result array
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    private static function add_prolong_history($account_id, $user_id, $prolong_credit, $prolong_days) {
        $prolong_credit = number_format($prolong_credit, 2, '.', '');
        $into = array(
            Model_Users::USER_ID,
            self::ACCOUNT_ID,
            self::PROLONG_DATE,
            self::PROLONG_CREDIT,
            self::PROLONG_DAYS
        );
        $values = array(
            (int) $user_id,
            (int) $account_id,
            DB::expr('UTC_TIMESTAMP()'),
            $prolong_credit,
            (int) $prolong_days
        );
        DB::insert(self::PROLONG_HISTORY_TABLE, $into)
                ->values($values)
                ->execute();
    }

    public static function count_prolong_history($user_id, $account_id = 0) {
        $query = DB::select(array('COUNT("*")', 'count'))
                ->from(self::PROLONG_HISTORY_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id);
        if ($account_id > 0) {
            $query->and_where(Model_Profile_Account::ACCOUNT_ID, '=', (int) $account_id);
        }
        $result = $query->execute();
        return $result->get('count');
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         account password hash                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function hash_account_password($password) {
        return md5(md5($password));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             account log                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_account_log($account_id, $log_type) {
        if (in_array($log_type, self::$LOG_ENUM_LIST)) {
            $into = array(
                self::ACCOUNT_ID,
                self::LOG_DATE,
                self::LOG_IP,
                self::LOG_TYPE
            );
            $values = array(
                (int) $account_id,
                DB::expr('UTC_TIMESTAMP()'),
                Request::$client_ip,
                $log_type,
            );
            DB::insert(self::LOG_TABLE, $into)
                    ->values($values)
                    ->execute();
        }
    }

    private static function get_account_log_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $log_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::LOG_TABLE . '.' . self::LOG_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y %H:%i\')', self::LOG_DATE_TZ_FORMAT);
        return array('*', $log_date_tz_format);
    }

    public static function get_account_logs($account_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_account_log_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::LOG_TABLE)
                ->where(self::ACCOUNT_ID, '=', (int) $account_id)
                ->order_by(self::LOG_ID, 'DESC')
                ->execute();
        // result array
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function get_user_account_logs($user_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_account_log_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::LOG_TABLE)
                ->join(self::ACCOUNT_PROFILES_TABLE)
                ->on(self::LOG_TABLE . '.' . self::ACCOUNT_ID, '=', self::ACCOUNT_PROFILES_TABLE . '.' . self::ACCOUNT_ID)
                ->where(self::ACCOUNT_PROFILES_TABLE . '.' . Model_Users::USER_ID, '=', (int) $user_id)
                ->order_by(self::LOG_TABLE . '.' . self::LOG_ID, 'DESC')
                ->execute();
        // result array
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function count_user_account_logs($user_id, $account_id = 0) {
        $query = DB::select(array('COUNT("*")', 'count'))
                ->from(self::LOG_TABLE)
                ->join(self::ACCOUNT_PROFILES_TABLE)
                ->on(self::LOG_TABLE . '.' . self::ACCOUNT_ID, '=', self::ACCOUNT_PROFILES_TABLE . '.' . self::ACCOUNT_ID)
                ->where(self::ACCOUNT_PROFILES_TABLE . '.' . Model_Users::USER_ID, '=', (int) $user_id);
        if ($account_id > 0) {
            $query->and_where(self::LOG_TABLE . '.' . self::ACCOUNT_ID, '=', (int) $account_id);
        }
        $result = $query->execute();
        return $result->get('count');
    }

}

?>
