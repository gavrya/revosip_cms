<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Model_Webmoney_Invoice
 *
 * @author Гаврищук
 */
class Model_Webmoney_Invoice extends Model {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       webmoney_invoice table                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const INVOICE_TABLE = 'webmoney_invoice';
    const INVOICE_ID = 'invoice_id';
    const INVOICE_UNIT = 'invoice_unit';
    const INVOICE_PURSE = 'invoice_purse';
    const INVOICE_STATE = 'invoice_state';
    const INVOICE_DATE = 'invoice_date';
    const INVOICE_REQUESTED_AMOUNT = 'invoice_requested_amount';
    const INVOICE_RECEIVED_CREDIT = 'invoice_received_credit';
    const INVOICE_PAYMENT_DATE = 'invoice_payment_date';
    // additional
    const INVOICE_DATE_TZ_FORMAT = 'invoice_date_tz_format';
    const INVOICE_PAYMENT_DATE_TZ_FORMAT = 'invoice_payment_date_tz_format';
    // unit enum
    const ENUM_UNIT_WMZ = 'WMZ';
    const ENUM_UNIT_WMR = 'WMR';
    const ENUM_UNIT_WMU = 'WMU';
    // state enum
    const ENUM_STATE_REQUEST = 'REQUEST';
    const ENUM_STATE_CHECK = 'CHECK';
    const ENUM_STATE_NOTIFY = 'NOTIFY';
    const ENUM_STATE_SUCCESS = 'SUCCESS';
    const ENUM_STATE_FAILURE = 'FAILURE';

    // unit enum list
    public static $UNIT_ENUM_LIST = array(
        self::ENUM_UNIT_WMZ,
        self::ENUM_UNIT_WMR,
        self::ENUM_UNIT_WMU,
    );
    // state enum list
    public static $STATE_ENUM_LIST = array(
        self::ENUM_STATE_REQUEST,
        self::ENUM_STATE_CHECK,
        self::ENUM_STATE_NOTIFY,
        self::ENUM_STATE_SUCCESS,
        self::ENUM_STATE_FAILURE,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       webmoney_payment table                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const PAYMENT_TABLE = 'webmoney_payment';
    const LMI_PAYMENT_NO = 'LMI_PAYMENT_NO';
    const LMI_MODE = 'LMI_MODE';
    const LMI_SYS_INVS_NO = 'LMI_SYS_INVS_NO';
    const LMI_SYS_TRANS_NO = 'LMI_SYS_TRANS_NO';
    const LMI_SYS_TRANS_DATE = 'LMI_SYS_TRANS_DATE';
    const LMI_PAYER_PURSE = 'LMI_PAYER_PURSE';
    const LMI_PAYER_WM = 'LMI_PAYER_WM';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             check methods                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_invoice_exists($invoice_id, $user_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::INVOICE_TABLE)
                ->where(self::INVOICE_ID, '=', (int) $invoice_id)
                ->and_where(Model_Users::USER_ID, '=', (int) $user_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    public static function is_payment_exists($payment_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::PAYMENT_TABLE)
                ->where(self::LMI_PAYMENT_NO, '=', (int) $payment_id)
                ->execute();
        return (bool) $result->get('count') == 1;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             get invoice                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function get_invoice_select_array($user_timezone = '') {
        $user_timezone = in_array($user_timezone, Model_Users::$TIMEZONE_ENUM_LIST) ? $user_timezone : Model_Users::ENUM_TIMEZONE_P_0;
        $invoice_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::INVOICE_TABLE . '.' . self::INVOICE_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y %H:%i\')', self::INVOICE_DATE_TZ_FORMAT);
        $invoice_payment_date_tz_format = array('DATE_FORMAT(CONVERT_TZ("' . self::INVOICE_TABLE . '.' . self::INVOICE_PAYMENT_DATE . '", \'+00:00\', \'' . $user_timezone . '\'), \'%d.%m.%Y %T\')', self::INVOICE_PAYMENT_DATE_TZ_FORMAT);
        return array('*', $invoice_date_tz_format, $invoice_payment_date_tz_format);
    }

    public static function get_invoices($user_id, $user_timezone = '', $offset = NULL, $limit = NULL) {
        $result = DB::select_array(self::get_invoice_select_array($user_timezone))
                ->offset($offset)
                ->limit($limit)
                ->from(self::INVOICE_TABLE)
                ->join(self::PAYMENT_TABLE, 'LEFT')
                ->on(self::INVOICE_TABLE . '.' . self::INVOICE_ID, '=', self::PAYMENT_TABLE . '.' . self::LMI_PAYMENT_NO)
                ->where(self::INVOICE_TABLE . '.' . Model_Users::USER_ID, '=', (int) $user_id)
                ->order_by(self::INVOICE_TABLE . '.' . self::INVOICE_ID, 'DESC')
                ->execute();
        return ($result->count() > 0) ? $result->as_array() : FALSE;
    }

    public static function get_invoice($invoice_id, $user_timezone = '') {
        $result = DB::select_array(self::get_invoice_select_array($user_timezone))
                ->from(self::INVOICE_TABLE)
                ->join(self::PAYMENT_TABLE, 'LEFT')
                ->on(self::INVOICE_TABLE . '.' . self::INVOICE_ID, '=', self::PAYMENT_TABLE . '.' . self::LMI_PAYMENT_NO)
                ->where(self::INVOICE_TABLE . '.' . self::INVOICE_ID, '=', (int) $invoice_id)
                ->execute();
        $as_array = $result->as_array();
        return ($result->count() == 1 && key_exists(0, $as_array)) ? $as_array[0] : FALSE;
    }

    public static function count_invoices($user_id) {
        $result = DB::select(array('COUNT("*")', 'count'))
                ->from(self::INVOICE_TABLE)
                ->where(Model_Users::USER_ID, '=', (int) $user_id)
                ->execute();
        return $result->get('count');
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             add invoice                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function add_invoice($user_id, $invoice_unit, $invoice_purse, $invoice_request_amount) {
        if (in_array($invoice_unit, self::$UNIT_ENUM_LIST)) {
            try {
                $into = array(
                    Model_Users::USER_ID,
                    self::INVOICE_UNIT,
                    self::INVOICE_PURSE,
                    self::INVOICE_STATE,
                    self::INVOICE_DATE,
                    self::INVOICE_REQUESTED_AMOUNT,
                    self::INVOICE_RECEIVED_CREDIT,
                    self::INVOICE_PAYMENT_DATE,
                );
                $values = array(
                    (int) $user_id,
                    $invoice_unit,
                    $invoice_purse,
                    self::ENUM_STATE_REQUEST,
                    DB::expr('UTC_TIMESTAMP()'),
                    number_format($invoice_request_amount, 2, '.', ''),
                    '0.00',
                    DB::expr('NULL'),
                );
                $result = DB::insert(self::INVOICE_TABLE, $into)
                        ->values($values)
                        ->execute();
                return $result[0];
                //return TRUE;
            } catch (Database_Exception $ex) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             add payment                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private static function add_payment_data($invoice_id, $payment_data) {
        try {
            $into = array(
                self::LMI_PAYMENT_NO,
                self::LMI_MODE,
                self::LMI_SYS_INVS_NO,
                self::LMI_SYS_TRANS_NO,
                self::LMI_SYS_TRANS_DATE,
                self::LMI_PAYER_PURSE,
                self::LMI_PAYER_WM,
            );
            $values = array(
                (int) $invoice_id,
                (bool) $payment_data[self::LMI_MODE],
                (int) $payment_data[self::LMI_SYS_INVS_NO],
                (int) $payment_data[self::LMI_SYS_TRANS_NO],
                $payment_data[self::LMI_SYS_TRANS_DATE],
                $payment_data[self::LMI_PAYER_PURSE],
                $payment_data[self::LMI_PAYER_WM],
            );
            DB::insert(self::PAYMENT_TABLE, $into)
                    ->values($values)
                    ->execute();
            return TRUE;
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          remove invoice/payment                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function remove_invoice($invoice_id) {
        DB::delete(self::INVOICE_TABLE)
                ->where(self::INVOICE_ID, '=', (int) $invoice_id)
                ->execute();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             update invoice                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function notify_invoice_payment($invoice_id, $received_credit, $payment_data) {
        try {
            $values = array(
                self::INVOICE_STATE => self::ENUM_STATE_NOTIFY,
                self::INVOICE_RECEIVED_CREDIT => number_format($received_credit, 2, '.', ''),
                self::INVOICE_PAYMENT_DATE => DB::expr('UTC_TIMESTAMP()'),
            );
            // update
            DB::update(self::INVOICE_TABLE)
                    ->set($values)
                    ->where(self::INVOICE_ID, '=', (int) $invoice_id)
                    ->execute();
            self::add_payment_data($invoice_id, $payment_data);
        } catch (Database_Exception $ex) {
            return FALSE;
        }
    }

    public static function update_invoice_state($invoice_id, $invoice_state) {
        if (in_array($invoice_state, self::$STATE_ENUM_LIST)) {
            DB::update(self::INVOICE_TABLE)
                    ->set(array(self::INVOICE_STATE => $invoice_state))
                    ->where(self::INVOICE_ID, '=', (int) $invoice_id)
                    ->execute();
        }
    }

}

?>
