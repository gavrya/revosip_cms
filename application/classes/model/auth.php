<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * 
 */
class Model_Auth extends Model {

    const AUTH_COOKIE = 'user_auth';

    // data
    private $user_data;
    private $user_online_enabled;

    /**
     * Singleton instance
     * @var Model_Auth 
     */
    private static $instance;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('site_enabled');
        $this->user_online_enabled = $config['user_online'];
        //unset($config);
    }

    /**
     * Get instance
     * @return Model_Auth
     */
    public static function instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->init_enabled_config();
        $this->detect_auth();
    }

    private function detect_auth() {
        // get user auth data
        $auth_data = Session::instance('cookie')->get(self::AUTH_COOKIE, FALSE);
        // check user auth data
        if (is_array($auth_data) && key_exists(Model_Users::USER_ID, $auth_data) && key_exists(Model_Users::USER_AUTH_HASH, $auth_data)) {
            // user data
            $user_data = Model_Users::get_user($auth_data[Model_Users::USER_ID], $auth_data[Model_Users::USER_AUTH_HASH]);
            // check user data
            if (is_array($user_data) && $user_data[Model_Users::USER_CONFIRMED] == TRUE && $user_data[Model_Users::USER_BLOCKED] == FALSE) {
                // Set user data
                $this->user_data = $user_data;
                // update user online date
                if ($this->user_online_enabled == TRUE) {
                    Model_Users::update_user_online_date($auth_data[Model_Users::USER_ID]);
                }
            }
        }
    }

    public function login($user_id) {
        if (!$this->logged_in()) {
            // generate user auth hash
            $user_auth_hash = Model_Users::auth_user($user_id);
            // check user auth hash
            if (!empty($user_auth_hash)) {
                // user data
                $user_data = Model_Users::get_user($user_id, $user_auth_hash);
                // check user data
                if (is_array($user_data) && $user_data[Model_Users::USER_CONFIRMED] == TRUE && $user_data[Model_Users::USER_BLOCKED] == FALSE) {
                    // auth data
                    $auth_data = array(
                        Model_Users::USER_ID => $user_id,
                        Model_Users::USER_AUTH_HASH => $user_auth_hash,
                    );
                    // start auth session
                    if (Session::instance('cookie')->set(self::AUTH_COOKIE, $auth_data)) {
                        // add user login history
                        Model_Users::add_user_log($user_data[Model_Users::USER_ID], Model_Users::ENUM_LOG_LOGIN);
                        // Set user data
                        $this->user_data = $user_data;
                        // update user online date
                        if ($this->user_online_enabled == TRUE) {
                            Model_Users::update_user_online_date($user_id);
                        }
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }

    public function logout() {
        if ($this->logged_in()) {
            // clear auth cookie
            Session::instance('cookie')->delete(self::AUTH_COOKIE);
            // remove user auth record
            Model_Users::remove_user_auth($this->user_data[Model_Users::USER_ID]);
            // add user logout history
            Model_Users::add_user_log($this->user_data[Model_Users::USER_ID], Model_Users::ENUM_LOG_LOGOUT);
            // unset user model
            unset($this->user_data);
        }
    }

    public function logged_in() {
        return isset($this->user_data);
    }

    public function get_user_data() {
        return $this->user_data;
    }

}