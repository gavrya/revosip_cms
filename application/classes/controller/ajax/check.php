<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Ajax_Check
 *
 * @author Гаврищук
 */
class Controller_Ajax_Check extends Controller {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                    Registration login check action                    */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_reglogin() {
        if ($this->request->is_ajax()) {
            $login = trim(rawurldecode($this->request->param('login', '')));
            //echo($login);
            if (!Valid::min_length($login, 4) || !Valid::max_length($login, 50) || !Valid::regex($login, Regexpr::USER_LOGIN)) {
                $this->response->body(__('Controller_Ajax_Check.nevernyj_format'));
            } else if (Model_Users::is_user_login_available($login)) {
                $this->response->body(__('Controller_Ajax_Check.dostupen'));
            } else {
                $this->response->body(__('Controller_Ajax_Check.nedostupen'));
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Account login check action                     */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_acclogin() {
        if ($this->request->is_ajax()) {
            $account_login = trim(rawurldecode($this->request->param('login', '')));
            $user_id = $this->request->param('user_id', 0);
            $account_id = $this->request->param('account_id', 0);
            //$this->response->body($account_login);
            if (!Valid::min_length($account_login, 4) || !Valid::max_length($account_login, 50) || !Valid::regex($account_login, Regexpr::ACCOUNT_LOGIN)) {
                $this->response->body(__('Controller_Ajax_Check.nevernyj_format'));
            } else if (Model_Profile_Account::is_account_login_available($account_login, $user_id, $account_id)) {
                $this->response->body(__('Controller_Ajax_Check.dostupen'));
            } else {
                $this->response->body(__('Controller_Ajax_Check.nedostupen'));
            }
        }
    }

}