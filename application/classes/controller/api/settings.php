<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Api_Settings
 *
 * @author Гаврищук
 */
class Controller_Api_Settings extends Controller {

    private $xml_request;
    private $authorization_id;
    private $authorization_access_hash;
    private $settings_hash;
    private $account_profile;
    private $user_data;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_xml_request() {
        $this->xml_request = $this->request->post('request');
    }

    private function init_authorization() {
        if ($this->xml_request) {
            $reader = new XMLReader();
            $reader->XML($this->xml_request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                    $this->authorization_id = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ID));
                    $this->authorization_access_hash = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ACCESS_HASH));
                } else if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                    break;
                }
            }
        }
    }

    private function init_settings_hash() {
        if (isset($this->xml_request)) {
            $this->settings_hash = Api_Xml_Helper::get_profile_hash($this->xml_request, Api_Xml_Constants::ELEMENT_SETTINGS_PROFILE);
        }
    }

    private function init_account_profile() {
        if (isset($this->authorization_id)) {
            $this->account_profile = Model_Profile_Account::get_account_profile($this->authorization_id);
        }
    }

    private function init_user_data() {
        if (is_array($this->account_profile) && key_exists(Model_Users::USER_ID, $this->account_profile)) {
            $this->user_data = Model_Users::get_user($this->account_profile[Model_Users::USER_ID]);
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Sync action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_sync() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_sync();
            $this->process_action_sync();
        }
    }

    private function init_action_sync() {
        $this->init_xml_request();
        $this->init_settings_hash();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_sync() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash) || empty($this->settings_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->settings_hash == Model_Profile_Hash::get_settings_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID])) {
            // success
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_SUCCESS));
        } else {
            // failure
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Load action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_load() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_load();
            $this->process_action_load();
        }
    }

    private function init_action_load() {
        $this->init_xml_request();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_load() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else {
            // get settings hash
            $settings_profiles_hash = Model_Profile_Hash::get_settings_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // get settings profile
            $settings_profile = Model_Profile_Settings::get_settings_profile($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            if (!empty($settings_profiles_hash) && is_array($settings_profile)) {
                // success
                $this->response->body(Api_Xml_Constructor::get_settings_load_success_response($settings_profiles_hash, $settings_profile));
            } else {
                // failure
                $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
            }
        }
    }

}