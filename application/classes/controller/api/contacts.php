<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of contacts
 *
 * @author Гаврищук
 */
class Controller_Api_Contacts extends Controller {

    private $xml_request;
    private $authorization_id;
    private $authorization_access_hash;
    private $contacts_hash;
    private $contact_id;
    private $contact_user_name;
    private $contact_phone_number;
    private $account_profile;
    private $user_data;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_xml_request() {
        $this->xml_request = $this->request->post('request');
    }

    private function init_authorization() {
        if ($this->xml_request) {
            $reader = new XMLReader();
            $reader->XML($this->xml_request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                    $this->authorization_id = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ID));
                    $this->authorization_access_hash = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ACCESS_HASH));
                } else if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                    break;
                }
            }
        }
    }

    private function init_contacts_hash() {
        if (isset($this->xml_request)) {
            $this->contacts_hash = Api_Xml_Helper::get_profile_hash($this->xml_request, Api_Xml_Constants::ELEMENT_CONTACT_PROFILES);
        }
    }

    private function init_contact() {
        if ($this->xml_request) {
            $reader = new XMLReader();
            $reader->XML($this->xml_request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_CONTACT_PROFILE) {
                    $this->contact_id = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ID));
                    $this->contact_user_name = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_USER_NAME));
                    $this->contact_phone_number = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_PHONE_NUMBER));
                } else if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_CONTACT_PROFILE) {
                    break;
                }
            }
        }
    }

    private function init_account_profile() {
        if (isset($this->authorization_id)) {
            $this->account_profile = Model_Profile_Account::get_account_profile($this->authorization_id);
        }
    }

    private function init_user_data() {
        if (is_array($this->account_profile) && key_exists(Model_Users::USER_ID, $this->account_profile)) {
            $this->user_data = Model_Users::get_user($this->account_profile[Model_Users::USER_ID]);
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Sync action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_sync() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_sync();
            $this->process_action_sync();
        }
    }

    private function init_action_sync() {
        $this->init_xml_request();
        $this->init_contacts_hash();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_sync() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash) || empty($this->contacts_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->contacts_hash == Model_Profile_Hash::get_contact_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID])) {
            // success
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_SUCCESS));
        } else {
            // failure
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Load action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_load() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_load();
            $this->process_action_load();
        }
    }

    private function init_action_load() {
        $this->init_xml_request();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_load() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else {
            // contact profiles hash
            $contact_profiles_hash = Model_Profile_Hash::get_contact_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // contact profiles
            $contact_profiles = Model_Profile_Contact::get_contact_profiles($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // success
            $this->response->body(Api_Xml_Constructor::get_contacts_load_success_response($contact_profiles_hash, $contact_profiles));
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Add action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_add() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_add();
            $this->process_action_add();
        }
    }

    private function init_action_add() {
        $this->init_xml_request();
        $this->init_contacts_hash();
        $this->init_contact();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_add() {
        if (empty($this->authorization_id)
                || empty($this->authorization_access_hash)
                || empty($this->contacts_hash)
                || empty($this->contact_user_name)
                || empty($this->contact_phone_number)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if (!preg_match(Regexpr::CONTACT_PHONE_NUMBER, $this->contact_phone_number)
                || UTF8::strlen($this->contact_user_name) > 50
                || UTF8::strlen($this->contact_phone_number) > 100
                || !Model_Profile_Contact::is_contact_phone_available($this->contact_phone_number, $this->account_profile[Model_Profile_Account::ACCOUNT_ID])) {
            // failure
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
        } else {
            // get contact profiles hash
            $contact_profiles_hash = Model_Profile_Hash::get_contact_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // add contact profile result
            $result = Model_Profile_Contact::add_contact_profile($this->account_profile[Model_Profile_Account::ACCOUNT_ID], $this->contact_user_name, $this->contact_phone_number);
            // send response
            if (is_array($result)) {
                // success
                $this->response->body(Api_Xml_Constructor::get_contact_add_success_response($this->contacts_hash == $contact_profiles_hash ? $result['contact_profiles_hash'] : md5(microtime()), $result['contact_id'], $this->contact_user_name, $this->contact_phone_number));
            } else {
                // failure
                $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Modify action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_modify() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_modify();
            $this->process_action_modify();
        }
    }

    private function init_action_modify() {
        $this->init_xml_request();
        $this->init_contacts_hash();
        $this->init_contact();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_modify() {
        if (empty($this->authorization_id)
                || empty($this->authorization_access_hash)
                || empty($this->contacts_hash)
                || empty($this->contact_id)
                || empty($this->contact_user_name)
                || empty($this->contact_phone_number)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if (!preg_match(Regexpr::CONTACT_PHONE_NUMBER, $this->contact_phone_number)
                || UTF8::strlen($this->contact_user_name) > 50
                || UTF8::strlen($this->contact_phone_number) > 100
                || !Model_Profile_Contact::is_contact_exists($this->account_profile[Model_Profile_Account::ACCOUNT_ID], $this->contact_id)) {
            // failure
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
        } else {
            // get contact profiles hash
            $contact_profiles_hash = Model_Profile_Hash::get_contact_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // updated contact profile
            $updated_contact_profiles_hash = Model_Profile_Contact::update_contact_profile($this->account_profile[Model_Profile_Account::ACCOUNT_ID], $this->contact_id, $this->contact_user_name, $this->contact_phone_number);
            // send response
            if (!empty($updated_contact_profiles_hash)) {
                // success
                $this->response->body(Api_Xml_Constructor::get_contacts_success_response($this->contacts_hash == $contact_profiles_hash ? $updated_contact_profiles_hash : md5(microtime())));
            } else {
                // failure
                $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Remove action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_remove() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_remove();
            $this->process_action_remove();
        }
    }

    private function init_action_remove() {
        $this->init_xml_request();
        $this->init_contacts_hash();
        $this->init_contact();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_remove() {
        if (empty($this->authorization_id)
                || empty($this->authorization_access_hash)
                || empty($this->contacts_hash)
                || empty($this->contact_id)
                || empty($this->contact_user_name)
                || empty($this->contact_phone_number)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data)
                || $this->user_data[Model_Users::USER_BLOCKED] == TRUE
                || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE
                || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if (!preg_match(Regexpr::CONTACT_PHONE_NUMBER, $this->contact_phone_number)
                || UTF8::strlen($this->contact_user_name) > 50
                || UTF8::strlen($this->contact_phone_number) > 100
                || !Model_Profile_Contact::is_contact_exists($this->account_profile[Model_Profile_Account::ACCOUNT_ID], $this->contact_id)) {
            // failure
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
        } else {
            // get contact profiles hash
            $contact_profiles_hash = Model_Profile_Hash::get_contact_profiles_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID]);
            // updated contact profile
            $updated_contact_profiles_hash = Model_Profile_Contact::remove_contact_profile($this->account_profile[Model_Profile_Account::ACCOUNT_ID], $this->contact_id);
            // send response
            if (!empty($updated_contact_profiles_hash)) {
                // success
                $this->response->body(Api_Xml_Constructor::get_contacts_success_response($this->contacts_hash == $contact_profiles_hash ? $updated_contact_profiles_hash : md5(microtime())));
            } else {
                // failure
                $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_FAILURE));
            }
        }
    }

}