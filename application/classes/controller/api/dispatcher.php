<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Api_Dispatcher
 *
 * @author Гаврищук
 */
class Controller_Api_Dispatcher extends Controller {

    private $api_secret_key = 'E9Oqk5TotF5s305X';
    private $account_api_enabled;
    private $account_api_gzip;
    private $xml_request;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_account_api_config() {
        $config = Kohana::$config->load('app')->get('account_api');
        $this->account_api_enabled = $config['enabled'];
        $this->account_api_gzip = $config['gzip'];
        //unset($config);
    }

    public function init_xml_request() {
        $this->xml_request = file_get_contents('php://input');
        $this->xml_request = strpos(strtolower($this->request->headers('Content-Encoding')), 'gzip') !== FALSE ? function_exists('gzdecode') ? gzdecode($this->xml_request) : gzinflate(substr($this->xml_request, 10, -8))  : $this->xml_request;
        $this->xml_request = $this->decrypt($this->xml_request, $this->api_secret_key);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Account action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_account() {
        if ($this->request->method() == HTTP_Request::POST && $this->is_valid_headers()) {
            $this->init_account_api_config();
            $this->init_response_headers();
            if ($this->account_api_enabled === TRUE) {
                try {
                    $this->init_xml_request();
                    $this->process_action_account();
                } catch (Exception $ex) {
                    // error
                    $this->send_response(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ERROR));
                }
            }
        }
    }

    private function process_action_account() {
        if (!Api_Xml_Helper::is_xml_request($this->xml_request) || !Api_Xml_Helper::is_valid_request($this->xml_request)) {
            // bad request
            $this->send_response(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (!Api_Xml_Helper::is_valid_protocol($this->xml_request)) {
            // invalid protocol
            $this->send_response(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_INVALID_PROTOCOL));
        } else if (!Api_Xml_Helper::is_valid_version($this->xml_request)) {
            // invalid version
            $this->send_response(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_INVALID_VERSION));
        } else {
            // dispatch
            switch (Api_Xml_Helper::get_request_code($this->xml_request)) {
                // Account profile requests
                case Api_Constants::REQUEST_ACCOUNT_LOGIN:
                    $this->send_response(
                            Request::factory('/api/account/account/login')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_ACCOUNT_LOGOUT:
                    $this->send_response(
                            Request::factory('/api/account/account/logout')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_ACCOUNT_PING:
                    $this->send_response(
                            Request::factory('/api/account/account/ping')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;

                // Settings profile requests
                case Api_Constants::REQUEST_SETTINGS_PROFILE_SYNC:
                    $this->send_response(
                            Request::factory('/api/account/settings/sync')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_SETTINGS_PROFILE_LOAD:
                    $this->send_response(
                            Request::factory('/api/account/settings/load')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;

                // Connections profile requests
                case Api_Constants::REQUEST_CONNECTION_PROFILES_SYNC:
                    $this->send_response(
                            Request::factory('/api/account/connections/sync')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_CONNECTION_PROFILES_LOAD:
                    $this->send_response(
                            Request::factory('/api/account/connections/load')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;

                // Contact profile requests
                case Api_Constants::REQUEST_CONTACT_PROFILES_SYNC:
                    $this->send_response(
                            Request::factory('/api/account/contacts/sync')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_CONTACT_PROFILES_LOAD:
                    $this->send_response(
                            Request::factory('/api/account/contacts/load')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_CONTACT_PROFILE_ADD:
                    $this->send_response(
                            Request::factory('/api/account/contacts/add')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_CONTACT_PROFILE_MODIFY:
                    $this->send_response(
                            Request::factory('/api/account/contacts/modify')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
                case Api_Constants::REQUEST_CONTACT_PROFILE_REMOVE:
                    $this->send_response(
                            Request::factory('/api/account/contacts/remove')
                                    ->method(Request::POST)
                                    ->post('request', $this->xml_request)
                                    ->execute()
                    );
                    break;
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Helper methods                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function is_valid_headers() {
        $content_type = $this->request->headers('Content-Type');
        $accept_charset = $this->request->headers('Accept-Charset');
        $accept = $this->request->headers('Accept');
        //$x_api_hash = $this->request->headers('X-Api-Hash');
        return strpos(strtolower($accept), 'text/xml') !== FALSE && strpos(strtolower($accept_charset), 'utf-8') !== FALSE;
    }

    private function init_response_headers() {
        $this->response->headers('Cache-Control', 'no-cache');
        $this->response->headers('Pragma', 'no-cache');
        $this->response->headers('Expires', '0');
        $this->response->headers('Content-Type', 'text/xml; charset=utf-8');
        //$this->response->headers('Content-Transfer-Encoding', 'base64');
        if ($this->account_api_gzip === TRUE) {
            ob_start("ob_gzhandler");
        }
    }

    public function send_response($response) {
        $this->response->body($this->encrypt($response, $this->api_secret_key));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Security encryption                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function encrypt($input, $key) {
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $input = $this->pkcs5_pad($input, $size);
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }

    private function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    private function decrypt($sStr, $sKey) {
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $sKey, base64_decode($sStr), MCRYPT_MODE_ECB);
        $dec_s = strlen($decrypted);
        $padding = ord($decrypted[$dec_s - 1]);
        $decrypted = substr($decrypted, 0, -$padding);
        return $decrypted;
    }

}