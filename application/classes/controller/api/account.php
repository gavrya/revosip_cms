<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Api_Account
 *
 * @author Гаврищук
 */
class Controller_Api_Account extends Controller {

    private $xml_request;
    private $user_login;
    private $account_login;
    private $account_password;
    private $account_profile;
    private $user_data;
    private $authorization_id;
    private $authorization_access_hash;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_xml_request() {
        $this->xml_request = $this->request->post('request');
    }

    private function init_account_login() {
        if ($this->xml_request) {
            $reader = new XMLReader();
            $reader->XML($this->xml_request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_ACCOUNT) {
                    $this->user_login = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_PROFILE));
                    $this->account_login = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_LOGIN));
                    $this->account_password = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_PASSWORD));
                } else if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_ACCOUNT) {
                    break;
                }
            }
        }
    }

    private function init_authorization() {
        if ($this->xml_request) {
            try {
                $reader = new XMLReader();
                $reader->XML($this->xml_request, 'utf-8');
                while ($reader->read()) {
                    if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                        $this->authorization_id = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ID));
                        $this->authorization_access_hash = UTF8::trim($reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_ACCESS_HASH));
                    } else if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_AUTHORIZATION) {
                        break;
                    }
                }
            } catch (Exception $ex) {
                
            }
        }
    }

    private function init_account_profile_by_login_pass() {
        if (is_array($this->user_data) && isset($this->user_data[Model_Users::USER_ID]) && !empty($this->account_login) && !empty($this->account_password)) {
            $this->account_profile = Model_Profile_Account::get_account_profile_by_login_pass($this->user_data[Model_Users::USER_ID], $this->account_login, $this->account_password);
        }
    }

    private function init_account_profile() {
        if (isset($this->authorization_id)) {
            $this->account_profile = Model_Profile_Account::get_account_profile($this->authorization_id);
        }
    }

    private function init_user_data() {
        if (is_array($this->account_profile) && key_exists(Model_Users::USER_ID, $this->account_profile)) {
            $this->user_data = Model_Users::get_user($this->account_profile[Model_Users::USER_ID]);
        }
    }
    
    private function init_user_data_by_login() {
        if (isset($this->user_login)) {
            $this->user_data = Model_Users::get_user_by_login($this->user_login);
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Login action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_login() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_login();
            $this->process_action_login();
        }
    }

    public function init_action_login() {
        $this->init_xml_request();
        $this->init_account_login();
        $this->init_user_data_by_login();
        $this->init_account_profile_by_login_pass();
    }

    public function process_action_login() {
        if (empty($this->user_login) || empty($this->account_login) || empty($this->account_password)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->user_data) || $this->user_data[Model_Users::USER_LOGIN] != $this->user_login ||  empty($this->account_profile) || $this->account_profile[Model_Profile_Account::ACCOUNT_LOGIN] != $this->account_login) {
            // incorrect login or password
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_INCORRECT_PROFILE_OR_LOGIN_OR_PASSWORD));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data) || $this->user_data[Model_Users::USER_BLOCKED] == TRUE || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else {
            $account_id = $this->account_profile[Model_Profile_Account::ACCOUNT_ID];
            $account_login = $this->account_profile[Model_Profile_Account::ACCOUNT_LOGIN];
            $account_user_name = $this->account_profile[Model_Profile_Account::ACCOUNT_USER];
            $account_access_hash = md5(microtime());
            // update access hash
            Model_Profile_Account::update_account_profile_access_hash($account_id, $account_access_hash);
            // add account login log
            Model_Profile_Account::add_account_log($account_id, Model_Profile_Account::ENUM_LOG_LOGIN);
            // success
            $this->response->body(Api_Xml_Constructor::get_account_login_success_response($account_id, $this->user_login, $account_login, HTML::chars($account_user_name), $account_access_hash));
            // send account expired email notification (todo)
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Logout action                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_logout() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_logout();
            $this->process_action_logout();
        }
    }

    private function init_action_logout() {
        $this->init_xml_request();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_logout() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data) || $this->user_data[Model_Users::USER_BLOCKED] == TRUE || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else {
            // update access hash
            Model_Profile_Account::update_account_profile_access_hash($this->account_profile[Model_Profile_Account::ACCOUNT_ID], md5(microtime()));
            // add account logout log
            Model_Profile_Account::add_account_log($this->account_profile[Model_Profile_Account::ACCOUNT_ID], Model_Profile_Account::ENUM_LOG_LOGOUT);
            // success
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_SUCCESS));
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Ping action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_ping() {
        if (!$this->request->is_initial() && $this->request->method() == HTTP_Request::POST) {
            $this->init_action_ping();
            $this->process_action_ping();
        }
    }

    private function init_action_ping() {
        $this->init_xml_request();
        $this->init_authorization();
        $this->init_account_profile();
        $this->init_user_data();
    }

    private function process_action_ping() {
        if (empty($this->authorization_id) || empty($this->authorization_access_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if (empty($this->account_profile)) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else if (empty($this->authorization_id) || empty($this->authorization_access_hash)) {
            // bad request
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_BAD_REQUEST));
        } else if ($this->authorization_access_hash != $this->account_profile[Model_Profile_Account::ACCOUNT_ACCESS_HASH]) {
            // account already in use
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_ALREADY_IN_USE));
        } else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRED] == TRUE) {
            // account expired
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCOUNT_EXPIRED));
        } else if (empty($this->user_data) || $this->user_data[Model_Users::USER_BLOCKED] == TRUE || $this->user_data[Model_Users::USER_CONFIRMED] == FALSE || $this->account_profile[Model_Profile_Account::ACCOUNT_ACTIVE] == FALSE) {
            // access denied
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_ACCESS_DENIED));
        } else {
            // success
            $this->response->body(Api_Xml_Constructor::get_response(Api_Constants::RESPONSE_SUCCESS));
        }
    }

}