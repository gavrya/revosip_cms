<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Template_Skeleton
 *
 * @author Гаврищук
 */
class Controller_Template_Skeleton extends Controller_Template {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          controller before                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function before() {
        parent::before();
        if ($this->auto_render) {
            $this->template->title = '';
            $this->template->meta_data = array(); // $this->template->meta_data[] = array('http-equiv' => 'Content-Type', 'content' => 'text/html; charset=utf-8')
            $this->template->links = array();
            $this->template->styles = array();
            $this->template->scripts = array();
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          controller after                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function after() {
        if ($this->auto_render) {
            $config = Kohana::$config->load('app')->get('site');
            $this->template->title = !empty($this->template->title) ? $this->template->title . ' | ' . $config['title'] : $config['title'];
            // unset($config);
        }
        parent::after();
    }

}

