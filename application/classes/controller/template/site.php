<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Site extends Controller_Template_Skeleton {

    // template
    public $template = '/template/site/template';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          controller before                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function before() {
        parent::before();
        // set default style and script
        $this->template->styles[Assets::get_bootstrap_css()] = array('rel' => 'stylesheet', 'media' => 'screen');
        $this->template->styles[Assets::get_bootstrap_responsive_css()] = array('rel' => 'stylesheet');
        $this->template->scripts[] = Assets::SCRIPT_JQUERY;
        $this->template->scripts[] = Assets::get_bootstrap_js();
        // template blocks
        $this->template->navigation_bar = View::factory('/template/site/navigation_bar')->set('logged_in', Model_Auth::instance()->logged_in());
        $this->template->content = View::factory('/template/site/content_block');
        $this->template->footer = View::factory('/profiler/stats');
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          show content message                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    protected function show_info_message($message) {
        $this->template->content->content_message = View::factory('/template/message/info')->bind('message', $message);
    }

    protected function show_success_message($message) {
        $this->template->content->content_message = View::factory('/template/message/success')->bind('message', $message);
    }

    protected function show_warning_message($message) {
        $this->template->content->content_message = View::factory('/template/message/warning')->bind('message', $message);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            show content                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    protected function show_content_title($title) {
        $this->template->content->content_title = $title;
    }

    protected function show_content($content) {
        $this->template->content->content = $content;
    }

    protected function show_content_pagination($pagination) {
        $this->template->content->content_pagination = $pagination;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          controller after                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function after() {
        // show windgets
        if (Request::current()->action() != 'conditions') {
            if (Request::current()->directory() == 'site' && Request::current()->controller() != 'news' || Request::current()->action() == 'view') {
                $this->template->widget_news = Request::factory('/widget/news')->execute();
            }
            $this->template->widget_launcher = Request::factory('/launch/launcher')->execute();
        }
        parent::after();
    }

}