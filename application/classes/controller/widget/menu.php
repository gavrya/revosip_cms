<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Widget_Menu
 *
 * @author Гаврищук
 */
class Controller_Widget_Menu extends Controller {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Accounts action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_accounts() {
        // param
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_accounts_menu($action_type);
        }
    }

    private function show_accounts_menu($action_type) {
        // menu label
        $accounts_title_label = __('Controller_Widget_Menu.accounts.akkaunty');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.accounts.spisok_akkauntov'),
            'link' => 'accounts/list',
            'selected' => $action_type == 'list',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.accounts.dobavit_akkaunt'),
            'link' => 'accounts/add',
            'selected' => $action_type == 'add',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.accounts.istoriya_aktivnosti_akkauntov'),
            'link' => 'accounts/log',
            'selected' => $action_type == 'log',
        );
        
        /*
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.accounts.istoriya_prodlenij_akkauntov'),
            'link' => 'accounts/prolongs',
            'selected' => $action_type == 'prolongs',
        );
        */
        
        // show menu
        $this->show_menu($accounts_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Account action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_account() {
        // param
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_account_menu($account_id, $action_type);
        }
    }

    private function show_account_menu($account_id, $action_type) {
        // menu label
        $account_title_label = __('Controller_Widget_Menu.account.akkaunt');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.informaciya_ob_akkaunte'),
            'link' => 'account/' . $account_id . '/info',
            'selected' => $action_type == 'info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.nastrojki_akkaunta'),
            'link' => 'account/' . $account_id . '/settings',
            'selected' => $action_type == 'settings',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.redaktirovat_akkaunt'),
            'link' => 'account/' . $account_id . '/edit',
            'selected' => $action_type == 'edit',
        );
        
        /*
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.prodlit_akkaunt'),
            'link' => 'account/' . $account_id . '/prolong',
            'selected' => $action_type == 'prolong',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.istoriya_prodlenij_akkaunta'),
            'link' => 'account/' . $account_id . '/prolongs',
            'selected' => $action_type == 'prolongs',
        );
        */
        
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.istoriya_aktivnosti_akkaunta'),
            'link' => 'account/' . $account_id . '/log',
            'selected' => $action_type == 'log',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.account.udalit_akkaunt'),
            'link' => 'account/' . $account_id . '/remove',
            'selected' => $action_type == 'remove',
        );
        // show menu
        $this->show_menu($account_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Contacts action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_contacts() {
        // param
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_contacts_menu($account_id, $action_type);
        }
    }

    public function show_contacts_menu($account_id, $action_type) {
        // menu label
        $contacts_title_label = __('Controller_Widget_Menu.contacts.kontakty');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.contacts.spisok_kontaktov'),
            'link' => 'contacts/' . $account_id . '/list',
            'selected' => $action_type == 'list',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.contacts.dobavit_kontakt'),
            'link' => 'contacts/' . $account_id . '/add',
            'selected' => $action_type == 'add',
        );
        // show menu
        $this->show_menu($contacts_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Contact action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_contact() {
        // param
        $account_id = $this->request->param('account_id');
        $contact_id = $this->request->param('contact_id');
        $action_type = $this->request->param('action_type');
        /// show menu
        if (!$this->request->is_initial()) {
            $this->show_contact_menu($account_id, $contact_id, $action_type);
        }
    }

    public function show_contact_menu($account_id, $contact_id, $action_type) {
        // menu label
        $contact_title_label = __('Controller_Widget_Menu.contact.kontakt');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.contact.informaciya_o_kontakte'),
            'link' => 'contact/' . $account_id . '/' . $contact_id . '/info',
            'selected' => $action_type == 'info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.contact.redaktirovat_kontakt'),
            'link' => 'contact/' . $account_id . '/' . $contact_id . '/edit',
            'selected' => $action_type == 'edit',
        );
        // show menu
        $this->show_menu($contact_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Connections action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_connections() {
        // param
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_connections_menu($account_id, $action_type);
        }
    }

    public function show_connections_menu($account_id, $action_type) {
        // menu label
        $connections_title_label = __('Controller_Widget_Menu.connections.profili_podklyucheniya');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.connections.spisok_profilej_podklyucheniya'),
            'link' => 'connections/' . $account_id . '/list',
            'selected' => $action_type == 'list',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.connections.dobavit_profil_podklyucheniya'),
            'link' => 'connections/' . $account_id . '/add',
            'selected' => $action_type == 'add',
        );
        // show menu
        $this->show_menu($connections_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Connection action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_connection() {
        // param
        $account_id = $this->request->param('account_id');
        $connection_id = $this->request->param('connection_id');
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_connection_menu($account_id, $connection_id, $action_type);
        }
    }

    public function show_connection_menu($account_id, $connection_id, $action_type) {
        // menu label
        $connection_title_label = __('Controller_Widget_Menu.connection.profil_podklyucheniya');
        // menu elements
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.connection.informaciya_o_profile_podklyucheniya'),
            'link' => 'connection/' . $account_id . '/' . $connection_id . '/info',
            'selected' => $action_type == 'info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.connection.redaktirovat_profil_podklyucheniya'),
            'link' => 'connection/' . $account_id . '/' . $connection_id . '/edit',
            'selected' => $action_type == 'edit',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.connection.udalit_profil_podklyucheniya'),
            'link' => 'connection/' . $account_id . '/' . $connection_id . '/remove',
            'selected' => $action_type == 'remove',
        );
        // show menu
        $this->show_menu($connection_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Profile action                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_profile() {
        // param info|edit|balance|invoice|invoices|prolongs|log
        $action_type = $this->request->param('action_type');
        // show menu
        if (!$this->request->is_initial()) {
            $this->show_profile_menu($action_type);
        }
    }

    public function show_profile_menu($action_type) {
        // menu label
        $profile_title_label = __('Controller_Widget_Menu.profile.profil');
        // menu elements
        /*
          if ($action_type == 'email') {
          $elements[] = array(
          'label' => 'Сменить email адрес',
          'link' => 'profile/email',
          'selected' => $action_type == 'email',
          );
          }
         */
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.profile.redaktirovat_profil'),
            'link' => 'profile/edit',
            'selected' => $action_type == 'edit',
        );
        
        /*
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.profile.balans_profilya'),
            'link' => 'profile/balance',
            'selected' => $action_type == 'balance',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.profile.popolnit_balans_profilya'),
            'link' => 'profile/invoice',
            'selected' => $action_type == 'invoice',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.profile.istoriya_proplat'),
            'link' => 'profile/invoices',
            'selected' => $action_type == 'invoices',
        );
        */
        
        $elements[] = array(
            'label' => __('Controller_Widget_Menu.profile.istoriya_aktivnosti'),
            'link' => 'profile/log',
            'selected' => $action_type == 'log',
        );
        // show menu
        $this->show_menu($profile_title_label, $elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Show menu                                 */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu($menu_title, $elements) {
        $this->response->body(View::factory('/widget/menu/menu')->bind('title', $menu_title)->bind('elements', $elements));
    }

}