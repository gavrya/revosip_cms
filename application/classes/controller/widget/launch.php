<?php



defined('SYSPATH') or die('No direct script access.');



/**

 * Description of Controller_Widget_Launch

 *

 * @author Гаврищук

 */

class Controller_Widget_Launch extends Controller {



    private $app_release_date;

    private $app_version;

    private $app_build;

    private $app_launch_enabled;



    // data



    /* --------------------------------------------------------------------- */

    /*                                                                       */

    /*                                Init                                   */

    /*                                                                       */

    /* --------------------------------------------------------------------- */



    private function init_application_config() {

        $config = Kohana::$config->load('app')->get('application');

        $this->app_release_date = $config['release_date'];

        $this->app_version = $config['version'];

        $this->app_build = $config['build'];

        $this->app_launch_enabled = $config['launch_enabled'];

        //unset($config);

    }



    /* --------------------------------------------------------------------- */

    /*                                                                       */

    /*                          Show versions action                         */

    /*                                                                       */

    /* --------------------------------------------------------------------- */



    public function action_launcher() {

        if (!$this->request->is_initial()) {

            $this->init_application_config();

            if ($this->app_launch_enabled) {

                $this->response->body(

                        View::factory('/widget/launch/launch_launcher')

                                ->bind('app_release_date', $this->app_release_date)

                                ->bind('app_version', $this->app_version)

                                ->bind('app_build', $this->app_build)

                );

            }

        }

    }



    /* --------------------------------------------------------------------- */

    /*                                                                       */

    /*                          Generate jnlp action                         */

    /*                                                                       */

    /* --------------------------------------------------------------------- */



    public function action_jnlp() {

        $this->response->headers(array(

            'Expires' => '0',

            'Pragma' => 'no-cache',

        ));
	//  file based jnlp

	$this->response->send_file('java/revosip.jnlp', 'revosip.jnlp', array('mime_type' => 'application/x-java-jnlp-file'));

       // content based jnlp

       //$this->response->body(View::factory('/widget/launch/launch_jnlp')->render());

       //$this->response->send_file(TRUE, 'revosip.jnlp', array('mime_type' => 'application/x-java-jnlp-file'));

    }



    /* --------------------------------------------------------------------- */

    /*                                                                       */

    /*                           Show applet action                          */

    /*                                                                       */

    /* --------------------------------------------------------------------- */



    public function action_applet() {

        $this->response->body(View::factory('/widget/launch/launch_applet'));

    }



    /* --------------------------------------------------------------------- */

    /*                                                                       */

    /*                       Download latest revosip jar                     */

    /*                                                                       */

    /* --------------------------------------------------------------------- */



    public function action_download() {

        $filename = file_get_contents('java/version.txt');

        $file_path = 'java/' . $filename;

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= filemtime($file_path)) {

            header('HTTP/1.1 304 Not Modified');

            exit;

        } else {

            $this->response->headers(array(

                'Date' => gmdate('D, d M Y H:i:s', time()) . ' GMT',

                'Last-Modified' => gmdate('D, d M Y H:i:s', filemtime($file_path)) . ' GMT',

            ));

            $this->response->send_file($file_path, $filename, array('mime_type' => 'application/java-archive'));

        }

    }



}