<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Widget_Navigation
 *
 * @author Гаврищук
 */
class Controller_Widget_Navigation extends Controller {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Accounts action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_accounts() {
        $action_type = $this->request->param('action_type');
        if (!$this->request->is_initial()) {
            $this->show_accounts_navigation($action_type);
        }
    }

    private function show_accounts_navigation($action_type) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.accounts.akkaunty'),
            'link' => 'accounts/list',
        );
        if ($action_type == 'add') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.accounts.dobavlenie_akkaunta'),
                'link' => 'accounts/add',
            );
        } else if ($action_type == 'log') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.accounts.istoriya_aktivnosti_akkauntov'),
                'link' => 'accounts/log',
            );
        } else if ($action_type == 'prolongs') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.accounts.istoriya_prodlenij_akkauntov'),
                'link' => 'accounts/prolongs',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Account action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_account() {
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        $account_label = $this->request->query('account_label');
        if (!$this->request->is_initial()) {
            $this->show_account_navigation($account_id, $action_type, $account_label);
        }
    }

    private function show_account_navigation($account_id, $action_type, $account_label) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.account.akkaunty'),
            'link' => 'accounts/list',
        );
        $elements[] = array(
            'label' => empty($account_label) ? __('Controller_Widget_Navigation.account.akkaunt') : HTML::chars($account_label),
            'link' => 'account/' . $account_id . '/info',
        );
        if ($action_type == 'settings') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.izmenenie_nastroek_akkaunta'),
                'link' => 'account/' . $account_id . '/settings',
            );
        } elseif ($action_type == 'edit') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.redaktirovanie_akkaunta'),
                'link' => 'account/' . $account_id . '/edit',
            );
        } elseif ($action_type == 'prolong') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.prodlenie_akkaunta'),
                'link' => 'account/' . $account_id . '/prolong',
            );
        } elseif ($action_type == 'prolongs') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.istoriya_prodlenij_akkaunta'),
                'link' => 'account/' . $account_id . '/prolongs',
            );
        } elseif ($action_type == 'log') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.istoriya_aktivnosti_akkaunta'),
                'link' => 'account/' . $account_id . '/log',
            );
        } elseif ($action_type == 'remove') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.account.udalenie_akkaunta'),
                'link' => 'account/' . $account_id . '/remove',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Contacts action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_contacts() {
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        $account_label = $this->request->query('account_label');
        if (!$this->request->is_initial()) {
            $this->show_contacts_navigation($account_id, $action_type, $account_label);
        }
    }

    public function show_contacts_navigation($account_id, $action_type, $account_label) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.contacts.akkaunty'),
            'link' => 'accounts/list',
        );
        $elements[] = array(
            'label' => empty($account_label) ? __('Controller_Widget_Navigation.contacts.akkaunt') : HTML::chars($account_label),
            'link' => 'account/' . $account_id . '/info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.contacts.kontakty'),
            'link' => 'contacts/' . $account_id . '/list',
        );
        if ($action_type == 'add') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.contacts.dobavlenie_kontakta'),
                'link' => 'contacts/' . $account_id . '/add',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Contact action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_contact() {
        $account_id = $this->request->param('account_id');
        $contact_id = $this->request->param('contact_id');
        $action_type = $this->request->param('action_type');
        $account_label = $this->request->query('account_label');
        $contact_label = $this->request->query('contact_label');
        if (!$this->request->is_initial()) {
            $this->show_contact_navigation($account_id, $contact_id, $action_type, $account_label, $contact_label);
        }
    }

    public function show_contact_navigation($account_id, $contact_id, $action_type, $account_label, $contact_label) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.contact.akkaunty'),
            'link' => 'accounts/list',
        );
        $elements[] = array(
            'label' => empty($account_label) ? __('Controller_Widget_Navigation.contact.akkaunt') : HTML::chars($account_label),
            'link' => 'account/' . $account_id . '/info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.contact.kontakty'),
            'link' => 'contacts/' . $account_id . '/list',
        );
        $elements[] = array(
            'label' => empty($contact_label) ? __('Controller_Widget_Navigation.contact.kontakt') : HTML::chars($contact_label),
            'link' => 'contact/' . $account_id . '/' . $contact_id . '/info',
        );
        if ($action_type == 'edit') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.contact.redaktirovanie_kontakta'),
                'link' => 'contact/' . $account_id . '/' . $contact_id . '/edit',
            );
        } elseif ($action_type == 'remove') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.contact.udalenie_kontakta'),
                'link' => 'contact/' . $account_id . '/' . $contact_id . '/remove',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Connections action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_connections() {
        $account_id = $this->request->param('account_id');
        $action_type = $this->request->param('action_type');
        $account_label = $this->request->query('account_label');
        if (!$this->request->is_initial()) {
            $this->show_connections_navigation($account_id, $action_type, $account_label);
        }
    }

    public function show_connections_navigation($account_id, $action_type, $account_label) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.connections.akkaunty'),
            'link' => 'accounts/list',
        );
        $elements[] = array(
            'label' => empty($account_label) ? __('Controller_Widget_Navigation.connections.akkaunt') : HTML::chars($account_label),
            'link' => 'account/' . $account_id . '/info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.connections.profili_podklyucheniya'),
            'link' => 'connections/' . $account_id . '/list',
        );
        if ($action_type == 'add') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.connections.dobavlenie_profilya_podklyucheniya'),
                'link' => 'connections/' . $account_id . '/add',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Connection action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_connection() {
        $account_id = $this->request->param('account_id');
        $connection_id = $this->request->param('connection_id');
        $action_type = $this->request->param('action_type');
        $account_label = $this->request->query('account_label');
        $connection_label = $this->request->query('connection_label');
        if (!$this->request->is_initial()) {
            $this->show_connection_navigation($account_id, $connection_id, $action_type, $account_label, $connection_label);
        }
    }

    public function show_connection_navigation($account_id, $connection_id, $action_type, $account_label, $connection_label) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.connection.akkaunty'),
            'link' => 'accounts/list',
        );
        $elements[] = array(
            'label' => empty($account_label) ? __('Controller_Widget_Navigation.connection.akkaunt') : HTML::chars($account_label),
            'link' => 'account/' . $account_id . '/info',
        );
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.connection.profili_podklyuchenij'),
            'link' => 'connections/' . $account_id . '/list',
        );
        $elements[] = array(
            'label' => empty($connection_label) ? __('Controller_Widget_Navigation.connection.profil_podklyucheniya') : HTML::chars($connection_label),
            'link' => 'connection/' . $account_id . '/' . $connection_id . '/info',
        );
        if ($action_type == 'edit') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.connection.redaktirovanie_profilya_podklyucheniya'),
                'link' => 'connection/' . $account_id . '/' . $connection_id . '/edit',
            );
        } elseif ($action_type == 'remove') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.connection.udalenie_profilya_podklyucheniya'),
                'link' => 'connection/' . $account_id . '/' . $connection_id . '/remove',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Profile action                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_profile() {
        $action_type = $this->request->param('action_type');
        if (!$this->request->is_initial()) {
            $this->show_profile_navigation($action_type);
        }
    }

    public function show_profile_navigation($action_type) {
        $elements[] = array(
            'label' => __('Controller_Widget_Navigation.profile.profil'),
            'link' => 'profile',
        );
        if ($action_type == 'edit') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.redaktirovanie_profilya'),
                'link' => 'profile/edit',
            );
        } elseif ($action_type == 'email') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.smena_email_adresa_profilya'),
                'link' => 'profile/email',
            );
        } elseif ($action_type == 'balance') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.balans_profilya'),
                'link' => 'profile/balance',
            );
        } elseif ($action_type == 'invoice') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.popolnenie_balansa_profilya'),
                'link' => 'profile/invoice',
            );
        } elseif ($action_type == 'invoices') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.istoriya_proplat'),
                'link' => 'profile/invoices',
            );
        } elseif ($action_type == 'log') {
            $elements[] = array(
                'label' => __('Controller_Widget_Navigation.profile.istoriya_aktivnosti'),
                'link' => 'profile/log',
            );
        }
        $this->show_navigation($elements);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          Show navigation                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_navigation(&$elements) {
        $this->response->body(View::factory('/widget/navigation/navigation_block')->bind('elements', $elements));
    }

}