<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Widget_News
 *
 * @author Гаврищук
 */
class Controller_Widget_News extends Controller {

    // data
    private $user_data;
    private $news_list;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_news_list($offset, $limit) {
        $this->news_list = Model_news::get_news_list(strtoupper(I18n::lang()), $this->user_data ? $this->user_data[Model_Users::USER_TIMEZONE] : '', $offset, $limit);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             News action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_list() {
        if (!$this->request->is_initial()) {
            $this->init_action_list();
            $this->show_news_list();
        }
    }

    private function init_action_list() {
        $this->init_user_data();
        $this->init_news_list(0, 6);
    }

    private function show_news_list() {
        if (!empty($this->news_list)) {
            $this->response->body(View::factory('/widget/news/news_list')->bind('news_list', $this->news_list));
        }
    }

}