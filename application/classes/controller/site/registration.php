<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Site_Registration
 *
 * @author Гаврищук
 */
class Controller_Site_Registration extends Controller_Template_Site {

    // data
    private $captcha;
    // enabled config
    private $registration_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_captcha() {
        $this->captcha = Captcha::instance();
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('site_enabled');
        $this->registration_enabled = $config['registration'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                      User registeration action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_registration() {
        $this->init_action_registration();
        $this->set_action_registration_html_data();
        $this->process_action_registration();
        $this->show_content_title(__('Controller_Site_Registration.registration.content_title'));
    }

    private function init_action_registration() {
        $this->init_captcha();
        $this->init_enabled_config();
    }

    private function set_action_registration_html_data() {
        $this->template->title = __('Controller_Site_Registration.registration.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Registration.registration.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Registration.registration.description'));
    }

    private function process_action_registration() {
        if (!$this->registration_enabled) {
            $this->show_warning_message(__('Controller_Site_Registration.registration.vozmozhnost_registracii_na_sajte_vremenno_priostanovlena'));
        } else if (Model_Auth::instance()->logged_in()) {
            $this->request->redirect();
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_registration_post_form();
        } else {
            $this->show_registration_form();
        }
    }

    private function process_registration_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_registration_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!key_exists(Field::CAPTCHA, $post) || !Captcha::valid($post[Field::CAPTCHA])) {
            $this->show_registration_form(__('Controller.vvedite_pravilnyj_proverochnyj_kod'));
        } else {
            $this->process_registration_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_registration_form_validation($post) {
        // registration validator
        $form_validator = Validation::factory($post);
        // form validator
        $form_validator
                ->rule(Field::USER_LOGIN, 'not_empty')
                ->rule(Field::USER_LOGIN, 'min_length', array(':value', 4))
                ->rule(Field::USER_LOGIN, 'max_length', array(':value', 50))
                ->rule(Field::USER_LOGIN, 'Model_Users::is_user_login_available')
                ->rule(Field::USER_LOGIN, 'regex', array(':value', Regexpr::USER_LOGIN));
        // pass validation rules
        $form_validator
                ->rule(Field::USER_PASSWORD, 'not_empty')
                ->rule(Field::USER_PASSWORD, 'min_length', array(':value', 5))
                ->rule(Field::USER_PASSWORD, 'max_length', array(':value', 50));
        // passconf validation rules
        $form_validator
                ->rule(Field::USER_REPASSWORD, 'not_empty')
                ->rule(Field::USER_REPASSWORD, 'matches', array(':validation', Field::USER_REPASSWORD, Field::USER_PASSWORD));
        // name validation rules
        $form_validator
                ->rule(Field::USER_NAME, 'not_empty')
                ->rule(Field::USER_NAME, 'max_length', array(':value', 50)); //->rule(Field::USER_NAME, 'regex', array(':value', '/^([a-zа-яё0-9 ])+$/ui'));
        // email validation rules
        $form_validator
                ->rule(Field::USER_EMAIL, 'not_empty')
                ->rule(Field::USER_EMAIL, 'email')
                ->rule(Field::USER_EMAIL, 'Model_Users::is_user_email_available');
        // language validation rules
        $form_validator
                ->rule(Field::USER_LANGUAGE, 'not_empty')
                ->rule(Field::USER_LANGUAGE, 'in_array', array(':value', Model_Users::$LANGUAGE_ENUM_LIST));
        // timezone validation rules
        $form_validator
                ->rule(Field::USER_TIMEZONE, 'not_empty')
                ->rule(Field::USER_TIMEZONE, 'in_array', array(':value', Model_Users::$TIMEZONE_ENUM_LIST));
        // accept validation rules
        $form_validator
                ->rule(Field::USER_ACCEPT, 'not_empty');
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_registration_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/registration');
            // repopulate reg form with errors
            $this->show_registration_form(reset($errors));
        }
    }

    private function process_registration_validated_form($post) {
        // registration data
        $register_data = Model_Users::register_user($post[Field::USER_LOGIN], $post[Field::USER_PASSWORD], $post[Field::USER_NAME], $post[Field::USER_EMAIL], $post[Field::USER_LANGUAGE], $post[Field::USER_TIMEZONE]);
        // check result send register confirm email
        if (is_array($register_data) && Emailer::instance()->send_registration_confirm_link($register_data[Model_Users::USER_ID], $post[Field::USER_NAME], $register_data[Model_Users::RECORD_HASH], $register_data[Model_Users::USER_EMAIL])) {
            $this->show_info_message(__('Controller_Site_Registration.registration.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_registracii'));
        } else {
            if (is_array($register_data) && key_exists(Model_Users::USER_ID, $register_data)) {
                Model_Users::remove_user($register_data[Model_Users::USER_ID]);
            }
            $this->show_registration_form(__('Controller_Site_Registration.registration.nevozmozhno_vypolnit_registraciyu,_povtorite_popytku'));
        }
    }

    private function show_registration_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/site/registration/registration_form')
                        ->bind('captcha', $this->captcha)
                        ->set('registration_timezones', Kohana::message(I18n::lang() . '/site/registration', 'registration_timezones'))
                        ->set('registration_languages', Kohana::message(I18n::lang() . '/site/registration', 'registration_languages'))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Register confirm action                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_confirm() {
        $user_id = $this->request->param('user_id');
        $confirm_hash = $this->request->param('confirm_hash');
        $user_data = Model_Users::get_user($user_id);
        // set page data
        $this->set_action_confirm_html_data();
        // content header
        $this->show_content_title(__('Controller_Site_Registration.confirm.content_title'));
        // process
        $this->process_action_confirm($user_id, $confirm_hash, $user_data);
    }

    private function set_action_confirm_html_data() {
        $this->template->title = __('Controller_Site_Registration.confirm.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Registration.confirm.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Registration.confirm.description'));
    }

    private function process_action_confirm($user_id, $confirm_hash, $user_data) {
        if (is_array($user_data)) {
            $this->process_registration_confirm($user_id, $confirm_hash, $user_data);
        } else {
            $this->show_warning_message(__('Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu'));
        }
    }

    private function process_registration_confirm($user_id, $confirm_hash, $user_data) {
        if (key_exists(Model_Users::USER_CONFIRMED, $user_data) && $user_data[Model_Users::USER_CONFIRMED] == TRUE) {
            //$this->show_success_message('Регистрация пользователя подтверждена, Вы можете войти на сайт используя логин и пароль');
            $this->request->redirect('/login');
        } else {
            $confirm_record = Model_Users::get_user_confirm_record($user_id);
            // check confirmation hash
            if (key_exists(Model_Users::RECORD_HASH, $confirm_record) && $confirm_record[Model_Users::RECORD_HASH] == $confirm_hash) {
                $this->confirm_user_registration($user_id);
				// Send user registration notification
				Email::factory('User registration', 'New user registration: ' . $user_data[Model_Users::USER_NAME] . ' email: ' . $user_data[Model_Users::USER_EMAIL])
                        ->to('revosip@gmail.com')
                        ->from('robot@revosip.com', 'Revosip')
                        ->send();
            } else {
                $this->show_warning_message(__('Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_srok_podtverzhdeniya_registracii_istek'));
            }
        }
    }

    private function confirm_user_registration($user_id) {
        if (Model_Users::confirm_user_registration($user_id)) {
            $this->show_success_message(__('Controller_Site_Registration.confirm.registraciya_polzovatelya_podtverzhdena,_vy_mozhete_vojti_na_sajt_ispolzuya_login_i_parol'));
        } else {
            $this->show_warning_message(__('Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_povtorite_popytku'));
        }
    }

}