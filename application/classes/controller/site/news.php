<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Site_News
 *
 * @author Гаврищук
 */
class Controller_Site_News extends Controller_Template_Site {

    // data
    private $user_data;
    private $news_id;
    private $magic_password;
    private $admin_id;
    private $pagination;
    private $news_count;
    private $news_list;
    private $news;
    private $news_contents;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_param_data() {
        $this->news_id = $this->request->param('news_id');
        if (!Model_News::is_news_exists($this->news_id)) {
            throw new HTTP_Exception_404('News does not exist');
        }
    }

    private function init_site_config() {
        $config = Kohana::$config->load('app')->get('site');
        $this->magic_password = $config['magic_password'];
        $this->admin_id = $config['admin_id'];
        if (!$this->user_data || $this->user_data[Model_Users::USER_ID] != $this->admin_id) {
            throw new HTTP_Exception_404('Not allowed');
        }
        //unset($config);
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
        ));
    }

    private function init_news_count() {
        $this->news_count = Model_news::count_news();
    }

    private function init_news_list($offset, $limit) {
        $this->news_list = Model_news::get_news_list(strtoupper(I18n::lang()), $this->user_data ? $this->user_data[Model_Users::USER_TIMEZONE] : '', $offset, $limit);
    }

    private function init_news() {
        $this->news = Model_news::get_news($this->news_id, strtoupper(I18n::lang()), $this->user_data ? $this->user_data[Model_Users::USER_TIMEZONE] : '');
    }

    private function init_news_contents() {
        $this->news_contents = Model_news::get_news_contents($this->news_id);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             News list action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_list() {
        $this->init_action_list();
        $this->set_action_list_html_data();
        $this->show_news_list();
        $this->show_content_title(__('Controller_Site_News.list.content_title'));
    }

    private function init_action_list() {
        $this->init_user_data();
        $this->init_news_count();
        $this->init_pagination($this->news_count, 6);
        $this->init_news_list($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_list_html_data() {
        $this->template->title = __('Controller_Site_News.list.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_News.list.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_News.list.description'));
    }

    private function show_news_list() {
        if (empty($this->news_list)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/site/news/news_list')->bind('news_list', $this->news_list));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             News view action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_view() {
        $this->init_action_view();
        $this->set_action_view_html_data();
        $this->show_news();
    }

    private function init_action_view() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_news();
    }

    private function set_action_view_html_data() {
        $this->template->title = $this->news[Model_News::NEWS_TITLE];
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => $this->news[Model_News::NEWS_META_KEYWORDS]);
        $this->template->meta_data[] = array('name' => 'description', 'content' => $this->news[Model_News::NEWS_META_DESCRIPTIONS]);
    }

    private function show_news() {
        if (empty($this->news)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/site/news/news_view')->bind('news', $this->news));
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             News add action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_add() {
        $this->init_action_add();
        $this->set_action_add_html_data();
        $this->process_action_add();
        $this->show_content_title(__('Controller_Site_News.add.content_title'));
    }

    private function init_action_add() {
        $this->init_user_data();
        $this->init_site_config();
    }

    private function set_action_add_html_data() {
        $this->template->title = __('Controller_Site_News.add.title');
    }

    private function process_action_add() {
        if ($this->request->method() == HTTP_Request::POST) {
            $this->process_news_add_form();
        } else {
            $this->show_news_add_form();
        }
    }

    private function process_news_add_form() {
        $post = $this->request->post();
        if (empty($post[Field::MAGIC_PASSWORD])) {
            $this->show_news_add_form(__('Controller_Site_News.add.vvedite_magicheskij_parol'));
        } else if ($post[Field::MAGIC_PASSWORD] != $this->magic_password) {
            $this->show_news_add_form(__('Controller_Site_News.add.magicheskij_parol_ne_sovpadaet'));
        } else if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_news_add_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_news_add_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_news_add_form_validation(Arr::map(array('strip_tags', 'UTF8::trim'), $post));
        }
    }

    private function process_news_add_form_validation($post) {
        // login validator
        $form_validator = Validation::factory($post);
        $form_validator
                // news mark
                ->rule(Field::NEWS_MARK, 'not_empty')
                ->rule(Field::NEWS_MARK, 'in_array', array(':value', Model_News::$MARK_ENUM_LIST));
        foreach (Model_News::$LANG_ENUM_LIST as $news_lang) {
            $form_validator
                    // news title
                    ->rule(Field::NEWS_TITLE . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_TITLE . $news_lang, 'max_length', array(':value', 75))
                    // news meta keywords
                    ->rule(Field::NEWS_META_KEYWORDS . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_META_KEYWORDS . $news_lang, 'max_length', array(':value', 150))
                    // news meta descriptiona
                    ->rule(Field::NEWS_META_DESCRIPTIONS . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_META_DESCRIPTIONS . $news_lang, 'max_length', array(':value', 150))
                    // news content
                    ->rule(Field::NEWS_CONTENT . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_CONTENT . $news_lang, 'max_length', array(':value', 21844));
        }
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_news_add_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/news');
            $this->show_news_add_form(reset($errors));
        }
    }

    private function process_news_add_validated_form($post) {
        $news_contents = array();
        foreach (Model_News::$LANG_ENUM_LIST as $news_lang) {
            $news_contents[] = array(
                Model_News::NEWS_LANG => $news_lang,
                Model_News::NEWS_TITLE => $post[Field::NEWS_TITLE . $news_lang],
                Model_News::NEWS_META_KEYWORDS => $post[Field::NEWS_META_KEYWORDS . $news_lang],
                Model_News::NEWS_META_DESCRIPTIONS => $post[Field::NEWS_META_DESCRIPTIONS . $news_lang],
                Model_News::NEWS_CONTENT => $post[Field::NEWS_CONTENT . $news_lang],
            );
        }
        if (Model_News::add_news($post[Field::NEWS_MARK], $news_contents)) {
            $this->show_success_message(__('Controller_Site_News.add.novost_dobavlena'));
        } else {
            $this->show_news_add_form(__('Controller_Site_News.add.nevozmozhno_dobavit_novost'));
        }
    }

    private function show_news_add_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/site/news/news_add_form')
                        ->set('news_languages', Kohana::message(I18n::lang() . '/site/news', 'news_languages'))
                        ->set('news_marks', Kohana::message(I18n::lang() . '/site/news', 'news_marks'))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             News edit action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_edit() {
        $this->init_action_edit();
        $this->set_action_edit_html_data();
        $this->process_action_edit();
        $this->show_content_title(__('Controller_Site_News.edit.content_title'));
    }

    private function init_action_edit() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_site_config();
        $this->init_news_contents();
    }

    private function set_action_edit_html_data() {
        $this->template->title = __('Controller_Site_News.edit.title');
    }

    private function process_action_edit() {
        if ($this->request->method() == HTTP_Request::POST) {
            $this->process_news_edit_form();
        } else {
            $this->show_news_edit_form();
        }
    }

    private function process_news_edit_form() {
        $post = $this->request->post();
        if (empty($post[Field::MAGIC_PASSWORD])) {
            $this->show_news_edit_form(__('Controller_Site_News.add.vvedite_magicheskij_parol'));
        } else if ($post[Field::MAGIC_PASSWORD] != $this->magic_password) {
            $this->show_news_edit_form(__('Controller_Site_News.add.magicheskij_parol_ne_sovpadaet'));
        } else if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_news_edit_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_news_edit_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_news_edit_form_validation(Arr::map(array('strip_tags', 'UTF8::trim'), $post));
        }
    }

    private function process_news_edit_form_validation($post) {
        // login validator
        $form_validator = Validation::factory($post);
        $form_validator
                // news mark
                ->rule(Field::NEWS_MARK, 'not_empty')
                ->rule(Field::NEWS_MARK, 'in_array', array(':value', Model_News::$MARK_ENUM_LIST));
        foreach (Model_News::$LANG_ENUM_LIST as $news_lang) {
            $form_validator
                    // news title
                    ->rule(Field::NEWS_TITLE . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_TITLE . $news_lang, 'max_length', array(':value', 75))
                    // news meta keywords
                    ->rule(Field::NEWS_META_KEYWORDS . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_META_KEYWORDS . $news_lang, 'max_length', array(':value', 150))
                    // news meta descriptiona
                    ->rule(Field::NEWS_META_DESCRIPTIONS . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_META_DESCRIPTIONS . $news_lang, 'max_length', array(':value', 150))
                    // news content
                    ->rule(Field::NEWS_CONTENT . $news_lang, 'not_empty')
                    ->rule(Field::NEWS_CONTENT . $news_lang, 'max_length', array(':value', 21844));
        }
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_news_edit_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/news');
            $this->show_news_edit_form(reset($errors));
        }
    }

    private function process_news_edit_validated_form($post) {
        $news_contents = array();
        foreach (Model_News::$LANG_ENUM_LIST as $news_lang) {
            $news_contents[] = array(
                Model_News::NEWS_LANG => $news_lang,
                Model_News::NEWS_TITLE => $post[Field::NEWS_TITLE . $news_lang],
                Model_News::NEWS_META_KEYWORDS => $post[Field::NEWS_META_KEYWORDS . $news_lang],
                Model_News::NEWS_META_DESCRIPTIONS => $post[Field::NEWS_META_DESCRIPTIONS . $news_lang],
                Model_News::NEWS_CONTENT => $post[Field::NEWS_CONTENT . $news_lang],
            );
        }
        if (Model_News::update_news($this->news_id, $post[Field::NEWS_MARK], $news_contents)) {
            $this->show_success_message(__('Controller_Site_News.edit.izmeneniya_soxraneny'));
        } else {
            $this->show_news_edit_form(__('Controller_Site_News.edit.nevozmozhno_soxranit_otredaktirovannuyu_novost'));
        }
    }

    private function show_news_edit_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/site/news/news_edit_form')
                        ->set('news_languages', Kohana::message(I18n::lang() . '/site/news', 'news_languages'))
                        ->set('news_marks', Kohana::message(I18n::lang() . '/site/news', 'news_marks'))
                        ->bind('news_contents', $this->news_contents)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           News remove action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_remove() {
        $this->init_action_remove();
        $this->set_action_remove_html_data();
        $this->process_action_remove();
        $this->show_content_title(__('Controller_Site_News.remove.content_title'));
    }

    private function init_action_remove() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_site_config();
    }

    private function set_action_remove_html_data() {
        $this->template->title = __('Controller_Site_News.remove.title');
    }

    private function process_action_remove() {
        if ($this->request->method() == HTTP_Request::POST) {
            $this->process_news_remove_form();
        } else {
            $this->show_news_remove_form();
        }
    }

    private function process_news_remove_form() {
        $post = $this->request->post();
        if (empty($post[Field::MAGIC_PASSWORD])) {
            $this->show_news_remove_form(__('Controller_Site_News.add.vvedite_magicheskij_parol'));
        } else if ($post[Field::MAGIC_PASSWORD] != $this->magic_password) {
            $this->show_news_remove_form(__('Controller_Site_News.add.magicheskij_parol_ne_sovpadaet'));
        } else if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_news_remove_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_news_remove_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->remove_news();
        }
    }

    private function remove_news() {
        if (Model_News::remove_news($this->news_id)) {
            $this->show_info_message(__('Controller_Site_News.remove.novost_udalena'));
        } else {
            $this->show_warning_message(__('Controller_Site_News.remove.nevozmozhno_udalit_novost'));
        }
    }

    private function show_news_remove_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(View::factory('/site/news/news_remove_form'));
    }

}