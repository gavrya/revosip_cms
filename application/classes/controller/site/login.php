<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Site_Login
 *
 * @author Гаврищук
 */
class Controller_Site_Login extends Controller_Template_Site {

    // data
    private $captcha;
    // enabled config
    private $login_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('site_enabled');
        $this->login_enabled = $config['login'];
        //unset($config);
    }

    private function init_captcha() {
        $this->captcha = Captcha::instance();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Login action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_login() {
        $this->init_action_login();
        $this->set_action_login_html_data();
        $this->process_action_login();
        $this->show_content_title(__('Controller_Site_Login.login.content_title'));
    }

    private function init_action_login() {
        $this->init_enabled_config();
        $this->init_captcha();
    }

    private function set_action_login_html_data() {
        $this->template->title = __('Controller_Site_Login.login.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Login.login.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Login.login.description'));
    }

    private function process_action_login() {
        if (!$this->login_enabled) {
            $this->show_warning_message(__('Controller_Site_Login.login.vozmozhnost_vxoda_na_sajt_vremenno_priostanovlena'));
        } else if (Model_Auth::instance()->logged_in()) {
            $this->request->redirect('/profile');
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_login_post_form();
        } else {
            $this->show_login_form();
        }
    }

    private function process_login_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_login_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!key_exists(Field::CAPTCHA, $post) || !Captcha::valid($post[Field::CAPTCHA])) {
            $this->show_login_form(__('Controller.vvedite_pravilnyj_proverochnyj_kod'));
        } else {
            $this->process_login_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_login_form_validation($post) {
        // form validator
        $form_validator = Validation::factory($post);
        // login validation rules
        $form_validator
                ->rule(Field::USER_LOGIN, 'not_empty')
                ->rule(Field::USER_LOGIN, 'min_length', array(':value', 4))
                ->rule(Field::USER_LOGIN, 'max_length', array(':value', 50))
                ->rule(Field::USER_LOGIN, 'regex', array(':value', Regexpr::USER_LOGIN));
        // password validation rules
        $form_validator
                ->rule(Field::USER_PASSWORD, 'not_empty')
                ->rule(Field::USER_PASSWORD, 'min_length', array(':value', 5))
                ->rule(Field::USER_PASSWORD, 'max_length', array(':value', 50));
        // check validation
        if ($form_validator->check()) {
            // process validated form
            $this->process_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/login');
            $this->show_login_form(reset($errors));
        }
    }

    private function process_validated_form($post) {
        // get user data
        $user_data = Model_Users::get_user_by_login_pass($post[Field::USER_LOGIN], $post[Field::USER_PASSWORD]);
        // check user
        if (is_array($user_data)) {
            // check user confirmed state
            if (key_exists(Model_Users::USER_CONFIRMED, $user_data) && $user_data[Model_Users::USER_CONFIRMED] == FALSE) {
                $this->show_warning_message(__('Controller_Site_Login.login.vxod_vozmozhen_tolko_posle_podtverzhdeniya_registracii'));
                // check user blocked state
            } else if (key_exists(Model_Users::USER_BLOCKED, $user_data) && $user_data[Model_Users::USER_BLOCKED] == TRUE) {
                $this->show_warning_message(__('Controller_Site_Login.login.dostup_vremenno_zablokirovan'));
                // process user login
            } else if (Model_Auth::instance()->login($user_data[Model_Users::USER_ID])) {
                $this->request->redirect('/profile');
            } else {
                $this->show_login_form(__('Controller_Site_Login.login.nevozmozhno_vypolnit_vxod,_povtorite_popytku'));
            }
        } else {
            // Неправильный логин или пароль
            $this->show_login_form(__('Controller_Site_Login.login.nepravilnyj_login_ili_parol'));
        }
    }

    private function show_login_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/site/login/login_form')->bind('captcha', $this->captcha)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Logout action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_logout() {
        Model_Auth::instance()->logout();
        $this->request->redirect();
    }

}