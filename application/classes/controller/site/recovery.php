<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Site_Recovery
 *
 * @author Гаврищук
 */
class Controller_Site_Recovery extends Controller_Template_Site {

    // data
    private $captcha;
    // enabled config
    private $recovery_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_captcha() {
        $this->captcha = Captcha::instance();
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('site_enabled');
        $this->recovery_enabled = $config['recovery'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                      Password recovery action                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_recovery() {
        $this->init_action_recovery();
        $this->set_action_recovery_html_data();
        $this->process_action_recovery();
        $this->show_content_title(__('Controller_Site_Recovery.recovery.content_title'));
    }

    private function init_action_recovery() {
        $this->init_captcha();
        $this->init_enabled_config();
    }

    private function set_action_recovery_html_data() {
        $this->template->title = __('Controller_Site_Recovery.recovery.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Recovery.recovery.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Recovery.recovery.description'));
    }

    private function process_action_recovery() {
        if (!$this->recovery_enabled) {
            $this->show_warning_message(__('Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_na_sajte_vremenno_priostanovlena'));
        } else if (Model_Auth::instance()->logged_in()) {
            $this->request->redirect();
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_recovery_post_form();
        } else {
            $this->show_recovery_form();
        }
    }

    private function process_recovery_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_recovery_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!key_exists(Field::CAPTCHA, $post) || !Captcha::valid($post[Field::CAPTCHA])) {
            $this->show_recovery_form(__('Controller.vvedite_pravilnyj_proverochnyj_kod'));
        } else {
            $this->process_recovery_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_recovery_form_validation($post) {
        // form validator
        $form_validator = Validation::factory($post);
        // form validation rules
        $form_validator
                ->rule(Field::USER_EMAIL, 'not_empty')
                ->rule(Field::USER_EMAIL, 'email');
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_recovery_validated_form($post);
        } else {
            // show validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/registration');
            $this->show_recovery_form(reset($errors));
        }
    }

    private function process_recovery_validated_form($post) {
        // get user by email
        $user_data = Model_Users::get_user_by_email($post[Field::USER_EMAIL]);
        // check user
        if (is_array($user_data)) {
            $this->process_pass_recovery($user_data);
        } else {
            $this->show_recovery_form(__('Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_polzovatel_s_takim_email_adresom_ne_najden'));
        }
    }

    private function process_pass_recovery($user_data) {
        if (key_exists(Model_Users::USER_CONFIRMED, $user_data) && $user_data[Model_Users::USER_CONFIRMED] == FALSE) {
            $this->show_warning_message(__('Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_vosstanovlenie_parolya_vozmozhno_tolko_posle_podtverzhdeniya_registracii'));
        } else if (key_exists(Model_Users::USER_BLOCKED, $user_data) && $user_data[Model_Users::USER_BLOCKED] == TRUE) {
            $this->show_warning_message(__('Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_vremenno_zablokirovana'));
        } else {
            $this->request_pass_recovery($user_data);
        }
    }

    private function request_pass_recovery($user_data) {
        $recovery_record = Model_Users::get_user_pass_recovery_record($user_data[Model_Users::USER_ID]);
        if (is_array($recovery_record)) {
            $this->show_warning_message(__('Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_podtverdite_sushhestvuyushhij_zapros_na_vosstanovlenie_parolya'));
        } else {
            $recovery_record = Model_Users::add_pass_recovery_record($user_data[Model_Users::USER_ID]);
            if (is_array($recovery_record) && Emailer::instance()->send_recovery_confirm_link($user_data[Model_Users::USER_ID], $user_data[Model_Users::USER_NAME], $user_data[Model_Users::USER_EMAIL], $recovery_record[Model_Users::RECORD_HASH])) {
                $this->show_info_message(__('Controller_Site_Recovery.recovery.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_procedury_vosstanovleniya_parolya'));
            } else {
                $this->show_warning_message(__('Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_povtorite_popytku_pozzhe'));
            }
        }
    }

    private function show_recovery_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(View::factory('/site/recovery/pass_recovery_form')->bind('captcha', $this->captcha));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Recovery confirm action                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_confirm() {
        // init param
        $user_id = $this->request->param('user_id');
        $recovery_hash = $this->request->param('recovery_hash');
        $user_data = Model_Users::get_user($user_id);
        // set html data
        $this->set_action_confirm_html_data();
        // set content header
        $this->show_content_title(__('Controller_Site_Recovery.confirm.content_title'));
        // process
        $this->process_action_confirm($user_id, $recovery_hash, $user_data);
    }

    private function set_action_confirm_html_data() {
        $this->template->title = __('Controller_Site_Recovery.confirm.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Recovery.confirm.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Recovery.confirm.description'));
    }

    private function process_action_confirm($user_id, $recovery_hash, $user_data) {
        if (is_array($user_data)) {
            $this->process_recovery_confirm($user_id, $recovery_hash, $user_data);
        } else {
            $this->show_warning_message(__('Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol,_polzovatel_ne_najden'));
        }
    }

    private function process_recovery_confirm($user_id, $recovery_hash, $user_data) {
        $recovery_record = Model_Users::get_user_pass_recovery_record($user_id);
        if (is_array($recovery_record) && key_exists(Model_Users::RECORD_HASH, $recovery_record) && $recovery_record[Model_Users::RECORD_HASH] == $recovery_hash && $user_data[Model_Users::USER_CONFIRMED] == TRUE) {
            $this->recovery_password($user_data);
        } else {
            $this->show_warning_message(__('Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol'));
        }
    }

    private function recovery_password($user_data) {
        $random_pass = Text::random();
        if (Model_Users::update_user_password($user_data[Model_Users::USER_ID], $random_pass)) {
            $this->show_success_message(__('Controller_Site_Recovery.confirm.vash_novyj_parol') . ' <strong>' . $random_pass . '</strong><br />' . __('Controller_Site_Recovery.confirm.vy_vsegda_mozhete_izmenit_parol_na_stranice_profilya'));
        } else {
            $this->show_warning_message(__('Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol'));
        }
    }

}