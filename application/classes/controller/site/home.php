<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Site_Home
 *
 * @author Гаврищук
 */
class Controller_Site_Home extends Controller_Template_Site {

    // data
    private $feedback_email;
    private $feedback_skype;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Init                                     */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_feedback_config() {
        $config = Kohana::$config->load('app')->get('site');
        $this->feedback_email = $config['feedback_email'];
        $this->feedback_skype = $config['feedback_skype'];
        // unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Home action                                */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_home() {
        $this->set_action_home_html_data();
        $this->show_home();
        //$this->show_content_title('Revosip - облачный SIP софтфон');
    }

    private function set_action_home_html_data() {
        $this->template->title = __('Controller_Site_Home.home.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Home.home.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Home.home.description'));
    }

    private function show_home() {
        $this->show_content(
                View::factory('/site/home/home')
                        ->set('content', Kohana::message(I18n::lang() . '/site/home'))
                        ->set('logged_in', Model_Auth::instance()->logged_in())
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            About action                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_about() {
        $this->set_action_about_html_data();
        $this->show_about();
        $this->show_content_title(__('Controller_Site_Home.about.content_title'));
    }

    private function set_action_about_html_data() {
        $this->template->title = __('Controller_Site_Home.about.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Home.about.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Home.about.description'));
    }

    public function show_about() {
        $this->show_content(
                View::factory('/site/home/about')
                        ->set('about', Kohana::message(I18n::lang() . '/site/about'))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            F.A.Q. action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_faq() {
        $this->set_action_faq_html_data();
        $this->show_faq();
        $this->show_content_title(__('Controller_Site_Home.faq.content_title'));
    }

    private function set_action_faq_html_data() {
        $this->template->title = __('Controller_Site_Home.faq.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Home.faq.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Home.faq.description'));
    }

    public function show_faq() {
        $this->show_content(
                View::factory('/site/home/faq')
                        ->set('faq', Kohana::message(I18n::lang() . '/site/faq'))
                        ->set('section_id', $this->request->param('section_id', 0))
                        ->set('question_id', $this->request->param('question_id', 0))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Feedback action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_feedback() {
        $this->init_feedback_config();
        $this->set_action_feedback_html_data();
        $this->show_feedback();
        $this->show_content_title(__('Controller_Site_Home.feedback.content_title'));
    }

    private function set_action_feedback_html_data() {
        $this->template->title = __('Controller_Site_Home.feedback.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Home.feedback.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Home.feedback.description'));
    }

    public function show_feedback() {
        $this->show_content(
                View::factory('/site/home/feedback')
                        ->bind('feedback_email', $this->feedback_email)
                        ->bind('feedback_skype', $this->feedback_skype)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Conditions action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_conditions() {
        $this->set_action_conditions_html_data();
        $this->show_conditions();
        $this->show_content_title(__('Controller_Site_Home.conditions.content_title'));
    }

    private function set_action_conditions_html_data() {
        $this->template->title = __('Controller_Site_Home.conditions.title');
        $this->template->meta_data[] = array('name' => 'keywords', 'content' => __('Controller_Site_Home.conditions.keywords'));
        $this->template->meta_data[] = array('name' => 'description', 'content' => __('Controller_Site_Home.conditions.description'));
    }

    public function show_conditions() {
        $this->show_content(
                View::factory('/site/home/conditions')
                        ->set('conditions', Kohana::message(I18n::lang() . '/site/conditions', 0))
        );
    }

}