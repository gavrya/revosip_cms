<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller_Template_Site {

    private $page = '';
    private $message = '';

    public function before() {
        parent::before();
        $this->page = URL::site(rawurldecode(Request::initial()->uri()), TRUE);
        // Internal request only!
        if (!Request::current()->is_initial()) {
            if ($message = rawurldecode($this->request->param('message'))) {
                $this->message = $message;
            }
        } else {
            $this->request->action(404);
        }
        $this->response->status((int) $this->request->action());
    }

    public function action_403() {
        $this->show_error('403 error', $this->message);
        //$this->response->body('403 error' . '<br/>' . $this->message);
    }

    public function action_404() {
        $this->show_error('404 Not Found', $this->message);
        //$this->response->body('404 Not Found' . '<br/>' . $this->message);
    }

    public function action_500() {
        $this->show_error('500 Internal Server Error', $this->message);
        //$this->response->body('500 Internal Server Error' . '<br/>' . $this->message);
    }

    public function action_503() {
        $this->show_error('503 Maintenance Mode', $this->message);
        //$this->response->body('503 Maintenance Mode' . '<br/>' . $this->message);
    }
    
    private function show_error($error_description, $error_message) {
        $this->template->title = __('Controller_Error.title');
        $this->show_content_title(__('Controller_Error.content_title'));
        $this->show_warning_message($error_description);
        //$this->show_content($error_message);
    }
    
}

// End Error
