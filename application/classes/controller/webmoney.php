<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Webmoney
 * result|success|fail
 *
 * @author Гаврищук
 */
class Controller_Webmoney extends Controller {

    // data
    private $user_data;
    private $invoice_payment;
    // enabled config
    private $profile_invoice_enabled;
    // webmoney config
    //private $webmoney_sim_mode;
    private $wmz_purse;
    private $wmr_purse;
    private $wmu_purse;
    private $wmz_secret_key;
    private $wmr_secret_key;
    private $wmu_secret_key;
    private $credit_to_wmz;
    private $credit_to_wmr;
    private $credit_to_wmu;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data($user_id) {
        $this->user_data = Model_Users::get_user($user_id);
    }

    private function init_invoice_payment($invoice_id) {
        $this->invoice_payment = Model_Webmoney_Invoice::get_invoice($invoice_id);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->profile_invoice_enabled = $config['profile_invoice'];
        // unset config
        //unset($config);
    }

    private function init_webmoney_config() {
        // webmoney config
        $config = Kohana::$config->load('app')->get('webmoney');
        //$this->webmoney_sim_mode = $config['sim_mode'];
        // purses
        $this->wmz_purse = $config['wmz_purse'];
        $this->wmr_purse = $config['wmr_purse'];
        $this->wmu_purse = $config['wmu_purse'];
        // secret keys
        $this->wmz_secret_key = $config['wmz_secret_key'];
        $this->wmr_secret_key = $config['wmr_secret_key'];
        $this->wmu_secret_key = $config['wmu_secret_key'];
        // exchange rate
        $this->credit_to_wmz = $config['credit_to_wmz'];
        $this->credit_to_wmr = $config['credit_to_wmr'];
        $this->credit_to_wmu = $config['credit_to_wmu'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Webmoney result action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_result() {
        if ($this->request->method() == HTTP_Request::POST) {
            if ($this->is_valid_webmoney_ip(Request::$client_ip)) {
                $post = $this->request->post();
                if (isset($post[Field::LMI_PAYMENT_NO])) {
                    // init invoice payment
                    $this->init_invoice_payment($post[Field::LMI_PAYMENT_NO]);
                    // init enabled config
                    $this->init_enabled_config();
                    // init webmoney config
                    $this->init_webmoney_config();
                    // detect pre request flag
                    if (isset($post[Field::LMI_PREREQUEST])) {
                        $this->process_pre_request($post);
                    } else {
                        $this->process_result($post);
                    }
                }
            }
        }
    }

    private function process_pre_request($post) {
        if (//Kohana::$environment == Kohana::PRODUCTION &&
                $this->profile_invoice_enabled &&
                $this->invoice_payment !== FALSE &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_PURSE] == $post[Field::LMI_PAYEE_PURSE] &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_REQUESTED_AMOUNT] == $post[Field::LMI_PAYMENT_AMOUNT] &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_REQUEST) {
            // update state
            Model_Webmoney_Invoice::update_invoice_state($this->invoice_payment[Model_Webmoney_Invoice::INVOICE_ID], Model_Webmoney_Invoice::ENUM_STATE_CHECK);
            // ok
            $this->response->body('YES');
        }
    }

    private function process_result($post) {
        if ($this->invoice_payment !== FALSE &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_PURSE] == $post[Field::LMI_PAYEE_PURSE] &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_REQUESTED_AMOUNT] == $post[Field::LMI_PAYMENT_AMOUNT] &&
                $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_CHECK) {

            $secret_keys = array(
                $this->wmz_purse => $this->wmz_secret_key,
                $this->wmr_purse => $this->wmr_secret_key,
                $this->wmu_purse => $this->wmu_secret_key,
            );
            $hash_elements = array(
                $post[Field::LMI_PAYEE_PURSE],
                $post[Field::LMI_PAYMENT_AMOUNT],
                $post[Field::LMI_PAYMENT_NO],
                $post[Field::LMI_MODE],
                $post[Field::LMI_SYS_INVS_NO],
                $post[Field::LMI_SYS_TRANS_NO],
                $post[Field::LMI_SYS_TRANS_DATE],
                $secret_keys[$post[Field::LMI_PAYEE_PURSE]],
                $post[Field::LMI_PAYER_PURSE],
                $post[Field::LMI_PAYER_WM],
            );
            // validation hash
            $validation_hash = strtoupper(md5(implode($hash_elements)));
            // check validation hash
            if ($validation_hash == $post[Field::LMI_HASH]) {
                $amount_to_credit = array(
                    Model_Webmoney_Invoice::ENUM_UNIT_WMZ => $this->credit_to_wmz,
                    Model_Webmoney_Invoice::ENUM_UNIT_WMR => $this->credit_to_wmz / $this->credit_to_wmr,
                    Model_Webmoney_Invoice::ENUM_UNIT_WMU => $this->credit_to_wmz / $this->credit_to_wmu,
                );
                $received_credit = round($post[Field::LMI_PAYMENT_AMOUNT] * $amount_to_credit[$this->invoice_payment[Model_Webmoney_Invoice::INVOICE_UNIT]], 2);
                // detect test mode
                if ($post[Field::LMI_MODE] == FALSE) {
                    Model_Users::add_user_credit($this->invoice_payment[Model_Users::USER_ID], $received_credit);
                    // send notification
                    $this->init_user_data($this->invoice_payment[Model_Users::USER_ID]);
                    // Set target language
                    I18n::lang(strtolower($this->user_data[Model_Users::USER_LANG]));
                    Emailer::instance()->send_invoice_payment_notification($this->user_data[Model_Users::USER_NAME], $this->user_data[Model_Users::USER_EMAIL]);
                }
                $payment_data = array(
                    Model_Webmoney_Invoice::LMI_MODE => $post[Field::LMI_MODE],
                    Model_Webmoney_Invoice::LMI_SYS_INVS_NO => $post[Field::LMI_SYS_INVS_NO],
                    Model_Webmoney_Invoice::LMI_SYS_TRANS_NO => $post[Field::LMI_SYS_TRANS_NO],
                    Model_Webmoney_Invoice::LMI_SYS_TRANS_DATE => $post[Field::LMI_SYS_TRANS_DATE],
                    Model_Webmoney_Invoice::LMI_PAYER_PURSE => $post[Field::LMI_PAYER_PURSE],
                    Model_Webmoney_Invoice::LMI_PAYER_WM => $post[Field::LMI_PAYER_WM],
                );
                // notify invoice payment
                Model_Webmoney_Invoice::notify_invoice_payment($this->invoice_payment[Model_Webmoney_Invoice::INVOICE_ID], $received_credit, $payment_data);
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Webmoney success action                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_success() {
        if ($this->request->method() == HTTP_Request::POST) {
            $post = $this->request->post();
            if (isset($post[Field::LMI_PAYMENT_NO])) {
                // init invoice payment
                $this->init_invoice_payment($post[Field::LMI_PAYMENT_NO]);
                if ($this->invoice_payment !== FALSE && $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_NOTIFY) {
                    // update state
                    Model_Webmoney_Invoice::update_invoice_state($this->invoice_payment[Model_Webmoney_Invoice::INVOICE_ID], Model_Webmoney_Invoice::ENUM_STATE_SUCCESS);
                }
                //$this->response->body(View::factory('/profiler/stats'));
                $this->request->redirect('profile/invoices');
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Webmoney fail action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_fail() {
        if ($this->request->method() == HTTP_Request::POST) {
            $post = $this->request->post();
            if (isset($post[Field::LMI_PAYMENT_NO])) {
                // init invoice payment
                $this->init_invoice_payment($post[Field::LMI_PAYMENT_NO]);
                if ($this->invoice_payment !== FALSE && ($this->invoice_payment[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_REQUEST || $this->invoice_payment[Model_Webmoney_Invoice::INVOICE_STATE] == Model_Webmoney_Invoice::ENUM_STATE_CHECK)) {
                    // update state
                    Model_Webmoney_Invoice::update_invoice_state($this->invoice_payment[Model_Webmoney_Invoice::INVOICE_ID], Model_Webmoney_Invoice::ENUM_STATE_FAILURE);
                }
                //$this->response->body(View::factory('/profiler/stats'));
                $this->request->redirect('profile/invoices');
            }
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                          check webmoney ip                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function is_valid_webmoney_ip($ip_address) {
        $patterns = array(
            '212.118.48.',
            '212.158.173.',
            '91.200.28.',
            '91.227.52.',
        );
        foreach ($patterns as $pattern) {
            if (strpos($ip_address, $pattern) !== FALSE) {
                return TRUE;
            }
        }
        return FALSE;
    }

}

?>
