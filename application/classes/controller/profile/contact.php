<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Contact
 * contact/$a_id/$c_id/info - show contact info
 * contact/$a_id/$c_id/edit - edit contact
 * contact/$a_id/$c_id/remove - remove contact
 *
 * @author Гаврищук
 */
class Controller_Profile_Contact extends Controller_Template_Profile {

    // data
    private $user_data;
    private $account_id;
    private $contact_id;
    private $account_profile;
    private $contact_profile;
    // enabled config
    private $contacts_edit_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_param_data() {
        $this->account_id = $this->request->param('account_id');
        $this->contact_id = $this->request->param('contact_id');
        if (!Model_Profile_Account::is_account_exists($this->account_id, $this->user_data[Model_Users::USER_ID])) {
            throw new HTTP_Exception_404('Account does not exist');
        }
        if (!Model_Profile_Contact::is_contact_exists($this->account_id, $this->contact_id)) {
            throw new HTTP_Exception_404('Contact does not exist');
        }
    }

    private function init_account_profile() {
        $this->account_profile = Model_Profile_Account::get_account_profile($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_contact_profile() {
        $this->contact_profile = Model_Profile_Contact::get_contact_profile($this->contact_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->contacts_edit_enabled = $config['contact_edit'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Contact info action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_info() {
        $this->init_action_info();
        $this->set_action_info_html_data();
        $this->show_contact_info();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Contact.info.content_title'));
    }

    private function init_action_info() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_contact_profile();
    }

    private function set_action_info_html_data() {
        $this->template->title = __('Controller_Profile_Contact.info.title');
    }

    private function show_contact_info() {
        $this->show_content(View::factory('/profile/contact/contact_info')->bind('contact_profile', $this->contact_profile));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Contact edit action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_edit() {
        $this->init_action_edit();
        $this->set_action_edit_html_data();
        $this->process_action_edit();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Contact.edit.content_title'));
    }

    private function init_action_edit() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_contact_profile();
        $this->init_enabled_config();
    }

    private function set_action_edit_html_data() {
        $this->template->title = __('Controller_Profile_Contact.edit.title');
    }

    private function process_action_edit() {
        if (!$this->contacts_edit_enabled) {
            $this->show_warning_message(__('Controller_Profile_Contact.edit.vozmozhnost_redaktirovaniya_kontaktov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_contact_edit_form();
        } else {
            $this->show_contact_edit_form();
        }
    }

    private function process_contact_edit_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_contact_edit_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_contact_edit_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_contact_edit_form_validation(Arr::map(array('strip_tags', 'UTF8::trim'), $post));
        }
    }

    private function process_contact_edit_form_validation($post) {
        // login validator
        $form_validator = Validation::factory($post);
        // account name validation rules
        $form_validator
                ->rule(Field::CONTACT_NAME, 'not_empty')
                ->rule(Field::CONTACT_NAME, 'max_length', array(':value', 50));
        // user name validation rules
        $form_validator
                ->rule(Field::CONTACT_PHONE, 'not_empty')
                ->rule(Field::CONTACT_PHONE, 'max_length', array(':value', 100))
                ->rule(Field::CONTACT_PHONE, 'regex', array(':value', Regexpr::CONTACT_PHONE_NUMBER))
                ->rule(Field::CONTACT_PHONE, 'Model_Profile_Contact::is_contact_phone_available', array(':value', $this->account_id, $this->contact_id));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_contact_edit_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/contacts_add');
            $this->show_contact_edit_form(reset($errors));
        }
    }

    private function process_contact_edit_validated_form($post) {
        if (Model_Profile_Contact::update_contact_profile($this->account_id, $this->contact_id, $post[Field::CONTACT_NAME], $post[Field::CONTACT_PHONE])) {
            $this->show_success_message(__('Controller_Profile_Contact.edit.izmeneniya_soxraneny'));
        } else {
            $this->show_contact_edit_form(__('Controller_Profile_Contact.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku'));
        }
    }

    private function show_contact_edit_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/contact/contact_edit_form')
                        ->bind('captcha', $this->captcha)
                        ->bind('contact_profile', $this->contact_profile)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Contact remove action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_remove() {
        $this->init_action_remove();
        $this->process_contact_remove();
    }

    private function init_action_remove() {
        $this->init_user_data();
        $this->init_param_data();
    }

    private function process_contact_remove() {
        // anti xss hack
        if ($this->request->method() == HTTP_Request::GET && strpos($this->request->referrer(), URL::base(TRUE)) !== FALSE) {
            Model_Profile_Contact::remove_contact_profile($this->account_id, $this->contact_id);
        }
        $this->request->redirect('/contacts/' . $this->account_id . '/list');
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(
            Request::factory('/menu/account/' . $this->account_id)->execute(),
            Request::factory('/menu/contacts/' . $this->account_id)->execute(),
            Request::factory('/menu/contact/' . $this->account_id . '/' . $this->contact_id . '/' . $this->request->action())->execute(),
            Request::factory('/menu/connections/' . $this->account_id)->execute(),
        ));
        // navigation
        $this->show_navigation(
                Request::factory('/navigation/contact/' . $this->account_id . '/' . $this->contact_id . '/' . $this->request->action())
                        ->query('account_label', $this->account_profile[Model_Profile_Account::ACCOUNT_NAME])
                        ->query('contact_label', $this->contact_profile[Model_Profile_Contact::CONTACT_NAME])
                        ->execute()
        );
    }

}
