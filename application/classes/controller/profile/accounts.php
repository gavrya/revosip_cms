<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Accounts
 * accounts/list - accounts list
 * accounts/add - add new account
 * accounts/log - accounts login/logout history
 * accounts/prolongs - accounts prolongs history
 *
 * @author Гаврищук
 */
class Controller_Profile_Accounts extends Controller_Template_Profile {

    // data
    private $user_data;
    // pagination
    private $pagination;
    // enabled config
    private $accounts_add_enabled;
    // other
    private $account_profiles_count;
    private $account_profiles;
    private $user_accounts_logs_count;
    private $user_accounts_logs;
    private $accounts_prolong_history_count;
    private $accounts_prolong_history;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
        ));
    }

    private function init_account_profiles_count() {
        $this->account_profiles_count = Model_Profile_Account::count_account_profiles($this->user_data[Model_Users::USER_ID]);
    }

    private function init_account_profiles($offset, $limit) {
        $this->account_profiles = Model_Profile_Account::get_account_profiles($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_user_accounts_logs_count() {
        $this->user_accounts_logs_count = Model_Profile_Account::count_user_account_logs($this->user_data[Model_Users::USER_ID]);
    }

    private function init_user_accounts_logs($offset, $limit) {
        $this->user_accounts_logs = Model_Profile_Account::get_user_account_logs($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_accounts_prolong_history_count() {
        $this->accounts_prolong_history_count = Model_Profile_Account::count_prolong_history($this->user_data[Model_Users::USER_ID]);
    }

    private function init_accounts_prolong_history($offset, $limit) {
        $this->accounts_prolong_history = Model_Profile_Account::get_prolong_history($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], 0, $offset, $limit);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->accounts_add_enabled = $config['accounts_add'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Accounts list action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_list() {
        $this->init_action_list();
        $this->set_action_list_html_data();
        $this->show_accounts_list();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Accounts.list.content_title'));
    }

    private function init_action_list() {
        $this->init_user_data();
        $this->init_account_profiles_count();
        $this->init_pagination($this->account_profiles_count, 20);
        $this->init_account_profiles($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_list_html_data() {
        $this->template->title = __('Controller_Profile_Accounts.list.title');
    }

    private function show_accounts_list() {
        if (empty($this->account_profiles)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/profile/accounts/accounts_list')->bind('account_profiles', $this->account_profiles));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Account add action                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_add() {
        $this->init_action_add();
        $this->set_action_add_html_data();
        $this->process_accounts_add();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Accounts.add.content_title'));
    }

    private function init_action_add() {
        $this->init_user_data();
        $this->init_enabled_config();
    }

    private function set_action_add_html_data() {
        $this->template->title = __('Controller_Profile_Accounts.add.title');
    }

    private function process_accounts_add() {
        if (!$this->accounts_add_enabled) {
            $this->show_warning_message(__('Controller_Profile_Accounts.add.vozmozhnost_dobavleniya_akkauntov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_accounts_add_post_form();
        } else {
            $this->show_accounts_add_form();
        }
    }

    private function process_accounts_add_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_accounts_add_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_accounts_add_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_accounts_add_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_accounts_add_form_validation($post) {
        // validator
        $form_validator = Validation::factory($post);
        // account name validation rules
        $form_validator
                ->rule(Field::ACCOUNT_NAME, 'not_empty')
                ->rule(Field::ACCOUNT_NAME, 'max_length', array(':value', 50))
                ->rule(Field::ACCOUNT_NAME, 'Model_Profile_Account::is_account_name_available', array(':value', $this->user_data[Model_Users::USER_ID]));
        // user name validation rules
        $form_validator
                ->rule(Field::ACCOUNT_USER, 'not_empty')
                ->rule(Field::ACCOUNT_USER, 'max_length', array(':value', 50));
        // login validation rules
        $form_validator
                ->rule(Field::ACCOUNT_LOGIN, 'not_empty')
                ->rule(Field::ACCOUNT_LOGIN, 'min_length', array(':value', 4))
                ->rule(Field::ACCOUNT_LOGIN, 'max_length', array(':value', 50))
                ->rule(Field::ACCOUNT_LOGIN, 'regex', array(':value', Regexpr::ACCOUNT_LOGIN))
                ->rule(Field::ACCOUNT_LOGIN, 'Model_Profile_Account::is_account_login_available', array(':value', $this->user_data[Model_Users::USER_ID]));
        // password validation rules
        $form_validator
                ->rule(Field::ACCOUNT_PASSWORD, 'not_empty')
                ->rule(Field::ACCOUNT_PASSWORD, 'min_length', array(':value', 5))
                ->rule(Field::ACCOUNT_PASSWORD, 'max_length', array(':value', 50));
        // passconf validation rules
        $form_validator
                ->rule(Field::ACCOUNT_REPASSWORD, 'not_empty')
                ->rule(Field::ACCOUNT_REPASSWORD, 'matches', array(':validation', Field::ACCOUNT_PASSWORD, Field::ACCOUNT_REPASSWORD));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_accounts_add_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/accounts_add');
            $this->show_accounts_add_form(reset($errors));
        }
    }

    private function process_accounts_add_validated_form($post) {
        if (Model_Profile_Account::add_account_profile($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_REG_DATE], $post[Field::ACCOUNT_NAME], $post[Field::ACCOUNT_USER], $post[Field::ACCOUNT_LOGIN], $post[Field::ACCOUNT_PASSWORD], isset($post[Field::ACCOUNT_ACTIVATE]))) {
            $this->show_success_message(__('Controller_Profile_Accounts.add.akkaunt_dobavlen'));
        } else {
            $this->show_accounts_add_form(__('Controller_Profile_Accounts.add.ne_udalos_dobavit_akkaunt,_povtorite_popytku'));
        }
    }

    private function show_accounts_add_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(View::factory('/profile/accounts/accounts_add_form')->bind('user_data', $this->user_data));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Account log action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_log() {
        $this->init_action_log();
        $this->set_action_log_html_data();
        $this->show_account_log();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Accounts.log.content_title'));
    }

    private function init_action_log() {
        $this->init_user_data();
        $this->init_user_accounts_logs_count();
        $this->init_pagination($this->user_accounts_logs_count, 20);
        $this->init_user_accounts_logs($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_log_html_data() {
        $this->template->title = __('Controller_Profile_Accounts.log.title');
    }

    private function show_account_log() {
        if (empty($this->user_accounts_logs)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/profile/accounts/accounts_log')->bind('user_accounts_logs', $this->user_accounts_logs));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Accounts prolongs action                      */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_prolongs() {
        $this->init_action_prolongs();
        $this->set_action_prolongs_html_data();
        $this->process_accounts_prolongs();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Accounts.prolongs.content_title'));
    }

    private function init_action_prolongs() {
        $this->init_user_data();
        $this->init_accounts_prolong_history_count();
        $this->init_pagination($this->accounts_prolong_history_count, 20);
        $this->init_accounts_prolong_history($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_prolongs_html_data() {
        $this->template->title = __('Controller_Profile_Accounts.prolongs.title');
    }

    private function process_accounts_prolongs() {
        if (empty($this->accounts_prolong_history)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/profile/accounts/accounts_prolongs')->bind('accounts_prolong_history', $this->accounts_prolong_history));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(Request::factory('/menu/accounts/' . $this->request->action())->execute()));
        // navigation
        $this->show_navigation(Request::factory('/navigation/accounts/' . $this->request->action())->execute());
    }

}