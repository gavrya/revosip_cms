<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Connections
 * connections/$a_id/list - show connection profiles list
 * connections/$a_id/add - add connection profile
 * connections/$a_id/copy - copy connection profile to other account (todo)
 *
 * @author Гаврищук
 */
class Controller_Profile_Connections extends Controller_Template_Profile {

    // data
    private $user_data;
    private $account_id;
    private $account_profile;
    private $connection_profiles_count;
    private $connection_profiles;
    // pagination
    private $pagination;
    // ebabled config
    private $connections_add_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_param_data() {
        $this->account_id = $this->request->param('account_id');
        if (!Model_Profile_Account::is_account_exists($this->account_id, $this->user_data[Model_Users::USER_ID])) {
            throw new HTTP_Exception_404('Account does not exist');
        }
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
            'account_id' => $this->request->param('account_id'),
        ));
    }

    private function init_account_profile() {
        $this->account_profile = Model_Profile_Account::get_account_profile($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_connection_profiles_count() {
        $this->connection_profiles_count = Model_Profile_Connection::count_connection_profiles($this->account_id);
    }

    private function init_connection_profiles($offset, $limit) {
        $this->connection_profiles = Model_Profile_Connection::get_connection_profiles($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->connections_add_enabled = $config['connections_add'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Connections list action                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_list() {
        $this->init_action_list();
        $this->set_action_list_html_data();
        $this->show_connections_list();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Connections.list.content_title'));
    }

    private function init_action_list() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_connection_profiles_count();
        $this->init_pagination($this->connection_profiles_count, 20);
        $this->init_connection_profiles($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_list_html_data() {
        $this->template->title = __('Controller_Profile_Connections.list.title');
    }

    private function show_connections_list() {
        if (empty($this->connection_profiles)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(
                    View::factory('/profile/connections/connections_list')
                            ->bind('connection_profiles', $this->connection_profiles)
                            ->bind('account_id', $this->account_id)
            );
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Connections add action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_add() {
        $this->init_action_add();
        $this->set_action_add_html_data();
        $this->process_connections_add();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Connections.add.content_title'));
    }

    private function init_action_add() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_enabled_config();
    }

    private function set_action_add_html_data() {
        $this->template->title = __('Controller_Profile_Connections.add.title');
    }

    private function process_connections_add() {
        if (!$this->connections_add_enabled) {
            $this->show_warning_message(__('Controller_Profile_Connections.add.vozmozhnost_dobavleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_connections_add_form();
        } else {
            $this->show_connections_add_form();
        }
    }

    private function process_connections_add_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_connections_add_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_connections_add_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $fields = array(
                Field::CONNECTION_NAME,
                Field::CONNECTION_DESCRIPTION,
                Field::ACC_CALLER_ID,
                Field::ACC_USER_NAME,
                Field::ACC_EXTENSION,
                Field::NETWORK_SIP_SERVER,
                Field::NETWORK_PROXY_SERVER,
                Field::NETWORK_LOCAL_ADDRESS,
                Field::NAT_STUN_SERVER
            );
            $this->process_connections_add_form_validation(Arr::map('UTF8::trim', Arr::map('strip_tags', $post, $fields)));
        }
    }

    private function process_connections_add_form_validation($post) {
        // validator
        $form_validator = Validation::factory($post);
        // connection
        $form_validator
                // connection
                ->rule(Field::CONNECTION_NAME, 'not_empty')
                ->rule(Field::CONNECTION_NAME, 'max_length', array(':value', 50))
                ->rule(Field::CONNECTION_NAME, 'Model_Profile_Connection::is_connection_name_available', array(':value', $this->account_id))
                // connection description
                ->rule(Field::CONNECTION_DESCRIPTION, 'not_empty')
                ->rule(Field::CONNECTION_DESCRIPTION, 'max_length', array(':value', 100));
        // account
        $form_validator
                // caller id
                ->rule(Field::ACC_CALLER_ID, 'not_empty')
                ->rule(Field::ACC_CALLER_ID, 'max_length', array(':value', 50))
                // extension
                ->rule(Field::ACC_EXTENSION, 'not_empty')
                ->rule(Field::ACC_EXTENSION, 'max_length', array(':value', 50))
                // user name
                ->rule(Field::ACC_USER_NAME, 'not_empty')
                ->rule(Field::ACC_USER_NAME, 'max_length', array(':value', 50))
                // password
                ->rule(Field::ACC_PASSWORD, 'max_length', array(':value', 50))
                // secret password (md5)
                ->rule(Field::ACC_SECRET_PASSWORD, 'exact_length', array(':value', 32))
                ->rule(Field::ACC_SECRET_PASSWORD, 'regex', array(':value', Regexpr::MD5_HASH));
        if (!isset($post[Field::CONNECTION_PASS_REQUIRED_FLAG]) AND !isset($post[Field::ACC_SECURE_FLAG])) {
            $form_validator->rule(Field::ACC_PASSWORD, 'not_empty');
        }
        if (!isset($post[Field::CONNECTION_PASS_REQUIRED_FLAG]) AND isset($post[Field::ACC_SECURE_FLAG])) {
            $form_validator->rule(Field::ACC_SECRET_PASSWORD, 'not_empty');
        }
        // network
        $form_validator
                // sip server address/port
                ->rule(Field::NETWORK_SIP_SERVER, 'not_empty')
                ->rule(Field::NETWORK_SIP_SERVER, 'max_length', array(':value', 50))
                ->rule(Field::NETWORK_SIP_SERVER_PORT, 'not_empty')
                ->rule(Field::NETWORK_SIP_SERVER_PORT, 'digit')
                ->rule(Field::NETWORK_SIP_SERVER_PORT, 'range', array(':value', 1, 65535))
                // sip proxy server address/port
                ->rule(Field::NETWORK_PROXY_SERVER, 'max_length', array(':value', 50))
                ->rule(Field::NETWORK_PROXY_SERVER_PORT, 'digit')
                ->rule(Field::NETWORK_PROXY_SERVER_PORT, 'range', array(':value', 1, 65535))
                // local address/port
                ->rule(Field::NETWORK_LOCAL_ADDRESS, 'max_length', array(':value', 50))
                ->rule(Field::NETWORK_LOCAL_PORT, 'digit')
                ->rule(Field::NETWORK_LOCAL_PORT, 'range', array(':value', 1, 65535))
                // rtp min/max port
                ->rule(Field::NETWORK_MIN_RTP_PORT, 'digit')
                ->rule(Field::NETWORK_MIN_RTP_PORT, 'range', array(':value', 1, 65500))
                ->rule(Field::NETWORK_MAX_RTP_PORT, 'digit')
                ->rule(Field::NETWORK_MAX_RTP_PORT, 'range', array(':value', (is_int($post[Field::NETWORK_MIN_RTP_PORT]) ? (int) $post[Field::NETWORK_MIN_RTP_PORT] : 100), 65535))
                // register expires
                ->rule(Field::NETWORK_REGISTER_EXPIRES, 'not_empty')
                ->rule(Field::NETWORK_REGISTER_EXPIRES, 'digit')
                ->rule(Field::NETWORK_REGISTER_EXPIRES, 'range', array(':value', 30, 7200));
        if (isset($post[Field::NETWORK_PROXY_FLAG])) {
            $form_validator
                    ->rule(Field::NETWORK_PROXY_SERVER, 'not_empty')
                    ->rule(Field::NETWORK_PROXY_SERVER_PORT, 'not_empty');
        }
        if (!isset($post[Field::NETWORK_DEFAULT_INTERFACE_FLAG])) {
            $form_validator->rule(Field::NETWORK_LOCAL_ADDRESS, 'not_empty');
        }
        if (!isset($post[Field::NETWORK_ANY_LOCAL_PORT_FLAG])) {
            $form_validator->rule(Field::NETWORK_LOCAL_PORT, 'not_empty');
        }
        // NAT
        $form_validator
                // keepalive
                ->rule(Field::NAT_KEEP_ALIVE, 'not_empty')
                ->rule(Field::NAT_KEEP_ALIVE, 'digit')
                ->rule(Field::NAT_KEEP_ALIVE, 'range', array(':value', 30, 300))
                // STUN server address/port
                ->rule(Field::NAT_STUN_SERVER, 'max_length', array(':value', 50))
                ->rule(Field::NAT_STUN_SERVER_PORT, 'digit')
                ->rule(Field::NAT_STUN_SERVER_PORT, 'range', array(':value', 1, 65535));
        if (!isset($post[Field::NAT_DEFAULT_STUN_SERVER_FLAG])) {
            $form_validator
                    ->rule(Field::NAT_STUN_SERVER, 'not_empty')
                    ->rule(Field::NAT_STUN_SERVER_PORT, 'not_empty');
        }
        // DTMF
        $form_validator
                // DTMF method
                ->rule(Field::DTMF_METHOD, 'not_empty')
                ->rule(Field::DTMF_METHOD, 'in_array', array(':value', Model_Profile_Connection::$DTMF_ENUM_LIST))
                // DTMF duration
                ->rule(Field::DTMF_DURATION, 'not_empty')
                ->rule(Field::DTMF_DURATION, 'digit')
                ->rule(Field::DTMF_DURATION, 'range', array(':value', 100, 5000))
                // DTMF payload
                ->rule(Field::DTMF_PAYLOAD, 'not_empty')
                ->rule(Field::DTMF_PAYLOAD, 'digit')
                ->rule(Field::DTMF_PAYLOAD, 'range', array(':value', 95, 128));
        // codec
        foreach (Field::$CODEC_FIELDS as $field) {
            if ($field == Field::CODEC_1) {
                $form_validator->rule($field, 'not_empty');
            }
            $form_validator->rule($field, 'in_array', array(':value', Model_Profile_Connection::$CODEC_ENUM_LIST));
        }
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_connections_add_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/connections_add');
            $this->show_connections_add_form(reset($errors));
        }
    }

    private function process_connections_add_validated_form(&$post) {
        $connection_data = array(
            Model_Profile_Connection::CONNECTION_NAME => $post[Field::CONNECTION_NAME],
            Model_Profile_Connection::CONNECTION_DESCRIPTION => $post[Field::CONNECTION_DESCRIPTION],
            Model_Profile_Connection::CONNECTION_ACTIVE_FLAG => isset($post[Field::CONNECTION_ACTIVE_FLAG]),
            Model_Profile_Connection::CONNECTION_PASS_REQUIRED_FLAG => isset($post[Field::CONNECTION_PASS_REQUIRED_FLAG])
        );
        $account_config = array(
            Model_Profile_Connection::ACCOUNT_CALLER_ID => $post[Field::ACC_CALLER_ID],
            Model_Profile_Connection::ACCOUNT_USER_NAME => $post[Field::ACC_USER_NAME],
            Model_Profile_Connection::ACCOUNT_EXTENSION => $post[Field::ACC_EXTENSION],
            Model_Profile_Connection::ACCOUNT_PASSWORD => $post[Field::ACC_PASSWORD],
            Model_Profile_Connection::ACCOUNT_SECRET_PASSWORD => $post[Field::ACC_SECRET_PASSWORD],
            Model_Profile_Connection::ACCOUNT_SECURE_FLAG => isset($post[Field::ACC_SECURE_FLAG])
        );
        $network_config = array(
            Model_Profile_Connection::NETWORK_SIP_SERVER => $post[Field::NETWORK_SIP_SERVER],
            Model_Profile_Connection::NETWORK_SIP_SERVER_PORT => $post[Field::NETWORK_SIP_SERVER_PORT],
            Model_Profile_Connection::NETWORK_PROXY_SERVER => $post[Field::NETWORK_PROXY_SERVER],
            Model_Profile_Connection::NETWORK_PROXY_SERVER_PORT => $post[Field::NETWORK_PROXY_SERVER_PORT],
            Model_Profile_Connection::NETWORK_LOCAL_ADDRESS => $post[Field::NETWORK_LOCAL_ADDRESS],
            Model_Profile_Connection::NETWORK_LOCAL_PORT => $post[Field::NETWORK_LOCAL_PORT],
            Model_Profile_Connection::NETWORK_MIN_RTP_PORT => $post[Field::NETWORK_MIN_RTP_PORT],
            Model_Profile_Connection::NETWORK_MAX_RTP_PORT => $post[Field::NETWORK_MAX_RTP_PORT],
            Model_Profile_Connection::NETWORK_REGISTER_EXPIRES => $post[Field::NETWORK_REGISTER_EXPIRES],
            Model_Profile_Connection::NETWORK_ANY_LOCAL_PORT_FLAG => isset($post[Field::NETWORK_ANY_LOCAL_PORT_FLAG]),
            Model_Profile_Connection::NETWORK_DEFAULT_INTERFACE_FLAG => isset($post[Field::NETWORK_DEFAULT_INTERFACE_FLAG]),
            Model_Profile_Connection::NETWORK_PROXY_FLAG => isset($post[Field::NETWORK_PROXY_FLAG])
        );
        $nat_config = array(
            Model_Profile_Connection::NAT_KEEP_ALIVE => $post[Field::NAT_KEEP_ALIVE],
            Model_Profile_Connection::NAT_STUN_SERVER => $post[Field::NAT_STUN_SERVER],
            Model_Profile_Connection::NAT_STUN_SERVER_PORT => $post[Field::NAT_STUN_SERVER_PORT],
            Model_Profile_Connection::NAT_STUN_LOOKUP_FLAG => isset($post[Field::NAT_STUN_LOOKUP_FLAG]),
            Model_Profile_Connection::NAT_DEFAULT_STUN_SERVER_FLAG => isset($post[Field::NAT_DEFAULT_STUN_SERVER_FLAG]),
            Model_Profile_Connection::NAT_KEEP_ALIVE_FLAG => isset($post[Field::NAT_KEEP_ALIVE_FLAG]),
            Model_Profile_Connection::NAT_SERVER_BIND_FLAG => isset($post[Field::NAT_SERVER_BIND_FLAG]),
            Model_Profile_Connection::NAT_SYMMETRIC_RTP_FLAG => isset($post[Field::NAT_SYMMETRIC_RTP_FLAG])
        );
        $dtmf_config = array(
            Model_Profile_Connection::DTMF_METHOD => $post[Field::DTMF_METHOD],
            Model_Profile_Connection::DTMF_DURATION => $post[Field::DTMF_DURATION],
            Model_Profile_Connection::DTMF_PAYLOAD => $post[Field::DTMF_PAYLOAD],
        );
        $codec_types = array(
            $post[Field::CODEC_1],
            $post[Field::CODEC_2],
            $post[Field::CODEC_3],
            $post[Field::CODEC_4],
            $post[Field::CODEC_5],
            $post[Field::CODEC_6],
            $post[Field::CODEC_7],
            $post[Field::CODEC_8],
            $post[Field::CODEC_9],
            $post[Field::CODEC_10],
            $post[Field::CODEC_11],
            $post[Field::CODEC_12]
        );
        if (Model_Profile_Connection::add_connection_profile($this->account_id, $connection_data, $account_config, $network_config, $nat_config, $dtmf_config, $codec_types)) {
            $this->show_success_message(__('Controller_Profile_Connections.add.profil_podklyucheniya_dobavlen'));
        } else {
            $this->show_connections_add_form(__('Controller_Profile_Connections.add.nevozmozhno_dobavit_profil_podklyucheniya,_povtorite_popytku'));
        }
    }

    private function show_connections_add_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/connections/connections_add_form')
                ->set('dtmf_methods', Kohana::message(I18n::lang() . '/profile/connections_add', 'dtmf_methods'))
                ->set('codec_types', Kohana::message(I18n::lang() . '/profile/connections_add', 'codec_types'))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(
            Request::factory('/menu/account/' . $this->account_id)->execute(),
            Request::factory('/menu/contacts/' . $this->account_id)->execute(),
            Request::factory('/menu/connections/' . $this->account_id . '/' . $this->request->action())->execute(),
        ));
        // navigation
        $this->show_navigation(
                Request::factory('/navigation/connections/' . $this->account_id . '/' . $this->request->action())
                        ->query('account_label', $this->account_profile[Model_Profile_Account::ACCOUNT_NAME])
                        ->execute()
        );
    }

}