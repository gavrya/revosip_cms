<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Contacts
 * contacts/$a_id/list - show contact list
 * contacts/$a_id/add - add contact
 * contacts/$a_id/remove - remove all contacts (todo)
 * contacts/$a_id/import - import account list from file (todo)
 * contacts/$a_id/export - export account list to file (todo)
 * contacts/$a_id/transfer - transfer account contact list to other account (todo)
 *
 * @author Гаврищук
 */
class Controller_Profile_Contacts extends Controller_Template_Profile {

    // data
    private $user_data;
    private $account_id;
    private $account_profile;
    private $contact_profiles_count;
    private $contact_profiles;
    // pagination
    private $pagination;
    // enabled config
    private $contacts_add_enabled;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_param_data() {
        $this->account_id = $this->request->param('account_id');
        if (!Model_Profile_Account::is_account_exists($this->account_id, $this->user_data[Model_Users::USER_ID])) {
            throw new HTTP_Exception_404('Account does not exist');
        }
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
            'account_id' => $this->request->param('account_id'),
        ));
    }

    private function init_account_profile() {
        $this->account_profile = Model_Profile_Account::get_account_profile($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_contact_profiles_count() {
        $this->contact_profiles_count = Model_Profile_Contact::count_contact_profiles($this->account_id);
    }

    private function init_contact_profiles($offset, $limit) {
        $this->contact_profiles = Model_Profile_Contact::get_contact_profiles($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->contacts_add_enabled = $config['contacts_add'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Contacts list action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_list() {
        $this->init_action_list();
        $this->set_action_list_html_data();
        $this->show_contacts_list();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Contacts.list.content_title'));
    }

    private function init_action_list() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_contact_profiles_count();
        $this->init_pagination($this->contact_profiles_count, 20);
        $this->init_contact_profiles($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_list_html_data() {
        $this->template->title = __('Controller_Profile_Contacts.list.title');
    }

    private function show_contacts_list() {
        if (empty($this->contact_profiles)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(
                    View::factory('/profile/contacts/contacts_list')
                            ->bind('contact_profiles', $this->contact_profiles)
                            ->bind('account_id', $this->account_id)
            );
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Add contact action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_add() {
        $this->init_action_add();
        $this->set_action_add_html_data();
        $this->process_contact_add();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Contacts.add.content_title'));
    }

    private function init_action_add() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_enabled_config();
    }

    private function set_action_add_html_data() {
        $this->template->title = __('Controller_Profile_Contacts.add.title');
    }

    private function process_contact_add() {
        if (!$this->contacts_add_enabled) {
            $this->show_warning_message(__('Controller_Profile_Contacts.add.vozmozhnost_dobavleniya_kontaktov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_contacts_add_form();
        } else {
            $this->show_contacts_add_form();
        }
    }

    private function process_contacts_add_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_contacts_add_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_contacts_add_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_contacts_add_form_validation(Arr::map(array('strip_tags', 'UTF8::trim'), $post));
        }
    }

    private function process_contacts_add_form_validation($post) {
        // login validator
        $form_validator = Validation::factory($post);
        // name validation rules
        $form_validator
                ->rule(Field::CONTACT_NAME, 'not_empty')
                ->rule(Field::CONTACT_NAME, 'max_length', array(':value', 50));
        // phone validation rules
        $form_validator
                ->rule(Field::CONTACT_PHONE, 'not_empty')
                ->rule(Field::CONTACT_PHONE, 'max_length', array(':value', 100))
                ->rule(Field::CONTACT_PHONE, 'regex', array(':value', Regexpr::CONTACT_PHONE_NUMBER))
                ->rule(Field::CONTACT_PHONE, 'Model_Profile_Contact::is_contact_phone_available', array(':value', $this->account_id));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_contacts_add_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/contacts_add');
            $this->show_contacts_add_form(reset($errors));
        }
    }

    private function process_contacts_add_validated_form($post) {
        if (Model_Profile_Contact::add_contact_profile($this->account_id, $post[Field::CONTACT_NAME], $post[Field::CONTACT_PHONE])) {
            $this->show_success_message(__('Controller_Profile_Contacts.add.kontakt_dobavlen'));
        } else {
            $this->show_contacts_add_form(__('Controller_Profile_Contacts.add.nevozmozhno_dobavit_kontakt,_povtorite_popytku'));
        }
    }

    private function show_contacts_add_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(View::factory('/profile/contacts/contacts_add_form'));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(
            Request::factory('/menu/account/' . $this->account_id)->execute(),
            Request::factory('/menu/contacts/' . $this->account_id . '/' . $this->request->action())->execute(),
            Request::factory('/menu/connections/' . $this->account_id)->execute(),
        ));
        // navigation
        $this->show_navigation(
                Request::factory('/navigation/contacts/' . $this->account_id . '/' . $this->request->action())
                        ->query('account_label', $this->account_profile[Model_Profile_Account::ACCOUNT_NAME])
                        ->execute()
        );
    }

}