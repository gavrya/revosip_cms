<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Profile
 *
 * @author Гаврищук
 */
class Controller_Profile_Profile extends Controller_Template_Profile {

    // data
    private $user_data;
    private $account_profiles_count;
    private $count_user_log_history;
    private $user_log_history;
    private $count_invoices;
    private $invoices;
    // pagination
    private $pagination;
    // enabled config
    private $profile_invoice_enabled;
    private $profile_edit_enabled;
    private $profile_email_enabled;
    // webmoney config
    private $webmoney_sim_mode;
    private $webmoney_use_url;
    private $webmoney_result_url;
    private $webmoney_success_url;
    private $webmoney_fail_url;
    private $credit_min_limit;
    private $credit_max_limit;
    private $wmz_purse;
    private $wmr_purse;
    private $wmu_purse;
    private $credit_to_wmz;
    private $credit_to_wmr;
    private $credit_to_wmu;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
        ));
    }

    private function init_account_profiles_count() {
        $this->account_profiles_count = Model_Profile_Account::count_account_profiles($this->user_data[Model_Users::USER_ID]);
    }

    private function init_count_user_log_history() {
        $this->count_user_log_history = Model_Users::count_user_log($this->user_data[Model_Users::USER_ID]);
    }

    private function init_user_log_history($offset, $limit) {
        $this->user_log_history = Model_Users::get_user_log($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_count_invoices() {
        $this->count_invoices = Model_Webmoney_Invoice::count_invoices($this->user_data[Model_Users::USER_ID]);
    }

    private function init_invoices($offset, $limit) {
        $this->invoices = Model_Webmoney_Invoice::get_invoices($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_webmoney_config() {
        // webmoney config group
        $config = Kohana::$config->load('app')->get('webmoney');
        // flags
        $this->webmoney_sim_mode = $config['sim_mode'];
        $this->webmoney_use_url = $config['use_url'];
        // webmoney callback urls
        $this->webmoney_result_url = $config['result_url'];
        $this->webmoney_success_url = $config['success_url'];
        $this->webmoney_fail_url = $config['fail_url'];
        // credit limits
        $this->credit_min_limit = $config['credit_min_limit'];
        $this->credit_max_limit = $config['credit_max_limit'];
        // purses
        $this->wmz_purse = $config['wmz_purse'];
        $this->wmr_purse = $config['wmr_purse'];
        $this->wmu_purse = $config['wmu_purse'];
        // exchange rate
        $this->credit_to_wmz = $config['credit_to_wmz'];
        $this->credit_to_wmr = $config['credit_to_wmr'];
        $this->credit_to_wmu = $config['credit_to_wmu'];
        //unset($config);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->profile_invoice_enabled = $config['profile_invoice'];
        $this->profile_edit_enabled = $config['profile_edit'];
        $this->profile_email_enabled = $config['profile_email'];
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Profile info action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_info() {
        $this->init_action_info();
        $this->set_action_info_html_data();
        $this->show_profile_info();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.info.content_title'));
    }

    private function init_action_info() {
        $this->init_user_data();
        $this->init_account_profiles_count();
    }

    private function set_action_info_html_data() {
        $this->template->title = __('Controller_Profile_Profile.info.title');
    }

    private function show_profile_info() {
        $this->show_content(View::factory('/profile/profile/profile_info')->bind('account_profiles_count', $this->account_profiles_count));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Profile edit action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_edit() {
        $this->init_action_edit();
        $this->set_action_edit_html_data();
        $this->process_profile_edit();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.edit.content_title'));
    }

    private function init_action_edit() {
        $this->init_user_data();
        $this->init_enabled_config();
    }

    private function set_action_edit_html_data() {
        $this->template->title = __('Controller_Profile_Profile.edit.title');
    }

    private function process_profile_edit() {
        if (!$this->profile_edit_enabled) {
            $this->show_warning_message(__('Controller_Profile_Profile.edit.vozmozhnost_redaktirovaniya_profilya_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_profile_edit_form();
        } else {
            $this->show_profile_edit_form();
        }
    }

    private function process_profile_edit_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_profile_edit_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_profile_edit_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_profile_edit_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_profile_edit_form_validation($post) {
        // registration validator
        $form_validator = Validation::factory($post);
        // name validation rules
        $form_validator
                ->rule(Field::USER_NAME, 'not_empty')
                ->rule(Field::USER_NAME, 'max_length', array(':value', 50)); //->rule(Field::USER_NAME, 'regex', array(':value', '/^([a-zа-яё0-9 ])+$/ui'));
        // pass validation rules
        $form_validator
                //->rule(Field::USER_PASSWORD, 'not_empty')
                ->rule(Field::USER_PASSWORD, 'min_length', array(':value', 5))
                ->rule(Field::USER_PASSWORD, 'max_length', array(':value', 50));
        // passconf validation rules
        $form_validator
                //->rule(Field::USER_REPASSWORD, 'not_empty')
                ->rule(Field::USER_REPASSWORD, 'matches', array(':validation', Field::USER_REPASSWORD, Field::USER_PASSWORD));
        // language validation rules
        $form_validator
                ->rule(Field::USER_LANGUAGE, 'not_empty')
                ->rule(Field::USER_LANGUAGE, 'in_array', array(':value', Model_Users::$LANGUAGE_ENUM_LIST));
        // timezone validation rules
        $form_validator
                ->rule(Field::USER_TIMEZONE, 'not_empty')
                ->rule(Field::USER_TIMEZONE, 'in_array', array(':value', Model_Users::$TIMEZONE_ENUM_LIST));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_profile_edit_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/registration');
            // repopulate reg form with errors
            $this->show_profile_edit_form(reset($errors));
        }
    }

    private function process_profile_edit_validated_form($post) {
        if (Model_Users::update_user_data($this->user_data[Model_Users::USER_ID], $post[Field::USER_NAME], $post[Field::USER_PASSWORD], $post[Field::USER_LANGUAGE], $post[Field::USER_TIMEZONE])) {
            $this->show_success_message(__('Controller_Profile_Profile.edit.izmeneniya_soxraneny'));
        } else {
            $this->show_profile_edit_form(__('Controller_Profile_Profile.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku'));
        }
    }

    private function show_profile_edit_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/profile/profile_edit_form')
                        ->bind('user_data', $this->user_data)
                        ->set('registration_timezones', Kohana::message(I18n::lang() . '/site/registration', 'registration_timezones'))
                        ->set('registration_languages', Kohana::message(I18n::lang() . '/site/registration', 'registration_languages'))
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Profile balance action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_balance() {
        $this->init_action_balance();
        $this->set_action_balance_html_data();
        $this->process_profile_balance();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.balance.content_title'));
    }

    private function init_action_balance() {
        $this->init_user_data();
    }

    private function set_action_balance_html_data() {
        $this->template->title = __('Controller_Profile_Profile.balance.title');
    }

    private function process_profile_balance() {
        $this->show_content(View::factory('/profile/profile/profile_balance')->bind('user_data', $this->user_data));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Profile invoice action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_invoice() {
        $this->init_action_invoice();
        $this->set_action_invoice_html_data();
        $this->process_profile_invoice();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.invoice.content_title'));
    }

    private function init_action_invoice() {
        $this->init_user_data();
        $this->init_enabled_config();
        $this->init_webmoney_config();
    }

    private function set_action_invoice_html_data() {
        $this->template->title = __('Controller_Profile_Profile.invoice.title');
    }

    private function process_profile_invoice() {
        if (!$this->profile_invoice_enabled) {
            $this->show_warning_message(__('Controller_Profile_Profile.invoice.vozmozhnost_popolneniya_balansa_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_profile_invoice_form();
        } else {
            $this->show_profile_invoice_form();
        }
    }

    private function process_profile_invoice_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_profile_invoice_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_profile_invoice_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_profile_invoice_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_profile_invoice_form_validation($post) {
        // registration validator
        $form_validator = Validation::factory($post);
        // name validation rules
        $form_validator
                ->rule(Field::REQUEST_CREDIT, 'not_empty')
                ->rule(Field::REQUEST_CREDIT, 'digit')
                ->rule(Field::REQUEST_CREDIT, 'range', array(':value', $this->credit_min_limit, $this->credit_max_limit));
        // pass validation rules
        $form_validator
                ->rule(Field::WEBMONEY_UNIT, 'not_empty')
                ->rule(Field::WEBMONEY_UNIT, 'in_array', array(':value', Model_Webmoney_Invoice::$UNIT_ENUM_LIST));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_profile_invoice_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/profile_invoice');
            // repopulate reg form with errors
            $this->show_profile_invoice_form(reset($errors));
        }
    }

    private function process_profile_invoice_validated_form($post) {
        $credit_to_amount = array(
            Model_Webmoney_Invoice::ENUM_UNIT_WMZ => $this->credit_to_wmz,
            Model_Webmoney_Invoice::ENUM_UNIT_WMR => $this->credit_to_wmr,
            Model_Webmoney_Invoice::ENUM_UNIT_WMU => $this->credit_to_wmu,
        );
        $unit_to_purse = array(
            Model_Webmoney_Invoice::ENUM_UNIT_WMZ => $this->wmz_purse,
            Model_Webmoney_Invoice::ENUM_UNIT_WMR => $this->wmr_purse,
            Model_Webmoney_Invoice::ENUM_UNIT_WMU => $this->wmu_purse,
        );
        $invoice_unit = $post[Field::WEBMONEY_UNIT];
        $request_credit = $post[Field::REQUEST_CREDIT];
        $invoice_amount = round($request_credit * $credit_to_amount[$invoice_unit], 2);
        $invoice_purse = $unit_to_purse[$invoice_unit];
        $invoice_desc = base64_encode(__('Controller_Profile_Profile.invoice.popolnenie_balansa_polzovatelya') . ' ' . HTML::chars($this->user_data[Model_Users::USER_NAME]));
        $invoice_id = Model_Webmoney_Invoice::add_invoice($this->user_data[Model_Users::USER_ID], $invoice_unit, $invoice_purse, $invoice_amount);
        if ($invoice_id > 0) {
            $this->show_profile_invoice_request_form($request_credit, $invoice_amount, $invoice_unit, $invoice_purse, $invoice_id, $invoice_desc);
        } else {
            $this->show_warning_message(__('Controller_Profile_Profile.invoice.nevozmozhno_popolnit_balans,_povtorite_popytku'));
        }
    }

    private function show_profile_invoice_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/profile/profile_invoice_form')
                        ->set('invoice_units', Kohana::message(I18n::lang() . '/profile/profile_invoice', 'invoice_units'))
                        ->bind('credit_min_limit', $this->credit_min_limit)
                        ->bind('credit_max_limit', $this->credit_max_limit)
                        ->bind('user_data', $this->user_data)
        );
    }

    private function show_profile_invoice_request_form($request_credit, $invoice_amount, $invoice_unit, $invoice_purse, $invoice_id, $invoice_desc) {
        $this->show_content(
                View::factory('/profile/profile/profile_invoice_request_form')
                        ->bind('request_credit', $request_credit)
                        ->bind('invoice_amount', $invoice_amount)
                        ->bind('invoice_unit', $invoice_unit)
                        ->bind('invoice_purse', $invoice_purse)
                        ->bind('invoice_id', $invoice_id)
                        ->bind('invoice_desc', $invoice_desc)
                        ->bind('sim_mode', $this->webmoney_sim_mode)
                        ->bind('use_url', $this->webmoney_use_url)
                        ->bind('result_url', $this->webmoney_result_url)
                        ->bind('success_url', $this->webmoney_success_url)
                        ->bind('fail_url', $this->webmoney_fail_url)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Profile invoices action                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_invoices() {
        $this->init_action_invoices();
        $this->set_action_invoices_html_data();
        $this->show_profile_invoices();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.invoices.content_title'));
    }

    private function init_action_invoices() {
        $this->init_user_data();
        $this->init_count_invoices();
        $this->init_pagination($this->count_invoices, 20);
        $this->init_invoices($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_invoices_html_data() {
        $this->template->title = __('Controller_Profile_Profile.invoices.title');
    }

    private function show_profile_invoices() {
        if (empty($this->invoices)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(
                    View::factory('/profile/profile/profile_invoices')
                            ->bind('invoices', $this->invoices)
                            ->bind('pagination', $this->pagination)
            );
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Profile log action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_log() {
        $this->init_action_log();
        $this->set_action_log_html_data();
        $this->process_profile_log();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.log.content_title'));
    }

    private function init_action_log() {
        $this->init_user_data();
        $this->init_count_user_log_history();
        $this->init_pagination($this->count_user_log_history, 20);
        $this->init_user_log_history($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_log_html_data() {
        $this->template->title = __('Controller_Profile_Profile.log.title');
    }

    private function process_profile_log() {
        if (empty($this->user_log_history)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(
                    View::factory('/profile/profile/profile_log')
                            ->bind('user_log_history', $this->user_log_history)
                            ->bind('pagination', $this->pagination)
            );
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Profile email action                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_email() {
        $this->init_action_email();
        $this->set_action_email_html_data();
        $this->process_profile_email();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Profile.email.content_title'));
    }

    private function init_action_email() {
        $this->init_user_data();
        $this->init_enabled_config();
    }

    private function set_action_email_html_data() {
        $this->template->title = __('Controller_Profile_Profile.email.title');
    }

    private function process_profile_email() {
        if ($this->request->param('action_type') == 'update') {
            $this->process_profile_email_update();
        } else if ($this->request->param('action_type') == 'confirm') {
            $this->process_profile_email_confirm();
        } else if (!$this->profile_email_enabled) {
            $this->show_warning_message(__('Controller_Profile_Profile.email.vozmozhnost_smeny_email_adresa_profilya_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_profile_email_form();
        } else {
            $this->show_profile_email();
        }
    }

    private function process_profile_email_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_profile_email(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_profile_email(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_profile_email_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_profile_email_form_validation($post) {
        // registration validator
        $form_validator = Validation::factory($post);
        // name validation rules
        $form_validator
                ->rule(Field::USER_EMAIL, 'not_empty')
                ->rule(Field::USER_EMAIL, 'email')
                ->rule(Field::USER_EMAIL, 'Model_Users::is_user_email_available');
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_profile_email_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/site/registration');
            // repopulate reg form with errors
            $this->show_profile_email(reset($errors));
        }
    }

    private function process_profile_email_validated_form($post) {
        $record_hash = Model_Users::add_user_email_update_record($this->user_data[Model_Users::USER_ID]);
        $result = Model_Users::set_user_new_email($this->user_data[Model_Users::USER_ID], $post[Field::USER_EMAIL]);
        if ($record_hash && $result && Emailer::instance()->send_profile_email_update_confirm_link($this->user_data[Model_Users::USER_NAME], $this->user_data[Model_Users::USER_EMAIL], $record_hash)) {
            $this->show_info_message(__('Controller_Profile_Profile.email.na_vash_tekushhij_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa'));
        } else {
            $this->show_warning_message(__('Controller_Profile_Profile.email.nevozmozhno_smenit_email_adres_profilya,_povtorite_popytku'));
        }
    }

    private function process_profile_email_update() {
        $update_record = Model_Users::get_user_email_update_record($this->user_data[Model_Users::USER_ID]);
        if ($update_record && $update_record[Model_Users::RECORD_HASH] == $this->request->param('hash')) {
            $record_hash = Model_Users::add_user_email_update_record($this->user_data[Model_Users::USER_ID]);
            if ($record_hash && Emailer::instance()->send_profile_email_confirm_link($this->user_data[Model_Users::USER_NAME], $this->user_data[Model_Users::USER_NEW_EMAIL], $record_hash)) {
                $this->show_info_message(__('Controller_Profile_Profile.email.na_vash_novyj_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa'));
            } else {
                $this->show_warning_message(__('Controller_Profile_Profile.email.nevozmozhno_smenit_email_adres_profilya,_povtorite_popytku'));
            }
        } else {
            $this->show_warning_message(__('Controller_Profile_Profile.email.nevozmozhno_podtverdit_smenu_email_adresa_profilya,_povtorite_popytku'));
        }
    }

    private function process_profile_email_confirm() {
        $update_record = Model_Users::get_user_email_update_record($this->user_data[Model_Users::USER_ID]);
        if ($update_record && $update_record[Model_Users::RECORD_HASH] == $this->request->param('hash') && !empty($this->user_data[Model_Users::USER_NEW_EMAIL]) && Model_Users::update_user_email($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_NEW_EMAIL])) {
            $this->show_success_message(__('Controller_Profile_Profile.email.email_adres_profilya_izmenen_na_sleduyushhij') . ' ' . '<strong>' . HTML::chars($this->user_data[Model_Users::USER_NEW_EMAIL]) . '</strong>');
        } else {
            $this->show_warning_message(__('Controller_Profile_Profile.email.nevozmozhno_podtverdit_smenu_email_adresa_profilya,_povtorite_popytku'));
        }
    }

    private function show_profile_email($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(View::factory('/profile/profile/profile_email'));
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(Request::factory('/menu/profile/' . $this->request->action())->execute()));
        // navigation
        $this->show_navigation(Request::factory('/navigation/profile/' . $this->request->action())->execute());
    }

}