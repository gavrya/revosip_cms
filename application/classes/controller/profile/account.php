<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Controller_Profile_Account
 * account/$a_id/info - show account info
 * account/$a_id/activate - activate account
 * account/$a_id/disactivate - disactivate account
 * account/$a_id/edit - edit account
 * account/$a_id/remove - remove account
 * account/$a_id/settings - edit account settings
 * account/$a_id/prolong - prolong account license
 * account/$a_id/prolongs - prolongs history
 * account/$a_id/log - login/logout history
 *
 * @author Гаврищук
 */
class Controller_Profile_Account extends Controller_Template_Profile {

    // data
    private $user_data;
    private $account_id;
    private $captcha;
    private $account_profile;
    private $contact_profiles_count;
    private $connection_profiles_count;
    private $connection_profiles;
    private $settings_profile;
    private $account_prolongs_count;
    private $account_prolongs;
    private $accounts_logs_count;
    private $account_logs;
    // pagination
    private $pagination;
    // enabled config
    private $account_prolong_enabled;
    private $account_settings_enabled;
    private $account_edit_enabled;
    private $account_remove_enabled;
    // prolong config
    private $prolong_tariff;
    private $prolong_days;
    private $prolong_days_min_limit;
    private $prolong_days_max_limit;
    private $credit_prolong_days_limit;
    private $credits_per_day;
    private $days_per_credit;
    private $prolong_days_min;
    private $prolong_days_max;
    private $prolong_credit_min;
    private $prolong_credit_max;

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Init                                   */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function init_user_data() {
        $this->user_data = Model_Auth::instance()->get_user_data();
    }

    private function init_param_data() {
        $this->account_id = $this->request->param('account_id');
        if (!Model_Profile_Account::is_account_exists($this->account_id, $this->user_data[Model_Users::USER_ID])) {
            throw new HTTP_Exception_404('Account does not exist');
        }
    }

    private function init_captcha() {
        $this->captcha = Captcha::instance();
    }

    private function init_pagination($total_items, $items_per_page) {
        $this->pagination = Pagination::factory(array(
                    'total_items' => $total_items,
                    'items_per_page' => $items_per_page,
                ))
                ->route_params(array(
            'controller' => Request::current()->controller(),
            'action' => Request::current()->action(),
            'account_id' => $this->request->param('account_id'),
        ));
    }

    private function init_account_profile() {
        $this->account_profile = Model_Profile_Account::get_account_profile($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_contact_profiles_count() {
        $this->contact_profiles_count = Model_Profile_Contact::count_contact_profiles($this->account_id);
    }

    private function init_connection_profiles_count() {
        $this->connection_profiles_count = Model_Profile_Connection::count_connection_profiles($this->account_id);
    }

    private function init_connection_profiles() {
        $this->connection_profiles = Model_Profile_Connection::get_connection_profiles($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE]);
    }

    private function init_settings_profile() {
        $this->settings_profile = Model_Profile_Settings::get_settings_profile($this->account_id);
    }

    private function init_account_prolongs_count() {
        $this->account_prolongs_count = Model_Profile_Account::count_prolong_history($this->user_data[Model_Users::USER_ID], $this->account_id);
    }

    private function init_account_prolongs($offset, $limit) {
        $this->account_prolongs = Model_Profile_Account::get_prolong_history($this->user_data[Model_Users::USER_ID], $this->user_data[Model_Users::USER_TIMEZONE], $this->account_id, $offset, $limit);
    }

    private function init_accounts_logs_count() {
        $this->accounts_logs_count = Model_Profile_Account::count_user_account_logs($this->user_data[Model_Users::USER_ID], $this->account_id);
    }

    private function init_account_logs($offset, $limit) {
        $this->account_logs = Model_Profile_Account::get_account_logs($this->account_id, $this->user_data[Model_Users::USER_TIMEZONE], $offset, $limit);
    }

    private function init_enabled_config() {
        $config = Kohana::$config->load('app')->get('profile_enabled');
        $this->account_prolong_enabled = $config['account_prolong'];
        $this->account_settings_enabled = $config['account_settings'];
        $this->account_edit_enabled = $config['account_edit'];
        $this->account_remove_enabled = $config['account_remove'];
        //unset($config);
    }

    private function init_prolong_config() {
        // prolong config
        $config = Kohana::$config->load('app')->get('prolong');
        // prolong data
        $this->prolong_tariff = $config['tariff'];
        $this->prolong_days = $config['days'];
        $this->credits_per_day = $this->prolong_tariff / $this->prolong_days;
        $this->days_per_credit = $this->prolong_days / $this->prolong_tariff;
        // prolong days min limit
        $this->prolong_days_min_limit = $config['days_min_limit'];
        // prolong days max limit
        $this->prolong_days_max_limit = $config['days_max_limit'];
        // prolong days min/max
        $this->credit_prolong_days_limit = (int) floor($this->user_data[Model_Users::USER_CREDIT] * $this->days_per_credit);
        // prolong days days remain
        $this->prolong_days_remain_limit = $this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRES_DAYS] >= $this->prolong_days_max_limit ? 0 : $this->prolong_days_max_limit - $this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRES_DAYS];
        // prolong days min/max
        //$prolong_min_array = array($this->prolong_days_min_limit, $this->prolong_days_max_limit, $this->credit_prolong_days_limit, $this->prolong_days_remain_limit);
        //$prolong_max_array = array($this->prolong_days_max_limit, $this->credit_prolong_days_limit, $this->prolong_days_remain_limit);
        //var_dump($prolong_min_array);
        //var_dump($prolong_max_array);
        $this->prolong_days_min = min($this->prolong_days_min_limit, $this->prolong_days_max_limit, $this->credit_prolong_days_limit, $this->prolong_days_remain_limit);
        $this->prolong_days_max = min($this->prolong_days_max_limit, $this->credit_prolong_days_limit, $this->prolong_days_remain_limit);
        // prolong credit min/max
        $this->prolong_credit_min = round($this->prolong_days_min * $this->credits_per_day, 2);
        $this->prolong_credit_max = round($this->prolong_days_max * $this->credits_per_day, 2);
        //unset($config);
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Account info action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_info() {
        $this->init_action_info();
        $this->set_action_info_html_data();
        $this->show_account_info();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.info.content_title'));
    }

    private function init_action_info() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_contact_profiles_count();
        $this->init_connection_profiles_count();
    }

    private function set_action_info_html_data() {
        $this->template->title = __('Controller_Profile_Account.info.title');
    }

    private function show_account_info() {
        $this->show_content(
                View::factory('/profile/account/account_info')
                        ->bind('account_profile', $this->account_profile)
                        ->bind('contact_profiles_count', $this->contact_profiles_count)
                        ->bind('connection_profiles_count', $this->connection_profiles_count)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Account edit action                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_edit() {
        $this->init_action_edit();
        $this->set_action_edit_html_data();
        $this->process_account_edit();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.edit.content_title'));
    }

    private function init_action_edit() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_enabled_config();
    }

    private function set_action_edit_html_data() {
        $this->template->title = __('Controller_Profile_Account.edit.title');
    }

    private function process_account_edit() {
        if (!$this->account_edit_enabled) {
            $this->show_warning_message(__('Controller_Profile_Account.edit.vozmozhnost_redaktirovaniya_akkauntov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_account_edit_post_form();
        } else {
            $this->show_account_edit_form();
        }
    }

    private function process_account_edit_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_account_edit_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_account_edit_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            $this->process_account_edit_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_account_edit_form_validation($post) {
        // form validator
        $form_validator = Validation::factory($post);
        // account name validation rules
        $form_validator
                ->rule(Field::ACCOUNT_NAME, 'not_empty')
                ->rule(Field::ACCOUNT_NAME, 'max_length', array(':value', 50))
                ->rule(Field::ACCOUNT_NAME, 'Model_Profile_Account::is_account_name_available', array(':value', $this->user_data[Model_Users::USER_ID], $this->account_id));
        // user validation rules
        $form_validator
                ->rule(Field::ACCOUNT_USER, 'not_empty')
                ->rule(Field::ACCOUNT_USER, 'max_length', array(':value', 50));
        // login validation rules
        $form_validator
                ->rule(Field::ACCOUNT_LOGIN, 'not_empty')
                ->rule(Field::ACCOUNT_LOGIN, 'min_length', array(':value', 4))
                ->rule(Field::ACCOUNT_LOGIN, 'max_length', array(':value', 50))
                ->rule(Field::ACCOUNT_LOGIN, 'regex', array(':value', Regexpr::ACCOUNT_LOGIN))
                ->rule(Field::ACCOUNT_LOGIN, 'Model_Profile_Account::is_account_login_available', array(':value', $this->user_data[Model_Users::USER_ID], $this->account_id));
        // password validation rules
        $form_validator
                //->rule(Field::ACCOUNT_PASSWORD, 'not_empty')
                ->rule(Field::ACCOUNT_PASSWORD, 'min_length', array(':value', 5))
                ->rule(Field::ACCOUNT_PASSWORD, 'max_length', array(':value', 50));
        // passconf validation rules
        $form_validator
                //->rule(Field::ACCOUNT_REPASSWORD, 'not_empty')
                ->rule(Field::ACCOUNT_REPASSWORD, 'matches', array(':validation', Field::ACCOUNT_PASSWORD, Field::ACCOUNT_REPASSWORD));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_account_edit_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(strtolower(I18n::lang()) . '/profile/accounts_add');
            $this->show_account_edit_form(reset($errors));
            //unset($errors);
        }
    }

    private function process_account_edit_validated_form($post) {
        if (Model_Profile_Account::update_account_profile($this->account_id, $post[Field::ACCOUNT_NAME], $post[Field::ACCOUNT_USER], $post[Field::ACCOUNT_LOGIN], $post[Field::ACCOUNT_PASSWORD], isset($post[Field::ACCOUNT_ACTIVATE]))) {
            $this->show_success_message(__('Controller_Profile_Account.edit.izmeneniya_soxraneny'));
        } else {
            $this->show_account_edit_form(__('Controller_Profile_Account.edit.ne_udalos_soxranit_izmeneniya,_povtorite_popytku'));
        }
    }

    private function show_account_edit_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/account/account_edit_form')
                        ->bind('user_data', $this->user_data)
                        ->bind('account_profile', $this->account_profile)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Account settings action                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_settings() {
        $this->init_action_settings();
        $this->set_action_settings_html_data();
        $this->process_account_settings();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.settings.content_title'));
    }

    private function init_action_settings() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_connection_profiles();
        $this->init_enabled_config();
    }

    private function set_action_settings_html_data() {
        $this->template->title = __('Controller_Profile_Account.settings.title');
    }

    private function process_account_settings() {
        if (!$this->account_settings_enabled) {
            $this->show_warning_message(__('Controller_Profile_Account.settings.vozmozhnost_izmeneniya_nastroek_akkauntov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_account_settings_post_form();
        } else {
            $this->show_account_settings_form();
        }
    }

    private function process_account_settings_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_account_settings_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!isset($post[Field::CSRF]) || !Security::check($post[Field::CSRF])) {
            $this->show_account_settings_form(__('Controller.identifikator_formy_ne_sovpadaet'));
        } else {
            // default_connection_id
            $default_connection_id = Arr::get($post, Field::DEFAULT_CONNECTION_ID, 0);
            // default_connection_flag
            $default_connection_flag = isset($post[Field::DEFAULT_CONNECTION_FLAG]);
            // update settings
            $this->update_account_settings($default_connection_id, $default_connection_flag);
        }
    }

    private function update_account_settings($default_connection_id, $default_connection_flag) {
        if (is_numeric($default_connection_id) && Model_Profile_Settings::update_settings_profile($this->account_id, $default_connection_id, $default_connection_flag)) {
            $this->show_success_message(__('Controller_Profile_Account.settings.nastrojki_akkaunta_soxraneny'));
        } else {
            $this->show_account_settings_form(__('Controller_Profile_Account.settings.nevozmozhno_soxranit_nastrojki_akkaunta,_povtorite_popytku'));
        }
    }

    private function show_account_settings_form($message = NULL) {
        $this->init_settings_profile();
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/account/account_settings_form')
                        ->bind('account_id', $this->account_id)
                        ->bind('connection_profiles', $this->connection_profiles)
                        ->bind('settings_profile', $this->settings_profile)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Account remove action                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_remove() {
        $this->init_action_remove();
        $this->set_action_remove_html_data();
        $this->process_account_remove();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.remove.content_title'));
    }

    private function init_action_remove() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_captcha();
        $this->init_account_profile();
        $this->init_enabled_config();
    }

    private function set_action_remove_html_data() {
        $this->template->title = __('Controller_Profile_Account.remove.title');
    }

    private function process_account_remove() {
        if (!$this->account_remove_enabled) {
            $this->show_warning_message(__('Controller_Profile_Account.remove.vozmozhnost_udaleniya_akkauntov_na_sajte_vremenno_priostanovlena'));
        //} else if ($this->account_profile[Model_Profile_Account::ACCOUNT_EXPIRES_DAYS] > 0) {
        //    $this->show_info_message(__('Controller_Profile_Account.remove.nevozmozhno_udalit_akkaunt,_srok_dejstviya_akkaunta_ne_istek'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_account_remove_post_form();
        } else {
            $this->show_account_remove_form();
        }
    }

    private function process_account_remove_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_account_remove_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!key_exists(Field::CAPTCHA, $post) || !Captcha::valid($post[Field::CAPTCHA])) {
            $this->show_account_remove_form(__('Controller.vvedite_pravilnyj_proverochnyj_kod'));
        } else {
            $this->remove_account();
        }
    }

    private function remove_account() {
        Model_Profile_Account::remove_account_profile($this->account_id);
        $this->request->redirect('/accounts/list');
    }

    private function show_account_remove_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/account/account_remove_form')
                        ->bind('captcha', $this->captcha)
                        ->bind('account_profile', $this->account_profile)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                       Account prolong action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_prolong() {
        $this->init_action_prolong();
        $this->set_action_prolong_html_data();
        $this->process_account_prolong();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.prolong.content_title'));
    }

    private function init_action_prolong() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_captcha();
        $this->init_account_profile();
        $this->init_enabled_config();
        $this->init_prolong_config();
    }

    private function set_action_prolong_html_data() {
        $this->template->title = __('Controller_Profile_Account.prolong.title');
    }

    private function process_account_prolong() {
        if (!$this->account_prolong_enabled) {
            $this->show_warning_message(__('Controller_Profile_Account.prolong.vozmozhnost_prodleniya_akkauntov_na_sajte_vremenno_priostanovlena'));
        } else if ($this->prolong_days_remain_limit == 0) {
            $this->show_info_message(__('Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_dostignut_maksimalnyj_srok_prodleniya_akkaunta'));
        } else if ($this->credit_prolong_days_limit == 0) {
            $this->show_warning_message(__('Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_neobxodimo_popolnit_balans'));
        } else if ($this->request->method() == HTTP_Request::POST) {
            $this->process_account_prolong_post_form();
        } else {
            $this->show_account_prolong_form();
        }
    }

    private function process_account_prolong_post_form() {
        $post = $this->request->post();
        if (strpos($this->request->referrer(), URL::base(TRUE)) === FALSE) {
            $this->show_account_prolong_form(__('Controller.mezhsajtovye_xss_zaprosy_zapreshheny'));
        } else if (!key_exists(Field::CAPTCHA, $post) || !Captcha::valid($post[Field::CAPTCHA])) {
            $this->show_account_prolong_form(__('Controller.vvedite_pravilnyj_proverochnyj_kod'));
        } else {
            $this->process_account_prolong_form_validation(Arr::map('UTF8::trim', $post));
        }
    }

    private function process_account_prolong_form_validation($post) {
        // form validator
        $form_validator = Validation::factory($post);
        // days validation rules
        $form_validator
                ->rule(Field::PROLONG_DAYS, 'not_empty')
                ->rule(Field::PROLONG_DAYS, 'digit')
                ->rule(Field::PROLONG_DAYS, 'range', array(':value', $this->prolong_days_min, $this->prolong_days_max));
        // check validation result
        if ($form_validator->check()) {
            // process validated form
            $this->process_account_prolong_validated_form($post);
        } else {
            // validation errors
            $errors = $form_validator->errors(I18n::lang() . '/profile/account_prolong');
            $this->show_account_prolong_form(reset($errors));
        }
    }

    private function process_account_prolong_validated_form($post) {
        // convert days to credit
        $prolong_credit = round($post[Field::PROLONG_DAYS] * $this->credits_per_day, 2);
        // check user credit
        if ($prolong_credit > $this->user_data[Model_Users::USER_CREDIT]) {
            $this->show_warning_message(__('Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_neobxodimo_popolnit_balans'));
        } else if (Model_Profile_Account::prolong_account_profile($this->account_id, $this->user_data[Model_Users::USER_ID], $prolong_credit, $post[Field::PROLONG_DAYS])) {
            $this->show_success_message(__('Controller_Profile_Account.prolong.akkaunt_uspeshno_prodlen'));
        } else {
            $this->show_account_prolong_form(__('Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_povtorite_popytku'));
        }
    }

    private function show_account_prolong_form($message = NULL) {
        if (isset($message)) {
            $this->show_warning_message($message);
        }
        $this->show_content(
                View::factory('/profile/account/account_prolong_form')
                        ->bind('captcha', $this->captcha)
                        ->bind('user_data', $this->user_data)
                        ->bind('account_profile', $this->account_profile)
                        ->bind('prolong_tariff', $this->prolong_tariff)
                        ->bind('prolong_days', $this->prolong_days)
                        ->bind('prolong_days_remain_limit', $this->prolong_days_remain_limit)
                        ->bind('prolong_days_min_limit', $this->prolong_days_min_limit)
                        ->bind('prolong_days_max_limit', $this->prolong_days_max_limit)
                        ->bind('credit_prolong_days_limit', $this->credit_prolong_days_limit)
                        ->bind('prolong_credit_min', $this->prolong_credit_min)
                        ->bind('prolong_credit_max', $this->prolong_credit_max)
                        ->bind('prolong_days_min', $this->prolong_days_min)
                        ->bind('prolong_days_max', $this->prolong_days_max)
        );
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                         Account prolongs action                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_prolongs() {
        $this->init_action_prolongs();
        $this->set_action_prolongs_html_data();
        $this->show_account_prolongs();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.prolongs.content_title'));
    }

    private function init_action_prolongs() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_account_prolongs_count();
        $this->init_pagination($this->account_prolongs_count, 20);
        $this->init_account_prolongs($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_prolongs_html_data() {
        $this->template->title = __('Controller_Profile_Account.prolongs.title');
    }

    private function show_account_prolongs() {
        if (empty($this->account_prolongs)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/profile/account/account_prolongs')->bind('account_prolongs', $this->account_prolongs));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Account log action                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_log() {
        $this->init_action_log();
        $this->set_action_log_html_data();
        $this->show_account_log();
        $this->show_menu_and_navigation();
        $this->show_content_title(__('Controller_Profile_Account.log.content_title'));
    }

    private function init_action_log() {
        $this->init_user_data();
        $this->init_param_data();
        $this->init_account_profile();
        $this->init_accounts_logs_count();
        $this->init_pagination($this->accounts_logs_count, 20);
        $this->init_account_logs($this->pagination->offset, $this->pagination->items_per_page);
    }

    private function set_action_log_html_data() {
        $this->template->title = __('Controller_Profile_Account.log.title');
    }

    private function show_account_log() {
        if (empty($this->account_logs)) {
            $this->show_info_message(__('Controller.net_dannyx_dlya_otobrazheniya'));
        } else {
            $this->show_content(View::factory('/profile/account/account_log')->bind('account_logs', $this->account_logs));
            $this->show_content_pagination($this->pagination->render());
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                 Account activate/disactivate actions                  */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public function action_activate() {
        $this->init_action_activate_disactivate();
        $this->update_account_status(TRUE);
    }

    public function action_disactivate() {
        $this->init_action_activate_disactivate();
        $this->update_account_status(FALSE);
    }

    private function init_action_activate_disactivate() {
        $this->init_user_data();
        $this->init_param_data();
    }

    private function update_account_status($status_flag) {
        // anti xss hack
        if ($this->request->method() == HTTP_Request::GET && strpos($this->request->referrer(), URL::base(TRUE)) !== FALSE) {
            Model_Profile_Account::update_account_profile_status($this->account_id, $status_flag);
            $this->request->redirect($this->request->referrer());
        } else {
            $this->request->redirect('/accounts/list');
        }
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        show menu and navigation                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    private function show_menu_and_navigation() {
        // menu
        $this->show_menu(array(
            Request::factory('/menu/account/' . $this->account_id . '/' . $this->request->action())->execute(),
            Request::factory('/menu/contacts/' . $this->account_id)->execute(),
            Request::factory('/menu/connections/' . $this->account_id)->execute(),
        ));
        // navigation
        $this->show_navigation(
                Request::factory('/navigation/account/' . $this->account_id . '/' . $this->request->action())
                        ->query('account_label', $this->account_profile[Model_Profile_Account::ACCOUNT_NAME])
                        ->execute()
        );
    }

}
