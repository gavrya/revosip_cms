<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Field
 *
 * @author Гаврищук
 */
class Field {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Base fields                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const ID = '_id';
    const CAPTCHA = 'captcha';
    const SUBMIT = 'submit';
    const CSRF = 'csrf';
    const MAGIC_PASSWORD = 'magic_password';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Account fields                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const ACCOUNT_NAME = 'a1';
    const ACCOUNT_USER = 'a2';
    const ACCOUNT_LOGIN = 'a3';
    const ACCOUNT_PASSWORD = 'a4';
    const ACCOUNT_REPASSWORD = 'a5';
    const ACCOUNT_ACTIVATE = 'a6';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                        Account settings fields                        */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const DEFAULT_CONNECTION_ID = 'b1';
    const DEFAULT_CONNECTION_FLAG = 'b2';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Prolong fields                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const PROLONG_CREDIT = 'c1';
    const PROLONG_DAYS = 'c2';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Contact fields                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const CONTACT_NAME = 'd1';
    const CONTACT_PHONE = 'd2';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            User fields                                */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const USER_LOGIN = 'e1';
    const USER_PASSWORD = 'e2';
    const USER_REPASSWORD = 'e3';
    const USER_NAME = 'e4';
    const USER_EMAIL = 'e5';
    const USER_LANGUAGE = 'e6';
    const USER_TIMEZONE = 'e7';
    const USER_ACCEPT = 'e8';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                           Connection fields                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const CONNECTION_NAME = 'f1';
    const CONNECTION_DESCRIPTION = 'f2';
    const CONNECTION_ACTIVE_FLAG = 'f3';
    const CONNECTION_PASS_REQUIRED_FLAG = 'f4';

    // Account config
    const ACC_CALLER_ID = 'f5';
    const ACC_USER_NAME = 'f6';
    const ACC_EXTENSION = 'f7';
    const ACC_PASSWORD = 'f8';
    const ACC_SECRET_PASSWORD = 'f9';
    const ACC_SECURE_FLAG = 'f10';

    // Network config
    const NETWORK_SIP_SERVER = 'f11';
    const NETWORK_SIP_SERVER_PORT = 'f12';
    const NETWORK_PROXY_SERVER = 'f13';
    const NETWORK_PROXY_SERVER_PORT = 'f14';
    const NETWORK_LOCAL_ADDRESS = 'f15';
    const NETWORK_LOCAL_PORT = 'f16';
    const NETWORK_MIN_RTP_PORT = 'f17';
    const NETWORK_MAX_RTP_PORT = 'f18';
    const NETWORK_REGISTER_EXPIRES = 'f19';
    const NETWORK_ANY_LOCAL_PORT_FLAG = 'f20';
    const NETWORK_DEFAULT_INTERFACE_FLAG = 'f21';
    const NETWORK_PROXY_FLAG = 'f22';

    // Nat config
    const NAT_KEEP_ALIVE = 'f23';
    const NAT_STUN_SERVER = 'f24';
    const NAT_STUN_SERVER_PORT = 'f25';
    const NAT_STUN_LOOKUP_FLAG = 'f26';
    const NAT_DEFAULT_STUN_SERVER_FLAG = 'f27';
    const NAT_KEEP_ALIVE_FLAG = 'f28';
    const NAT_SERVER_BIND_FLAG = 'f29';
    const NAT_SYMMETRIC_RTP_FLAG = 'f30';

    // DTMF config
    const DTMF_METHOD = 'f31';
    const DTMF_DURATION = 'f32';
    const DTMF_PAYLOAD = 'f33';

    // Codec config
    const CODEC_1 = 'g1';
    const CODEC_2 = 'g2';
    const CODEC_3 = 'g3';
    const CODEC_4 = 'g4';
    const CODEC_5 = 'g5';
    const CODEC_6 = 'g6';
    const CODEC_7 = 'g7';
    const CODEC_8 = 'g8';
    const CODEC_9 = 'g9';
    const CODEC_10 = 'g10';
    const CODEC_11 = 'g11';
    const CODEC_12 = 'g12';

    public static $CODEC_FIELDS = array(
        Field::CODEC_1,
        Field::CODEC_2,
        Field::CODEC_3,
        Field::CODEC_4,
        Field::CODEC_5,
        Field::CODEC_6,
        Field::CODEC_7,
        Field::CODEC_8,
        Field::CODEC_9,
        Field::CODEC_10,
        Field::CODEC_11,
        Field::CODEC_12,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Profile fields                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const REQUEST_CREDIT = 'h1';
    const WEBMONEY_UNIT = 'h2';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Webmoney fields                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const LMI_PAYEE_PURSE = 'LMI_PAYEE_PURSE';
    const LMI_PAYMENT_AMOUNT = 'LMI_PAYMENT_AMOUNT';
    const LMI_PAYMENT_NO = 'LMI_PAYMENT_NO';
    const LMI_PAYMENT_DESC = 'LMI_PAYMENT_DESC';
    const LMI_PAYMENT_DESC_BASE64 = 'LMI_PAYMENT_DESC_BASE64';
    const LMI_SIM_MODE = 'LMI_SIM_MODE';
    const LMI_MODE = 'LMI_MODE';
    const LMI_RESULT_URL = 'LMI_RESULT_URL';
    const LMI_SUCCESS_URL = 'LMI_SUCCESS_URL';
    const LMI_SUCCESS_METHOD = 'LMI_SUCCESS_METHOD';
    const LMI_FAIL_URL = 'LMI_FAIL_URL';
    const LMI_FAIL_METHOD = 'LMI_FAIL_METHOD';
    const LMI_PAYMER_PINNUMBERINSIDE = 'LMI_PAYMER_PINNUMBERINSIDE';
    const LMI_WMNOTE_PINNUMBERINSIDE = 'LMI_WMNOTE_PINNUMBERINSIDE';
    const LMI_PAYMER_EMAIL = 'LMI_PAYMER_EMAIL';
    const LMI_WMCHECK_NUMBERINSIDE = 'LMI_WMCHECK_NUMBERINSIDE';
    const LMI_WMCHECK_CODEINSIDE = 'LMI_WMCHECK_CODEINSIDE';
    const LMI_ALLOW_SDP = 'LMI_ALLOW_SDP';
    const LMI_FAST_PHONENUMBER = 'LMI_FAST_PHONENUMBER';
    const LMI_PAYMENT_CREDITDAYS = 'LMI_PAYMENT_CREDITDAYS';
    const LMI_PREREQUEST = 'LMI_PREREQUEST';
    const LMI_SYS_INVS_NO = 'LMI_SYS_INVS_NO';
    const LMI_SYS_TRANS_NO = 'LMI_SYS_TRANS_NO';
    const LMI_PAYER_PURSE = 'LMI_PAYER_PURSE';
    const LMI_PAYER_WM = 'LMI_PAYER_WM';
    const LMI_CAPITALLER_WMID = 'LMI_CAPITALLER_WMID';
    const LMI_PAYMER_NUMBER = 'LMI_PAYMER_NUMBER';
    const LMI_EURONOTE_NUMBER = 'LMI_EURONOTE_NUMBER';
    const LMI_EURONOTE_EMAIL = 'LMI_EURONOTE_EMAIL';
    const LMI_WMCHECK_NUMBER = 'LMI_WMCHECK_NUMBER';
    const LMI_TELEPAT_PHONENUMBER = 'LMI_TELEPAT_PHONENUMBER';
    const LMI_TELEPAT_ORDERID = 'LMI_TELEPAT_ORDERID';
    const LMI_HASH = 'LMI_HASH';
    const LMI_SYS_TRANS_DATE = 'LMI_SYS_TRANS_DATE';
    const LMI_SECRET_KEY = 'LMI_SECRET_KEY';
    const LMI_SDP_TYPE = 'LMI_SDP_TYPE';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              News fields                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const NEWS_MARK = 'i1';
    const NEWS_TITLE = 'i2';
    const NEWS_META_KEYWORDS = 'i3';
    const NEWS_META_DESCRIPTIONS = 'i4';
    const NEWS_CONTENT = 'i5';

}

?>
