<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Assets
 *
 * @author Гаврищук
 */
class Assets {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                CSS Styles                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const STYLE_CSS_BASE = '/assets/styles/base.css';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Javascript                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const SCRIPT_JQUERY = '/assets/scripts/jquery-1.10.2.min.js';

    public static function get_bootstrap_css($theme = 'default') {
        return '/assets/bootstrap/' . $theme . '/css/bootstrap.min.css';
    }

    public static function get_bootstrap_responsive_css($theme = 'default') {
        return '/assets/bootstrap/' . $theme . '/css/bootstrap-responsive.min.css';
    }

    public static function get_bootstrap_js($theme = 'default') {
        return '/assets/bootstrap/' . $theme . '/js/bootstrap.min.js';
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                                Images                                 */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const IMAGE_AJAX_LOADER = '/assets/images/loader.gif';
    const IMAGE_FLAG_RU = '/assets/images/ru.png';
    const IMAGE_FLAG_UA = '/assets/images/ua.png';
    const IMAGE_FLAG_EN = '/assets/images/en.png';

}
