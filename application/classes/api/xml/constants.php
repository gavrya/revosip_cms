<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Constants
 *
 * @author Гаврищук
 */
class Api_Xml_Constants {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            XML elements                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const ELEMENT_REQUEST = 'request';
    const ELEMENT_RESPONSE = 'response';
    const ELEMENT_ACCOUNT = 'account';
    const ELEMENT_ACCOUNT_PROFILE = 'account_profile';
    const ELEMENT_AUTHORIZATION = 'authorization';
    const ELEMENT_SETTINGS_PROFILE = 'settings_profile';
    const ELEMENT_MAP = 'map';
    const ELEMENT_CONNECTION_PROFILES = 'connection_profiles';
    const ELEMENT_CONNECTION_PROFILE = 'connection_profile';
    const ELEMENT_ACCOUNT_CONFIG = 'account_config';
    const ELEMENT_NETWORK_CONFIG = 'network_config';
    const ELEMENT_NAT_CONFIG = 'nat_config';
    const ELEMENT_DTMF_CONFIG = 'dtmf_config';
    const ELEMENT_CODEC_CONFIG = 'codec_config';
    const ELEMENT_CODEC = 'codec';
    const ELEMENT_CONFIG = 'config';
    const ELEMENT_CONTACT_PROFILES = 'contact_profiles';
    const ELEMENT_CONTACT_PROFILE = 'contact_profile';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            XML attributes                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const ATTRIBUTE_CODE = 'code';
    const ATTRIBUTE_PROTOCOL = 'protocol';
    const ATTRIBUTE_VERSION = 'version';
    const ATTRIBUTE_HASH = 'hash';
    const ATTRIBUTE_KEY = 'key';
    const ATTRIBUTE_VALUE = 'value';
    const ATTRIBUTE_ID = 'id';
    const ATTRIBUTE_PROFILE = 'profile';
    const ATTRIBUTE_LOGIN = 'login';
    const ATTRIBUTE_USER_NAME = 'user_name';
    const ATTRIBUTE_ACCESS_HASH = 'access_hash';
    const ATTRIBUTE_PASSWORD = 'password';
    const ATTRIBUTE_NAME = 'name';
    const ATTRIBUTE_DESCRIPTION = 'description';
    const ATTRIBUTE_PASSWORD_REQUIRED = 'password_required';
    const ATTRIBUTE_CALLER_ID = 'caller_id';
    const ATTRIBUTE_EXTENSION = 'extension';
    const ATTRIBUTE_SECRET_PASSWORD = 'secret_password';
    const ATTRIBUTE_SECURE_FLAG = 'secure_flag';
    const ATTRIBUTE_SIP_SERVER = 'sip_server';
    const ATTRIBUTE_SIP_SERVER_PORT = 'sip_server_port';
    const ATTRIBUTE_PROXY_SERVER = 'proxy_server';
    const ATTRIBUTE_PROXY_SERVER_PORT = 'proxy_server_port';
    const ATTRIBUTE_LOCAL_ADDRESS = 'local_address';
    const ATTRIBUTE_LOCAL_PORT = 'local_port';
    const ATTRIBUTE_MIN_RTP_PORT = 'min_rtp_port';
    const ATTRIBUTE_MAX_RTP_PORT = 'max_rtp_port';
    const ATTRIBUTE_REGISTER_EXPIRES = 'register_expires';
    const ATTRIBUTE_ANY_LOCAL_PORT_FLAG = 'any_local_port_flag';
    const ATTRIBUTE_DEFAULT_INTERFACE_FLAG = 'default_interface_flag';
    const ATTRIBUTE_DEFAULT_PROXY_FLAG = 'default_proxy_flag';
    const ATTRIBUTE_KEEP_ALIVE = 'keep_alive';
    const ATTRIBUTE_STUN_SERVER = 'stun_server';
    const ATTRIBUTE_STUN_SERVER_PORT = 'stun_server_port';
    const ATTRIBUTE_STUN_LOOKUP_FLAG = 'stun_lookup_flag';
    const ATTRIBUTE_DEFAULT_STUN_SERVER_FLAG = 'default_stun_server_flag';
    const ATTRIBUTE_KEEP_ALIVE_FLAG = 'keep_alive_flag';
    const ATTRIBUTE_SERVER_BIND_FLAG = 'server_bind_flag';
    const ATTRIBUTE_SYMMETRIC_RTP_FLAG = 'symmetric_rtp_flag';
    const ATTRIBUTE_METHOD = 'method';
    const ATTRIBUTE_DURATION = 'duration';
    const ATTRIBUTE_PAYLOAD = 'payload';
    const ATTRIBUTE_TYPE = 'type';
    const ATTRIBUTE_PHONE_NUMBER = 'phone_number';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            XML attribute values                       */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    const VALUE_DEFAULT_CONNECTION_ID = 'default_connection_id';
    const VALUE_DEFAULT_CONNECTION_FLAG = 'default_connection_flag';

}