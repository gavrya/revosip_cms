<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Helper
 *
 * @author Гаврищук
 */
class Api_Xml_Helper {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Is methods                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function is_xml_request($request) {
        return UTF8::strpos(UTF8::trim(UTF8::strtolower($request)), '<?xml') === 0;
    }

    public static function is_valid_request($request) {
        try {
            $reader = new XMLReader();
            $reader->XML($request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_REQUEST) {
                    $code = $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_CODE);
                    $protocol = $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_PROTOCOL);
                    $version = $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_VERSION);
                    return !empty($code) && !empty($protocol) && !empty($version) && in_array($code, Api_Constants::$request_codes);
                }
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    public static function is_valid_protocol($request) {
        return self::get_protocol_version($request) == Api_Constants::PROTOCOL_VERSION;
    }

    public static function is_valid_version($request) {
        return self::get_application_version($request) == Api_Constants::APPLICATION_VERSION;
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                              Get methods                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_request_code($request) {
        try {
            $reader = new XMLReader();
            $reader->XML($request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_REQUEST) {
                    return (int) $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_CODE);
                }
            }
        } catch (Exception $ex) {
            return null;
        }
    }

    public static function get_protocol_version($request) {
        try {
            $reader = new XMLReader();
            $reader->XML($request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_REQUEST) {
                    return $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_PROTOCOL);
                }
            }
        } catch (Exception $ex) {
            return null;
        }
    }

    public static function get_application_version($request) {
        try {
            $reader = new XMLReader();
            $reader->XML($request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == Api_Xml_Constants::ELEMENT_REQUEST) {
                    return $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_VERSION);
                }
            }
        } catch (Exception $ex) {
            return null;
        }
    }

    public static function get_profile_hash($request, $element) {
        try {
            $reader = new XMLReader();
            $reader->XML($request, 'utf-8');
            while ($reader->read()) {
                if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == $element) {
                    return $reader->getAttribute(Api_Xml_Constants::ATTRIBUTE_HASH);
                }
            }
        } catch (Exception $ex) {
            return null;
        }
    }

}