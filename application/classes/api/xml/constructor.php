<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Constructor
 *
 * @author Гаврищук
 */
class Api_Xml_Constructor {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            AccountProfile                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_account_login_success_response($id, $profile, $login, $user_name, $access_hash) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_ACCOUNT_PROFILE);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ID, $id);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PROFILE, $profile);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_LOGIN, $login);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_USER_NAME, $user_name);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ACCESS_HASH, $access_hash);
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            SettingsProfile                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_settings_load_success_response($settings_hash, $settings_map) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_SETTINGS_PROFILE);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_HASH, $settings_hash); {
                    // default_connection_id
                    $writer->startElement(Api_Xml_Constants::ELEMENT_MAP);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_KEY, Api_Xml_Constants::VALUE_DEFAULT_CONNECTION_ID);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_VALUE, (string) $settings_map[Model_Profile_Settings::SETTINGS_DEFAULT_CONNECTION_ID]);
                    $writer->endElement();
                    // default_connection_flag
                    $writer->startElement(Api_Xml_Constants::ELEMENT_MAP);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_KEY, Api_Xml_Constants::VALUE_DEFAULT_CONNECTION_FLAG);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_VALUE, $settings_map[Model_Profile_Settings::SETTINGS_DEFAULT_CONNECTION_FLAG] == TRUE ? 'true' : 'false');
                    $writer->endElement();
                }
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            ConnectionsProfile                         */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_connections_load_success_response($connection_profiles_hash, $connection_profiles, $connections_codec_config) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_CONNECTION_PROFILES);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_HASH, $connection_profiles_hash); {
                    if (is_array($connection_profiles) && !empty($connection_profiles)) {
                        foreach ($connection_profiles as $connection_profile) {
                            if ($connection_profile[Model_Profile_Connection::CONNECTION_ACTIVE_FLAG] == FALSE) {
                                continue;
                            }
                            // connection profile
                            $writer->startElement(Api_Xml_Constants::ELEMENT_CONNECTION_PROFILE);
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ID, $connection_profile[Model_Profile_Connection::CONNECTION_ID]);
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_NAME, HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_NAME]));
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_DESCRIPTION, HTML::chars($connection_profile[Model_Profile_Connection::CONNECTION_DESCRIPTION]));
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PASSWORD_REQUIRED, $connection_profile[Model_Profile_Connection::CONNECTION_PASS_REQUIRED_FLAG] == TRUE ? 'true' : 'false'); {
                                $writer->startElement(Api_Xml_Constants::ELEMENT_CONFIG); {
                                    // account config
                                    $writer->startElement(Api_Xml_Constants::ELEMENT_ACCOUNT_CONFIG);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CALLER_ID, HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_CALLER_ID]));
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_USER_NAME, HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_USER_NAME]));
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_EXTENSION, HTML::chars($connection_profile[Model_Profile_Connection::ACCOUNT_EXTENSION]));
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PASSWORD, (string) $connection_profile[Model_Profile_Connection::ACCOUNT_PASSWORD]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SECRET_PASSWORD, (string) $connection_profile[Model_Profile_Connection::ACCOUNT_SECRET_PASSWORD]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SECURE_FLAG, $connection_profile[Model_Profile_Connection::ACCOUNT_SECURE_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->endElement();
                                    // network config
                                    $writer->startElement(Api_Xml_Constants::ELEMENT_NETWORK_CONFIG);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SIP_SERVER, HTML::chars($connection_profile[Model_Profile_Connection::NETWORK_SIP_SERVER]));
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SIP_SERVER_PORT, $connection_profile[Model_Profile_Connection::NETWORK_SIP_SERVER_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PROXY_SERVER, (string) $connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PROXY_SERVER_PORT, (string) $connection_profile[Model_Profile_Connection::NETWORK_PROXY_SERVER_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_LOCAL_ADDRESS, (string) $connection_profile[Model_Profile_Connection::NETWORK_LOCAL_ADDRESS]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_LOCAL_PORT, (string) $connection_profile[Model_Profile_Connection::NETWORK_LOCAL_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_MIN_RTP_PORT, $connection_profile[Model_Profile_Connection::NETWORK_MIN_RTP_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_MAX_RTP_PORT, $connection_profile[Model_Profile_Connection::NETWORK_MAX_RTP_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_REGISTER_EXPIRES, $connection_profile[Model_Profile_Connection::NETWORK_REGISTER_EXPIRES]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ANY_LOCAL_PORT_FLAG, $connection_profile[Model_Profile_Connection::NETWORK_ANY_LOCAL_PORT_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_DEFAULT_INTERFACE_FLAG, $connection_profile[Model_Profile_Connection::NETWORK_DEFAULT_INTERFACE_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_DEFAULT_PROXY_FLAG, $connection_profile[Model_Profile_Connection::NETWORK_PROXY_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->endElement();
                                    // nat config
                                    $writer->startElement(Api_Xml_Constants::ELEMENT_NAT_CONFIG);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_KEEP_ALIVE, $connection_profile[Model_Profile_Connection::NAT_KEEP_ALIVE]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_STUN_SERVER, (string) $connection_profile[Model_Profile_Connection::NAT_STUN_SERVER]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_STUN_SERVER_PORT, (string) $connection_profile[Model_Profile_Connection::NAT_STUN_SERVER_PORT]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_STUN_LOOKUP_FLAG, $connection_profile[Model_Profile_Connection::NAT_STUN_LOOKUP_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_DEFAULT_STUN_SERVER_FLAG, $connection_profile[Model_Profile_Connection::NAT_DEFAULT_STUN_SERVER_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_KEEP_ALIVE_FLAG, $connection_profile[Model_Profile_Connection::NAT_KEEP_ALIVE_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SERVER_BIND_FLAG, $connection_profile[Model_Profile_Connection::NAT_SERVER_BIND_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_SYMMETRIC_RTP_FLAG, $connection_profile[Model_Profile_Connection::NAT_SYMMETRIC_RTP_FLAG] == TRUE ? 'true' : 'false');
                                    $writer->endElement();
                                    // dtmf config
                                    $writer->startElement(Api_Xml_Constants::ELEMENT_DTMF_CONFIG);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_METHOD, $connection_profile[Model_Profile_Connection::DTMF_METHOD]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_DURATION, $connection_profile[Model_Profile_Connection::DTMF_DURATION]);
                                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PAYLOAD, $connection_profile[Model_Profile_Connection::DTMF_PAYLOAD]);
                                    $writer->endElement(); {
                                        $writer->startElement(Api_Xml_Constants::ELEMENT_CODEC_CONFIG); {
                                            foreach ($connections_codec_config[$connection_profile[Model_Profile_Connection::CONNECTION_ID]] as $connection_codec_config) {
                                                $writer->startElement(Api_Xml_Constants::ELEMENT_CODEC);
                                                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_TYPE, $connection_codec_config[Model_Profile_Connection::CODEC_TYPE]);
                                                $writer->endElement();
                                            }
                                        }
                                        $writer->endElement();
                                    }
                                }
                                $writer->endElement();
                            }
                            $writer->endElement();
                        }
                    }
                }
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            ContactsProfile                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_contacts_load_success_response($contact_profiles_hash, $contact_profiles) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_CONTACT_PROFILES);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_HASH, $contact_profiles_hash); {
                    if (is_array($contact_profiles) && !empty($contact_profiles)) {
                        foreach ($contact_profiles as $contact_profile) {
                            // contact_profile
                            $writer->startElement(Api_Xml_Constants::ELEMENT_CONTACT_PROFILE);
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ID, $contact_profile[Model_Profile_Contact::CONTACT_ID]);
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_USER_NAME, HTML::chars($contact_profile[Model_Profile_Contact::CONTACT_NAME]));
                            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PHONE_NUMBER, HTML::chars($contact_profile[Model_Profile_Contact::CONTACT_PHONE]));
                            $writer->endElement();
                        }
                    }
                }
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    public static function get_contact_add_success_response($contact_profiles_hash, $contact_id, $contact_user_name, $contact_phone) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_CONTACT_PROFILES);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_HASH, $contact_profiles_hash); {
                    // contact_profile
                    $writer->startElement(Api_Xml_Constants::ELEMENT_CONTACT_PROFILE);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_ID, $contact_id);
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_USER_NAME, HTML::chars($contact_user_name));
                    $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_PHONE_NUMBER, HTML::chars($contact_phone));
                    $writer->endElement();
                }
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    public static function get_contacts_success_response($contact_profiles_hash) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, Api_Constants::RESPONSE_SUCCESS); {
                $writer->startElement(Api_Xml_Constants::ELEMENT_CONTACT_PROFILES);
                $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_HASH, $contact_profiles_hash);
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Get response                               */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    public static function get_response($code) {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString('    ');
        $writer->startDocument('1.0', 'utf-8'); {
            $writer->startElement(Api_Xml_Constants::ELEMENT_RESPONSE);
            $writer->writeAttribute(Api_Xml_Constants::ATTRIBUTE_CODE, $code);
            $writer->endElement();
        }
        $writer->endDocument();
        return $writer->outputMemory();
    }

}