<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Constants
 *
 * @author Гаврищук
 */
class Api_Constants {
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Basic constants                            */
    /*                                                                       */
    /* --------------------------------------------------------------------- */

    const PROTOCOL_VERSION = '1.0';
    const APPLICATION_VERSION = '1.0.4';

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Request codes                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Account requests
    const REQUEST_ACCOUNT_LOGIN = 1;
    const REQUEST_ACCOUNT_LOGOUT = 2;
    const REQUEST_ACCOUNT_PING = 3;
    // Settings profile requests
    const REQUEST_SETTINGS_PROFILE_SYNC = 10;
    const REQUEST_SETTINGS_PROFILE_LOAD = 11;
    // Connection profiles requests
    const REQUEST_CONNECTION_PROFILES_SYNC = 20;
    const REQUEST_CONNECTION_PROFILES_LOAD = 21;
    // Contact profiles requests
    const REQUEST_CONTACT_PROFILES_SYNC = 30;
    const REQUEST_CONTACT_PROFILES_LOAD = 31;
    const REQUEST_CONTACT_PROFILE_ADD = 32;
    const REQUEST_CONTACT_PROFILE_MODIFY = 33;
    const REQUEST_CONTACT_PROFILE_REMOVE = 34;

    public static $request_codes = array(
        self::REQUEST_ACCOUNT_LOGIN,
        self::REQUEST_ACCOUNT_LOGOUT,
        self::REQUEST_ACCOUNT_PING,
        self::REQUEST_SETTINGS_PROFILE_SYNC,
        self::REQUEST_SETTINGS_PROFILE_LOAD,
        self::REQUEST_CONNECTION_PROFILES_SYNC,
        self::REQUEST_CONNECTION_PROFILES_LOAD,
        self::REQUEST_CONTACT_PROFILES_SYNC,
        self::REQUEST_CONTACT_PROFILES_LOAD,
        self::REQUEST_CONTACT_PROFILE_ADD,
        self::REQUEST_CONTACT_PROFILE_MODIFY,
        self::REQUEST_CONTACT_PROFILE_REMOVE,
    );

    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                            Response codes                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Success repsonse

    const RESPONSE_SUCCESS = 200;
    // Failure repsonse
    const RESPONSE_BAD_REQUEST = 400;
    const RESPONSE_INVALID_PROTOCOL = 401;
    const RESPONSE_INVALID_VERSION = 402;
    const RESPONSE_ACCESS_DENIED = 403;
    const RESPONSE_ACCOUNT_EXPIRED = 404;
    const RESPONSE_ACCOUNT_ALREADY_IN_USE = 405;
    const RESPONSE_INCORRECT_PROFILE_OR_LOGIN_OR_PASSWORD = 406;
    const RESPONSE_FAILURE = 407;
    const RESPONSE_ERROR = 408;
    const RESPONSE_TIMEOUT = 409;

}