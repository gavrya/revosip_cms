<?php

defined('SYSPATH') or die('No direct script access.');

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                           Account API routes                          */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('api', 'api/account(/<controller>/<action>)', array('controller' => '(account|settings|connections|contacts)', 'action' => '(login|logout|ping|sync|load|add|modify|remove)'))
        ->defaults(array(
            'directory' => 'api',
            'controller' => 'dispatcher',
            'action' => 'account',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                      Profile controller routes                        */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('profile', 'profile(/<action>)(/<page>)', array('action' => '(edit|balance|invoice|invoices|prolongs|log)', 'page' => '\d+'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'profile',
            'action' => 'info',
        ));

Route::set('profile_email', 'profile/email(/<action_type>/<hash>)', array('action_type' => '(update|confirm)', 'hash' => '[0-9a-f]{32}'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'profile',
            'action' => 'email',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Accounts controller routes                      */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('accounts', 'accounts/<action>(/<page>)', array('action' => '(list|add|log|prolongs)', 'page' => '\d+'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'accounts'
        ));

Route::set('account', 'account/<account_id>/<action>(/<page>)', array('account_id' => '\d+', 'action' => '(info|edit|settings|prolong|prolongs|log|remove|activate|disactivate)', 'page' => '\d+'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'account'
        ));


/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Contacts controller routes                      */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('contacts', 'contacts/<account_id>/<action>', array('account_id' => '\d+', 'action' => '(list|add|import|export|transfer|remove)'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'contacts'
        ));

Route::set('contact', 'contact/<account_id>/<contact_id>/<action>', array('account_id' => '\d+', 'contact_id' => '\d+', 'action' => '(info|edit|remove)'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'contact'
        ));


/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Connections controller routes                   */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('connections', 'connections/<account_id>/<action>', array('account_id' => '\d+', 'action' => '(list|add)'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'connections'
        ));

Route::set('connection', 'connection/<account_id>/<connection_id>/<action>', array('account_id' => '\d+', 'connection_id' => '\d+', 'action' => '(info|edit|remove|activate|disactivate)'))
        ->defaults(array(
            'directory' => 'profile',
            'controller' => 'connection'
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                      Registration controller routes                   */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('registration', 'registration(/<action>/<user_id>/<confirm_hash>)', array('action' => 'confirm', 'user_id' => '\d+', 'confirm_hash' => '[0-9a-f]{32}'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'registration',
            'action' => 'registration',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Login controller routes                         */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('login', '<action>', array('action' => '(login|logout)'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'login',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Recovery controller routes                      */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('recovery', 'recovery(/<action>/<user_id>/<recovery_hash>)', array('action' => 'confirm', 'user_id' => '\d+', 'recovery_hash' => '[0-9a-f]{32}'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'recovery',
            'action' => 'recovery',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Menu controller routes                          */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('menu_accounts', 'menu/accounts(/<action_type>)', array('action_type' => '(list|log|add|prolongs)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'accounts',
        ));

Route::set('menu_account', 'menu/account/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(info|edit|settings|prolong|prolongs|log|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'account',
        ));

Route::set('menu_contacts', 'menu/contacts/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(list|add)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'contacts',
        ));

Route::set('menu_contact', 'menu/contact/<account_id>/<contact_id>(/<action_type>)', array('account_id' => '\d+', 'contact_id' => '\d+', 'action_type' => '(info|edit|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'contact',
        ));

Route::set('menu_connections', 'menu/connections/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(list|add)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'connections',
        ));

Route::set('menu_connection', 'menu/connection/<account_id>/<connection_id>(/<action_type>)', array('account_id' => '\d+', 'connection_id' => '\d+', 'action_type' => '(info|edit|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'connection',
        ));

Route::set('menu_profile', 'menu/profile(/<action_type>)', array('action_type' => '(info|edit|email|balance|invoice|invoices|log)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'menu',
            'action' => 'profile',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Navigation controller routes                    */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('navigation_accounts', 'navigation/accounts(/<action_type>)', array('action_type' => '(list|log|add|prolongs)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'accounts',
        ));

Route::set('navigation_account', 'navigation/account/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(info|edit|settings|prolong|prolongs|log|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'account',
        ));

Route::set('navigation_contacts', 'navigation/contacts/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(list|add)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'contacts',
        ));

Route::set('navigation_contact', 'navigation/contact/<account_id>/<contact_id>(/<action_type>)', array('account_id' => '\d+', 'contact_id' => '\d+', 'action_type' => '(info|edit|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'contact',
        ));

Route::set('navigation_connections', 'navigation/connections/<account_id>(/<action_type>)', array('account_id' => '\d+', 'action_type' => '(list|add)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'connections',
        ));

Route::set('navigation_connection', 'navigation/connection/<account_id>/<connection_id>(/<action_type>)', array('account_id' => '\d+', 'connection_id' => '\d+', 'action_type' => '(info|edit|remove)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'connection',
        ));

Route::set('navigation_profile', 'navigation/profile/<action_type>', array('action_type' => '(info|edit|email|balance|invoice|invoices|log)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'navigation',
            'action' => 'profile',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                             Widget routes                             */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('widget_news', 'widget/news')
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'news',
            'action' => 'list',
        ));

Route::set('widget_launch', 'launch/<action>', array('action' => '(launcher|jnlp|applet|download)'))
        ->defaults(array(
            'directory' => 'widget',
            'controller' => 'launch',
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                              Ajax routes                              */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('ajax_check', 'ajax/check/<action>/-<login>-(/<user_id>)(/<account_id>)', array('login' => '.+', 'user_id' => '\d+', 'account_id' => '\d+'))
        ->defaults(array(
            'directory' => 'ajax',
            'controller' => 'check',
            'login' => '',
            'user_id' => 0,
            'account_id' => 0,
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Webmoney controller routes                      */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('webmoney', 'webmoney/<action>', array('action' => '(result|success|fail)'))
        ->defaults(array(
            'controller' => 'webmoney'
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                       Error controller routes                         */
/*                                                                       */
/* --------------------------------------------------------------------- */
Route::set('error', 'error/<action>(/<message>)', array('action' => '\d+', 'message' => '.+'))
        ->defaults(array(
            'controller' => 'error'
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                        News controller routes                         */
/*                                                                       */
/* --------------------------------------------------------------------- */
Route::set('news_list', 'news(/<page>)', array('page' => '\d+'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'news',
            'action' => 'list',
        ));

Route::set('news', 'news/<action>(/<news_id>)', array('action' => '(view|edit|remove|add)', 'news_id' => '\d+'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'news'
        ));

/* --------------------------------------------------------------------- */
/*                                                                       */
/*                          Default routes                               */
/*                                                                       */
/* --------------------------------------------------------------------- */

Route::set('default', '(<action>)(/<section_id>-<question_id>)', array('action' => '(about|faq|feedback|conditions)', 'section_id' => '\d+', 'question_id' => '\d+'))
        ->defaults(array(
            'directory' => 'site',
            'controller' => 'home',
            'action' => 'home',
        ));