<?php

defined('SYSPATH') or die('No direct script access.');

// http://mainspy.ru/translit_perevodchik
return array(
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Basic section                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller
    'Controller.identifikator_formy_ne_sovpadaet' => 'Идентификатор формы не совпадает',
    'Controller.vvedite_pravilnyj_proverochnyj_kod' => 'Введите правильный проверочный код',
    'Controller.proverochnyj_kod' => 'Проверочный код:',
    'Controller.mezhsajtovye_xss_zaprosy_zapreshheny' => 'Межсайтовые XSS запросы запрещены',
    'Controller.net_dannyx_dlya_otobrazheniya' => 'Нет данных для отображения',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Site section                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Site_Home
    // home action
    'Controller_Site_Home.home.title' => 'Облачный JAVA SIP клиент',
    'Controller_Site_Home.home.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Home.home.description' => 'Revosip - облачный JAVA SIP клиент',
    'Controller_Site_Home.home.revosip_-_oblachnyj_sip_softfon' => 'Revosip - облачный JAVA SIP клиент',
    'Controller_Site_Home.home.otkrytyj_beta_test' => 'Открытый бета тест',
    'Controller_Site_Home.home.registraciya' => 'Регистрация',
    'Controller_Site_Home.home.preimushhestva' => 'Преимущества',
    'Controller_Site_Home.home.sravnenie_s_ip_telefonom' => 'Сравнение с IP телефоном',
    'Controller_Site_Home.home.xarakteristiki' => 'Характеристики',
    'Controller_Site_Home.home.skrinshoty' => 'Скриншоты',
    'Controller_Site_Home.home.osobennosti' => 'Особенности',
    'Controller_Site_Home.home.vozmozhnosti' => 'Возможности',
    'Controller_Site_Home.home.predmet_sravneniya' => 'Предмет сравнения',
    'Controller_Site_Home.home.programmnyj_telefon_revosip' => 'Программный телефон Revosip',
    'Controller_Site_Home.home.apparatnyj_ip_telefon' => 'Аппаратный IP телефон',
    // about action
    'Controller_Site_Home.about.content_title' => 'О сервисе',
    'Controller_Site_Home.about.title' => 'О сервисе',
    'Controller_Site_Home.about.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Home.about.description' => 'Revosip - облачный JAVA SIP клиент',
    // faq action
    'Controller_Site_Home.faq.content_title' => 'Вопросы - Ответы',
    'Controller_Site_Home.faq.title' => 'Вопросы - Ответы',
    'Controller_Site_Home.faq.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Home.faq.description' => 'Вопросы - Ответы',
    'Controller_Site_Home.faq.ssylka_na_otvet' => 'Ссылка на ответ',
    // feedback action
    'Controller_Site_Home.feedback.content_title' => 'Обратная связь',
    'Controller_Site_Home.feedback.title' => 'Обратная связь',
    'Controller_Site_Home.feedback.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Home.feedback.description' => 'Обратная связь',
    'Controller_Site_Home.feedback.revosip_-_oblachnyj_sip_softfon' => '<strong>Revosip</strong> - облачный сервис SIP софтфона',
    'Controller_Site_Home.feedback.email_adres' => 'Email адрес:',
    'Controller_Site_Home.feedback.skype' => 'Skype:',
    'Controller_Site_Home.feedback.kiev,_ukraina' => 'Киев, Украина',
    // conditions action
    'Controller_Site_Home.conditions.content_title' => 'Условия пользовательского соглашения',
    'Controller_Site_Home.conditions.title' => 'Условия пользовательского соглашения',
    'Controller_Site_Home.conditions.keywords' => 'Условия пользовательского соглашения',
    'Controller_Site_Home.conditions.description' => 'Условия пользовательского соглашения',
    // Controller_Site_Login
    // login action
    'Controller_Site_Login.login.content_title' => 'Вход',
    'Controller_Site_Login.login.title' => 'Вход',
    'Controller_Site_Login.login.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Login.login.description' => 'Вход на страницу пользователя',
    'Controller_Site_Login.login.vozmozhnost_vxoda_na_sajt_vremenno_priostanovlena' => 'Возможность входа на сайт временно приостановлена',
    'Controller_Site_Login.login.vxod_vozmozhen_tolko_posle_podtverzhdeniya_registracii' => 'Вход возможен только после подтверждения регистрации',
    'Controller_Site_Login.login.dostup_vremenno_zablokirovan' => 'Доступ временно заблокирован',
    'Controller_Site_Login.login.nevozmozhno_vypolnit_vxod,_povtorite_popytku' => 'Невозможно выполнить вход, повторите попытку',
    'Controller_Site_Login.login.nepravilnyj_login_ili_parol' => 'Неправильный логин или пароль',
    'Controller_Site_Login.login.login' => 'Логин:',
    'Controller_Site_Login.login.parol' => 'Пароль:',
    'Controller_Site_Login.login.zabyli_parol' => 'Забыли пароль?',
    'Controller_Site_Login.login.vojti' => 'Войти',
    // Controller_Site_News
    // login action
    'Controller_Site_News.list.content_title' => 'Новости',
    'Controller_Site_News.list.title' => 'Новости',
    'Controller_Site_News.list.keywords' => 'Revosip - Новости',
    'Controller_Site_News.list.description' => 'Revosip - Новости',
    // add action
    'Controller_Site_News.add.content_title' => 'Добавление новости',
    'Controller_Site_News.add.title' => 'Добавление новости',
    'Controller_Site_News.add.vvedite_magicheskij_parol' => 'Введите магический пароль',
    'Controller_Site_News.add.magicheskij_parol_ne_sovpadaet' => 'Магический пароль не совпадает',
    'Controller_Site_News.add.novost_dobavlena' => 'Новость добавлена',
    'Controller_Site_News.add.nevozmozhno_dobavit_novost' => 'Невозможно добавить новость',
    'Controller_Site_News.add.uroven_vazhnosti' => 'Уровень важности:',
    'Controller_Site_News.add.oglavlenie_novosti' => 'Оглавление новости:',
    'Controller_Site_News.add.klyuchevye_slova' => 'Ключевые слова (meta):',
    'Controller_Site_News.add.kratkoe_opisanie' => 'Краткое описание (meta):',
    'Controller_Site_News.add.soderzhimoe_novosti' => 'Содержимое новости:',
    'Controller_Site_News.add.magicheskij_parol' => 'Магический пароль:',
    'Controller_Site_News.add.dobavit' => 'Добавить',
    // edit action
    'Controller_Site_News.edit.content_title' => 'Редактирование новости',
    'Controller_Site_News.edit.title' => 'Редактирование новости',
    'Controller_Site_News.edit.izmeneniya_soxraneny' => 'Изменения сохранены',
    'Controller_Site_News.edit.nevozmozhno_soxranit_otredaktirovannuyu_novost' => 'Невозможно сохранить отредактированную новость',
    'Controller_Site_News.edit.soxranit' => 'Сохранить',
    // remove action
    'Controller_Site_News.remove.content_title' => 'Удаление новости',
    'Controller_Site_News.remove.title' => 'Удаление новости',
    'Controller_Site_News.remove.novost_udalena' => 'Новость удалена',
    'Controller_Site_News.remove.nevozmozhno_udalit_novost' => 'Невозможно удалить новость',
    'Controller_Site_News.remove.magicheskij_parol' => 'Магический пароль:',
    'Controller_Site_News.remove.udalit' => 'Удалить',
    // Controller_Site_Recovery
    // recovery action
    'Controller_Site_Recovery.recovery.content_title' => 'Восстановление пароля',
    'Controller_Site_Recovery.recovery.title' => 'Восстановление пароля',
    'Controller_Site_Recovery.recovery.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Recovery.recovery.description' => 'Восстановление пароля пользователя',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_na_sajte_vremenno_priostanovlena' => 'Возможность восстановления пароля на сайте временно приостановлена',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_polzovatel_s_takim_email_adresom_ne_najden' => 'Невозможно восстановить пароль, пользователь с таким email адресом не найден',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_vosstanovlenie_parolya_vozmozhno_tolko_posle_podtverzhdeniya_registracii' => 'Невозможно восстановить пароль, восстановление пароля возможно только после подтверждения регистрации',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_vremenno_zablokirovana' => 'Возможность восстановления пароля временно заблокирована',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_podtverdite_sushhestvuyushhij_zapros_na_vosstanovlenie_parolya' => 'Невозможно восстановить пароль, подтвердите существующий запрос на восстановление пароля',
    'Controller_Site_Recovery.recovery.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_procedury_vosstanovleniya_parolya' => 'На ваш email адрес отправлена ссылка для подтверждения процедуры восстановления пароля',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_povtorite_popytku_pozzhe' => 'Невозможно восстановить пароль, повторите попытку позже',
    'Controller_Site_Recovery.recovery.email_adres' => 'Email адрес:',
    'Controller_Site_Recovery.recovery.vosstanovit_parol' => 'Восстановить пароль',
    // confirm action
    'Controller_Site_Recovery.confirm.content_title' => 'Подтверждение процедуры восстановления пароля пользователя',
    'Controller_Site_Recovery.confirm.title' => 'Подтверждение процедуры восстановления пароля пользователя',
    'Controller_Site_Recovery.confirm.keywords' => 'Подтверждение процедуры восстановления пароля пользователя',
    'Controller_Site_Recovery.confirm.description' => 'Подтверждение процедуры восстановления пароля пользователя',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol,_polzovatel_ne_najden' => 'Невозможно восстановить пароль, пользователь не найден',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol' => 'Невозможно восстановить пароль',
    'Controller_Site_Recovery.confirm.vash_novyj_parol' => 'Ваш новый пароль:',
    'Controller_Site_Recovery.confirm.vy_vsegda_mozhete_izmenit_parol_na_stranice_profilya' => 'Вы всегда можете изменить пароль на странице профиля',
    // Controller_Site_Registration
    // registration
    'Controller_Site_Registration.registration.content_title' => 'Регистрация пользователя',
    'Controller_Site_Registration.registration.title' => 'Регистрация пользователя',
    'Controller_Site_Registration.registration.keywords' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'Controller_Site_Registration.registration.description' => 'Регистрация пользователя',
    'Controller_Site_Registration.registration.vozmozhnost_registracii_na_sajte_vremenno_priostanovlena' => 'Возможность регистрации на сайте временно приостановлена',
    'Controller_Site_Registration.registration.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_registracii' => 'На ваш email адрес отправлена ссылка для подтверждения регистрации',
    'Controller_Site_Registration.registration.nevozmozhno_vypolnit_registraciyu,_povtorite_popytku' => 'Невозможно выполнить регистрацию, повторите попытку',
    'Controller_Site_Registration.registration.proverit' => 'проверить',
    'Controller_Site_Registration.registration.login' => 'Логин:',
    'Controller_Site_Registration.registration.parol' => 'Пароль:',
    'Controller_Site_Registration.registration.podtverzhdenie_parolya' => 'Подтверждение пароля:',
    'Controller_Site_Registration.registration.polnoe_imya' => 'Полное имя:',
    'Controller_Site_Registration.registration.email_adres' => 'Email адрес:',
    'Controller_Site_Registration.registration.yazyk' => 'Язык:',
    'Controller_Site_Registration.registration.vremennaya_zona' => 'Временная зона:',
    'Controller_Site_Registration.registration.ya_prinimayu' => 'Я принимаю',
    'Controller_Site_Registration.registration.usloviya_polzovatelskogo_soglasheniya' => 'Условия пользовательского соглашения',
    'Controller_Site_Registration.registration.zaregistrirovatsya' => 'Зарегистрироваться',
    // confirm action
    'Controller_Site_Registration.confirm.content_title' => 'Подтверждение регистрации пользователя',
    'Controller_Site_Registration.confirm.title' => 'Подтверждение регистрации пользователя',
    'Controller_Site_Registration.confirm.keywords' => 'Подтверждение регистрации пользователя',
    'Controller_Site_Registration.confirm.description' => 'Подтверждение регистрации пользователя',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu' => 'Невозможно подтвердить регистрацию',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_srok_podtverzhdeniya_registracii_istek' => 'Невозможно подтвердить регистрацию, срок подтверждения регистрации истек',
    'Controller_Site_Registration.confirm.registraciya_polzovatelya_podtverzhdena,_vy_mozhete_vojti_na_sajt_ispolzuya_login_i_parol' => 'Регистрация пользователя подтверждена, Вы можете войти на сайт используя логин и пароль',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_povtorite_popytku' => 'Невозможно подтвердить регистрацию, повторите попытку',
    // Controller_Ajax_Check
    'Controller_Ajax_Check.nevernyj_format' => 'неверный формат',
    'Controller_Ajax_Check.dostupen' => 'доступен',
    'Controller_Ajax_Check.nedostupen' => 'недоступен',
    // Controller_Error
    'Controller_Error.content_title' => 'Страница не найдена',
    'Controller_Error.title' => 'Страница не найдена',
    // View_Widget_Launch
    // applet view
    'View_Widget_Launch.applet.keywords' => 'Revosip applet | SIP клиент для браузера',
    'View_Widget_Launch.applet.description' => 'облачный sip софтфон, облачный sip клиент, java sip софтфон, java sip клиент, sip клиент для браузера, sip клиент asterisk, sip клиент windows, sip клиент linux, sip клиент mac, sip клиент ubuntu',
    'View_Widget_Launch.applet.vash_brauzer_ne_podderzhivaet_zapusk_java_appletov' => 'Ваш браузер не поддерживает запуск Java апплетов.',
    'View_Widget_Launch.applet.dlya_podderzhki_zapuska_java_appletov_neobxodimo_ustanovit_virtualnuyu_mashinu_java' => 'Для поддержки запуска Java апплетов необходимо установить виртуальную машину Java',
    'View_Widget_Launch.applet.skachat' => 'скачать',
    // launcher view
    'View_Widget_Launch.launcher.versiya' => 'версия',
    'View_Widget_Launch.launcher.zapustit' => 'Запустить',
    // View_Widget_News
    'View_Widget_News.list.poslednie_novosti' => 'Последние новости',
    'View_Widget_News.list.vse_novosti' => 'Все новости',
    // Controller_Widget_Menu
    // accounts action
    'Controller_Widget_Menu.accounts.akkaunty' => 'Аккаунты',
    'Controller_Widget_Menu.accounts.spisok_akkauntov' => 'Список аккаунтов',
    'Controller_Widget_Menu.accounts.dobavit_akkaunt' => 'Добавить аккаунт',
    'Controller_Widget_Menu.accounts.istoriya_aktivnosti_akkauntov' => 'История активности аккаунтов',
    'Controller_Widget_Menu.accounts.istoriya_prodlenij_akkauntov' => 'История продлений аккаунтов',
    // account action
    'Controller_Widget_Menu.account.akkaunt' => 'Аккаунт',
    'Controller_Widget_Menu.account.informaciya_ob_akkaunte' => 'Информация об аккаунте',
    'Controller_Widget_Menu.account.nastrojki_akkaunta' => 'Настройки аккаунта',
    'Controller_Widget_Menu.account.redaktirovat_akkaunt' => 'Редактировать аккаунт',
    'Controller_Widget_Menu.account.prodlit_akkaunt' => 'Продлить аккаунт',
    'Controller_Widget_Menu.account.istoriya_prodlenij_akkaunta' => 'История продлений аккаунта',
    'Controller_Widget_Menu.account.istoriya_aktivnosti_akkaunta' => 'История активности аккаунта',
    'Controller_Widget_Menu.account.udalit_akkaunt' => 'Удалить аккаунт',
    // contacts action
    'Controller_Widget_Menu.contacts.kontakty' => 'Контакты',
    'Controller_Widget_Menu.contacts.spisok_kontaktov' => 'Список контактов',
    'Controller_Widget_Menu.contacts.dobavit_kontakt' => 'Добавить контакт',
    // contact action
    'Controller_Widget_Menu.contact.kontakt' => 'Контакт',
    'Controller_Widget_Menu.contact.informaciya_o_kontakte' => 'Информация о контакте',
    'Controller_Widget_Menu.contact.redaktirovat_kontakt' => 'Редактировать контакт',
    // connections action
    'Controller_Widget_Menu.connections.profili_podklyucheniya' => 'Профили подключения',
    'Controller_Widget_Menu.connections.spisok_profilej_podklyucheniya' => 'Список профилей подключения',
    'Controller_Widget_Menu.connections.dobavit_profil_podklyucheniya' => 'Добавить профиль подключения',
    // connection action
    'Controller_Widget_Menu.connection.profil_podklyucheniya' => 'Профиль подключения',
    'Controller_Widget_Menu.connection.informaciya_o_profile_podklyucheniya' => 'Информация о профиле подключения',
    'Controller_Widget_Menu.connection.redaktirovat_profil_podklyucheniya' => 'Редактировать профиль подключения',
    'Controller_Widget_Menu.connection.udalit_profil_podklyucheniya' => 'Удалить профиль подключения',
    // profile action
    'Controller_Widget_Menu.profile.profil' => 'Профиль',
    'Controller_Widget_Menu.profile.redaktirovat_profil' => 'Редактировать профиль',
    'Controller_Widget_Menu.profile.balans_profilya' => 'Баланс профиля',
    'Controller_Widget_Menu.profile.popolnit_balans_profilya' => 'Пополнить баланс профиля',
    'Controller_Widget_Menu.profile.istoriya_proplat' => 'История проплат',
    'Controller_Widget_Menu.profile.istoriya_aktivnosti' => 'История активности',
    // Controller_Widget_Navigation
    // accounts action
    'Controller_Widget_Navigation.accounts.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.accounts.dobavlenie_akkaunta' => 'Добавление аккаунта',
    'Controller_Widget_Navigation.accounts.istoriya_aktivnosti_akkauntov' => 'История активности аккаунтов',
    'Controller_Widget_Navigation.accounts.istoriya_prodlenij_akkauntov' => 'История продлений аккаунтов',
    // account action
    'Controller_Widget_Navigation.account.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.account.akkaunt' => 'Аккаунт',
    'Controller_Widget_Navigation.account.izmenenie_nastroek_akkaunta' => 'Изменение настроек аккаунта',
    'Controller_Widget_Navigation.account.redaktirovanie_akkaunta' => 'Редактирование аккаунта',
    'Controller_Widget_Navigation.account.prodlenie_akkaunta' => 'Продление аккаунта',
    'Controller_Widget_Navigation.account.istoriya_prodlenij_akkaunta' => 'История продлений аккаунта',
    'Controller_Widget_Navigation.account.istoriya_aktivnosti_akkaunta' => 'История активности аккаунта',
    'Controller_Widget_Navigation.account.udalenie_akkaunta' => 'Удаление аккаунта',
    // contacts action
    'Controller_Widget_Navigation.contacts.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.contacts.akkaunt' => 'Аккаунт',
    'Controller_Widget_Navigation.contacts.kontakty' => 'Контакты',
    'Controller_Widget_Navigation.contacts.dobavlenie_kontakta' => 'Добавление контакта',
    // contact action
    'Controller_Widget_Navigation.contact.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.contact.akkaunt' => 'Аккаунт',
    'Controller_Widget_Navigation.contact.kontakty' => 'Контакты',
    'Controller_Widget_Navigation.contact.kontakt' => 'Контакт',
    'Controller_Widget_Navigation.contact.redaktirovanie_kontakta' => 'Редактирование контакта',
    'Controller_Widget_Navigation.contact.udalenie_kontakta' => 'Удаление контакта',
    // connections action
    'Controller_Widget_Navigation.connections.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.connections.akkaunt' => 'Аккаунт',
    'Controller_Widget_Navigation.connections.profili_podklyucheniya' => 'Профили подключения',
    'Controller_Widget_Navigation.connections.dobavlenie_profilya_podklyucheniya' => 'Добавление профиля подключения',
    // connection action
    'Controller_Widget_Navigation.connection.akkaunty' => 'Аккаунты',
    'Controller_Widget_Navigation.connection.akkaunt' => 'Аккаунт',
    'Controller_Widget_Navigation.connection.profili_podklyuchenij' => 'Профили подключений',
    'Controller_Widget_Navigation.connection.profil_podklyucheniya' => 'Профиль подключения',
    'Controller_Widget_Navigation.connection.redaktirovanie_profilya_podklyucheniya' => 'Редактирование профиля подключения',
    'Controller_Widget_Navigation.connection.udalenie_profilya_podklyucheniya' => 'Удаление профиля подключения',
    // profile action
    'Controller_Widget_Navigation.profile.profil' => 'Профиль',
    'Controller_Widget_Navigation.profile.redaktirovanie_profilya' => 'Редактирование профиля',
    'Controller_Widget_Navigation.profile.smena_email_adresa_profilya' => 'Смена email адреса профиля',
    'Controller_Widget_Navigation.profile.balans_profilya' => 'Баланс профиля',
    'Controller_Widget_Navigation.profile.popolnenie_balansa_profilya' => 'Пополнение баланса профиля',
    'Controller_Widget_Navigation.profile.istoriya_proplat' => 'История проплат',
    'Controller_Widget_Navigation.profile.istoriya_aktivnosti' => 'История активности',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Profile section                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Profile_Account
    // info action
    'Controller_Profile_Account.info.content_title' => 'Информация об аккаунте',
    'Controller_Profile_Account.info.title' => 'Информация об аккаунте',
    'Controller_Profile_Account.info.nazvanie' => 'Название:',
    'Controller_Profile_Account.info.imya_polzovatelya' => 'Имя пользователя:',
    'Controller_Profile_Account.info.login_polzovatelya' => 'Логин пользователя:',
    'Controller_Profile_Account.info.status' => 'Статус:',
    'Controller_Profile_Account.info.data_sozdaniya' => 'Дата создания:',
    'Controller_Profile_Account.info.srok_dejstviya' => 'Срок действия:',
    'Controller_Profile_Account.info.kolichestvo_dnej_do_zaversheniya_sroka_dejstviya' => 'Количество дней до завершения срока действия:',
    'Controller_Profile_Account.info.kolichestvo_kontaktov' => 'Количество контактов:',
    'Controller_Profile_Account.info.kolichestvo_profilej_podklyuchenij' => 'Количество профилей подключений:',
    'Controller_Profile_Account.info.aktivnyj' => 'Активный',
    'Controller_Profile_Account.info.neaktivnyj' => 'Неактивный',
    'Controller_Profile_Account.info.istek' => 'истек',
    'Controller_Profile_Account.info.do' => 'до',
    // edit action
    'Controller_Profile_Account.edit.content_title' => 'Редактирование аккаунта',
    'Controller_Profile_Account.edit.title' => 'Редактирование аккаунта',
    'Controller_Profile_Account.edit.vozmozhnost_redaktirovaniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Возможность редактирования аккаунтов на сайте временно приостановлена',
    'Controller_Profile_Account.edit.izmeneniya_soxraneny' => 'Изменения сохранены',
    'Controller_Profile_Account.edit.ne_udalos_soxranit_izmeneniya,_povtorite_popytku' => 'Не удалось сохранить изменения, повторите попытку',
    'Controller_Profile_Account.edit.nazvanie_akkaunta' => 'Название аккаунта:',
    'Controller_Profile_Account.edit.imya_polzovatelya' => 'Имя пользователя:',
    'Controller_Profile_Account.edit.login_polzovatelya' => 'Логин пользователя:',
    'Controller_Profile_Account.edit.proverit' => 'проверить',
    'Controller_Profile_Account.edit.parol_polzovatelya' => 'Пароль пользователя:',
    'Controller_Profile_Account.edit.podtverzhdenie_parolya' => 'Подтверждение пароля:',
    'Controller_Profile_Account.edit.sdelat_akkaunt_aktivnym' => 'Сделать аккаунт активным',
    'Controller_Profile_Account.edit.soxranit' => 'Сохранить',
    // settings action
    'Controller_Profile_Account.settings.content_title' => 'Настройки аккаунта',
    'Controller_Profile_Account.settings.title' => 'Настройки аккаунта',
    'Controller_Profile_Account.settings.vozmozhnost_izmeneniya_nastroek_akkauntov_na_sajte_vremenno_priostanovlena' => 'Возможность изменения настроек аккаунтов на сайте временно приостановлена',
    'Controller_Profile_Account.settings.nastrojki_akkaunta_soxraneny' => 'Настройки аккаунта сохранены',
    'Controller_Profile_Account.settings.nevozmozhno_soxranit_nastrojki_akkaunta,_povtorite_popytku' => 'Невозможно сохранить настройки аккаунта, повторите попытку',
    'Controller_Profile_Account.settings.vybor_profilya_podklyucheniya_po_umolchaniyu' => 'Выбор профиля подключения по умолчанию',
    'Controller_Profile_Account.settings.otsutstvuet' => 'Отсутствует',
    'Controller_Profile_Account.settings.ispolzovat_tolko_vybrannyj_profil_podklyucheniya' => 'Использовать только выбранный профиль подключения',
    'Controller_Profile_Account.settings.soxranit' => 'Сохранить',
    // remove action
    'Controller_Profile_Account.remove.content_title' => 'Удаление аккаунта',
    'Controller_Profile_Account.remove.title' => 'Удаление аккаунта',
    'Controller_Profile_Account.remove.vozmozhnost_udaleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Возможность удаления аккаунтов на сайте временно приостановлена',
    'Controller_Profile_Account.remove.nevozmozhno_udalit_akkaunt,_srok_dejstviya_akkaunta_ne_istek' => 'Невозможно удалить аккаунт, срок действия аккаунта не истек',
    'Controller_Profile_Account.remove.podtverdite_udalenie_akkaunta' => 'Подтвердите удаление аккаунта:',
    'Controller_Profile_Account.remove.udalit' => 'Удалить',
    // prolong action
    'Controller_Profile_Account.prolong.content_title' => 'Продление аккаунта',
    'Controller_Profile_Account.prolong.title' => 'Продление аккаунта',
    'Controller_Profile_Account.prolong.vozmozhnost_prodleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Возможность продления аккаунтов на сайте временно приостановлена',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_dostignut_maksimalnyj_srok_prodleniya_akkaunta' => 'Невозможно продлить аккаунт, достигнут максимальный срок продления аккаунта',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_neobxodimo_popolnit_balans' => 'Невозможно продлить аккаунт, необходимо пополнить баланс',
    'Controller_Profile_Account.prolong.akkaunt_uspeshno_prodlen' => 'Аккаунт успешно продлен',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_povtorite_popytku' => 'Невозможно продлить аккаунт, повторите попытку',
    // --- form
    'Controller_Profile_Account.prolong.obshhie_svedeniya' => 'Общие сведения',
    'Controller_Profile_Account.prolong.tekushhij_balans_profilya' => 'Текущий баланс профиля:',
    'Controller_Profile_Account.prolong.dnya/dnej_prodleniya' => 'дня/дней продления',
    'Controller_Profile_Account.prolong.tekushhij_tarif' => 'Текущий тариф:',
    'Controller_Profile_Account.prolong.srok_dejstviya_akkaunta' => 'Срок действия аккаунта:',
    'Controller_Profile_Account.prolong.istek' => 'истек',
    'Controller_Profile_Account.prolong.ot' => 'от',
    'Controller_Profile_Account.prolong.do' => 'до',
    'Controller_Profile_Account.prolong.do_daty' => 'до',
    'Controller_Profile_Account.prolong.ostalos' => 'осталось',
    'Controller_Profile_Account.prolong.dnya/dnej' => 'дня/дней',
    'Controller_Profile_Account.prolong.dostupno_k_prodleniyu' => 'Доступно к продлению:',
    'Controller_Profile_Account.prolong.dnya/dnej_(bez_ucheta_balansa)' => 'дня/дней (без учета баланса)',
    'Controller_Profile_Account.prolong.min-maks_srok_prodleniya_akkaunta' => 'Мин-макс срок продления аккаунта:',
    'Controller_Profile_Account.prolong.prodlenie' => 'Продление',
    'Controller_Profile_Account.prolong.zhelaemoe_kolichestvo_dnej_prodleniya' => 'Желаемое количество дней продления',
    'Controller_Profile_Account.prolong.prodlit' => 'Продлить',
    // prolongs action
    'Controller_Profile_Account.prolongs.content_title' => 'История продлений аккаунта',
    'Controller_Profile_Account.prolongs.title' => 'История продлений аккаунта',
    'Controller_Profile_Account.prolongs.data_prodleniya' => 'Дата продления',
    'Controller_Profile_Account.prolongs.summa_prodleniya' => 'Сумма продления',
    'Controller_Profile_Account.prolongs.kolichestvo_dnej_prodleniya' => 'Количество дней продления',
    // log action     
    'Controller_Profile_Account.log.content_title' => 'История активности аккаунта',
    'Controller_Profile_Account.log.title' => 'История активности аккаунта',
    'Controller_Profile_Account.log.data_sobytiya' => 'Дата события',
    'Controller_Profile_Account.log.ip_adres' => 'IP адрес',
    'Controller_Profile_Account.log.tip_sobytiya' => 'Тип события',
    'Controller_Profile_Account.log.vxod' => 'Вход',
    'Controller_Profile_Account.log.vyxod' => 'Выход',
    // Controller_Profile_Accounts
    // list action
    'Controller_Profile_Accounts.list.content_title' => 'Список аккаунтов',
    'Controller_Profile_Accounts.list.title' => 'Список аккаунтов',
    'Controller_Profile_Accounts.list.nazvanie_akkaunta' => 'Название аккаунта',
    'Controller_Profile_Accounts.list.srok_dejstviya_akkaunta' => 'Срок действия аккаунта',
    'Controller_Profile_Accounts.list.data_sozdaniya' => 'Дата создания',
    'Controller_Profile_Accounts.list.status' => 'Статус',
    'Controller_Profile_Accounts.list.istek' => 'истек',
    'Controller_Profile_Accounts.list.do' => 'до',
    'Controller_Profile_Accounts.list.aktivnyj' => 'Активный',
    'Controller_Profile_Accounts.list.neaktivnyj' => 'Неактивный',
    // add action
    'Controller_Profile_Accounts.add.content_title' => 'Добавление аккаунта',
    'Controller_Profile_Accounts.add.title' => 'Добавление аккаунта',
    'Controller_Profile_Accounts.add.vozmozhnost_dobavleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Возможность добавления аккаунтов на сайте временно приостановлена',
    'Controller_Profile_Accounts.add.akkaunt_dobavlen' => 'Аккаунт добавлен',
    'Controller_Profile_Accounts.add.ne_udalos_dobavit_akkaunt,_povtorite_popytku' => 'Не удалось добавить аккаунт, повторите попытку',
    'Controller_Profile_Accounts.add.nazvanie_akkaunta' => 'Название аккаунта:',
    'Controller_Profile_Accounts.add.imya_polzovatelya' => 'Имя пользователя:',
    'Controller_Profile_Accounts.add.login_polzovatelya' => 'Логин пользователя:',
    'Controller_Profile_Accounts.add.proverit' => 'проверить',
    'Controller_Profile_Accounts.add.parol_polzovatelya' => 'Пароль пользователя:',
    'Controller_Profile_Accounts.add.podtverzhdenie_parolya' => 'Подтверждение пароля:',
    'Controller_Profile_Accounts.add.sdelat_akkaunt_aktivnym' => 'Сделать аккаунт активным',
    'Controller_Profile_Accounts.add.dobavit' => 'Добавить',
    // log action
    'Controller_Profile_Accounts.log.content_title' => 'История активности аккаунтов',
    'Controller_Profile_Accounts.log.title' => 'История активности аккаунтов',
    'Controller_Profile_Accounts.log.nazvanie_akkaunta' => 'Название аккаунта',
    'Controller_Profile_Accounts.log.data_sobytiya' => 'Дата события',
    'Controller_Profile_Accounts.log.ip_adres' => 'IP адрес',
    'Controller_Profile_Accounts.log.tip_sobytiya' => 'Тип события',
    'Controller_Profile_Accounts.log.vxod' => 'Вход',
    'Controller_Profile_Accounts.log.vyxod' => 'Выход',
    // prolongs action
    'Controller_Profile_Accounts.prolongs.content_title' => 'История продлений аккаунтов',
    'Controller_Profile_Accounts.prolongs.title' => 'История продлений аккаунтов',
    'Controller_Profile_Accounts.prolongs.nazvanie_akkaunta' => 'Название аккаунта',
    'Controller_Profile_Accounts.prolongs.data_prodleniya' => 'Дата продления',
    'Controller_Profile_Accounts.prolongs.summa_prodleniya' => 'Сумма продления',
    'Controller_Profile_Accounts.prolongs.kolichestvo_dnej_prodleniya' => 'Количество дней продления',
    // Controller_Profile_Connection
    // info action
    'Controller_Profile_Connection.info.content_title' => 'Информация о профиле подключения',
    'Controller_Profile_Connection.info.title' => 'Информация о профиле подключения',
    'Controller_Profile_Connection.info.da' => 'да',
    'Controller_Profile_Connection.info.net' => 'нет',
    'Controller_Profile_Connection.info.profil_podklyucheniya' => 'Профиль подключения',
    'Controller_Profile_Connection.info.nazvanie' => 'Название:',
    'Controller_Profile_Connection.info.kratkoe_opisanie' => 'Краткое описание:',
    'Controller_Profile_Connection.info.aktivnyj' => 'Активный:',
    'Controller_Profile_Connection.info.vvodit_parol_lokalno' => 'Вводить пароль локально:',
    'Controller_Profile_Connection.info.nastrojki_sip_akkaunta' => 'Настройки SIP аккаунта',
    'Controller_Profile_Connection.info.otobrazhaemoe_imya' => 'Отображаемое имя:',
    'Controller_Profile_Connection.info.dobavochnyj_nomer' => 'Добавочный номер:',
    'Controller_Profile_Connection.info.imya_polzovatelya' => 'Имя пользователя:',
    'Controller_Profile_Connection.info.parol' => 'Пароль:',
    'Controller_Profile_Connection.info.ne_ukazan' => 'Не указан',
    'Controller_Profile_Connection.info.sekretnyj_parol' => 'Секретный пароль:',
    'Controller_Profile_Connection.info.ispolzovat_sekretnyj_parol' => 'Использовать секретный пароль:',
    'Controller_Profile_Connection.info.setevye_nastrojki' => 'Сетевые настройки',
    'Controller_Profile_Connection.info.adres_sip_servera' => 'Адрес SIP сервера:',
    'Controller_Profile_Connection.info.port_sip_servera' => 'Порт SIP сервера:',
    'Controller_Profile_Connection.info.adres_sip_proksi_servera' => 'Адрес SIP прокси сервера:',
    'Controller_Profile_Connection.info.port_sip_proksi_servera' => 'Порт SIP прокси сервера:',
    'Controller_Profile_Connection.info.lokalnyj_ip_adres' => 'Локальный IP адрес:',
    'Controller_Profile_Connection.info.lokalnyj_port' => 'Локальный порт:',
    'Controller_Profile_Connection.info.minimalnyj_rtp_port' => 'Минимальный RTP порт:',
    'Controller_Profile_Connection.info.maksimalnyj_rtp_port' => 'Максимальный RTP порт:',
    'Controller_Profile_Connection.info.tajmer_registracii' => 'Таймер регистрации:',
    'Controller_Profile_Connection.info.ispolzovat_sluchajnyj_lokalnyj_port' => 'Использовать случайный локальный порт:',
    'Controller_Profile_Connection.info.opredelyat_lokalnyj_adres_avtomaticheski' => 'Определять локальный адрес автоматически:',
    'Controller_Profile_Connection.info.ispolzovat_proksi_server' => 'Использовать прокси сервер:',
    'Controller_Profile_Connection.info.nastrojki_parametrov_nat' => 'Настройки параметров NAT',
    'Controller_Profile_Connection.info.tajmer_keepalive' => 'Таймер KeepAlive:',
    'Controller_Profile_Connection.info.adres_stun_servera' => 'Адрес STUN сервера:',
    'Controller_Profile_Connection.info.port_stun_servera' => 'Порт STUN сервера:',
    'Controller_Profile_Connection.info.ispolzovat_stun_razvedku' => 'Использовать STUN разведку:',
    'Controller_Profile_Connection.info.ispolzovat_stun_server_po_umolchaniyu' => 'Использовать STUN сервер по умолчанию:',
    'Controller_Profile_Connection.info.ispolzovat_tajmer_keepalive' => 'Использовать таймер KeepAlive:',
    'Controller_Profile_Connection.info.ispolzovat_privyazku_k_sip_serveru' => 'Использовать привязку к SIP серверу:',
    'Controller_Profile_Connection.info.ispolzovat_simmetricheskij_rtp' => 'Использовать симметричный RTP:',
    'Controller_Profile_Connection.info.nastrojki_prioritetov_kodekov' => 'Настройки приоритетов кодеков',
    'Controller_Profile_Connection.info.kodek' => 'Кодек',
    'Controller_Profile_Connection.info.nastrojki_parametrov_dtmf' => 'Настройки параметров DTMF',
    'Controller_Profile_Connection.info.predpochitaemyj_dtmf_metod' => 'Предпочитаемый DTMF метод:',
    'Controller_Profile_Connection.info.dlitelnost_dtmf_signala' => 'Длительность DTMF сигнала:',
    // edit action
    'Controller_Profile_Connection.edit.content_title' => 'Редактирование профиля подключения',
    'Controller_Profile_Connection.edit.title' => 'Редактирование профиля подключения',
    'Controller_Profile_Connection.edit.vozmozhnost_redaktirovaniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Возможность редактирования профилей подключения на сайте временно приостановлена',
    'Controller_Profile_Connection.edit.izmeneniya_soxraneny' => 'Изменения сохранены',
    'Controller_Profile_Connection.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Невозможно сохранить изменения, повторите попытку',
    'Controller_Profile_Connection.edit.profil_podklyucheniya' => 'Профиль подключения',
    'Controller_Profile_Connection.edit.nazvanie' => 'Название:',
    'Controller_Profile_Connection.edit.kratkoe_opisanie' => 'Краткое описание:',
    'Controller_Profile_Connection.edit.sdelat_aktivnym' => 'Сделать активным',
    'Controller_Profile_Connection.edit.vvodit_parol_lokalno' => 'Вводить пароль локально',
    'Controller_Profile_Connection.edit.nastrojki_sip_akkaunta' => 'Настройки SIP аккаунта',
    'Controller_Profile_Connection.edit.otobrazhaemoe_imya' => 'Отображаемое имя:',
    'Controller_Profile_Connection.edit.dobavochnyj_nomer' => 'Добавочный номер:',
    'Controller_Profile_Connection.edit.imya_polzovatelya' => 'Имя пользователя:',
    'Controller_Profile_Connection.edit.parol' => 'Пароль:',
    'Controller_Profile_Connection.edit.sekretnyj_parol' => 'Секретный пароль:',
    'Controller_Profile_Connection.edit.ispolzovat_sekretnyj_parol' => 'Использовать секретный пароль',
    'Controller_Profile_Connection.edit.setevye_nastrojki' => 'Сетевые настройки',
    'Controller_Profile_Connection.edit.adres_sip_servera' => 'Адрес SIP сервера:',
    'Controller_Profile_Connection.edit.port_sip_servera' => 'Порт SIP сервера:',
    'Controller_Profile_Connection.edit.adres_sip_proksi_servera' => 'Адрес SIP прокси сервера:',
    'Controller_Profile_Connection.edit.port_sip_proksi_servera' => 'Порт SIP прокси сервера:',
    'Controller_Profile_Connection.edit.lokalnyj_ip_adres' => 'Локальный IP адрес:',
    'Controller_Profile_Connection.edit.lokalnyj_port' => 'Локальный порт:',
    'Controller_Profile_Connection.edit.minimalnyj_rtp_port' => 'Минимальный RTP порт:',
    'Controller_Profile_Connection.edit.maksimalnyj_rtp_port' => 'Максимальный RTP порт:',
    'Controller_Profile_Connection.edit.tajmer_registracii' => 'Таймер регистрации:',
    'Controller_Profile_Connection.edit.ispolzovat_sluchajnyj_lokalnyj_port' => 'Использовать случайный локальный порт',
    'Controller_Profile_Connection.edit.opredelyat_lokalnyj_adres_avtomaticheski' => 'Определять локальный адрес автоматически',
    'Controller_Profile_Connection.edit.ispolzovat_proksi_server' => 'Использовать прокси сервер',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_nat' => 'Настройки параметров NAT',
    'Controller_Profile_Connection.edit.tajmer_keepalive' => 'Таймер KeepAlive:',
    'Controller_Profile_Connection.edit.adres_stun_servera' => 'Адрес STUN сервера:',
    'Controller_Profile_Connection.edit.port_stun_servera' => 'Порт STUN сервера:',
    'Controller_Profile_Connection.edit.ispolzovat_stun_razvedku' => 'Использовать STUN разведку',
    'Controller_Profile_Connection.edit.ispolzovat_stun_server_po_umolchaniyu' => 'Использовать STUN сервер по умолчанию',
    'Controller_Profile_Connection.edit.ispolzovat_tajmer_keepalive' => 'Использовать таймер KeepAlive',
    'Controller_Profile_Connection.edit.ispolzovat_privyazku_k_sip_serveru' => 'Использовать привязку к SIP серверу',
    'Controller_Profile_Connection.edit.ispolzovat_simmetricheskij_rtp' => 'Использовать симметричный RTP',
    'Controller_Profile_Connection.edit.nastrojki_prioritetov_kodekov' => 'Настройки приоритетов кодеков',
    'Controller_Profile_Connection.edit.kodek' => 'Кодек',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_dtmf' => 'Настройки параметров DTMF',
    'Controller_Profile_Connection.edit.predpochitaemyj_dtmf_metod' => 'Предпочитаемый DTMF метод:',
    'Controller_Profile_Connection.edit.dlitelnost_dtmf_signala' => 'Длительность DTMF сигнала:',
    'Controller_Profile_Connection.edit.soxranit' => 'Сохранить',
    // remove action
    'Controller_Profile_Connection.remove.content_title' => 'Удаление профиля подключения',
    'Controller_Profile_Connection.remove.title' => 'Удаление профиля подключения',
    'Controller_Profile_Connection.remove.vozmozhnost_udaleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Возможность удаления профилей подключения на сайте временно приостановлена',
    'Controller_Profile_Connection.remove.podtverdite_udalenie_profilya_podklyucheniya' => 'Подтвердите удаление профиля подключения:',
    'Controller_Profile_Connection.remove.udalit' => 'Удалить',
    // Controller_Profile_Connections
    // list action
    'Controller_Profile_Connections.list.content_title' => 'Список профилей подключения',
    'Controller_Profile_Connections.list.title' => 'Список профилей подключения',
    'Controller_Profile_Connections.list.profil_podklyucheniya' => 'Профиль подключения',
    'Controller_Profile_Connections.list.data_dobavleniya' => 'Дата добавления',
    'Controller_Profile_Connections.list.status' => 'Статус',
    'Controller_Profile_Connections.list.aktivnyj' => 'Активный',
    'Controller_Profile_Connections.list.neaktivnyj' => 'Неактивный',
    // add action
    'Controller_Profile_Connections.add.content_title' => 'Добавление профиля подключения',
    'Controller_Profile_Connections.add.title' => 'Добавление профиля подключения',
    'Controller_Profile_Connections.add.vozmozhnost_dobavleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Возможность добавления профилей подключения на сайте временно приостановлена',
    'Controller_Profile_Connections.add.profil_podklyucheniya_dobavlen' => 'Профиль подключения добавлен',
    'Controller_Profile_Connections.add.nevozmozhno_dobavit_profil_podklyucheniya,_povtorite_popytku' => 'Невозможно добавить профиль подключения, повторите попытку',
    'Controller_Profile_Connections.add.dobavit' => 'Добавить',
    // Controller_Profile_Contact
    // info action
    'Controller_Profile_Contact.info.content_title' => 'Информация о контакте',
    'Controller_Profile_Contact.info.title' => 'Информация о контакте',
    'Controller_Profile_Contact.info.imya_kontakta' => 'Имя контакта:',
    'Controller_Profile_Contact.info.nomer_telefona' => 'Номер телефона:',
    'Controller_Profile_Contact.info.data_dobavleniya_kontakta' => 'Дата добавления контакта:',
    // edit action
    'Controller_Profile_Contact.edit.content_title' => 'Редактирование контакта',
    'Controller_Profile_Contact.edit.title' => 'Редактирование контакта',
    'Controller_Profile_Contact.edit.vozmozhnost_redaktirovaniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Возможность редактирования контактов на сайте временно приостановлена',
    'Controller_Profile_Contact.edit.izmeneniya_soxraneny' => 'Изменения сохранены',
    'Controller_Profile_Contact.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Невозможно сохранить изменения, повторите попытку',
    'Controller_Profile_Contact.edit.imya_kontakta' => 'Имя контакта:',
    'Controller_Profile_Contact.edit.nomer_telefona' => 'Номер телефона:',
    'Controller_Profile_Contact.edit.soxranit' => 'Сохранить',
    // Controller_Profile_Contacts
    // list action
    'Controller_Profile_Contacts.list.content_title' => 'Список контактов',
    'Controller_Profile_Contacts.list.title' => 'Список контактов',
    'Controller_Profile_Contacts.list.imya_kontakta' => 'Имя контакта',
    'Controller_Profile_Contacts.list.nomer_telefona' => 'Номер телефона',
    'Controller_Profile_Contacts.list.data_dobavleniya' => 'Дата добавления',
    'Controller_Profile_Contacts.list.dejstvie' => 'Действие',
    'Controller_Profile_Contacts.list.udalit' => 'Удалить',
    // add action
    'Controller_Profile_Contacts.add.content_title' => 'Добавление контакта',
    'Controller_Profile_Contacts.add.title' => 'Добавление контакта',
    'Controller_Profile_Contacts.add.vozmozhnost_dobavleniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Возможность добавления контактов на сайте временно приостановлена',
    'Controller_Profile_Contacts.add.kontakt_dobavlen' => 'Контакт добавлен',
    'Controller_Profile_Contacts.add.nevozmozhno_dobavit_kontakt,_povtorite_popytku' => 'Невозможно добавить контакт, повторите попытку',
    'Controller_Profile_Contacts.add.imya_kontakta' => 'Имя контакта:',
    'Controller_Profile_Contacts.add.nomer_telefona' => 'Номер телефона:',
    'Controller_Profile_Contacts.add.dobavit' => 'Добавить',
    // Controller_Profile_Profile
    // info action
    'Controller_Profile_Profile.info.content_title' => 'Профиль',
    'Controller_Profile_Profile.info.title' => 'Профиль',
    'Controller_Profile_Profile.info.kolichestvo_akkauntov' => 'Количество аккаунтов:',
    'Controller_Profile_Profile.info.dobavit_akkaunt' => 'Добавить аккаунт',
    'Controller_Profile_Profile.info.otobrazit_spisok_akkauntov' => 'Отобразить список аккаунтов',
    // edit action
    'Controller_Profile_Profile.edit.content_title' => 'Редактирование профиля',
    'Controller_Profile_Profile.edit.title' => 'Редактирование профиля',
    'Controller_Profile_Profile.edit.vozmozhnost_redaktirovaniya_profilya_na_sajte_vremenno_priostanovlena' => 'Возможность редактирования профиля на сайте временно приостановлена',
    'Controller_Profile_Profile.edit.izmeneniya_soxraneny' => 'Изменения сохранены',
    'Controller_Profile_Profile.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Невозможно сохранить изменения, повторите попытку',
    'Controller_Profile_Profile.edit.polnoe_imya' => 'Полное имя:',
    'Controller_Profile_Profile.edit.login' => 'Логин:',
    'Controller_Profile_Profile.edit.parol' => 'Пароль:',
    'Controller_Profile_Profile.edit.podtverzhdenie_parolya' => 'Подтверждение пароля:',
    'Controller_Profile_Profile.edit.email' => 'Email:',
    'Controller_Profile_Profile.edit.smenit_email_adres' => 'Сменить email адрес',
    'Controller_Profile_Profile.edit.yazyk' => 'Язык:',
    'Controller_Profile_Profile.edit.vremennaya_zona' => 'Временная зона:',
    'Controller_Profile_Profile.edit.soxranit' => 'Сохранить',
    // balance action
    'Controller_Profile_Profile.balance.content_title' => 'Баланс профиля',
    'Controller_Profile_Profile.balance.title' => 'Баланс профиля',
    'Controller_Profile_Profile.balance.tekushhij_balans' => 'Текущий баланс:',
    // invoice action
    'Controller_Profile_Profile.invoice.content_title' => 'Пополнение баланса профиля',
    'Controller_Profile_Profile.invoice.title' => 'Пополнение баланса профиля',
    'Controller_Profile_Profile.invoice.vozmozhnost_popolneniya_balansa_na_sajte_vremenno_priostanovlena' => 'Возможность пополнения баланса на сайте временно приостановлена',
    'Controller_Profile_Profile.invoice.popolnenie_balansa_polzovatelya' => 'Пополнение баланса пользователя:',
    'Controller_Profile_Profile.invoice.nevozmozhno_popolnit_balans,_povtorite_popytku' => 'Невозможно пополнить баланс, повторите попытку',
    'Controller_Profile_Profile.invoice.summa_popolneniya' => 'Сумма пополнения:',
    'Controller_Profile_Profile.invoice.minimum' => 'минимум',
    'Controller_Profile_Profile.invoice.tip_valyuty_webmoney' => 'Тип валюты Webmoney:',
    'Controller_Profile_Profile.invoice.prodolzhit' => 'Продолжить',
    'Controller_Profile_Profile.invoice.zhelaemaya_summa_popolneniya' => 'Желаемая сумма пополнения:',
    'Controller_Profile_Profile.invoice.neobxodimaya_summa_dlya_proplaty' => 'Необходимая сумма для проплаты:',
    'Controller_Profile_Profile.invoice.proplatit' => 'Проплатить',
    // invoices action
    'Controller_Profile_Profile.invoices.content_title' => 'История проплат',
    'Controller_Profile_Profile.invoices.title' => 'История проплат',
    'Controller_Profile_Profile.invoices.schet' => 'Счет №',
    'Controller_Profile_Profile.invoices.tip_valyuty' => 'Тип валюты',
    'Controller_Profile_Profile.invoices.data_sozdaniya' => 'Дата создания',
    'Controller_Profile_Profile.invoices.sostoyanie' => 'Состояние',
    'Controller_Profile_Profile.invoices.summa_popolneniya' => 'Сумма пополнения',
    'Controller_Profile_Profile.invoices.data_proplaty' => 'Дата проплаты',
    'Controller_Profile_Profile.invoices.otmenen' => 'Отменен',
    'Controller_Profile_Profile.invoices.proplachen' => 'Проплачен',
    'Controller_Profile_Profile.invoices.ozhidaet_proplaty' => 'Ожидает проплаты',
    // log action
    'Controller_Profile_Profile.log.content_title' => 'История активности',
    'Controller_Profile_Profile.log.title' => 'История активности',
    'Controller_Profile_Profile.log.data_sobytiya' => 'Дата события',
    'Controller_Profile_Profile.log.ip_adres' => 'IP адрес',
    'Controller_Profile_Profile.log.tip_sobytiya' => 'Тип события',
    'Controller_Profile_Profile.log.vxod' => 'Вход',
    'Controller_Profile_Profile.log.vyxod' => 'Выход',
    // email action
    'Controller_Profile_Profile.email.content_title' => 'Смена email адреса профиля',
    'Controller_Profile_Profile.email.title' => 'Смена email адреса профиля',
    'Controller_Profile_Profile.email.vozmozhnost_smeny_email_adresa_profilya_na_sajte_vremenno_priostanovlena' => 'Возможность смены email адреса профиля на сайте временно приостановлена',
    'Controller_Profile_Profile.email.na_vash_tekushhij_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'На ваш текущий email адрес отправлена ссылка для подтверждения смены email адреса',
    'Controller_Profile_Profile.email.nevozmozhno_smenit_email_adres_profilya,_povtorite_popytku' => 'Невозможно сменить email адрес профиля, повторите попытку',
    'Controller_Profile_Profile.email.na_vash_novyj_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'На ваш новый email адрес отправлена ссылка для подтверждения смены email адреса',
    'Controller_Profile_Profile.email.nevozmozhno_podtverdit_smenu_email_adresa_profilya,_povtorite_popytku' => 'Невозможно подтвердить смену email адреса профиля, повторите попытку',
    'Controller_Profile_Profile.email.email_adres_profilya_izmenen_na_sleduyushhij' => 'Email адрес профиля изменен на следующий:',
    'Controller_Profile_Profile.email.novyj_email_adres' => 'Новый email адрес:',
    'Controller_Profile_Profile.email.izmenit' => 'Изменить',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Template section                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // View_Template_Profile
    // navigation_bar view
    'View_Template_Profile.navigation_bar.profil' => 'Профиль',
    'View_Template_Profile.navigation_bar.akkaunty' => 'Аккаунты',
    'View_Template_Profile.navigation_bar.podderzhka' => 'Поддержка',
    'View_Template_Profile.navigation_bar.voprosy-otvety' => 'Вопросы - Ответы',
    'View_Template_Profile.navigation_bar.obratnaya_svyaz' => 'Обратная связь',
    'View_Template_Profile.navigation_bar.vyjti' => 'Выйти',
    // View_Template_Site
    // navigation_bar view
    'View_Template_Site.navigation_bar.o_servise' => 'О сервисе',
    'View_Template_Site.navigation_bar.novosti' => 'Новости',
    'View_Template_Site.navigation_bar.podderzhka' => 'Поддержка',
    'View_Template_Site.navigation_bar.voprosy-otvety' => 'Вопросы - Ответы',
    'View_Template_Site.navigation_bar.vosstanovlenie_parolya' => 'Восстановление пароля',
    'View_Template_Site.navigation_bar.obratnaya_svyaz' => 'Обратная связь',
    'View_Template_Site.navigation_bar.profil' => 'Профиль',
    'View_Template_Site.navigation_bar.vyjti' => 'Выйти',
    'View_Template_Site.navigation_bar.vxod' => 'Вход',
    'View_Template_Site.navigation_bar.registraciya' => 'Регистрация',
);
