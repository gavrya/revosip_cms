<?php

defined('SYSPATH') or die('No direct script access.');

// http://mainspy.ru/translit_perevodchik
return array(
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Basic section                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller
    'Controller.identifikator_formy_ne_sovpadaet' => 'Form identificator does not match',
    'Controller.vvedite_pravilnyj_proverochnyj_kod' => 'Type the correct security code',
    'Controller.proverochnyj_kod' => 'Security code:',
    'Controller.mezhsajtovye_xss_zaprosy_zapreshheny' => 'XSS requests are prohibited',
    'Controller.net_dannyx_dlya_otobrazheniya' => 'No data to display',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Site section                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Site_Home
    // home action
    'Controller_Site_Home.home.title' => 'Cloud JAVA SIP client',
    'Controller_Site_Home.home.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Home.home.description' => 'Cloud JAVA SIP client',
    'Controller_Site_Home.home.revosip_-_oblachnyj_sip_softfon' => 'Revosip - cloud JAVA SIP client',
    'Controller_Site_Home.home.otkrytyj_beta_test' => 'Open beta test',
    'Controller_Site_Home.home.registraciya' => 'Registration',
    'Controller_Site_Home.home.preimushhestva' => 'Advantages',
    'Controller_Site_Home.home.sravnenie_s_ip_telefonom' => 'Comparing to IP phone',
    'Controller_Site_Home.home.xarakteristiki' => 'Characteristics',
    'Controller_Site_Home.home.skrinshoty' => 'Screenshots',
    'Controller_Site_Home.home.osobennosti' => 'Features',
    'Controller_Site_Home.home.vozmozhnosti' => 'Description',
    'Controller_Site_Home.home.predmet_sravneniya' => 'Subject of comparison',
    'Controller_Site_Home.home.programmnyj_telefon_revosip' => 'Softphone Revosip',
    'Controller_Site_Home.home.apparatnyj_ip_telefon' => 'Hardware IP phone',
    // about action
    'Controller_Site_Home.about.content_title' => 'About the service',
    'Controller_Site_Home.about.title' => 'About the service',
    'Controller_Site_Home.about.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Home.about.description' => 'About the service',
    // faq action
    'Controller_Site_Home.faq.content_title' => 'Questions - Answers',
    'Controller_Site_Home.faq.title' => 'Questions - Answers',
    'Controller_Site_Home.faq.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Home.faq.description' => 'Questions - Answers',
    'Controller_Site_Home.faq.ssylka_na_otvet' => 'Link to the answer',
    // feedback action
    'Controller_Site_Home.feedback.content_title' => 'Feedback',
    'Controller_Site_Home.feedback.title' => 'Feedback',
    'Controller_Site_Home.feedback.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Home.feedback.description' => 'Feedback',
    'Controller_Site_Home.feedback.revosip_-_oblachnyj_sip_softfon' => '<strong>Revosip</strong> - a cloud service of SIP softphone',
    'Controller_Site_Home.feedback.email_adres' => 'Email address:',
    'Controller_Site_Home.feedback.skype' => 'Skype:',
    'Controller_Site_Home.feedback.kiev,_ukraina' => 'Kyiv, Ukraine',
    // conditions action
    'Controller_Site_Home.conditions.content_title' => 'The User Agreement',
    'Controller_Site_Home.conditions.title' => 'The User Agreement',
    'Controller_Site_Home.conditions.keywords' => 'The User Agreement',
    'Controller_Site_Home.conditions.description' => 'The User Agreement',
    // Controller_Site_Login
    // login action
    'Controller_Site_Login.login.content_title' => 'Log in',
    'Controller_Site_Login.login.title' => 'Log in',
    'Controller_Site_Login.login.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Login.login.description' => 'Log in',
    'Controller_Site_Login.login.vozmozhnost_vxoda_na_sajt_vremenno_priostanovlena' => 'Log in is temporarily suspended',
    'Controller_Site_Login.login.vxod_vozmozhen_tolko_posle_podtverzhdeniya_registracii' => 'Log in is possible after confirmation of registration only',
    'Controller_Site_Login.login.dostup_vremenno_zablokirovan' => 'Access is temporarily disabled',
    'Controller_Site_Login.login.nevozmozhno_vypolnit_vxod,_povtorite_popytku' => 'Unable to login, please try again',
    'Controller_Site_Login.login.nepravilnyj_login_ili_parol' => 'Incorrect username or password',
    'Controller_Site_Login.login.login' => 'Login:',
    'Controller_Site_Login.login.parol' => 'Password:',
    'Controller_Site_Login.login.zabyli_parol' => 'Forgot your password?',
    'Controller_Site_Login.login.vojti' => 'Log in',
    // Controller_Site_News
    // login action
    'Controller_Site_News.list.content_title' => 'News',
    'Controller_Site_News.list.title' => 'News',
    'Controller_Site_News.list.keywords' => 'Revosip - News',
    'Controller_Site_News.list.description' => 'Revosip - News',
    // add action
    'Controller_Site_News.add.content_title' => 'Adding the news',
    'Controller_Site_News.add.title' => 'Adding the news',
    'Controller_Site_News.add.vvedite_magicheskij_parol' => 'Type magic password',
    'Controller_Site_News.add.magicheskij_parol_ne_sovpadaet' => 'Incorrect magic password',
    'Controller_Site_News.add.novost_dobavlena' => 'News is added',
    'Controller_Site_News.add.nevozmozhno_dobavit_novost' => 'Unable to add the news',
    'Controller_Site_News.add.uroven_vazhnosti' => 'Level of Importance:',
    'Controller_Site_News.add.oglavlenie_novosti' => 'Title of the news:',
    'Controller_Site_News.add.klyuchevye_slova' => 'Keywords (meta):',
    'Controller_Site_News.add.kratkoe_opisanie' => 'Short descriptions (meta):',
    'Controller_Site_News.add.soderzhimoe_novosti' => 'News content:',
    'Controller_Site_News.add.magicheskij_parol' => 'Magic password:',
    'Controller_Site_News.add.dobavit' => 'Add',
    // edit action
    'Controller_Site_News.edit.content_title' => 'Editing the news',
    'Controller_Site_News.edit.title' => 'Editing the news',
    'Controller_Site_News.edit.izmeneniya_soxraneny' => 'The changes are saved',
    'Controller_Site_News.edit.nevozmozhno_soxranit_otredaktirovannuyu_novost' => 'Unable to save the edited news',
    'Controller_Site_News.edit.soxranit' => 'Save',
    // remove action
    'Controller_Site_News.remove.content_title' => 'Deleting the news',
    'Controller_Site_News.remove.title' => 'Deleting the news',
    'Controller_Site_News.remove.novost_udalena' => 'The news has been added',
    'Controller_Site_News.remove.nevozmozhno_udalit_novost' => 'Unable to delete the news',
    'Controller_Site_News.remove.magicheskij_parol' => 'Magic password:',
    'Controller_Site_News.remove.udalit' => 'Delete',
    // Controller_Site_Recovery
    // recovery action
    'Controller_Site_Recovery.recovery.content_title' => 'Password recovery',
    'Controller_Site_Recovery.recovery.title' => 'Password recovery',
    'Controller_Site_Recovery.recovery.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Recovery.recovery.description' => 'Password recovery',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_na_sajte_vremenno_priostanovlena' => 'Password recovery is temporarily suspended',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_polzovatel_s_takim_email_adresom_ne_najden' => 'Unable to recover the password, a user with the specified email address is not found',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_vosstanovlenie_parolya_vozmozhno_tolko_posle_podtverzhdeniya_registracii' => 'Unable to recover the password, Невозможно восстановить пароль, password recovery is possible after confirmation of registration only',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_vremenno_zablokirovana' => 'Password recovery is temporarily blocked',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_podtverdite_sushhestvuyushhij_zapros_na_vosstanovlenie_parolya' => 'Unable to recover the password, confirm the existing password recovery request',
    'Controller_Site_Recovery.recovery.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_procedury_vosstanovleniya_parolya' => 'The link with the confirmation of paasword recovery procedure was sent to your email address',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_povtorite_popytku_pozzhe' => 'Unable to recover the password, please try again later',
    'Controller_Site_Recovery.recovery.email_adres' => 'Email address:',
    'Controller_Site_Recovery.recovery.vosstanovit_parol' => 'Recover',
    // confirm action
    'Controller_Site_Recovery.confirm.content_title' => 'Confirmation of paasword recovery procedure',
    'Controller_Site_Recovery.confirm.title' => 'Confirmation of paasword recovery procedure',
    'Controller_Site_Recovery.confirm.keywords' => 'Confirmation of paasword recovery procedure',
    'Controller_Site_Recovery.confirm.description' => 'Confirmation of paasword recovery procedure',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol,_polzovatel_ne_najden' => 'Unable to recover the password, the user is not found',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol' => 'Unable to recover the password',
    'Controller_Site_Recovery.confirm.vash_novyj_parol' => 'Your new password is:',
    'Controller_Site_Recovery.confirm.vy_vsegda_mozhete_izmenit_parol_na_stranice_profilya' => 'You can always change your password on the profile page',
    // Controller_Site_Registration
    // registration
    'Controller_Site_Registration.registration.content_title' => 'User registration',
    'Controller_Site_Registration.registration.title' => 'User registration',
    'Controller_Site_Registration.registration.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'Controller_Site_Registration.registration.description' => 'User registration',
    'Controller_Site_Registration.registration.vozmozhnost_registracii_na_sajte_vremenno_priostanovlena' => 'Registration is temporarily suspended',
    'Controller_Site_Registration.registration.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_registracii' => 'The link with the confirmation of registration was sent to your email address',
    'Controller_Site_Registration.registration.nevozmozhno_vypolnit_registraciyu,_povtorite_popytku' => 'Unable to register, please try again',
    'Controller_Site_Registration.registration.proverit' => 'check',
    'Controller_Site_Registration.registration.login' => 'Login:',
    'Controller_Site_Registration.registration.parol' => 'Password:',
    'Controller_Site_Registration.registration.podtverzhdenie_parolya' => 'Password confirmation:',
    'Controller_Site_Registration.registration.polnoe_imya' => 'Full name:',
    'Controller_Site_Registration.registration.email_adres' => 'Email address:',
    'Controller_Site_Registration.registration.yazyk' => 'Language:',
    'Controller_Site_Registration.registration.vremennaya_zona' => 'Time zone:',
    'Controller_Site_Registration.registration.ya_prinimayu' => 'I accept',
    'Controller_Site_Registration.registration.usloviya_polzovatelskogo_soglasheniya' => 'The User Agreement',
    'Controller_Site_Registration.registration.zaregistrirovatsya' => 'Register',
    // confirm action
    'Controller_Site_Registration.confirm.content_title' => 'Confirmation of registration',
    'Controller_Site_Registration.confirm.title' => 'Confirmation of registration',
    'Controller_Site_Registration.confirm.keywords' => 'Confirmation of registration',
    'Controller_Site_Registration.confirm.description' => 'Confirmation of registration',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu' => 'Unable to confirm registration',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_srok_podtverzhdeniya_registracii_istek' => 'Unable to confirm the registration, Невозможно подтвердить регистрацию, period of registration confirmation has expired',
    'Controller_Site_Registration.confirm.registraciya_polzovatelya_podtverzhdena,_vy_mozhete_vojti_na_sajt_ispolzuya_login_i_parol' => 'Registration is confirmed, you may enter the web site using your login and password',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_povtorite_popytku' => 'Unable to confirm the registration, please try again',
    // Controller_Ajax_Check
    'Controller_Ajax_Check.nevernyj_format' => 'incorrect format',
    'Controller_Ajax_Check.dostupen' => 'available',
    'Controller_Ajax_Check.nedostupen' => 'unavailable',
    // Controller_Error
    'Controller_Error.content_title' => 'The page is not found',
    'Controller_Error.title' => 'The page is not found',
    // View_Widget_Launch
    // applet view
    'View_Widget_Launch.applet.keywords' => 'cloud sip softphone, cloud sip client, java sip softphone, java sip client, web sip client, asterisk sip client, windows sip client, linux sip client, mac sip client, ubuntu sip client',
    'View_Widget_Launch.applet.description' => 'Revosip applet',
    'View_Widget_Launch.applet.vash_brauzer_ne_podderzhivaet_zapusk_java_appletov' => 'Your browser can not run Java applets.',
    'View_Widget_Launch.applet.dlya_podderzhki_zapuska_java_appletov_neobxodimo_ustanovit_virtualnuyu_mashinu_java' => 'You need to install the Java Virtual Machine in order launch of Java applets',
    'View_Widget_Launch.applet.skachat' => 'download',
    // launcher view
    'View_Widget_Launch.launcher.versiya' => 'version',
    'View_Widget_Launch.launcher.zapustit' => 'Launch',
    // View_Widget_News
    'View_Widget_News.list.poslednie_novosti' => 'The latest news',
    'View_Widget_News.list.vse_novosti' => 'Views all news',
    // Controller_Widget_Menu
    // accounts action
    'Controller_Widget_Menu.accounts.akkaunty' => 'Accounts',
    'Controller_Widget_Menu.accounts.spisok_akkauntov' => 'Accounts list',
    'Controller_Widget_Menu.accounts.dobavit_akkaunt' => 'Add an account',
    'Controller_Widget_Menu.accounts.istoriya_aktivnosti_akkauntov' => 'Accounts activity history',
    'Controller_Widget_Menu.accounts.istoriya_prodlenij_akkauntov' => 'Accounts prolongation history',
    // account action
    'Controller_Widget_Menu.account.akkaunt' => 'Account',
    'Controller_Widget_Menu.account.informaciya_ob_akkaunte' => 'Account information',
    'Controller_Widget_Menu.account.nastrojki_akkaunta' => 'Account settings',
    'Controller_Widget_Menu.account.redaktirovat_akkaunt' => 'Edit the account',
    'Controller_Widget_Menu.account.prodlit_akkaunt' => 'Prolong the accont',
    'Controller_Widget_Menu.account.istoriya_prodlenij_akkaunta' => 'Account prolongation history',
    'Controller_Widget_Menu.account.istoriya_aktivnosti_akkaunta' => 'Account activity history',
    'Controller_Widget_Menu.account.udalit_akkaunt' => 'Remove the account',
    // contacts action
    'Controller_Widget_Menu.contacts.kontakty' => 'Contacts',
    'Controller_Widget_Menu.contacts.spisok_kontaktov' => 'Contacts list',
    'Controller_Widget_Menu.contacts.dobavit_kontakt' => 'Add a contact',
    // contact action
    'Controller_Widget_Menu.contact.kontakt' => 'Contact',
    'Controller_Widget_Menu.contact.informaciya_o_kontakte' => 'About contact',
    'Controller_Widget_Menu.contact.redaktirovat_kontakt' => 'Edit the contact',
    // connections action
    'Controller_Widget_Menu.connections.profili_podklyucheniya' => 'Connection profiles',
    'Controller_Widget_Menu.connections.spisok_profilej_podklyucheniya' => 'Connection profiles list',
    'Controller_Widget_Menu.connections.dobavit_profil_podklyucheniya' => 'Add a connection profile',
    // connection action
    'Controller_Widget_Menu.connection.profil_podklyucheniya' => 'Connection profile',
    'Controller_Widget_Menu.connection.informaciya_o_profile_podklyucheniya' => 'Connection profile information',
    'Controller_Widget_Menu.connection.redaktirovat_profil_podklyucheniya' => 'Edit the connection profile',
    'Controller_Widget_Menu.connection.udalit_profil_podklyucheniya' => 'Remove the connection profile',
    // profile action
    'Controller_Widget_Menu.profile.profil' => 'Profile',
    'Controller_Widget_Menu.profile.redaktirovat_profil' => 'Edit the profile',
    'Controller_Widget_Menu.profile.balans_profilya' => 'Balance',
    'Controller_Widget_Menu.profile.popolnit_balans_profilya' => 'Balance refilling',
    'Controller_Widget_Menu.profile.istoriya_proplat' => 'Payment history',
    'Controller_Widget_Menu.profile.istoriya_aktivnosti' => 'Activity history',
    // Controller_Widget_Navigation
    // accounts action
    'Controller_Widget_Navigation.accounts.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.accounts.dobavlenie_akkaunta' => 'Account adding',
    'Controller_Widget_Navigation.accounts.istoriya_aktivnosti_akkauntov' => 'Accounts activity history',
    'Controller_Widget_Navigation.accounts.istoriya_prodlenij_akkauntov' => 'Accounts prolongation history',
    // account action
    'Controller_Widget_Navigation.account.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.account.akkaunt' => 'Account',
    'Controller_Widget_Navigation.account.izmenenie_nastroek_akkaunta' => 'Account settings',
    'Controller_Widget_Navigation.account.redaktirovanie_akkaunta' => 'Account editing',
    'Controller_Widget_Navigation.account.prodlenie_akkaunta' => 'Account prolonging',
    'Controller_Widget_Navigation.account.istoriya_prodlenij_akkaunta' => 'Account prolongation history',
    'Controller_Widget_Navigation.account.istoriya_aktivnosti_akkaunta' => 'Account activity history',
    'Controller_Widget_Navigation.account.udalenie_akkaunta' => 'Account removing',
    // contacts action
    'Controller_Widget_Navigation.contacts.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.contacts.akkaunt' => 'Account',
    'Controller_Widget_Navigation.contacts.kontakty' => 'Contacts',
    'Controller_Widget_Navigation.contacts.dobavlenie_kontakta' => 'Contact adding',
    // contact action
    'Controller_Widget_Navigation.contact.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.contact.akkaunt' => 'Account',
    'Controller_Widget_Navigation.contact.kontakty' => 'Contacts',
    'Controller_Widget_Navigation.contact.kontakt' => 'Contact',
    'Controller_Widget_Navigation.contact.redaktirovanie_kontakta' => 'Contact editing',
    'Controller_Widget_Navigation.contact.udalenie_kontakta' => 'Contact removing',
    // connections action
    'Controller_Widget_Navigation.connections.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.connections.akkaunt' => 'Account',
    'Controller_Widget_Navigation.connections.profili_podklyucheniya' => 'Connection profiles',
    'Controller_Widget_Navigation.connections.dobavlenie_profilya_podklyucheniya' => 'Connection profile adding',
    // connection action
    'Controller_Widget_Navigation.connection.akkaunty' => 'Accounts',
    'Controller_Widget_Navigation.connection.akkaunt' => 'Account',
    'Controller_Widget_Navigation.connection.profili_podklyuchenij' => 'Connection profiles',
    'Controller_Widget_Navigation.connection.profil_podklyucheniya' => 'Connection profile',
    'Controller_Widget_Navigation.connection.redaktirovanie_profilya_podklyucheniya' => 'Connection profile editing',
    'Controller_Widget_Navigation.connection.udalenie_profilya_podklyucheniya' => 'Connection profile removing',
    // profile action
    'Controller_Widget_Navigation.profile.profil' => 'Profile',
    'Controller_Widget_Navigation.profile.redaktirovanie_profilya' => 'Profile editing',
    'Controller_Widget_Navigation.profile.smena_email_adresa_profilya' => 'Changing the profile email address',
    'Controller_Widget_Navigation.profile.balans_profilya' => 'Balance',
    'Controller_Widget_Navigation.profile.popolnenie_balansa_profilya' => 'Balance refilling',
    'Controller_Widget_Navigation.profile.istoriya_proplat' => 'Payment history',
    'Controller_Widget_Navigation.profile.istoriya_aktivnosti' => 'Activity history',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Profile section                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Profile_Account
    // info action
    'Controller_Profile_Account.info.content_title' => 'Account information',
    'Controller_Profile_Account.info.title' => 'Account information',
    'Controller_Profile_Account.info.nazvanie' => 'Name:',
    'Controller_Profile_Account.info.imya_polzovatelya' => 'Username:',
    'Controller_Profile_Account.info.login_polzovatelya' => 'User login:',
    'Controller_Profile_Account.info.status' => 'Status:',
    'Controller_Profile_Account.info.data_sozdaniya' => 'Creation date:',
    'Controller_Profile_Account.info.srok_dejstviya' => 'Expiration date:',
    'Controller_Profile_Account.info.kolichestvo_dnej_do_zaversheniya_sroka_dejstviya' => 'Days left:',
    'Controller_Profile_Account.info.kolichestvo_kontaktov' => 'Number of contacts:',
    'Controller_Profile_Account.info.kolichestvo_profilej_podklyuchenij' => 'Number of connection profiles:',
    'Controller_Profile_Account.info.aktivnyj' => 'Active',
    'Controller_Profile_Account.info.neaktivnyj' => 'Not active',
    'Controller_Profile_Account.info.istek' => 'expired',
    'Controller_Profile_Account.info.do' => 'until',
    // edit action
    'Controller_Profile_Account.edit.content_title' => 'Account editing',
    'Controller_Profile_Account.edit.title' => 'Account editing',
    'Controller_Profile_Account.edit.vozmozhnost_redaktirovaniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Account editing is temporarily suspended',
    'Controller_Profile_Account.edit.izmeneniya_soxraneny' => 'Changes have been saved',
    'Controller_Profile_Account.edit.ne_udalos_soxranit_izmeneniya,_povtorite_popytku' => 'Unable to save the changes, please try again',
    'Controller_Profile_Account.edit.nazvanie_akkaunta' => 'Account name:',
    'Controller_Profile_Account.edit.imya_polzovatelya' => 'Username:',
    'Controller_Profile_Account.edit.login_polzovatelya' => 'User login:',
    'Controller_Profile_Account.edit.proverit' => 'check',
    'Controller_Profile_Account.edit.parol_polzovatelya' => 'User password:',
    'Controller_Profile_Account.edit.podtverzhdenie_parolya' => 'Password confirmation:',
    'Controller_Profile_Account.edit.sdelat_akkaunt_aktivnym' => 'Make account active',
    'Controller_Profile_Account.edit.soxranit' => 'Save',
    // settings action
    'Controller_Profile_Account.settings.content_title' => 'Account settings',
    'Controller_Profile_Account.settings.title' => 'Account settings',
    'Controller_Profile_Account.settings.vozmozhnost_izmeneniya_nastroek_akkauntov_na_sajte_vremenno_priostanovlena' => 'Accounts settings changing is temporarily suspended',
    'Controller_Profile_Account.settings.nastrojki_akkaunta_soxraneny' => 'Accounts settings have been saved',
    'Controller_Profile_Account.settings.nevozmozhno_soxranit_nastrojki_akkaunta,_povtorite_popytku' => 'Unable to change account settings, please try again',
    'Controller_Profile_Account.settings.vybor_profilya_podklyucheniya_po_umolchaniyu' => 'Selection of the default connection profile',
    'Controller_Profile_Account.settings.otsutstvuet' => 'Not specified',
    'Controller_Profile_Account.settings.ispolzovat_tolko_vybrannyj_profil_podklyucheniya' => 'Use only the selected connection profile',
    'Controller_Profile_Account.settings.soxranit' => 'Save',
    // remove action
    'Controller_Profile_Account.remove.content_title' => 'Account removing',
    'Controller_Profile_Account.remove.title' => 'Account removing',
    'Controller_Profile_Account.remove.vozmozhnost_udaleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Account removing is temporarily suspended',
    'Controller_Profile_Account.remove.nevozmozhno_udalit_akkaunt,_srok_dejstviya_akkaunta_ne_istek' => 'Unable to remove the account, account validity term hasnt expired',
    'Controller_Profile_Account.remove.podtverdite_udalenie_akkaunta' => 'Confirm the account removing:',
    'Controller_Profile_Account.remove.udalit' => 'Remove',
    // prolong action
    'Controller_Profile_Account.prolong.content_title' => 'Account prolongation',
    'Controller_Profile_Account.prolong.title' => 'Account prolongation',
    'Controller_Profile_Account.prolong.vozmozhnost_prodleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Account prolongation is temporarily suspended',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_dostignut_maksimalnyj_srok_prodleniya_akkaunta' => 'Unable to prolong the account, account prolongation reached a maximum period',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_neobxodimo_popolnit_balans' => 'Unable to prolong the account, please refill balance',
    'Controller_Profile_Account.prolong.akkaunt_uspeshno_prodlen' => 'The account validity term has been prolonged',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_povtorite_popytku' => 'Unable to prolong the account, please try again',
    // --- form
    'Controller_Profile_Account.prolong.obshhie_svedeniya' => 'General Information',
    'Controller_Profile_Account.prolong.tekushhij_balans_profilya' => 'Current balance:',
    'Controller_Profile_Account.prolong.dnya/dnej_prodleniya' => 'day/days of prolongation',
    'Controller_Profile_Account.prolong.tekushhij_tarif' => 'Current tariff:',
    'Controller_Profile_Account.prolong.srok_dejstviya_akkaunta' => 'Account validity term:',
    'Controller_Profile_Account.prolong.istek' => 'expired',
    'Controller_Profile_Account.prolong.ot' => 'from',
    'Controller_Profile_Account.prolong.do' => 'to',
    'Controller_Profile_Account.prolong.do_daty' => 'until',
    'Controller_Profile_Account.prolong.ostalos' => 'left',
    'Controller_Profile_Account.prolong.dnya/dnej' => 'day/days',
    'Controller_Profile_Account.prolong.dostupno_k_prodleniyu' => 'Days available to prolongation:',
    'Controller_Profile_Account.prolong.dnya/dnej_(bez_ucheta_balansa)' => 'day/days (not considering the balance)',
    'Controller_Profile_Account.prolong.min-maks_srok_prodleniya_akkaunta' => 'Min-max days of prolongation:',
    'Controller_Profile_Account.prolong.prodlenie' => 'Prolongation',
    'Controller_Profile_Account.prolong.zhelaemoe_kolichestvo_dnej_prodleniya' => 'Number of prolongation days',
    'Controller_Profile_Account.prolong.prodlit' => 'Prolong',
    // prolongs action
    'Controller_Profile_Account.prolongs.content_title' => 'Account prolongation history',
    'Controller_Profile_Account.prolongs.title' => 'Account prolongation history',
    'Controller_Profile_Account.prolongs.data_prodleniya' => 'Prolongation date',
    'Controller_Profile_Account.prolongs.summa_prodleniya' => 'Prolongation amount',
    'Controller_Profile_Account.prolongs.kolichestvo_dnej_prodleniya' => 'Number of prolongation days',
    // log action     
    'Controller_Profile_Account.log.content_title' => 'Account activity history',
    'Controller_Profile_Account.log.title' => 'Account activity history',
    'Controller_Profile_Account.log.data_sobytiya' => 'Event date',
    'Controller_Profile_Account.log.ip_adres' => 'IP address',
    'Controller_Profile_Account.log.tip_sobytiya' => 'Event type',
    'Controller_Profile_Account.log.vxod' => 'Log in',
    'Controller_Profile_Account.log.vyxod' => 'Log out',
    // Controller_Profile_Accounts
    // list action
    'Controller_Profile_Accounts.list.content_title' => 'Accounts list',
    'Controller_Profile_Accounts.list.title' => 'Accounts list',
    'Controller_Profile_Accounts.list.nazvanie_akkaunta' => 'Account name',
    'Controller_Profile_Accounts.list.srok_dejstviya_akkaunta' => 'Account validity term',
    'Controller_Profile_Accounts.list.status' => 'Status',
    'Controller_Profile_Accounts.list.data_sozdaniya' => 'Date of creation',
    'Controller_Profile_Accounts.list.istek' => 'expired',
    'Controller_Profile_Accounts.list.do' => 'until',
    'Controller_Profile_Accounts.list.aktivnyj' => 'Active',
    'Controller_Profile_Accounts.list.neaktivnyj' => 'Not active',
    // add action
    'Controller_Profile_Accounts.add.content_title' => 'Account adding',
    'Controller_Profile_Accounts.add.title' => 'Account adding',
    'Controller_Profile_Accounts.add.vozmozhnost_dobavleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Account adding is temporarily suspended',
    'Controller_Profile_Accounts.add.akkaunt_dobavlen' => 'Account added',
    'Controller_Profile_Accounts.add.ne_udalos_dobavit_akkaunt,_povtorite_popytku' => 'Unable to add an account, please try again',
    'Controller_Profile_Accounts.add.nazvanie_akkaunta' => 'Account name:',
    'Controller_Profile_Accounts.add.imya_polzovatelya' => 'Username:',
    'Controller_Profile_Accounts.add.login_polzovatelya' => 'User login:',
    'Controller_Profile_Accounts.add.proverit' => 'check',
    'Controller_Profile_Accounts.add.parol_polzovatelya' => 'User password:',
    'Controller_Profile_Accounts.add.podtverzhdenie_parolya' => 'Password confirmation:',
    'Controller_Profile_Accounts.add.sdelat_akkaunt_aktivnym' => 'Make account active',
    'Controller_Profile_Accounts.add.dobavit' => 'Add',
    // log action
    'Controller_Profile_Accounts.log.content_title' => 'Accounts activity history',
    'Controller_Profile_Accounts.log.title' => 'Accounts activity history',
    'Controller_Profile_Accounts.log.nazvanie_akkaunta' => 'Account name',
    'Controller_Profile_Accounts.log.data_sobytiya' => 'Event date',
    'Controller_Profile_Accounts.log.ip_adres' => 'IP address',
    'Controller_Profile_Accounts.log.tip_sobytiya' => 'Event type',
    'Controller_Profile_Accounts.log.vxod' => 'Log in',
    'Controller_Profile_Accounts.log.vyxod' => 'Log out',
    // prolongs action
    'Controller_Profile_Accounts.prolongs.content_title' => 'Accounts prolongation history',
    'Controller_Profile_Accounts.prolongs.title' => 'Accounts prolongation history',
    'Controller_Profile_Accounts.prolongs.nazvanie_akkaunta' => 'Account name',
    'Controller_Profile_Accounts.prolongs.data_prodleniya' => 'Prolongation date',
    'Controller_Profile_Accounts.prolongs.summa_prodleniya' => 'Prolongation amount',
    'Controller_Profile_Accounts.prolongs.kolichestvo_dnej_prodleniya' => 'Number of prolongation days',
    // Controller_Profile_Connection
    // info action
    'Controller_Profile_Connection.info.content_title' => 'Connection profile information',
    'Controller_Profile_Connection.info.title' => 'Connection profile information',
    'Controller_Profile_Connection.info.da' => 'yes',
    'Controller_Profile_Connection.info.net' => 'no',
    'Controller_Profile_Connection.info.profil_podklyucheniya' => 'Connection profile',
    'Controller_Profile_Connection.info.nazvanie' => 'Name:',
    'Controller_Profile_Connection.info.kratkoe_opisanie' => 'Short description:',
    'Controller_Profile_Connection.info.aktivnyj' => 'Active:',
    'Controller_Profile_Connection.info.vvodit_parol_lokalno' => 'Type the password locally:',
    'Controller_Profile_Connection.info.nastrojki_sip_akkaunta' => 'SIP account settings',
    'Controller_Profile_Connection.info.otobrazhaemoe_imya' => 'Display name:',
    'Controller_Profile_Connection.info.dobavochnyj_nomer' => 'Extension:',
    'Controller_Profile_Connection.info.imya_polzovatelya' => 'Username:',
    'Controller_Profile_Connection.info.parol' => 'Password:',
    'Controller_Profile_Connection.info.ne_ukazan' => 'Not specified',
    'Controller_Profile_Connection.info.sekretnyj_parol' => 'Secret password:',
    'Controller_Profile_Connection.info.ispolzovat_sekretnyj_parol' => 'Use secret password:',
    'Controller_Profile_Connection.info.setevye_nastrojki' => 'Network settings',
    'Controller_Profile_Connection.info.adres_sip_servera' => 'SIP server address:',
    'Controller_Profile_Connection.info.port_sip_servera' => 'SIP server port:',
    'Controller_Profile_Connection.info.adres_sip_proksi_servera' => 'SIP proxy server address:',
    'Controller_Profile_Connection.info.port_sip_proksi_servera' => 'SIP proxy server port:',
    'Controller_Profile_Connection.info.lokalnyj_ip_adres' => 'Local IP address:',
    'Controller_Profile_Connection.info.lokalnyj_port' => 'Local port:',
    'Controller_Profile_Connection.info.minimalnyj_rtp_port' => 'Minimum RTP port:',
    'Controller_Profile_Connection.info.maksimalnyj_rtp_port' => 'Maximum RTP port:',
    'Controller_Profile_Connection.info.tajmer_registracii' => 'Registration timer:',
    'Controller_Profile_Connection.info.ispolzovat_sluchajnyj_lokalnyj_port' => 'Use random local port:',
    'Controller_Profile_Connection.info.opredelyat_lokalnyj_adres_avtomaticheski' => 'Detect local address automatically:',
    'Controller_Profile_Connection.info.ispolzovat_proksi_server' => 'Use proxy server:',
    'Controller_Profile_Connection.info.nastrojki_parametrov_nat' => 'NAT settings',
    'Controller_Profile_Connection.info.tajmer_keepalive' => 'Keep-Alive timer:',
    'Controller_Profile_Connection.info.adres_stun_servera' => 'STUN server address:',
    'Controller_Profile_Connection.info.port_stun_servera' => 'STUN server port:',
    'Controller_Profile_Connection.info.ispolzovat_stun_razvedku' => 'Use STUN lookup:',
    'Controller_Profile_Connection.info.ispolzovat_stun_server_po_umolchaniyu' => 'Use default STUN server:',
    'Controller_Profile_Connection.info.ispolzovat_tajmer_keepalive' => 'Use Keep-Alive timer:',
    'Controller_Profile_Connection.info.ispolzovat_privyazku_k_sip_serveru' => 'Bind to SIP server IP address:',
    'Controller_Profile_Connection.info.ispolzovat_simmetricheskij_rtp' => 'Use symmetric RTP:',
    'Controller_Profile_Connection.info.nastrojki_prioritetov_kodekov' => 'Codecs priority settings',
    'Controller_Profile_Connection.info.kodek' => 'Codec',
    'Controller_Profile_Connection.info.nastrojki_parametrov_dtmf' => 'DTMF settings',
    'Controller_Profile_Connection.info.predpochitaemyj_dtmf_metod' => 'DTMF method:',
    'Controller_Profile_Connection.info.dlitelnost_dtmf_signala' => 'DTMF signal duration:',
    // edit action
    'Controller_Profile_Connection.edit.content_title' => 'Connection profile editing',
    'Controller_Profile_Connection.edit.title' => 'Connection profile editing',
    'Controller_Profile_Connection.edit.vozmozhnost_redaktirovaniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Connection profiles editing is temporarily suspended',
    'Controller_Profile_Connection.edit.izmeneniya_soxraneny' => 'The changes have been saved',
    'Controller_Profile_Connection.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Unable to save the changes, please try again',
    'Controller_Profile_Connection.edit.profil_podklyucheniya' => 'Connection profile',
    'Controller_Profile_Connection.edit.nazvanie' => 'Name:',
    'Controller_Profile_Connection.edit.kratkoe_opisanie' => 'Short description:',
    'Controller_Profile_Connection.edit.sdelat_aktivnym' => 'Make active',
    'Controller_Profile_Connection.edit.vvodit_parol_lokalno' => 'Type the password locally',
    'Controller_Profile_Connection.edit.nastrojki_sip_akkaunta' => 'SIP account settings',
    'Controller_Profile_Connection.edit.otobrazhaemoe_imya' => 'Display name:',
    'Controller_Profile_Connection.edit.dobavochnyj_nomer' => 'Extension:',
    'Controller_Profile_Connection.edit.imya_polzovatelya' => 'Username:',
    'Controller_Profile_Connection.edit.parol' => 'Password:',
    'Controller_Profile_Connection.edit.sekretnyj_parol' => 'Secret password:',
    'Controller_Profile_Connection.edit.ispolzovat_sekretnyj_parol' => 'Use secret password',
    'Controller_Profile_Connection.edit.setevye_nastrojki' => 'Network settings',
    'Controller_Profile_Connection.edit.adres_sip_servera' => 'SIP server address:',
    'Controller_Profile_Connection.edit.port_sip_servera' => 'SIP server port:',
    'Controller_Profile_Connection.edit.adres_sip_proksi_servera' => 'SIP proxy server address:',
    'Controller_Profile_Connection.edit.port_sip_proksi_servera' => 'SIP proxy server port:',
    'Controller_Profile_Connection.edit.lokalnyj_ip_adres' => 'Local IP address:',
    'Controller_Profile_Connection.edit.lokalnyj_port' => 'Local port:',
    'Controller_Profile_Connection.edit.minimalnyj_rtp_port' => 'Minimum RTP port:',
    'Controller_Profile_Connection.edit.maksimalnyj_rtp_port' => 'Maximum RTP port:',
    'Controller_Profile_Connection.edit.tajmer_registracii' => 'Registration timer:',
    'Controller_Profile_Connection.edit.ispolzovat_sluchajnyj_lokalnyj_port' => 'Use random local port',
    'Controller_Profile_Connection.edit.opredelyat_lokalnyj_adres_avtomaticheski' => 'Detect local address automatically',
    'Controller_Profile_Connection.edit.ispolzovat_proksi_server' => 'Use proxy server',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_nat' => 'NAT settings',
    'Controller_Profile_Connection.edit.tajmer_keepalive' => 'Keep-Alive timer:',
    'Controller_Profile_Connection.edit.adres_stun_servera' => 'STUN server address:',
    'Controller_Profile_Connection.edit.port_stun_servera' => 'STUN server port:',
    'Controller_Profile_Connection.edit.ispolzovat_stun_razvedku' => 'Use STUN lookup',
    'Controller_Profile_Connection.edit.ispolzovat_stun_server_po_umolchaniyu' => 'Use default STUN server',
    'Controller_Profile_Connection.edit.ispolzovat_tajmer_keepalive' => 'Use Keep-Alive timer',
    'Controller_Profile_Connection.edit.ispolzovat_privyazku_k_sip_serveru' => 'Bind to SIP server IP address',
    'Controller_Profile_Connection.edit.ispolzovat_simmetricheskij_rtp' => 'Use symmetric RTP',
    'Controller_Profile_Connection.edit.nastrojki_prioritetov_kodekov' => 'Codecs priority settings',
    'Controller_Profile_Connection.edit.kodek' => 'Codec',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_dtmf' => 'DTMF settings',
    'Controller_Profile_Connection.edit.predpochitaemyj_dtmf_metod' => 'DTMF method:',
    'Controller_Profile_Connection.edit.dlitelnost_dtmf_signala' => 'DTMF signal duration:',
    'Controller_Profile_Connection.edit.soxranit' => 'Save',
    // remove action
    'Controller_Profile_Connection.remove.content_title' => 'Connection profile removing',
    'Controller_Profile_Connection.remove.title' => 'Connection profile removing',
    'Controller_Profile_Connection.remove.vozmozhnost_udaleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Connection profile removing is temporarily suspended',
    'Controller_Profile_Connection.remove.podtverdite_udalenie_profilya_podklyucheniya' => 'Confirm the removing of the connection profile:',
    'Controller_Profile_Connection.remove.udalit' => 'Remove',
    // Controller_Profile_Connections
    // list action
    'Controller_Profile_Connections.list.content_title' => 'Connection profiles list',
    'Controller_Profile_Connections.list.title' => 'Connection profiles list',
    'Controller_Profile_Connections.list.profil_podklyucheniya' => 'Connection profile',
    'Controller_Profile_Connections.list.data_dobavleniya' => 'Date of adding',
    'Controller_Profile_Connections.list.status' => 'Status',
    'Controller_Profile_Connections.list.aktivnyj' => 'Active',
    'Controller_Profile_Connections.list.neaktivnyj' => 'Not active',
    // add action
    'Controller_Profile_Connections.add.content_title' => 'Connection profile adding',
    'Controller_Profile_Connections.add.title' => 'Connection profile adding',
    'Controller_Profile_Connections.add.vozmozhnost_dobavleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Connection profile adding is temporarily suspended',
    'Controller_Profile_Connections.add.profil_podklyucheniya_dobavlen' => 'Connection profile has been added',
    'Controller_Profile_Connections.add.nevozmozhno_dobavit_profil_podklyucheniya,_povtorite_popytku' => 'Unable to add the connection profile, please try again',
    'Controller_Profile_Connections.add.dobavit' => 'Add',
    // Controller_Profile_Contact
    // info action
    'Controller_Profile_Contact.info.content_title' => 'About the contact',
    'Controller_Profile_Contact.info.title' => 'About the contact',
    'Controller_Profile_Contact.info.imya_kontakta' => 'Contact\'s name:',
    'Controller_Profile_Contact.info.nomer_telefona' => 'Phone number:',
    'Controller_Profile_Contact.info.data_dobavleniya_kontakta' => 'Date of adding:',
    // edit action
    'Controller_Profile_Contact.edit.content_title' => 'Contact editing',
    'Controller_Profile_Contact.edit.title' => 'Contact editing',
    'Controller_Profile_Contact.edit.vozmozhnost_redaktirovaniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Contact editing is temporarily suspended',
    'Controller_Profile_Contact.edit.izmeneniya_soxraneny' => 'The changes have been saved',
    'Controller_Profile_Contact.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Unable to save the changes, please try again',
    'Controller_Profile_Contact.edit.imya_kontakta' => 'Contact\'s name:',
    'Controller_Profile_Contact.edit.nomer_telefona' => 'Phone number:',
    'Controller_Profile_Contact.edit.soxranit' => 'Save',
    // Controller_Profile_Contacts
    // list action
    'Controller_Profile_Contacts.list.content_title' => 'Contacts list',
    'Controller_Profile_Contacts.list.title' => 'Contacts list',
    'Controller_Profile_Contacts.list.imya_kontakta' => 'Имя контакта',
    'Controller_Profile_Contacts.list.nomer_telefona' => 'Contact\'s name',
    'Controller_Profile_Contacts.list.data_dobavleniya' => 'Phone number',
    'Controller_Profile_Contacts.list.dejstvie' => 'Action',
    'Controller_Profile_Contacts.list.udalit' => 'Remove',
    // add action
    'Controller_Profile_Contacts.add.content_title' => 'Contact adding',
    'Controller_Profile_Contacts.add.title' => 'Contact adding',
    'Controller_Profile_Contacts.add.vozmozhnost_dobavleniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Contact adding is temporarily suspended',
    'Controller_Profile_Contacts.add.kontakt_dobavlen' => 'Contact has been added',
    'Controller_Profile_Contacts.add.nevozmozhno_dobavit_kontakt,_povtorite_popytku' => 'Unable to add the contact, please try again',
    'Controller_Profile_Contacts.add.imya_kontakta' => 'Contact\'s name:',
    'Controller_Profile_Contacts.add.nomer_telefona' => 'Phone number:',
    'Controller_Profile_Contacts.add.dobavit' => 'Add',
    // Controller_Profile_Profile
    // info action
    'Controller_Profile_Profile.info.content_title' => 'Profile',
    'Controller_Profile_Profile.info.title' => 'Profile',
    'Controller_Profile_Profile.info.kolichestvo_akkauntov' => 'Number of accounts:',
    'Controller_Profile_Profile.info.dobavit_akkaunt' => 'Add an account',
    'Controller_Profile_Profile.info.otobrazit_spisok_akkauntov' => 'Show the accounts list',
    // edit action
    'Controller_Profile_Profile.edit.content_title' => 'Profile editing',
    'Controller_Profile_Profile.edit.title' => 'Profile editing',
    'Controller_Profile_Profile.edit.vozmozhnost_redaktirovaniya_profilya_na_sajte_vremenno_priostanovlena' => 'Profile editing is temporarily suspended',
    'Controller_Profile_Profile.edit.izmeneniya_soxraneny' => 'The changes have been saved',
    'Controller_Profile_Profile.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Unable to save the changes, please try again',
    'Controller_Profile_Profile.edit.polnoe_imya' => 'Full name:',
    'Controller_Profile_Profile.edit.login' => 'Login:',
    'Controller_Profile_Profile.edit.parol' => 'Password:',
    'Controller_Profile_Profile.edit.podtverzhdenie_parolya' => 'Password confirmation:',
    'Controller_Profile_Profile.edit.email' => 'Email:',
    'Controller_Profile_Profile.edit.smenit_email_adres' => 'Change the email address',
    'Controller_Profile_Profile.edit.yazyk' => 'Language:',
    'Controller_Profile_Profile.edit.vremennaya_zona' => 'Time zone:',
    'Controller_Profile_Profile.edit.soxranit' => 'Save',
    // balance action
    'Controller_Profile_Profile.balance.content_title' => 'Balance',
    'Controller_Profile_Profile.balance.title' => 'Balance',
    'Controller_Profile_Profile.balance.tekushhij_balans' => 'Balance:',
    // invoice action
    'Controller_Profile_Profile.invoice.content_title' => 'Balance refilling',
    'Controller_Profile_Profile.invoice.title' => 'Balance refilling',
    'Controller_Profile_Profile.invoice.vozmozhnost_popolneniya_balansa_na_sajte_vremenno_priostanovlena' => 'Balance refilling is temporarily suspended',
    'Controller_Profile_Profile.invoice.popolnenie_balansa_polzovatelya' => 'Balance refilling:',
    'Controller_Profile_Profile.invoice.nevozmozhno_popolnit_balans,_povtorite_popytku' => 'Unable to refill the balance, please try again',
    'Controller_Profile_Profile.invoice.summa_popolneniya' => 'Refilling amount:',
    'Controller_Profile_Profile.invoice.minimum' => 'minimum',
    'Controller_Profile_Profile.invoice.tip_valyuty_webmoney' => 'Type of Webmoney currency:',
    'Controller_Profile_Profile.invoice.prodolzhit' => 'Continue',
    'Controller_Profile_Profile.invoice.zhelaemaya_summa_popolneniya' => 'Refilling amount:',
    'Controller_Profile_Profile.invoice.neobxodimaya_summa_dlya_proplaty' => 'Payment amount:',
    'Controller_Profile_Profile.invoice.proplatit' => 'Pay',
    // invoices action
    'Controller_Profile_Profile.invoices.content_title' => 'Payment history',
    'Controller_Profile_Profile.invoices.title' => 'Payment history',
    'Controller_Profile_Profile.invoices.schet' => 'Invoice №',
    'Controller_Profile_Profile.invoices.tip_valyuty' => 'Type of currency',
    'Controller_Profile_Profile.invoices.data_sozdaniya' => 'Date of creation',
    'Controller_Profile_Profile.invoices.sostoyanie' => 'State',
    'Controller_Profile_Profile.invoices.summa_popolneniya' => 'Refilling amount',
    'Controller_Profile_Profile.invoices.data_proplaty' => 'Date of payment',
    'Controller_Profile_Profile.invoices.otmenen' => 'Cancelled',
    'Controller_Profile_Profile.invoices.proplachen' => 'Paid',
    'Controller_Profile_Profile.invoices.ozhidaet_proplaty' => 'Waiting for payment',
    // log action
    'Controller_Profile_Profile.log.content_title' => 'Activity history',
    'Controller_Profile_Profile.log.title' => 'Activity history',
    'Controller_Profile_Profile.log.data_sobytiya' => 'Date of event',
    'Controller_Profile_Profile.log.ip_adres' => 'IP address',
    'Controller_Profile_Profile.log.tip_sobytiya' => 'Type of event',
    'Controller_Profile_Profile.log.vxod' => 'Log in',
    'Controller_Profile_Profile.log.vyxod' => 'Log out',
    // email action
    'Controller_Profile_Profile.email.content_title' => 'Profile email address changing',
    'Controller_Profile_Profile.email.title' => 'Profile email address changing',
    'Controller_Profile_Profile.email.vozmozhnost_smeny_email_adresa_profilya_na_sajte_vremenno_priostanovlena' => 'Profile email address changing is temporarily suspended',
    'Controller_Profile_Profile.email.na_vash_tekushhij_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'The link for confirmation of the email address changing is sent to your current email address',
    'Controller_Profile_Profile.email.nevozmozhno_smenit_email_adres_profilya,_povtorite_popytku' => 'Unable to change the profile email address, please try again',
    'Controller_Profile_Profile.email.na_vash_novyj_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'The link for confirmation of the email address changing is sent to your new email address',
    'Controller_Profile_Profile.email.nevozmozhno_podtverdit_smenu_email_adresa_profilya,_povtorite_popytku' => 'Unable to confirm the profile email address changing, please try again',
    'Controller_Profile_Profile.email.email_adres_profilya_izmenen_na_sleduyushhij' => 'Profile eamil address is changed to:',
    'Controller_Profile_Profile.email.novyj_email_adres' => 'Your new email address:',
    'Controller_Profile_Profile.email.izmenit' => 'Change',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Template section                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // View_Template_Profile
    // navigation_bar view
    'View_Template_Profile.navigation_bar.profil' => 'Profile',
    'View_Template_Profile.navigation_bar.akkaunty' => 'Accounts',
    'View_Template_Profile.navigation_bar.podderzhka' => 'Support',
    'View_Template_Profile.navigation_bar.voprosy-otvety' => 'Questions - Answers',
    'View_Template_Profile.navigation_bar.obratnaya_svyaz' => 'Feedback',
    'View_Template_Profile.navigation_bar.vyjti' => 'Log out',
    // View_Template_Site
    // navigation_bar view
    'View_Template_Site.navigation_bar.o_servise' => 'About the service',
    'View_Template_Site.navigation_bar.novosti' => 'News',
    'View_Template_Site.navigation_bar.podderzhka' => 'Support',
    'View_Template_Site.navigation_bar.voprosy-otvety' => 'Questions - Answers',
    'View_Template_Site.navigation_bar.vosstanovlenie_parolya' => 'Password recovery',
    'View_Template_Site.navigation_bar.obratnaya_svyaz' => 'Feedback',
    'View_Template_Site.navigation_bar.profil' => 'Profile',
    'View_Template_Site.navigation_bar.vyjti' => 'Log out',
    'View_Template_Site.navigation_bar.vxod' => 'Log in',
    'View_Template_Site.navigation_bar.registraciya' => 'Registration',
);