<?php

defined('SYSPATH') or die('No direct script access.');

// http://mainspy.ru/translit_perevodchik
return array(
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Basic section                             */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller
    'Controller.identifikator_formy_ne_sovpadaet' => 'Ідентифікатор форми не співпадає',
    'Controller.vvedite_pravilnyj_proverochnyj_kod' => 'Введіть правильний код перевірки',
    'Controller.proverochnyj_kod' => 'Код перевірки:',
    'Controller.mezhsajtovye_xss_zaprosy_zapreshheny' => 'Міжсайтові XSS запити заборонено',
    'Controller.net_dannyx_dlya_otobrazheniya' => 'Немає даних для відображення',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Site section                              */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Site_Home
    // home action
    'Controller_Site_Home.home.title' => 'Хмарний JAVA SIP клієнт',
    'Controller_Site_Home.home.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Home.home.description' => 'Revosip - хмарний JAVA SIP клієнт',
    'Controller_Site_Home.home.revosip_-_oblachnyj_sip_softfon' => 'Revosip - хмарний JAVA SIP клієнт',
    'Controller_Site_Home.home.otkrytyj_beta_test' => 'Відкритий бета тест',
    'Controller_Site_Home.home.registraciya' => 'Реєстрація',
    'Controller_Site_Home.home.preimushhestva' => 'Переваги',
    'Controller_Site_Home.home.sravnenie_s_ip_telefonom' => 'Порівняння з IP телефоном',
    'Controller_Site_Home.home.xarakteristiki' => 'Характеристики',
    'Controller_Site_Home.home.skrinshoty' => 'Скріншоти',
    'Controller_Site_Home.home.osobennosti' => 'Особливості',
    'Controller_Site_Home.home.vozmozhnosti' => 'Можливості',
    'Controller_Site_Home.home.predmet_sravneniya' => 'Предмет порівняння',
    'Controller_Site_Home.home.programmnyj_telefon_revosip' => 'Програмний телефон Revosip',
    'Controller_Site_Home.home.apparatnyj_ip_telefon' => 'Апаратний IP телефон',
    // about action
    'Controller_Site_Home.about.content_title' => 'Про сервіс',
    'Controller_Site_Home.about.title' => 'Про сервіс',
    'Controller_Site_Home.about.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Home.about.description' => 'Про сервіс',
    // faq action
    'Controller_Site_Home.faq.content_title' => 'Запитання - Відповіді',
    'Controller_Site_Home.faq.title' => 'Запитання - Відповіді',
    'Controller_Site_Home.faq.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Home.faq.description' => 'Запитання - Відповіді',
    'Controller_Site_Home.faq.ssylka_na_otvet' => 'Посилання на відповідь',
    // feedback action
    'Controller_Site_Home.feedback.content_title' => 'Зворотний зв\'язок',
    'Controller_Site_Home.feedback.title' => 'Зворотний зв\'язок',
    'Controller_Site_Home.feedback.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Home.feedback.description' => 'Зворотний зв\'язок',
    'Controller_Site_Home.feedback.revosip_-_oblachnyj_sip_softfon' => '<strong>Revosip</strong> - хмарний сервіс SIP софтфону',
    'Controller_Site_Home.feedback.email_adres' => 'Email адреса:',
    'Controller_Site_Home.feedback.skype' => 'Skype:',
    'Controller_Site_Home.feedback.kiev,_ukraina' => 'Київ, Україна',
    // conditions action
    'Controller_Site_Home.conditions.content_title' => 'Умови угоди користувача',
    'Controller_Site_Home.conditions.title' => 'Умови угоди користувача',
    'Controller_Site_Home.conditions.keywords' => 'Умови угоди користувача',
    'Controller_Site_Home.conditions.description' => 'Умови угоди користувача',
    // Controller_Site_Login
    // login action
    'Controller_Site_Login.login.content_title' => 'Вхід',
    'Controller_Site_Login.login.title' => 'Вхід',
    'Controller_Site_Login.login.keywords' => 'Вхід в профіль',
    'Controller_Site_Login.login.description' => 'Вхід на сторінку користувача',
    'Controller_Site_Login.login.vozmozhnost_vxoda_na_sajt_vremenno_priostanovlena' => 'Можливість входу на сайт тимчасово припинено',
    'Controller_Site_Login.login.vxod_vozmozhen_tolko_posle_podtverzhdeniya_registracii' => 'Вхід можливий тільки після підтвердження реєстрації',
    'Controller_Site_Login.login.dostup_vremenno_zablokirovan' => 'Доступ тимчасово заблоковано',
    'Controller_Site_Login.login.nevozmozhno_vypolnit_vxod,_povtorite_popytku' => 'Неможливо виконати вхід, повторіть спробу',
    'Controller_Site_Login.login.nepravilnyj_login_ili_parol' => 'Неправильний логін або пароль',
    'Controller_Site_Login.login.login' => 'Логін:',
    'Controller_Site_Login.login.parol' => 'Пароль:',
    'Controller_Site_Login.login.zabyli_parol' => 'Забули пароль?',
    'Controller_Site_Login.login.vojti' => 'Ввійти',
    // Controller_Site_News
    // login action
    'Controller_Site_News.list.content_title' => 'Новини',
    'Controller_Site_News.list.title' => 'Новини',
    'Controller_Site_News.list.keywords' => 'Revosip - Новини',
    'Controller_Site_News.list.description' => 'Revosip - Новини',
    // add action
    'Controller_Site_News.add.content_title' => 'Додавання новини',
    'Controller_Site_News.add.title' => 'Додавання новини',
    'Controller_Site_News.add.vvedite_magicheskij_parol' => 'Введіть магічний пароль',
    'Controller_Site_News.add.magicheskij_parol_ne_sovpadaet' => 'Магічний пароль не співпадає',
    'Controller_Site_News.add.novost_dobavlena' => 'Новина добавлена',
    'Controller_Site_News.add.nevozmozhno_dobavit_novost' => 'Неможливо додати новину',
    'Controller_Site_News.add.uroven_vazhnosti' => 'Рівень важливості:',
    'Controller_Site_News.add.oglavlenie_novosti' => 'Зміст новини:',
    'Controller_Site_News.add.klyuchevye_slova' => 'Ключові слова (meta):',
    'Controller_Site_News.add.kratkoe_opisanie' => 'Короткий опис (meta):',
    'Controller_Site_News.add.soderzhimoe_novosti' => 'Зміст новини:',
    'Controller_Site_News.add.magicheskij_parol' => 'Магічний пароль:',
    'Controller_Site_News.add.dobavit' => 'Додати',
    // edit action
    'Controller_Site_News.edit.content_title' => 'Редагування новини',
    'Controller_Site_News.edit.title' => 'Редагування новини',
    'Controller_Site_News.edit.izmeneniya_soxraneny' => 'Зміни збережено',
    'Controller_Site_News.edit.nevozmozhno_soxranit_otredaktirovannuyu_novost' => 'Неможливо зберегти відредаговану новину',
    'Controller_Site_News.edit.soxranit' => 'Зберегти',
    // remove action
    'Controller_Site_News.remove.content_title' => 'Видалення новини',
    'Controller_Site_News.remove.title' => 'Видалення новини',
    'Controller_Site_News.remove.novost_udalena' => 'Новина видалена',
    'Controller_Site_News.remove.nevozmozhno_udalit_novost' => 'Неможливо видалити новину',
    'Controller_Site_News.remove.magicheskij_parol' => 'Магічний пароль:',
    'Controller_Site_News.remove.udalit' => 'Видалити',
    // Controller_Site_Recovery
    // recovery action
    'Controller_Site_Recovery.recovery.content_title' => 'Відновлення пароля',
    'Controller_Site_Recovery.recovery.title' => 'Відновлення пароля',
    'Controller_Site_Recovery.recovery.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Recovery.recovery.description' => 'Відновлення пароля користувача',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_na_sajte_vremenno_priostanovlena' => 'Можливість відновлення пароля на сайті тимчасово припинено',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_polzovatel_s_takim_email_adresom_ne_najden' => 'Неможливо відновити пароль, користувача з вказаною email адресою не знайдено',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_vosstanovlenie_parolya_vozmozhno_tolko_posle_podtverzhdeniya_registracii' => 'Неможливо відновити пароль, відновлення пароля можливо тільки після підтвердження реєстрації',
    'Controller_Site_Recovery.recovery.vozmozhnost_vosstanovleniya_parolya_vremenno_zablokirovana' => 'Можливість відновлення пароля тимчасово заблоковано',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_podtverdite_sushhestvuyushhij_zapros_na_vosstanovlenie_parolya' => 'Неможливо відновити пароль, підтвердіть існуючий запит на відновлення паролю',
    'Controller_Site_Recovery.recovery.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_procedury_vosstanovleniya_parolya' => 'На вашу email адресу надіслано посилання для підтвердження процедури відновлення пароля',
    'Controller_Site_Recovery.recovery.nevozmozhno_vosstanovit_parol,_povtorite_popytku_pozzhe' => 'Неможливо відновити пароль, повторіть спробу пізніше',
    'Controller_Site_Recovery.recovery.email_adres' => 'Email адреса:',
    'Controller_Site_Recovery.recovery.vosstanovit_parol' => 'Відновити пароль',
    // confirm action
    'Controller_Site_Recovery.confirm.content_title' => 'Підтвердження процедури відновлення пароля користувача',
    'Controller_Site_Recovery.confirm.title' => 'Підтвердження процедури відновлення пароля користувача',
    'Controller_Site_Recovery.confirm.keywords' => 'Підтвердження процедури відновлення пароля користувача',
    'Controller_Site_Recovery.confirm.description' => 'Підтвердження процедури відновлення пароля користувача',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol,_polzovatel_ne_najden' => 'Неможливо відновити пароль, користувач не знайдений',
    'Controller_Site_Recovery.confirm.nevozmozhno_vosstanovit_parol' => 'Неможливо відновити пароль',
    'Controller_Site_Recovery.confirm.vash_novyj_parol' => 'Ваш новий пароль:',
    'Controller_Site_Recovery.confirm.vy_vsegda_mozhete_izmenit_parol_na_stranice_profilya' => 'Ви завжди можете змінити пароль на сторінці профілю',
    // Controller_Site_Registration
    // registration
    'Controller_Site_Registration.registration.content_title' => 'Реєстрація користувача',
    'Controller_Site_Registration.registration.title' => 'Реєстрація користувача',
    'Controller_Site_Registration.registration.keywords' => 'хмарний sip софтфон, хмарний sip клієнт, java sip софтфон, java sip клієнт, sip клієнт для браузера, sip клієнт asterisk, sip клієнт windows, sip клієнт linux, sip клієнт mac, sip клієнт ubuntu',
    'Controller_Site_Registration.registration.description' => 'Реєстрація користувача',
    'Controller_Site_Registration.registration.vozmozhnost_registracii_na_sajte_vremenno_priostanovlena' => 'Можливість реєстрації на сайті тимчасово припинено',
    'Controller_Site_Registration.registration.na_vash_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_registracii' => 'На вашу email адресу надіслано посилання для підтвердження реєстрації',
    'Controller_Site_Registration.registration.nevozmozhno_vypolnit_registraciyu,_povtorite_popytku' => 'Неможливо виконати реєстрацію, повторіть спробу пізніше',
    'Controller_Site_Registration.registration.proverit' => 'перевірити',
    'Controller_Site_Registration.registration.login' => 'Логін:',
    'Controller_Site_Registration.registration.parol' => 'Пароль:',
    'Controller_Site_Registration.registration.podtverzhdenie_parolya' => 'Підтвердження пароля:',
    'Controller_Site_Registration.registration.polnoe_imya' => 'Повне ім\'я:',
    'Controller_Site_Registration.registration.email_adres' => 'Email адреса:',
    'Controller_Site_Registration.registration.yazyk' => 'Мова:',
    'Controller_Site_Registration.registration.vremennaya_zona' => 'Часова зона:',
    'Controller_Site_Registration.registration.ya_prinimayu' => 'Я приймаю',
    'Controller_Site_Registration.registration.usloviya_polzovatelskogo_soglasheniya' => 'Умови угоди користувача',
    'Controller_Site_Registration.registration.zaregistrirovatsya' => 'Зареєструватися',
    // confirm action
    'Controller_Site_Registration.confirm.content_title' => 'Підтвердження реєстрації користувача',
    'Controller_Site_Registration.confirm.title' => 'Підтвердження реєстрації користувача',
    'Controller_Site_Registration.confirm.keywords' => 'Підтвердження реєстрації користувача',
    'Controller_Site_Registration.confirm.description' => 'Підтвердження реєстрації користувача',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu' => 'Неможливо підтвердити реєстрацію',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_srok_podtverzhdeniya_registracii_istek' => 'Неможливо підтвердити реєстрацію, термін підтвердження реєстрації минув',
    'Controller_Site_Registration.confirm.registraciya_polzovatelya_podtverzhdena,_vy_mozhete_vojti_na_sajt_ispolzuya_login_i_parol' => 'Реєстрація користувача підтверджена, Ви можете ввійти на сайт використовуючи логін та пароль',
    'Controller_Site_Registration.confirm.nevozmozhno_podtverdit_registraciyu,_povtorite_popytku' => 'Неможливо підтвердити реєстрацію, повторіть спробу',
    // Controller_Ajax_Check
    'Controller_Ajax_Check.nevernyj_format' => 'неправильний формат',
    'Controller_Ajax_Check.dostupen' => 'доступний',
    'Controller_Ajax_Check.nedostupen' => 'недоступний',
    // Controller_Error
    'Controller_Error.content_title' => 'Сторінку не знайдено',
    'Controller_Error.title' => 'Сторінку не знайдено',
    // View_Widget_Launch
    // applet view
    'View_Widget_Launch.applet.keywords' => 'Revosip applet',
    'View_Widget_Launch.applet.description' => 'Revosip applet',
    'View_Widget_Launch.applet.vash_brauzer_ne_podderzhivaet_zapusk_java_appletov' => 'Ваш браузер не підтримує запуск Java аплетів.',
    'View_Widget_Launch.applet.dlya_podderzhki_zapuska_java_appletov_neobxodimo_ustanovit_virtualnuyu_mashinu_java' => 'Для підтримки запуску Java аплетів необхідно встановити віртуальну машину Java',
    'View_Widget_Launch.applet.skachat' => 'скачати',
    // launcher view
    'View_Widget_Launch.launcher.versiya' => 'версія',
    'View_Widget_Launch.launcher.zapustit' => 'Запустити',
    // View_Widget_News
    'View_Widget_News.list.poslednie_novosti' => 'Останні новини',
    'View_Widget_News.list.vse_novosti' => 'Всі новини',
    // Controller_Widget_Menu
    // accounts action
    'Controller_Widget_Menu.accounts.akkaunty' => 'Акаунти',
    'Controller_Widget_Menu.accounts.spisok_akkauntov' => 'Список акаунтів',
    'Controller_Widget_Menu.accounts.dobavit_akkaunt' => 'Додати акаунт',
    'Controller_Widget_Menu.accounts.istoriya_aktivnosti_akkauntov' => 'Історія активності акаунтів',
    'Controller_Widget_Menu.accounts.istoriya_prodlenij_akkauntov' => 'Історія продовжень дії акаунтів',
    // account action
    'Controller_Widget_Menu.account.akkaunt' => 'Акаунт',
    'Controller_Widget_Menu.account.informaciya_ob_akkaunte' => 'Інформація про акаунт',
    'Controller_Widget_Menu.account.nastrojki_akkaunta' => 'Налаштування акаунта',
    'Controller_Widget_Menu.account.redaktirovat_akkaunt' => 'Редагувати акаунт',
    'Controller_Widget_Menu.account.prodlit_akkaunt' => 'Продовжити термін дії акаунта',
    'Controller_Widget_Menu.account.istoriya_prodlenij_akkaunta' => 'Історія продовжень терміну дії акаунта',
    'Controller_Widget_Menu.account.istoriya_aktivnosti_akkaunta' => 'Історія активності акаунта',
    'Controller_Widget_Menu.account.udalit_akkaunt' => 'Видалити акаунт',
    // contacts action
    'Controller_Widget_Menu.contacts.kontakty' => 'Контакти',
    'Controller_Widget_Menu.contacts.spisok_kontaktov' => 'Список контактів',
    'Controller_Widget_Menu.contacts.dobavit_kontakt' => 'Додати контакт',
    // contact action
    'Controller_Widget_Menu.contact.kontakt' => 'Контакт',
    'Controller_Widget_Menu.contact.informaciya_o_kontakte' => 'Інформація про контакт',
    'Controller_Widget_Menu.contact.redaktirovat_kontakt' => 'Редагувати контакт',
    // connections action
    'Controller_Widget_Menu.connections.profili_podklyucheniya' => 'Профілі підключень',
    'Controller_Widget_Menu.connections.spisok_profilej_podklyucheniya' => 'Список профілів підключень',
    'Controller_Widget_Menu.connections.dobavit_profil_podklyucheniya' => 'Додати профіль підключення',
    // connection action
    'Controller_Widget_Menu.connection.profil_podklyucheniya' => 'Профіль підключення',
    'Controller_Widget_Menu.connection.informaciya_o_profile_podklyucheniya' => 'Інформація про профіль підключення',
    'Controller_Widget_Menu.connection.redaktirovat_profil_podklyucheniya' => 'Редагувати профіль підключення',
    'Controller_Widget_Menu.connection.udalit_profil_podklyucheniya' => 'Видалити профіль підключення',
    // profile action
    'Controller_Widget_Menu.profile.profil' => 'Профіль',
    'Controller_Widget_Menu.profile.redaktirovat_profil' => 'Редагувати профіль',
    'Controller_Widget_Menu.profile.balans_profilya' => 'Баланс профілю',
    'Controller_Widget_Menu.profile.popolnit_balans_profilya' => 'Поповнити баланс профілю',
    'Controller_Widget_Menu.profile.istoriya_proplat' => 'Історія проплат',
    'Controller_Widget_Menu.profile.istoriya_aktivnosti' => 'Історія активності',
    // Controller_Widget_Navigation
    // accounts action
    'Controller_Widget_Navigation.accounts.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.accounts.dobavlenie_akkaunta' => 'Додавання акаунту',
    'Controller_Widget_Navigation.accounts.istoriya_aktivnosti_akkauntov' => 'Історія активності акаунтів',
    'Controller_Widget_Navigation.accounts.istoriya_prodlenij_akkauntov' => 'Історія продовжень терміну дії акаунтів',
    // account action
    'Controller_Widget_Navigation.account.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.account.akkaunt' => 'Акаунт',
    'Controller_Widget_Navigation.account.izmenenie_nastroek_akkaunta' => 'Зміна налаштувань акаунту',
    'Controller_Widget_Navigation.account.redaktirovanie_akkaunta' => 'Редагування акаунту',
    'Controller_Widget_Navigation.account.prodlenie_akkaunta' => 'Продовження терміну дії акаунту',
    'Controller_Widget_Navigation.account.istoriya_prodlenij_akkaunta' => 'Історія продовжень терміну дії акаунту',
    'Controller_Widget_Navigation.account.istoriya_aktivnosti_akkaunta' => 'Історія активності акаунту',
    'Controller_Widget_Navigation.account.udalenie_akkaunta' => 'Видалення акаунту',
    // contacts action
    'Controller_Widget_Navigation.contacts.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.contacts.akkaunt' => 'Акаунт',
    'Controller_Widget_Navigation.contacts.kontakty' => 'Контакти',
    'Controller_Widget_Navigation.contacts.dobavlenie_kontakta' => 'Додавання контакту',
    // contact action
    'Controller_Widget_Navigation.contact.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.contact.akkaunt' => 'Акаунт',
    'Controller_Widget_Navigation.contact.kontakty' => 'Контакти',
    'Controller_Widget_Navigation.contact.kontakt' => 'Контакт',
    'Controller_Widget_Navigation.contact.redaktirovanie_kontakta' => 'Редагування контакту',
    'Controller_Widget_Navigation.contact.udalenie_kontakta' => 'Видалення контакту',
    // connections action
    'Controller_Widget_Navigation.connections.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.connections.akkaunt' => 'Акаунт',
    'Controller_Widget_Navigation.connections.profili_podklyucheniya' => 'Профілі підключень',
    'Controller_Widget_Navigation.connections.dobavlenie_profilya_podklyucheniya' => 'Додавання профілю підключення',
    // connection action
    'Controller_Widget_Navigation.connection.akkaunty' => 'Акаунти',
    'Controller_Widget_Navigation.connection.akkaunt' => 'Акаунт',
    'Controller_Widget_Navigation.connection.profili_podklyuchenij' => 'Профілі підключень',
    'Controller_Widget_Navigation.connection.profil_podklyucheniya' => 'Профіль підключення',
    'Controller_Widget_Navigation.connection.redaktirovanie_profilya_podklyucheniya' => 'Редагування профілю підключення',
    'Controller_Widget_Navigation.connection.udalenie_profilya_podklyucheniya' => 'Видалення профілю підключення',
    // profile action
    'Controller_Widget_Navigation.profile.profil' => 'Профіль',
    'Controller_Widget_Navigation.profile.redaktirovanie_profilya' => 'Редагування профілю',
    'Controller_Widget_Navigation.profile.smena_email_adresa_profilya' => 'Зміна email адреси профілю',
    'Controller_Widget_Navigation.profile.balans_profilya' => 'Баланс профілю',
    'Controller_Widget_Navigation.profile.popolnenie_balansa_profilya' => 'Поповнення балансу профілю',
    'Controller_Widget_Navigation.profile.istoriya_proplat' => 'Історія проплат',
    'Controller_Widget_Navigation.profile.istoriya_aktivnosti' => 'Історія активності',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Profile section                           */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // Controller_Profile_Account
    // info action
    'Controller_Profile_Account.info.content_title' => 'Інформація про акаунт',
    'Controller_Profile_Account.info.title' => 'Інформація про акаунт',
    'Controller_Profile_Account.info.nazvanie' => 'Назва:',
    'Controller_Profile_Account.info.imya_polzovatelya' => 'Ім\'я користувача:',
    'Controller_Profile_Account.info.login_polzovatelya' => 'Логін користувача:',
    'Controller_Profile_Account.info.status' => 'Статус:',
    'Controller_Profile_Account.info.data_sozdaniya' => 'Дата створення:',
    'Controller_Profile_Account.info.srok_dejstviya' => 'Термін дії:',
    'Controller_Profile_Account.info.kolichestvo_dnej_do_zaversheniya_sroka_dejstviya' => 'Кількість днів до завершення терміну дії:',
    'Controller_Profile_Account.info.kolichestvo_kontaktov' => 'Кількість контактів:',
    'Controller_Profile_Account.info.kolichestvo_profilej_podklyuchenij' => 'Кількість профілів підключень:',
    'Controller_Profile_Account.info.aktivnyj' => 'Активний',
    'Controller_Profile_Account.info.neaktivnyj' => 'Неактивний',
    'Controller_Profile_Account.info.istek' => 'минув',
    'Controller_Profile_Account.info.do' => 'до',
    // edit action
    'Controller_Profile_Account.edit.content_title' => 'Редагування акаунту',
    'Controller_Profile_Account.edit.title' => 'Редагування акаунту',
    'Controller_Profile_Account.edit.vozmozhnost_redaktirovaniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Можливість редагування акаунтів на сайті тимчасово припинено',
    'Controller_Profile_Account.edit.izmeneniya_soxraneny' => 'Зміни збережено',
    'Controller_Profile_Account.edit.ne_udalos_soxranit_izmeneniya,_povtorite_popytku' => 'Не вдалося зберегти зміни, повторіть спробу',
    'Controller_Profile_Account.edit.nazvanie_akkaunta' => 'Назва акаунту:',
    'Controller_Profile_Account.edit.imya_polzovatelya' => 'Ім\'я користувача:',
    'Controller_Profile_Account.edit.login_polzovatelya' => 'Логін користувача:',
    'Controller_Profile_Account.edit.proverit' => 'перевірити',
    'Controller_Profile_Account.edit.parol_polzovatelya' => 'Пароль користувача:',
    'Controller_Profile_Account.edit.podtverzhdenie_parolya' => 'Підтвердження пароля:',
    'Controller_Profile_Account.edit.sdelat_akkaunt_aktivnym' => 'Зробити акаунт активним',
    'Controller_Profile_Account.edit.soxranit' => 'Зберегти',
    // settings action
    'Controller_Profile_Account.settings.content_title' => 'Налаштування акаунту',
    'Controller_Profile_Account.settings.title' => 'Налаштування акаунту',
    'Controller_Profile_Account.settings.vozmozhnost_izmeneniya_nastroek_akkauntov_na_sajte_vremenno_priostanovlena' => 'Можливість зміни налаштувань акаунтів на сайті тимчасово припинено',
    'Controller_Profile_Account.settings.nastrojki_akkaunta_soxraneny' => 'Налаштування акаунту збережено',
    'Controller_Profile_Account.settings.nevozmozhno_soxranit_nastrojki_akkaunta,_povtorite_popytku' => 'Неможливо зберегти налаштування акаунту, повторіть спробу',
    'Controller_Profile_Account.settings.vybor_profilya_podklyucheniya_po_umolchaniyu' => 'Вибір профілю підключення за замовчуванням',
    'Controller_Profile_Account.settings.otsutstvuet' => 'Відсутній',
    'Controller_Profile_Account.settings.ispolzovat_tolko_vybrannyj_profil_podklyucheniya' => 'Використовувати тільки вибраний профіль підключення',
    'Controller_Profile_Account.settings.soxranit' => 'Зберегти',
    // remove action
    'Controller_Profile_Account.remove.content_title' => 'Видалення акаунту',
    'Controller_Profile_Account.remove.title' => 'Видалення акаунту',
    'Controller_Profile_Account.remove.vozmozhnost_udaleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Можливість видалення акаунтів на сайті тимчасово припинено',
    'Controller_Profile_Account.remove.nevozmozhno_udalit_akkaunt,_srok_dejstviya_akkaunta_ne_istek' => 'Неможливо видалити акаунт, термін дії акаунту не минув',
    'Controller_Profile_Account.remove.podtverdite_udalenie_akkaunta' => 'Підтвердіть видалення акаунту:',
    'Controller_Profile_Account.remove.udalit' => 'Видалити',
    // prolong action
    'Controller_Profile_Account.prolong.content_title' => 'Продовження терміну дії акаунту',
    'Controller_Profile_Account.prolong.title' => 'Продовження терміну дії акаунту',
    'Controller_Profile_Account.prolong.vozmozhnost_prodleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Можливість продовження терміну дії акаунтів на сайті тимчасово припинено',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_dostignut_maksimalnyj_srok_prodleniya_akkaunta' => 'Неможливо продовжити термін дії акаунту, досягнуто максимальний термін продовження дії акаунту',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_neobxodimo_popolnit_balans' => 'Неможливо продовжити термін дії акаунту, необхідно поповнити баланс',
    'Controller_Profile_Account.prolong.akkaunt_uspeshno_prodlen' => 'Термін дії аккаунту успішно продовжено',
    'Controller_Profile_Account.prolong.nevozmozhno_prodlit_akkaunt,_povtorite_popytku' => 'Неможливо продовжити термін дії акаунту, повторіть спробу',
    // --- form
    'Controller_Profile_Account.prolong.obshhie_svedeniya' => 'Загальні відомості',
    'Controller_Profile_Account.prolong.tekushhij_balans_profilya' => 'Поточний баланс профілю:',
    'Controller_Profile_Account.prolong.dnya/dnej_prodleniya' => 'дні/днів продовження',
    'Controller_Profile_Account.prolong.tekushhij_tarif' => 'Поточний тариф:',
    'Controller_Profile_Account.prolong.srok_dejstviya_akkaunta' => 'Термін дії акаунту:',
    'Controller_Profile_Account.prolong.istek' => 'минув',
    'Controller_Profile_Account.prolong.ot' => 'від',
    'Controller_Profile_Account.prolong.do' => 'до',
    'Controller_Profile_Account.prolong.do_daty' => 'до',
    'Controller_Profile_Account.prolong.ostalos' => 'залишилось',
    'Controller_Profile_Account.prolong.dnya/dnej' => 'дні/днів',
    'Controller_Profile_Account.prolong.dostupno_k_prodleniyu' => 'Доступно для продовження:',
    'Controller_Profile_Account.prolong.dnya/dnej_(bez_ucheta_balansa)' => 'дні/днів (без врахування балансу)',
    'Controller_Profile_Account.prolong.min-maks_srok_prodleniya_akkaunta' => 'Мін-макс термін продовження акаунту:',
    'Controller_Profile_Account.prolong.prodlenie' => 'Продовження',
    'Controller_Profile_Account.prolong.zhelaemoe_kolichestvo_dnej_prodleniya' => 'Бажана кількість днів продовження',
    'Controller_Profile_Account.prolong.prodlit' => 'Продовжити',
    // prolongs action
    'Controller_Profile_Account.prolongs.content_title' => 'Історія продовжень терміну дії акаунту',
    'Controller_Profile_Account.prolongs.title' => 'Історія продовжень терміну дії акаунту',
    'Controller_Profile_Account.prolongs.data_prodleniya' => 'Дата продовження',
    'Controller_Profile_Account.prolongs.summa_prodleniya' => 'Сума продовження',
    'Controller_Profile_Account.prolongs.kolichestvo_dnej_prodleniya' => 'Кількість днів продовження',
    // log action     
    'Controller_Profile_Account.log.content_title' => 'Історія активності акаунту',
    'Controller_Profile_Account.log.title' => 'Історія активності акаунту',
    'Controller_Profile_Account.log.data_sobytiya' => 'Дата події',
    'Controller_Profile_Account.log.ip_adres' => 'IP адреса',
    'Controller_Profile_Account.log.tip_sobytiya' => 'Тип події',
    'Controller_Profile_Account.log.vxod' => 'Вхід',
    'Controller_Profile_Account.log.vyxod' => 'Вихід',
    // Controller_Profile_Accounts
    // list action
    'Controller_Profile_Accounts.list.content_title' => 'Список акаунтів',
    'Controller_Profile_Accounts.list.title' => 'Список акаунтів',
    'Controller_Profile_Accounts.list.nazvanie_akkaunta' => 'Назва акаунту',
    'Controller_Profile_Accounts.list.srok_dejstviya_akkaunta' => 'Термін дії акаунту',
    'Controller_Profile_Accounts.list.data_sozdaniya' => 'Дата створення',
    'Controller_Profile_Accounts.list.status' => 'Статус',
    'Controller_Profile_Accounts.list.istek' => 'минув',
    'Controller_Profile_Accounts.list.do' => 'до',
    'Controller_Profile_Accounts.list.aktivnyj' => 'Активний',
    'Controller_Profile_Accounts.list.neaktivnyj' => 'Неактивний',
    // add action
    'Controller_Profile_Accounts.add.content_title' => 'Додавання акаунту',
    'Controller_Profile_Accounts.add.title' => 'Додавання акаунту',
    'Controller_Profile_Accounts.add.vozmozhnost_dobavleniya_akkauntov_na_sajte_vremenno_priostanovlena' => 'Можливість додавання акаунтів на сайті тимчасово припинено',
    'Controller_Profile_Accounts.add.akkaunt_dobavlen' => 'Аккаунт додано',
    'Controller_Profile_Accounts.add.ne_udalos_dobavit_akkaunt,_povtorite_popytku' => 'Не вдалося додати акаунт, повторіть спробу',
    'Controller_Profile_Accounts.add.nazvanie_akkaunta' => 'Назва акаунту:',
    'Controller_Profile_Accounts.add.imya_polzovatelya' => 'Ім\'я користувача:',
    'Controller_Profile_Accounts.add.login_polzovatelya' => 'Логін користувача:',
    'Controller_Profile_Accounts.add.proverit' => 'перевірити',
    'Controller_Profile_Accounts.add.parol_polzovatelya' => 'Пароль користувача:',
    'Controller_Profile_Accounts.add.podtverzhdenie_parolya' => 'Підтвердження пароля:',
    'Controller_Profile_Accounts.add.sdelat_akkaunt_aktivnym' => 'Зробити акаунт активним',
    'Controller_Profile_Accounts.add.dobavit' => 'Добавити',
    // log action
    'Controller_Profile_Accounts.log.content_title' => 'Історія активності акаунтів',
    'Controller_Profile_Accounts.log.title' => 'Історія активності акаунтів',
    'Controller_Profile_Accounts.log.nazvanie_akkaunta' => 'Назва акаунту',
    'Controller_Profile_Accounts.log.data_sobytiya' => 'Дата події',
    'Controller_Profile_Accounts.log.ip_adres' => 'IP адреса',
    'Controller_Profile_Accounts.log.tip_sobytiya' => 'Тип події',
    'Controller_Profile_Accounts.log.vxod' => 'Вхід',
    'Controller_Profile_Accounts.log.vyxod' => 'Вихід',
    // prolongs action
    'Controller_Profile_Accounts.prolongs.content_title' => 'Історія продовжень акаунтів',
    'Controller_Profile_Accounts.prolongs.title' => 'Історія продовжень акаунтів',
    'Controller_Profile_Accounts.prolongs.nazvanie_akkaunta' => 'Назва акаунту',
    'Controller_Profile_Accounts.prolongs.data_prodleniya' => 'Дата продовження',
    'Controller_Profile_Accounts.prolongs.summa_prodleniya' => 'Сума продовження',
    'Controller_Profile_Accounts.prolongs.kolichestvo_dnej_prodleniya' => 'Кількість днів продовження',
    // Controller_Profile_Connection
    // info action
    'Controller_Profile_Connection.info.content_title' => 'Інформація про профіль підключення',
    'Controller_Profile_Connection.info.title' => 'Інформація про профіль підключення',
    'Controller_Profile_Connection.info.da' => 'так',
    'Controller_Profile_Connection.info.net' => 'ні',
    'Controller_Profile_Connection.info.profil_podklyucheniya' => 'Профіль підключення',
    'Controller_Profile_Connection.info.nazvanie' => 'Назва:',
    'Controller_Profile_Connection.info.kratkoe_opisanie' => 'Короткий опис:',
    'Controller_Profile_Connection.info.aktivnyj' => 'Активний:',
    'Controller_Profile_Connection.info.vvodit_parol_lokalno' => 'Вводити пароль локально:',
    'Controller_Profile_Connection.info.nastrojki_sip_akkaunta' => 'Налаштування SIP аккаунту',
    'Controller_Profile_Connection.info.otobrazhaemoe_imya' => 'Ім\'я для відображення:',
    'Controller_Profile_Connection.info.dobavochnyj_nomer' => 'Додатковий номер:',
    'Controller_Profile_Connection.info.imya_polzovatelya' => 'Ім\'я користувача:',
    'Controller_Profile_Connection.info.parol' => 'Пароль:',
    'Controller_Profile_Connection.info.ne_ukazan' => 'Не вказаний',
    'Controller_Profile_Connection.info.sekretnyj_parol' => 'Секретний пароль:',
    'Controller_Profile_Connection.info.ispolzovat_sekretnyj_parol' => 'Використовувати секретний пароль:',
    'Controller_Profile_Connection.info.setevye_nastrojki' => 'Мережеві налаштування',
    'Controller_Profile_Connection.info.adres_sip_servera' => 'Адреса SIP сервера:',
    'Controller_Profile_Connection.info.port_sip_servera' => 'Порт SIP сервера:',
    'Controller_Profile_Connection.info.adres_sip_proksi_servera' => 'Адреса SIP проксі сервера:',
    'Controller_Profile_Connection.info.port_sip_proksi_servera' => 'Порт SIP проксі сервера:',
    'Controller_Profile_Connection.info.lokalnyj_ip_adres' => 'Локальна IP адреса:',
    'Controller_Profile_Connection.info.lokalnyj_port' => 'Локальний порт:',
    'Controller_Profile_Connection.info.minimalnyj_rtp_port' => 'Мінімальний RTP порт:',
    'Controller_Profile_Connection.info.maksimalnyj_rtp_port' => 'Максимальний RTP порт:',
    'Controller_Profile_Connection.info.tajmer_registracii' => 'Таймер реєстрації:',
    'Controller_Profile_Connection.info.ispolzovat_sluchajnyj_lokalnyj_port' => 'Використовувати випадковий локальний порт:',
    'Controller_Profile_Connection.info.opredelyat_lokalnyj_adres_avtomaticheski' => 'Визначати локальну адресу автоматично:',
    'Controller_Profile_Connection.info.ispolzovat_proksi_server' => 'Використовувати проксі сервер:',
    'Controller_Profile_Connection.info.nastrojki_parametrov_nat' => 'Налаштування параметрів NAT',
    'Controller_Profile_Connection.info.tajmer_keepalive' => 'Таймер Keep-Alive:',
    'Controller_Profile_Connection.info.adres_stun_servera' => 'Адреса STUN сервера:',
    'Controller_Profile_Connection.info.port_stun_servera' => 'Порт STUN сервера:',
    'Controller_Profile_Connection.info.ispolzovat_stun_razvedku' => 'Використовувати STUN розвідку:',
    'Controller_Profile_Connection.info.ispolzovat_stun_server_po_umolchaniyu' => 'Використовувати STUN сервер за замовчуванням:',
    'Controller_Profile_Connection.info.ispolzovat_tajmer_keepalive' => 'Використовувати таймер Keep-Alive:',
    'Controller_Profile_Connection.info.ispolzovat_privyazku_k_sip_serveru' => 'Використовувати прив\'язку до SIP серверу:',
    'Controller_Profile_Connection.info.ispolzovat_simmetricheskij_rtp' => 'Використовувати симетричний RTP:',
    'Controller_Profile_Connection.info.nastrojki_prioritetov_kodekov' => 'Налаштування пріоритетів кодеків',
    'Controller_Profile_Connection.info.kodek' => 'Кодек',
    'Controller_Profile_Connection.info.nastrojki_parametrov_dtmf' => 'Налаштування параметрів DTMF',
    'Controller_Profile_Connection.info.predpochitaemyj_dtmf_metod' => 'DTMF метод:',
    'Controller_Profile_Connection.info.dlitelnost_dtmf_signala' => 'Тривалість DTMF сигналу:',
    // edit action
    'Controller_Profile_Connection.edit.content_title' => 'Редагування профілю підключення',
    'Controller_Profile_Connection.edit.title' => 'Редагування профілю підключення',
    'Controller_Profile_Connection.edit.vozmozhnost_redaktirovaniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Можливість редагування профілів підключень на сайті тимчасово припинено',
    'Controller_Profile_Connection.edit.izmeneniya_soxraneny' => 'Зміни збережено',
    'Controller_Profile_Connection.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Неможливо зберегти зміни, повторіть спробу',
    'Controller_Profile_Connection.edit.profil_podklyucheniya' => 'Профіль підключення',
    'Controller_Profile_Connection.edit.nazvanie' => 'Назва:',
    'Controller_Profile_Connection.edit.kratkoe_opisanie' => 'Короткий опис:',
    'Controller_Profile_Connection.edit.sdelat_aktivnym' => 'Зробити активним',
    'Controller_Profile_Connection.edit.vvodit_parol_lokalno' => 'Вводити пароль локально',
    'Controller_Profile_Connection.edit.nastrojki_sip_akkaunta' => 'Налаштування SIP аккаунта',
    'Controller_Profile_Connection.edit.otobrazhaemoe_imya' => 'Ім\'я для відображення:',
    'Controller_Profile_Connection.edit.dobavochnyj_nomer' => 'Додатковий номер:',
    'Controller_Profile_Connection.edit.imya_polzovatelya' => 'Ім\'я користувача:',
    'Controller_Profile_Connection.edit.parol' => 'Пароль:',
    'Controller_Profile_Connection.edit.sekretnyj_parol' => 'Секретний пароль:',
    'Controller_Profile_Connection.edit.ispolzovat_sekretnyj_parol' => 'Використовувати секретний пароль',
    'Controller_Profile_Connection.edit.setevye_nastrojki' => 'Мережеві налаштування',
    'Controller_Profile_Connection.edit.adres_sip_servera' => 'Адреса SIP сервера:',
    'Controller_Profile_Connection.edit.port_sip_servera' => 'Порт SIP сервера:',
    'Controller_Profile_Connection.edit.adres_sip_proksi_servera' => 'Адреса SIP проксі сервера:',
    'Controller_Profile_Connection.edit.port_sip_proksi_servera' => 'Порт SIP проксі сервера:',
    'Controller_Profile_Connection.edit.lokalnyj_ip_adres' => 'Локальний IP адрес:',
    'Controller_Profile_Connection.edit.lokalnyj_port' => 'Локальний порт:',
    'Controller_Profile_Connection.edit.minimalnyj_rtp_port' => 'Мінімальний RTP порт:',
    'Controller_Profile_Connection.edit.maksimalnyj_rtp_port' => 'Максимальний RTP порт:',
    'Controller_Profile_Connection.edit.tajmer_registracii' => 'Таймер реєстрації:',
    'Controller_Profile_Connection.edit.ispolzovat_sluchajnyj_lokalnyj_port' => 'Використовувати випадковий локальний порт',
    'Controller_Profile_Connection.edit.opredelyat_lokalnyj_adres_avtomaticheski' => 'Визначати локальну адресу автоматично',
    'Controller_Profile_Connection.edit.ispolzovat_proksi_server' => 'Використовувати проксі сервер',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_nat' => 'Налаштування параметрів NAT',
    'Controller_Profile_Connection.edit.tajmer_keepalive' => 'Таймер Keep-Alive:',
    'Controller_Profile_Connection.edit.adres_stun_servera' => 'Адреса STUN сервера:',
    'Controller_Profile_Connection.edit.port_stun_servera' => 'Порт STUN сервера:',
    'Controller_Profile_Connection.edit.ispolzovat_stun_razvedku' => 'Використовувати STUN розвідку',
    'Controller_Profile_Connection.edit.ispolzovat_stun_server_po_umolchaniyu' => 'Використовувати STUN сервер за замовчуванням',
    'Controller_Profile_Connection.edit.ispolzovat_tajmer_keepalive' => 'Використовувати таймер Keep-Alive',
    'Controller_Profile_Connection.edit.ispolzovat_privyazku_k_sip_serveru' => 'Використовувати прив\'язку до SIP серверу',
    'Controller_Profile_Connection.edit.ispolzovat_simmetricheskij_rtp' => 'Використовувати симетричний RTP',
    'Controller_Profile_Connection.edit.nastrojki_prioritetov_kodekov' => 'Налаштування пріоритетів кодеків',
    'Controller_Profile_Connection.edit.kodek' => 'Кодек',
    'Controller_Profile_Connection.edit.nastrojki_parametrov_dtmf' => 'Налаштування параметрів DTMF',
    'Controller_Profile_Connection.edit.predpochitaemyj_dtmf_metod' => 'DTMF метод:',
    'Controller_Profile_Connection.edit.dlitelnost_dtmf_signala' => 'Тривалість DTMF сигналу:',
    'Controller_Profile_Connection.edit.soxranit' => 'Зберегти',
    // remove action
    'Controller_Profile_Connection.remove.content_title' => 'Видалення профілю підключення',
    'Controller_Profile_Connection.remove.title' => 'Видалення профілю підключення',
    'Controller_Profile_Connection.remove.vozmozhnost_udaleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Можливість видалення профілів підключень на сайті тимчасово припинена',
    'Controller_Profile_Connection.remove.podtverdite_udalenie_profilya_podklyucheniya' => 'Підтвердіть видалення профілю підключень:',
    'Controller_Profile_Connection.remove.udalit' => 'Видалити',
    // Controller_Profile_Connections
    // list action
    'Controller_Profile_Connections.list.content_title' => 'Список профілів підключень',
    'Controller_Profile_Connections.list.title' => 'Список профілів підключень',
    'Controller_Profile_Connections.list.profil_podklyucheniya' => 'Профіль підключення',
    'Controller_Profile_Connections.list.data_dobavleniya' => 'Дата додавання',
    'Controller_Profile_Connections.list.status' => 'Статус',
    'Controller_Profile_Connections.list.aktivnyj' => 'Активний',
    'Controller_Profile_Connections.list.neaktivnyj' => 'Неактивний',
    // add action
    'Controller_Profile_Connections.add.content_title' => 'Додавання профілю підключення',
    'Controller_Profile_Connections.add.title' => 'Додавання профілю підключення',
    'Controller_Profile_Connections.add.vozmozhnost_dobavleniya_profilej_podklyucheniya_na_sajte_vremenno_priostanovlena' => 'Можливість додавання профілів підключень на сайті тимчасово припинено',
    'Controller_Profile_Connections.add.profil_podklyucheniya_dobavlen' => 'Профіль підключення додано',
    'Controller_Profile_Connections.add.nevozmozhno_dobavit_profil_podklyucheniya,_povtorite_popytku' => 'Неможливо додати профіль підключення, повторіть спробу',
    'Controller_Profile_Connections.add.dobavit' => 'Додати',
    // Controller_Profile_Contact
    // info action
    'Controller_Profile_Contact.info.content_title' => 'Інформація про контакт',
    'Controller_Profile_Contact.info.title' => 'Інформація про контакт',
    'Controller_Profile_Contact.info.imya_kontakta' => 'Ім\'я контакту:',
    'Controller_Profile_Contact.info.nomer_telefona' => 'Номер телефону:',
    'Controller_Profile_Contact.info.data_dobavleniya_kontakta' => 'Дата додавання контакту:',
    // edit action
    'Controller_Profile_Contact.edit.content_title' => 'Редагування контакту',
    'Controller_Profile_Contact.edit.title' => 'Редагування контакту',
    'Controller_Profile_Contact.edit.vozmozhnost_redaktirovaniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Можливість редагування контактів на сайті тимчасово припинено',
    'Controller_Profile_Contact.edit.izmeneniya_soxraneny' => 'зміни збережено',
    'Controller_Profile_Contact.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Неможливо зберегти зміни, повторіть спробу',
    'Controller_Profile_Contact.edit.imya_kontakta' => 'Ім\'я контакту:',
    'Controller_Profile_Contact.edit.nomer_telefona' => 'Номер телефону:',
    'Controller_Profile_Contact.edit.soxranit' => 'Зберегти',
    // Controller_Profile_Contacts
    // list action
    'Controller_Profile_Contacts.list.content_title' => 'Список контактів',
    'Controller_Profile_Contacts.list.title' => 'Список контактів',
    'Controller_Profile_Contacts.list.imya_kontakta' => 'Ім\'я контакту',
    'Controller_Profile_Contacts.list.nomer_telefona' => 'Номер телефону',
    'Controller_Profile_Contacts.list.data_dobavleniya' => 'Дата додавання контакту',
    'Controller_Profile_Contacts.list.dejstvie' => 'Дія',
    'Controller_Profile_Contacts.list.udalit' => 'Видалити',
    // add action
    'Controller_Profile_Contacts.add.content_title' => 'Додавання контакту',
    'Controller_Profile_Contacts.add.title' => 'Додавання контакту',
    'Controller_Profile_Contacts.add.vozmozhnost_dobavleniya_kontaktov_na_sajte_vremenno_priostanovlena' => 'Можливість додавання контактів на сайті тимчасово припинено',
    'Controller_Profile_Contacts.add.kontakt_dobavlen' => 'Контакт додано',
    'Controller_Profile_Contacts.add.nevozmozhno_dobavit_kontakt,_povtorite_popytku' => 'Неможливо додати контакт, повторіть спробу',
    'Controller_Profile_Contacts.add.imya_kontakta' => 'Ім\'я контакту:',
    'Controller_Profile_Contacts.add.nomer_telefona' => 'Номер телефону:',
    'Controller_Profile_Contacts.add.dobavit' => 'Додати',
    // Controller_Profile_Profile
    // info action
    'Controller_Profile_Profile.info.content_title' => 'Профіль',
    'Controller_Profile_Profile.info.title' => 'Профіль',
    'Controller_Profile_Profile.info.kolichestvo_akkauntov' => 'Кількість акаунтів:',
    'Controller_Profile_Profile.info.dobavit_akkaunt' => 'Додати акаунт',
    'Controller_Profile_Profile.info.otobrazit_spisok_akkauntov' => 'Відобразити список акаунтів',
    // edit action
    'Controller_Profile_Profile.edit.content_title' => 'Редагування профілю',
    'Controller_Profile_Profile.edit.title' => 'Редагування профілю',
    'Controller_Profile_Profile.edit.vozmozhnost_redaktirovaniya_profilya_na_sajte_vremenno_priostanovlena' => 'Можливість редагування профілю на сайті тимчасово припинено',
    'Controller_Profile_Profile.edit.izmeneniya_soxraneny' => 'Зміни збережено',
    'Controller_Profile_Profile.edit.nevozmozhno_soxranit_izmeneniya,_povtorite_popytku' => 'Неможливо зберегти зміни, повторіть спробу',
    'Controller_Profile_Profile.edit.polnoe_imya' => 'Повне Ім\'я:',
    'Controller_Profile_Profile.edit.login' => 'Логін:',
    'Controller_Profile_Profile.edit.parol' => 'Пароль:',
    'Controller_Profile_Profile.edit.podtverzhdenie_parolya' => 'Підтвердження пароля:',
    'Controller_Profile_Profile.edit.email' => 'Email:',
    'Controller_Profile_Profile.edit.smenit_email_adres' => 'Змінити email адресу',
    'Controller_Profile_Profile.edit.yazyk' => 'Мова:',
    'Controller_Profile_Profile.edit.vremennaya_zona' => 'Часова зона:',
    'Controller_Profile_Profile.edit.soxranit' => 'Зберегти',
    // balance action
    'Controller_Profile_Profile.balance.content_title' => 'Баланс профілю',
    'Controller_Profile_Profile.balance.title' => 'Баланс профілю',
    'Controller_Profile_Profile.balance.tekushhij_balans' => 'Поточний баланс:',
    // invoice action
    'Controller_Profile_Profile.invoice.content_title' => 'Поповнення балансу профілю',
    'Controller_Profile_Profile.invoice.title' => 'Поповнення балансу профілю',
    'Controller_Profile_Profile.invoice.vozmozhnost_popolneniya_balansa_na_sajte_vremenno_priostanovlena' => 'Можливість поповнення балансу на сайті тимчасово припинено',
    'Controller_Profile_Profile.invoice.popolnenie_balansa_polzovatelya' => 'Поповнення балансу користувача:',
    'Controller_Profile_Profile.invoice.nevozmozhno_popolnit_balans,_povtorite_popytku' => 'Неможливо поповнити баланс, повторіть спробу',
    'Controller_Profile_Profile.invoice.summa_popolneniya' => 'Сума поповнення:',
    'Controller_Profile_Profile.invoice.minimum' => 'мінімум',
    'Controller_Profile_Profile.invoice.tip_valyuty_webmoney' => 'Тип валюти Webmoney:',
    'Controller_Profile_Profile.invoice.prodolzhit' => 'Продовжити',
    'Controller_Profile_Profile.invoice.zhelaemaya_summa_popolneniya' => 'Бажана сума поповнення:',
    'Controller_Profile_Profile.invoice.neobxodimaya_summa_dlya_proplaty' => 'Необхідна сума для проплати:',
    'Controller_Profile_Profile.invoice.proplatit' => 'Проплатити',
    // invoices action
    'Controller_Profile_Profile.invoices.content_title' => 'Історія проплат',
    'Controller_Profile_Profile.invoices.title' => 'Історія проплат',
    'Controller_Profile_Profile.invoices.schet' => 'Рахунок №',
    'Controller_Profile_Profile.invoices.tip_valyuty' => 'Тип валюти',
    'Controller_Profile_Profile.invoices.data_sozdaniya' => 'Дата створення',
    'Controller_Profile_Profile.invoices.sostoyanie' => 'Стан',
    'Controller_Profile_Profile.invoices.summa_popolneniya' => 'Сума поповнення',
    'Controller_Profile_Profile.invoices.data_proplaty' => 'Дата проплати',
    'Controller_Profile_Profile.invoices.otmenen' => 'Скасований',
    'Controller_Profile_Profile.invoices.proplachen' => 'Проплачений',
    'Controller_Profile_Profile.invoices.ozhidaet_proplaty' => 'Чекає проплати',
    // log action
    'Controller_Profile_Profile.log.content_title' => 'Історія активності',
    'Controller_Profile_Profile.log.title' => 'Історія активності',
    'Controller_Profile_Profile.log.data_sobytiya' => 'Дата події',
    'Controller_Profile_Profile.log.ip_adres' => 'IP адреса',
    'Controller_Profile_Profile.log.tip_sobytiya' => 'Тип події',
    'Controller_Profile_Profile.log.vxod' => 'Вхід',
    'Controller_Profile_Profile.log.vyxod' => 'Вихід',
    // email action
    'Controller_Profile_Profile.email.content_title' => 'Зміна email адреси профілю',
    'Controller_Profile_Profile.email.title' => 'Зміна email адреси профілю',
    'Controller_Profile_Profile.email.vozmozhnost_smeny_email_adresa_profilya_na_sajte_vremenno_priostanovlena' => 'Можливість зміни email адреси профілю на сайті тимчасово припинено',
    'Controller_Profile_Profile.email.na_vash_tekushhij_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'На вашу поточну email адресу надіслано посилання для підтвердження зміни email адреси',
    'Controller_Profile_Profile.email.nevozmozhno_smenit_email_adres_profilya,_povtorite_popytku' => 'Неможливо змінити email адресу профілю, повторіть спробу',
    'Controller_Profile_Profile.email.na_vash_novyj_email_adres_otpravlena_ssylka_dlya_podtverzhdeniya_smeny_email_adresa' => 'На вашу нову email адресу надіслано посилання для підтвердження зміни email адреси',
    'Controller_Profile_Profile.email.nevozmozhno_podtverdit_smenu_email_adresa_profilya,_povtorite_popytku' => 'Неможливо підтвердити зміну email адреси профілю, повторіть спробу',
    'Controller_Profile_Profile.email.email_adres_profilya_izmenen_na_sleduyushhij' => 'Email адреса профілю змінена на наступну:',
    'Controller_Profile_Profile.email.novyj_email_adres' => 'Нова email адреса:',
    'Controller_Profile_Profile.email.izmenit' => 'Змінити',
    /* --------------------------------------------------------------------- */
    /*                                                                       */
    /*                             Template section                          */
    /*                                                                       */
    /* --------------------------------------------------------------------- */
    // View_Template_Profile
    // navigation_bar view
    'View_Template_Profile.navigation_bar.profil' => 'Профіль',
    'View_Template_Profile.navigation_bar.akkaunty' => 'Акаунти',
    'View_Template_Profile.navigation_bar.podderzhka' => 'Підтримка',
    'View_Template_Profile.navigation_bar.voprosy-otvety' => 'Запитання - Відповіді',
    'View_Template_Profile.navigation_bar.obratnaya_svyaz' => 'Зворотний зв\'язок',
    'View_Template_Profile.navigation_bar.vyjti' => 'Вийти',
    // View_Template_Site
    // navigation_bar view
    'View_Template_Site.navigation_bar.o_servise' => 'Про сервіс',
    'View_Template_Site.navigation_bar.novosti' => 'Новини',
    'View_Template_Site.navigation_bar.podderzhka' => 'Підтримка',
    'View_Template_Site.navigation_bar.voprosy-otvety' => 'Запитання - Відповіді',
    'View_Template_Site.navigation_bar.vosstanovlenie_parolya' => 'Відновлення пароля',
    'View_Template_Site.navigation_bar.obratnaya_svyaz' => 'Зворотний зв\'язок',
    'View_Template_Site.navigation_bar.profil' => 'Профіль',
    'View_Template_Site.navigation_bar.vyjti' => 'Вийти',
    'View_Template_Site.navigation_bar.vxod' => 'Вхід',
    'View_Template_Site.navigation_bar.registraciya' => 'Реєстрація',
);
