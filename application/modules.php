<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'database' => MODPATH . 'database', // Database access
    'captcha' => MODPATH . 'captcha', //Captcha
    'email' => MODPATH . 'email', // E-mail
    'mlang' => MODPATH . 'mlang', // mlang
    'pagination' => MODPATH . 'pagination', // Pagination
        // 'userguide' => MODPATH . 'userguide', // User guide and API documentation
        // 'auth'       => MODPATH.'auth',       // Basic authentication
        // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
        // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
        // 'image'      => MODPATH.'image',      // Image manipulation
        // 'orm'        => MODPATH . 'orm',      // Object Relationship Mapping
        // 'unittest'   => MODPATH.'unittest',   // Unit testing
));
